<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User_model extends CI_Model
{
	var $table;
	function __construct()
	{
		parent::__construct();
		$this->table = 'folk_users';		
		
	}

	function find_data($return_type='array',$conditions='',$limit=0,$offset=0)
	{
        $result = array();
		
		$this->db->select('*');
		if($conditions != '')
		{
			$this->db->where($conditions);
			
		}
		
		if($limit != 0)
		{
			$this->db->limit($limit,$offset);
		}		
		
		$query = $this->db->get($this->table);
		
		switch($return_type) 
		{
			case 'array':
			case '':
				if($query->num_rows() > 0){$result = $query->result();}
				break;
				
			case 'row':
				if($query->num_rows() > 0){$result = $query->row();}
				break;
				
			case 'list':
				$list_arr[''] = 'Select';
				if($query->num_rows() > 0){
					foreach ($query->result() as $row)
					{
						$list_arr[$row->id] = $row->banner_text;
					}
					
				}$result = $list_arr;
				break;
				
			case 'count':
				$result = $query->num_rows(); 
				break;
		}
		//echo $this->db->last_query();die;
        return $result;
    }

	function save_data($postData = array(),$id=0)
	{
		if($id == 0)
		{
			$this->db->insert($this->table,$postData);
			return $this->db->insert_id();
		}
		else
		{
			$this->db->where('id', $id);
			$this->db->update($this->table,$postData);
			return $this->db->affected_rows();
		} 
	}
	function activate_user($activation_code)
	{ 
		$table['name'] = 'folk_users';
		$get_user = $this->Common_model->find_data($table,'row','',array('user_activation_code'=>$activation_code));
		
		if(!empty($get_user)){
			$data = array(
					'user_activation_code'=>'',					
					'modified'=>date('Y-m-d H:i:s'),
					'published'=>1
				);
			$this->db->where('user_activation_code', $activation_code);
			$this->db->update($this->table,$data);
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		else
		{
			return false;
		}
			
			
    }
}
?>