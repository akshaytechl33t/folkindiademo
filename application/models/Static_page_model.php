<?php
class Static_page_model extends CI_Model
{
	var $table;
	
    function  __construct(){
        parent::__construct();
		$this->table = 'folk_static_pages';
		
    }
    
############################### Function To Fetch Static Pages ###############################
	
	function find_data($return_type='array',$conditions='',$limit=0,$offset=0,$sort_by=NULL,$select=NULL,$group_by=NULL){
        $result = array();	
		
		$this->db->select('*');
		
		if($conditions != ''){
			$this->db->where($conditions);
		}
		
		if($limit != 0){
			$this->db->limit($limit,$offset);
		}
				
		$query = $this->db->get($this->table);
		switch ($return_type) {
			case 'array':
			case '':
				if($query->num_rows() > 0){$result = $query->result();}
				break;
				
			case 'row':
				if($query->num_rows() > 0){$result = $query->row();}
				break;
				
			case 'list':
				$list_arr[''] = 'Select';
				if($query->num_rows() > 0){
					foreach ($query->result() as $row)
					{
						$list_arr[$row->id] = $row->page_name;
					}
					
				}$result = $list_arr;
				break;
				
			case 'count':
				$result = $query->num_rows();
				break;
		}
		
        return $result;
    }
	

	
	function save_data($postData = array(),$id=0)
	{
		if($id == 0){
			$this->db->insert($this->table,$postData);
		}else{
			$this->db->where('id',$id);
			$this->db->update($this->table,$postData);
		}
        return $this->db->affected_rows();
	}

}