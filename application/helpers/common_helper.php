<?php
	function user_pagination($per_page,$site_url,$total_rows,$uri_segment,$page_segment)
	{

		$db->load->library('pagination');

		$config['use_page_numbers'] = true;
		$config['base_url'] = $site_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] = $uri_segment;
		$config['display_pages'] = true;
		$config['num_links'] = 5;

		$db->pagination->initialize($config);

		if($page_segment != '')
		{
			$offset = $page_segment;
		}
		else
		{
			$offset = 1;
		}

		$start = ($per_page * ($offset-1));
		return $start;
	}
	function change_date_format($original_date)
	{
		$db =& get_instance();
		$date = new DateTime($original_date);
		return $date->format('jS, M Y');
	}
	function change_date_time_format($original_date)
	{
		$db =& get_instance();
		$date = new DateTime($original_date);
		return $date->format('F j, Y, g:i a');
	}
	function change_date_time_format2($original_date)
	{
		$db =& get_instance();
		$date = new DateTime($original_date);
		return $date->format('M j, Y, g:i a');
	}
	function change_time_format($original_time)
	{
		$db =& get_instance();
		$hours = substr($original_time,0,2);
		$mins = substr($original_time,3,2);

		return $hours.':'.$mins;
	}
	function empty_datetime($content)
	{
		if($content == '0000-00-00' || $content == '0000-00-00 00:00:00')
		{
			$show_content = "NA";
		}
		else
		{
			$show_content = change_date_time_format($content);
		}

		return $show_content;
	}
	function years_difference($endDate, $beginDate)
    {
        $date_parts1=explode("-", $beginDate);
   		$date_parts2=explode("-", $endDate);
		$years=$date_parts2[0] - $date_parts1[0];
   		return $years;
	}
	function strip_html_tags( $text )
	{
		$db =& get_instance();
		$text = preg_replace(
			array(
				// Remove invisible content
				'@<head[^>]*?>.*?</head>@siu',
				'@<style[^>]*?>.*?</style>@siu',
				'@<script[^>]*?.*?</script>@siu',
				'@<object[^>]*?.*?</object>@siu',
				'@<embed[^>]*?.*?</embed>@siu',
				'@<applet[^>]*?.*?</applet>@siu',
				'@<noframes[^>]*?.*?</noframes>@siu',
				'@<noscript[^>]*?.*?</noscript>@siu',
				'@<noembed[^>]*?.*?</noembed>@siu',

				// Add line breaks before & after blocks
				'@<((br)|(hr))@iu',
				'@</?((address)|(blockquote)|(center)|(del))@iu',
				'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
				'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
				'@</?((table)|(th)|(td)|(caption))@iu',
				'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
				'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
				'@</?((frameset)|(frame)|(iframe))@iu',
			),
			array(
				' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
				"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
				"\n\$0", "\n\$0",
			),
			$text );

		// Remove all remaining tags and comments and return.
		return strip_tags( $text );
	}
	function create_unique_slug($string,$table,$field,$key=NULL,$value=NULL,$count=0)
	{
		$ci =& get_instance();

		$slug=strtolower(url_title($string));

		$params = array ();

		$params[$field] = $slug;

		if($count!=0)$params[$field] = $slug.$count;

		if($key)$params["$key !="] = $value;

		if($ci->db->where($params)->get($table)->num_rows()>0)
		{
			$count++;
			return create_unique_slug($string,$table,$field,$key,$value,$count);
		}
		else
		{
			if($count == 0)
			{
				return $slug;
			}
			else
			{
				return $slug.$count;
			}
		}

	}
function sendtestmail($email_content,$subject,$to,$from)
	{
		$ci =& get_instance();

$config = Array(
     'protocol' => 'smtp',
     'smtp_host' => 'mail.folkindia.in',
'smtp_crypto'=>'tls',
     'smtp_port' => 25,
     'smtp_user' => 'contact@folkindia.in', // change it to yours
     'smtp_pass' => 'Pmmodi@2018', // change it to yours
     'mailtype' => 'html',
     'charset' => 'iso-8859-1',
     'wordwrap' => TRUE
  );

$ci->load->library('email', $config);
		//$ci->email->initialize($config);
		//$ci->email->clear();
		$ci->email->to($to);
		$ci->email->from($from,'folk');
		$ci->email->subject("bbbnmb");
		$ci->email->message('<table class="body-wrap" style="margin: 0; padding: 0; font-size: 100%; line-height: 1.65; font-family:Arial, Helvetica, sans-serif; max-width: 580px !important; margin:0px auto; height: 100%; background: #efefef; -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none; ">
	<tbody>
		<tr>
			<td class="mail_container" style="display: block !important; clear: both !important; margin: 0 auto !important; max-width: 580px !important;">
				<table>
					<tbody>
						<tr>
							<td align="center" class="masthead" style="padding:20px 0; background:#131313; color: white;">
								<a href="#"><img alt="LOGO" src="[LOGO]" style="max-width:100%; margin: 0 auto; display: block;" /> </a></td>
						</tr>
						<tr>
							<td class="content" style="background: white; padding: 30px 35px;">
								<h2>
									Hello [USERNAME],</h2>
								Thank you for registering with us. Please <a href="[CLICK HERE]">click here</a> to activate your account. Or you can copy and paste the link to the browser.
								<p style="font-size: 14px; font-weight: normal;   margin-top: 5px;">
									----------------------------------------------------------------------------</p>
								<p style="font-size: 14px; font-weight: normal;   margin-top: 5px;">
									<strong>Your activation link is: </strong>[CLICK HERE]</p>
								<p style="font-size: 14px; font-weight: normal;   margin-top: 5px;">
									-------------------------------------------------------------------------------</p>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td class="mail_container">
				<table width="100%">
					<tbody>
						<tr>
							<td align="center" class="content footer" style="background: none;">
								<p style="margin-bottom: 0; color: #888; text-align: center; font-size: 14px; ">
									<a href="[SITE_URL]" style="color: #888; text-decoration: none; font-weight: bold;">Folkindia.in </a> ? Folk Ready, [YEAR]. All Rights Reserved.</p>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<p>
	 </p>
');
$ci->email->set_mailtype("html");
		$send=$ci->email->send();
		return $send;

	}
	function send_template_mail($tokens=NULL,$email_content,$subject,$to,$from)
	{
		$ci =& get_instance();
		$ci->load->database();

		if($tokens!=NULL)
		{
			$pattern = array();
			$pattern = '[%s]';
			$map = array();

			foreach($tokens as $var => $value)
			{
				$map[sprintf($pattern, $var)] = $value;
			}
			$body = strtr($email_content, $map);
		}
		else
		{
			$body = $email_content;
		}

		//$ci->load->library('email');
		$config = Array(
     'protocol' => 'smtp',
     'smtp_host' => 'mail.folkindia.in',
'smtp_crypto'=>'tls',
     'smtp_port' => 25,
     'smtp_user' => 'contact@folkindia.in', // change it to yours
     'smtp_pass' => 'Pmmodi@2018', // change it to yours
     'mailtype' => 'html',
     'charset' => 'iso-8859-1',
     'wordwrap' => TRUE
  );

$ci->load->library('email', $config);
		//$ci->email->initialize($config);
		//$ci->email->clear();
		$ci->email->to($to);
		$ci->email->from($from,'Folkindia.in');
		$ci->email->subject($subject.'.');
		$ci->email->message($body.'<p></p>');
$ci->email->set_mailtype("html");
		$send=$ci->email->send();

		if($send==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function adjust_thumbnail_size($dimensions = array(), $image_dimensions=array())
	{
		foreach($image_dimensions as $image_key=>$image)
		{
			$image_details[$image_key] = explode(',',$image);

			foreach($dimensions as $key=>$dimension)
			{
				$adjusted_dimension[$image_key][$key]['width'] = ($dimension['width'] < $image_details[$image_key][0]) ? $dimension['width'] : $image_details[$image_key][0];
				$adjusted_dimension[$image_key][$key]['height'] = ($dimension['height'] < $image_details[$image_key][1]) ? $dimension['height'] : $image_details[$image_key][1];
			}
		}
		//print_r($image_details); echo br(4); print_r($adjusted_dimension); die;
		return $adjusted_dimension;
	}
  	function call_editor($field_name=NULL,$field_value=NULL,$field_options=NULL,$toolbar_name=NULL,$background_color=NULL,$height=NULL)
	{
		require_once('assets/editor/ckeditor/ckeditor.php');
		require_once('assets/editor/ckfinder/ckfinder.php');

		$toolbar = ($toolbar_name)? $toolbar_name : 'mycustomtoolbar';

		$basePathUrl=base_url()."assets/editor";
		$CKEditor = new CKEditor();
		$CKEditor->basePath = base_url().'assets/editor/ckeditor/';
		if($height)$CKEditor->config['height'] = $height;
		$CKEditor->config['height'] = '200';
		$CKEditor->config['width'] = '100%';
		if($background_color)$CKEditor->config['uiColor'] = $background_color;
		$CKEditor->config['toolbar'] = $toolbar;

		//######## SET ckfinder PATH FOR IMAGE UPLOAD #########//

		$CKEditor->config['filebrowserBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html";
		$CKEditor->config['filebrowserImageBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html?Type=Images";
		$CKEditor->config['filebrowserFlashBrowseUrl'] = $basePathUrl."/ckfinder/ckfinder.html?Type=Flash";
		$CKEditor->config['filebrowserUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files";
		$CKEditor->config['filebrowserImageUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";
		$CKEditor->config['filebrowserFlashUploadUrl'] = $basePathUrl."/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";

		//######## SET ckfinder PATH FOR IMAGE UPLOAD #############//
		$CKEditor->returnOutput = true;
		$code = $CKEditor->editor($field_name,html_entity_decode($field_value),$field_options);
		return $code;
	}
	function frame_cat_nested_dropdown($categoryId = 0, $counter = 0, $id = 0)
	{
		$CI =& get_instance();

		$table['name']="eye_product_groups";
		$table['alias_name']='pg';

		$select = 'pg.id,pg.name,pg.published,acg.category_id,acg.published,acg.group_id,c.name as cat_name,c.seo_name as category_seo_name';

		$join[0]['table']="eye_assign_categories_group";
		$join[0]['table_alias']="acg";

		$join[0]['field']="group_id";
		$join[0]['table_master']="pg";
		$join[0]['field_table_master']="id";
		$join[0]['type']="LEFT";

		$join[1]['table']="eye_categories";
		$join[1]['table_alias']="c";

		$join[1]['field']="id";
		$join[1]['table_master']="acg";
		$join[1]['field_table_master']="category_id";
		$join[1]['type']="LEFT";
		$join[1]['and']=' AND c.published = 1 ';

		$details = $CI->Common_model->find_data($table,'array','',array('pg.published'=>1,'pg.id !='=>10),$select,$join);

		if($details)
		{
			$product_categories = array();
			foreach($details as $key=>$category_name)
			{
				$product_categories[$category_name->name][$key]=$category_name;
				if($category_name->cat_name == '')
				{
					 unset($product_categories[$category_name->name][$key]);
				}
			}
			foreach($product_categories as $key=>$details):
				echo "<optgroup label='".$key."'>";
					foreach($details as $d):
					$selected='';
					if($id==$d->category_id)
					{
						$selected="selected=selected";
					}
						echo " <option value='".$d->category_id."' $selected data-selected='".$d->category_seo_name."'>".$d->cat_name."</option>";
					endforeach;
					echo "</optgroup>";
			endforeach;
		}
	}

	function lense_cat_nested_dropdown($categoryId = 0, $counter = 0, $id = 0)
	{
		$CI =& get_instance();

		$table['name']="eye_product_groups";
		$table['alias_name']='pg';

		$select = 'pg.id,pg.name,pg.published,acg.category_id,acg.published,acg.group_id,c.name as cat_name,c.seo_name as category_seo_name';

		$join[0]['table']="eye_assign_categories_group";
		$join[0]['table_alias']="acg";

		$join[0]['field']="group_id";
		$join[0]['table_master']="pg";
		$join[0]['field_table_master']="id";
		$join[0]['type']="LEFT";

		$join[1]['table']="eye_categories";
		$join[1]['table_alias']="c";

		$join[1]['field']="id";
		$join[1]['table_master']="acg";
		$join[1]['field_table_master']="category_id";
		$join[1]['type']="LEFT";
		$join[1]['and']=' AND c.published = 1 ';

		$details = $CI->Common_model->find_data($table,'array','',array('pg.published'=>1,'pg.id ='=>10),$select,$join);

		if($details)
		{
			$product_categories = array();
			foreach($details as $key=>$category_name)
			{
				$product_categories[$category_name->name][$key]=$category_name;
				if($category_name->cat_name == '')
				{
					 unset($product_categories[$category_name->name][$key]);
				}
			}
			foreach($product_categories as $key=>$details):
				echo "<optgroup label='".$key."'>";
					foreach($details as $d):
					$selected='';
					if($id==$d->category_id)
					{
						$selected="selected=selected";
					}
						echo " <option value='".$d->category_id."' $selected data-selected='".$d->category_seo_name."'>".$d->cat_name."</option>";
					endforeach;
					echo "</optgroup>";
			endforeach;
		}
	}


	function product_count($category_id=0,$feature_value_id=0,$brand_id=0,$group_id=0,$country_id=0)
	{	//echo $category_id;die;
		$CI =& get_instance();

		if($feature_value_id !=0){
			$table_product_features['name']="eye_product_features";
			$table_product_features['alias_name']='pf';

			$join[0]['table']="eye_products";
			$join[0]['table_alias']="p";
			$join[0]['field']="id";
			$join[0]['table_master']="pf";
			$join[0]['field_table_master']="product_id";
			$join[0]['type']="LEFT";
			$count_feature_value = $CI->Common_model->find_data($table_product_features,'count','',array('pf.features_value_id'=>$feature_value_id,'p.category_id'=>$category_id),'',$join);
			return $count_feature_value;
		}
		 if($brand_id !=0){
			$table_brand['name']='eye_products';
			$count_brand = $CI->Common_model->find_data($table_brand,'count','',array('category_id'=>$category_id,'brand_id'=>$brand_id));

			return $count_brand;
		}
		if($group_id !=0){
			$table_category['name']='eye_products';
			$count_category = $CI->Common_model->find_data($table_category,'count','',array('category_id'=>$category_id));

			return $count_category;
		}
		if($country_id !=0){
			$table_country['name']='eye_products';
			$count_country = $CI->Common_model->find_data($table_country,'count','',array('category_id'=>$category_id,'country_id'=>$country_id));	return $count_country;
		}
	}

	function nested_all_categories($id=0)
	{
		$CI =& get_instance();

		$table['name']="eye_product_groups";
		$table['alias_name']='pg';

		$select = 'pg.id,pg.name,pg.published,acg.category_id,acg.published,acg.group_id,c.name as cat_name,c.seo_name as category_seo_name';

		$join[0]['table']="eye_assign_categories_group";
		$join[0]['table_alias']="acg";

		$join[0]['field']="group_id";
		$join[0]['table_master']="pg";
		$join[0]['field_table_master']="id";
		$join[0]['type']="LEFT";

		$join[1]['table']="eye_categories";
		$join[1]['table_alias']="c";

		$join[1]['field']="id";
		$join[1]['table_master']="acg";
		$join[1]['field_table_master']="category_id";
		$join[1]['type']="LEFT";
		$join[1]['and']=' AND c.published = 1 ';

		$details = $CI->Common_model->find_data($table,'array','',array('pg.published'=>1),$select,$join);
		//echo $CI->db->last_query(); die;
		if($details)
		{
			$product_categories = array();
			foreach($details as $key=>$category_name)
			{
				$product_categories[$category_name->name][$key]=$category_name;
				if($category_name->cat_name == '')
				{
					 unset($product_categories[$category_name->name][$key]);
				}
			}
			$cat_array= array();
			foreach($product_categories as $key=>$details)
			{
				$new_array = array();
				foreach($details as $d)
				{
					$new_array[$d->category_id] = $d->cat_name;
				}
				$cat_array[$key] = $new_array;
			}
		}
		return $cat_array;
	}
	function get_cart_brand_id(){
		$ci =& get_instance();
		if($ci->cart->total_items() > 0)
		{
			foreach($ci->cart->contents() as $items)
			{
			echo $cart_brand_id;
				$cart_brand_id[] = $items['options']['brand_id'];
			}
		}
		return array_unique($cart_brand_id);
	}
	function get_cart_category_id(){
		$ci =& get_instance();
		if($ci->cart->total_items() > 0)
		{
			foreach($ci->cart->contents() as $items)
			{
			//print_r($items);
				$cart_category_id[] = $items['options']['category_id'];
			}
		}
		//print_r($cart_category_id);die;
		return array_unique($cart_category_id);
	}
