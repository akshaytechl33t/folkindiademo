<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

################################ Frontend #####################################

//$route['index1'] = "Home/index1/";
$route['product/(:any)'] = "Products/index/$1";
$route['product-details/(:any)'] = "Products/product_details/$1";
$route['event-details/(:any)'] = "Static_page/event_detail/$1";
$route['contact-us'] = "Contact_us/index";
$route['logout'] = "User/logout";
$route['my-account'] = "User/account";
$route['change-password'] = "User/change_password";
$route['address'] = "Shipping/index";
$route['viewcart'] = "Cart/cart";
$route['checkout'] = "Checkout/index";
$route['success'] = "Checkout/success";
$route['error'] = "Checkout/failure";
$route['currency-paypal'] = "Checkout/currencyPaypal";
$route['cancel'] = "Checkout/cancel";
$route['(:any)'] = "Static_page/index/$1";
$route['orders'] = "User/orders";
$route['test'] = "User/justtest";
$route['order-details/(:any)'] = "User/order_details/$1";
$route['skill-development'] = "Home/skillDevelopmentDemo";


################################ Admin #####################################
$route['admin'] = "admin/Administrator/index";
$route['admin/login'] = "admin/Administrator/login";
$route['admin/logout'] = "admin/Administrator/logout";
$route['admin/home'] = "admin/Administrator/index";
$route['admin/change_password'] = "admin/Administrator/password_change";
$route['admin/country'] = "admin/Country/index";
$route['admin/home_feature'] = "admin/home_feature";
$route['admin/category'] = "admin/Category/index";
$route['admin/home_featue_product'] = "admin/Home_feature_product/index";
$route['admin/home_footer_section_image'] = "admin/Home_footer_section_image/index";
$route['admin/attributes'] = "admin/Attributes/index";
$route['admin/features'] = "admin/Features/index";
$route['admin/products'] = "admin/Products/index";
$route['admin/product_attribute_image/(:any)'] = "admin/Product_attribute_image/index/$1";
$route['admin/faq'] = "admin/Faq/index";
$route['admin/about-us'] = "admin/Cms_email/about_us";
$route['admin/event'] = "admin/Event/index";
$route['admin/promocode'] = "admin/Promocode/index";

