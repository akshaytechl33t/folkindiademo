<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Public_init_elements {

    var $CI;
    var $data;
	
    function __construct() 
	{
        $this->CI = & get_instance();		
    }	
	function is_user_logged_in()
	{	
        $is_user_logged_in = $this->CI->session->userdata['is_user_signed_in']['user_signed_in'];
        if (!isset($is_user_logged_in) || $is_user_logged_in != TRUE) 
		{             
            $data = array('redirect_url' => $_SERVER['HTTP_REFERER'], 'error_message' => "Please login & continue.......");
            $this->CI->session->set_userdata($data);
        	redirect('/');
        }
    }
	function init_site_setting() {       
        $table['name']="folk_site_settings";
		$select = '*';			
		$record = $this->CI->Common_model->find_data($table,'row','','',$select);	
        if ($this->CI->uri->segment(1) == "") {
            $title = str_replace("-", " ", $this->CI->uri->segment(1));

            $sub_title = "&nbsp;" . ucfirst('home');

            $site_meta_description = $record->site_meta_description;
        } else {
            $title = str_replace("-", " ", $this->CI->uri->segment(1));

            if ($this->CI->uri->segment(2) != NULL) {
                $segment = str_replace("-", " ", $this->CI->uri->segment(2));
                $sub_title = "&nbsp;-&nbsp;" . ucfirst($segment);
            }

            $site_meta_description = $record->site_meta_description;
        }
		define('SITE_TITLE', $record->site_title . " :: " . ucfirst($title) . $sub_title);		
        define('SITE_NAME', $record->site_name);		
        define('SITE_FIRST_SET_OF_RESULTS', $record->site_first_set_of_results);		
        define('SITE_RECEIVER_EMAIL', $record->site_receiver_email);	
		define('SITE_RECEIVER_EMAIL_2', $record->site_receiver_email2);
        define('SITE_SENDER_NAME', $record->site_sender_name);		
        define('SITE_SENDER_EMAIL', $record->site_sender_email);
        define('SITE_FACEBOOK_LINK', $record->site_facebook_link);		
        define('SITE_TWITTER_LINK', $record->site_twitter_link);		
        define('SITE_Linkedin_LINK', $record->site_linkedin);					
        define('SITE_ADDRESS', $record->site_address);	
        define('SITE_PHONE_1', $record->site_phone_1);
        define('SITE_PHONE_2', $record->site_phone_2);  
		define('SITE_FAX_NUMBER', $record->site_fax_number); 
        define('SITE_META_DESCRIPTION', $record->site_meta_description);
        define('SITE_META_KEYWORDS', $record->site_meta_keywords);   
		define('SITE_CONTACT_DETAIL', $record->site_contact_detail); 		
		define('SITE_PAYPAL_MERCHANT_ID', $record->site_paypal_merchant_email_id); 
		define('SITE_PAYPAL_MODE}', $record->site_paypal_mode); 
		define('SITE_PAYPAL_URL', $record->site_paypal_url);
 		define('SITE_CONTACT_DETAIL1', $record->site_contact_detail1);
	   
    }
	function init_head($data=NULL){			
		$this->CI->data['head'] = $this->CI->load->view('frontend/elements/head', $data, true);
	}
	function init_header(){			
		$table['name'] = 'folk_categories';
		$this->data['rows'] = $this->CI->Common_model->find_data($table,'array','',array('published'=>1),'id,name,seo_name');
		$table_static_page['name'] = 'folk_static_pages';
		$this->data['static_page'] = $this->CI->Common_model->find_data($table_static_page,'array','',array('top_menu'=>1,'published'=>1));
		$this->data['login_popup'] = $this->CI->load->view('frontend/elements/login_popup',$this->data, false);
		$this->data['registration_popup'] = $this->CI->load->view('frontend/elements/registration_popup',$this->data, false);
$this->data['forgot_password_popup'] = $this->CI->load->view('frontend/elements/forgot_password_popup',$this->data, false);		
$this->data['promocode'] = $this->CI->Common_model->find_data(array('name'=>'folk_promocodes'),'row','',array('published'=>'1','expire >='=>date('Y-m-d')),'code,expire,discount_amount');		
		
		$this->CI->data['header'] = $this->CI->load->view('frontend/elements/header',$this->data, true);
	}	
	function init_header_demo(){			
		$table['name'] = 'folk_categories';
		$this->data['rows'] = $this->CI->Common_model->find_data($table,'array','',array('published'=>1),'id,name,seo_name');
		$table_static_page['name'] = 'folk_static_pages';
		$this->data['static_page'] = $this->CI->Common_model->find_data($table_static_page,'array','',array('top_menu'=>1,'published'=>1));
		$this->data['login_popup'] = $this->CI->load->view('frontend/elements/login_popup',$this->data, false);
		$this->data['registration_popup'] = $this->CI->load->view('frontend/elements/registration_popup',$this->data, false);
$this->data['forgot_password_popup'] = $this->CI->load->view('frontend/elements/forgot_password_popup',$this->data, false);		
$this->data['promocode'] = $this->CI->Common_model->find_data(array('name'=>'folk_promocodes'),'row','',array('published'=>'1','expire >='=>date('Y-m-d')),'code,expire,discount_amount');		
		
		$this->CI->data['header'] = $this->CI->load->view('frontend/elements/header-demo',$this->data, true);
	}	
	function init_banner(){		
		$table['name'] = 'folk_banners';
		$this->data['rows'] = $this->CI->Common_model->find_data($table,'array','',array('published'=>1));		
		$this->CI->data['banner'] = $this->CI->load->view('frontend/elements/banner', $this->data, true);
	}
	function init_footer1(){
		$table_static_page['name'] = 'folk_static_pages';
		$this->data['static_page_all'] = $this->CI->Common_model->find_data($table_static_page,'array','',array('top_menu'=>0,'published'=>1),'id,page_name,page_seo_url');
		$this->CI->data['footer1'] = $this->CI->load->view('frontend/elements/footer1', $this->data, true);
	}
	function init_footer(){
		$this->CI->data['footer'] = $this->CI->load->view('frontend/elements/footer', '', true);
	}
    function init_elements()
	{	
		$this->init_site_setting();		
		$this->init_head();
		if(uri_string() != 'skill-development'){
			$this->init_header();
		} else {
			$this->init_header_demo();
			
		}
		if($this->CI->uri->segment(1) == '' ){ $this->init_banner();} 
		$this->init_footer1();
		$this->init_footer();
    }

}
