<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_init_elements
{
    var $CI;

    function  __construct()
    {
        $this->CI =& get_instance();
    }

    function init_head()
    {		
		$this->CI->load->model('Settings_model');
		$row = $this->CI->Settings_model->find_data('row',array('id'=>1));
		$data['title'] = $row->site_title." - Admin Panel";
		$data['site_name'] = $row->site_name;
        $data['meta_keyword'] = $row->site_meta_keywords;
        $data['meta_description'] = $row->site_meta_description;
		
		define('SITE_NAME', $row->site_name);
		define('SITE_RECEIVER_EMAIL', $row->site_receiver_email);
		define('SITE_SENDER_NAME', $row->site_sender_name);
		define('SITE_SENDER_EMAIL', $row->site_sender_email);
		define('DISPLAY_NUM_RESULTS', $row->site_first_set_of_results);
		
        $this->CI->data['head'] = $this->CI->load->view('admin/elements/head', $data, true);
    }
	function init_header()
    {
		$this->CI->data['header'] = $this->CI->load->view('admin/elements/header', $data, true);
    }
	function init_sidebar()
    {
		$this->CI->data['sidebar'] = $this->CI->load->view('admin/elements/left_side_bar', $data, true);
    }
	function init_footer()
    {
		$this->CI->data['footer'] = $this->CI->load->view('admin/elements/footer', $data, true);
    }
    function init_elements()
    {		
		$this->is_admin_logged_in();      
        $this->init_head();
		$this->init_header();
		$this->init_sidebar();
		$this->init_footer();
    }
    function is_admin_logged_in()
    {
        $is_admin_logged_in = $this->CI->session->userdata('is_admin_logged_in');

        if(!isset($is_admin_logged_in) || $is_admin_logged_in != true)
        {
           redirect('admin/login');
        }
    }
}
?>