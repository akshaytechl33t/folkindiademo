<?php 
class paypal_inc
{
	function index() 
	{
		$paypal = array();
		$paypal['business']      = SITE_PAYPAL_MERCHANT_ID;   
		$paypal['site_url']      = base_url();
		$paypal['image_url']     = base_url()."assets/frontend/images/logo.png";    // 217*112
		$paypal['success_url']   = "Payment/success";
		$paypal['cancel_url']    = "Payment/error";
		
		$paypal['notify_url']    = "Payment/ipn";
		$paypal['return_method'] = "2"; //1=GET 2=POST       --> Use post since we will need the return values to check if order is valid
		$paypal['currency_code'] = "USD"; //['USD,GBP,JPY,CAD,EUR']
		$paypal['lc']            = "IN";		
		$paypal['url']           = SITE_PAYPAL_URL;//"https://www.sandbox.paypal.com/cgi-bin/webscr/";
		$paypal['post_method']   = "fso"; //fso=fsockopen(); curl=curl command line libCurl=php compiled with libCurl support
		$paypal['curl_location'] = "/usr/local/bin/curl";
		
		$paypal['bn']  = "toolkit-php";
		$paypal['cmd'] = "_xclick"; /// Normal Pyament
		//$paypal['cmd'] = "_xclick-subscriptions"; /// Recuring Normal Pyament
		//Payment Page Settings
		$paypal['display_comment']          = "0"; //0=yes 1=no
		$paypal['comment_header']           = "Comments";
		$paypal['continue_button_text']     = "Continue >>";
		$paypal['background_color']         = ""; //""=white 1=black
		$paypal['display_shipping_address'] = "1"; //""=yes 1=no     --> We already asked for the shipping address so tell paypal not to ask it again
		$paypal['display_comment']          = "1"; //""=yes 1=no
		
		//Product Settings
		$paypal['item_name']     = isset($_POST['item_name']) ? $_POST['item_name']: "";
		$paypal['item_number']   = isset($_POST['item_number']) ? $_POST['item_number']: "";
		$paypal['amount']        = isset($_POST['amount']) ? $_POST['amount']: "";
		$paypal['on0']           = isset($_POST['on0']) ? $_POST['on0']: "";
		$paypal['os0']           = isset($_POST['os0']) ? $_POST['os0']: "";
		$paypal['on1']           = isset($_POST['on1']) ? $_POST['on1']: "";
		$paypal['os1']           = isset($_POST['os1']) ? $_POST['os1']: "";
		$paypal['quantity']      = isset($_POST['quantity']) ? $_POST['quantity']: "";
		$paypal['edit_quantity'] = ""; //1=yes ""=no
		$paypal['invoice']       = isset($_POST['invoice']) ? $_POST['invoice']: "";
		$paypal['tax']           = isset($_POST['tax']) ? $_POST['tax']: "";
		
		//Shipping and Taxes
		$paypal['shipping_amount']          = isset($_POST['shipping_amount']) ? $_POST['shipping_amount']: "";
		$paypal['shipping_amount_per_item'] = "";
		$paypal['handling_amount']          = "";
		$paypal['custom_field']             = "";
		
		//Customer Settings
		$paypal['name'] = isset($_POST['name']) ? $_POST['name']: "";
		$paypal['street_address']  = isset($_POST['street_address']) ? $_POST['street_address']: "";
		$paypal['land_mark']  = isset($_POST['land_mark']) ? $_POST['land_mark']: "";
		$paypal['phone_number']  = isset($_POST['phone_number']) ? $_POST['phone_number']: "";
		$paypal['pincode']      = isset($_POST['pincode']) ? $_POST['pincode']: "";
		$paypal['country_name']     = isset($_POST['country_name']) ? $_POST['country_name']: "";
		$paypal['state_name']       = isset($_POST['state_name']) ? $_POST['state_name']: "";
		$paypal['city_name']     = isset($_POST['email']) ? $_POST['city_name']: "";
		return $paypal;
	}


/********************************************************************************
*
*                           PAYPAL FUNCTIONS 
*
********************************************************************************/

//create variable names to perform additional order processing

function create_local_variables() 
{ 
	$array_name = array();
	$array_name['business'] = $_POST['business'];
	$array_name['receiver_email'] = $_POST['receiver_email'];
	$array_name['receiver_id'] = $_POST['receiver_id'];
	$array_name['item_name'] = $_POST['item_name'];
	$array_name['item_number'] = $_POST['item_number'];
	$array_name['quantity'] = $_POST['quantity'];
	$array_name['invoice'] = $_POST['invoice'];
	$array_name['custom'] = $_POST['custom'];
	$array_name['memo'] = $_POST['memo'];
	$array_name['tax'] = $_POST['tax'];
	$array_name['option_name1'] = $_POST['option_name1'];
	$array_name['option_selection1'] = $_POST['option_selection1'];
	$array_name['option_name2'] = $_POST['option_name2'];
	$array_name['option_selection2'] = $_POST['option_selection2'];
	$array_name['num_cart_items'] = $_POST['num_cart_items'];
	$array_name['mc_gross'] = $_POST['mc_gross'];
	$array_name['mc_fee'] = $_POST['mc_fee'];
	$array_name['mc_currency'] = $_POST['mc_currency'];
	$array_name['settle_amount'] = $_POST['settle_amount'];
	$array_name['settle_currency'] = $_POST['settle_currency'];
	$array_name['exchange_rate'] = $_POST['exchange_rate'];
	$array_name['payment_gross'] = $_POST['payment_gross'];
	$array_name['payment_fee'] = $_POST['payment_fee'];
	$array_name['payment_status'] = $_POST['payment_status'];
	$array_name['pending_reason'] = $_POST['pending_reason'];
	$array_name['reason_code'] = $_POST['reason_code'];
	$array_name['payment_date'] = $_POST['payment_date'];
	$array_name['txn_id'] = $_POST['txn_id'];
	$array_name['txn_type'] = $_POST['txn_type'];
	$array_name['payment_type'] = $_POST['payment_type'];
	$array_name['for_auction'] = $_POST['for_auction'];
	$array_name['auction_buyer_id'] = $_POST['auction_buyer_id'];
	$array_name['auction_closing_date'] = $_POST['auction_closing_date'];
	$array_name['auction_multi_item'] = $_POST['auction_multi_item'];
	$array_name['name'] = $_POST['name'];
	$array_name['payer_business_name'] = $_POST['payer_business_name'];
	$array_name['street_address'] = $_POST['street_address'];
	$array_name['land_mark'] = $_POST['land_mark'];
	$array_name['phone_number'] = $_POST['phone_number'];
	$array_name['pincode'] = $_POST['pincode'];
	$array_name['country_name'] = $_POST['country_name'];
	$array_name['state_name'] = $_POST['state_name'];
	$array_name['city_name'] = $_POST['city_name'];
	$array_name['payer_email'] = $_POST['payer_email'];
	$array_name['payer_id'] = $_POST['payer_id'];
	$array_name['payer_status'] = $_POST['payer_status'];
	$array_name['notify_version'] = $_POST['notify_version'];
	$array_name['verify_sign'] = $_POST['verify_sign'];
	
	return $array_name; 
}


//this function creates a comma separated value file from an array. 

function create_csv_file($file,$data) 
{
	// the return value
	$success = false;
	
	//check for array
	if (is_array($data)) { 
		$post_values = array_values($data); 
		
		//build csv data
		foreach ($post_values as $i) { 
			$csv.="\"$i\","; 
		}
		
		//remove the last comma from string
		$csv = substr($csv,0,-1); 
		
		//check for existence of file
		if(file_exists($file) && is_writeable($file)) { 
			$mode="a"; 
		} else { 
			$mode="w"; 
		}
		
		//create file pointer
		$fp=@fopen($file,$mode);
		 
		//write to file
		fwrite($fp,$csv . "\n"); 
		
		//close file pointer
		fclose($fp); 
		
		$success = true; 
	} 
	
	return $success;	 
}

//posts transaction data using fsockopen. 
	function fsockPost($url,$data) 
	{   
		$postData = '';
		
		// return value
		$info = '';
		
		//Parse url 
		//$web=parse_url($url); print_r($web); echo "<br>"; 
		/*---------------------------------------------------*/
		//$data = array("Return_url");
		/*---------------------------------------------------*/
		//print_r($data); echo "<br>";
		
	
		
		
		//build post string 
		foreach ($data as $i=>$v) { 
			$postData.= $i . "=" . urlencode($v) . "&"; 
		}
		
		// we must append cmd=_notify-validate to the POST string
		// so paypal know that this is a confirmation post
		$postData .= "cmd=_notify-validate";
		
		//Set the port number
		if ($web['scheme'] == "https") { 
			$web['port'] = "443";  
			$ssl       = "ssl://"; 
		} else { 
			$web['port'] = "80"; 
			$ssl       = "";
		}  
		
		//Create paypal connection
		$fp = @fsockopen($ssl . $web[host], $web[port], $errnum, $errstr,30); 
		
		//Error checking
		if(!$fp) { 
			echo "$errnum: $errstr"; 
		} else { 
			//Post Data
			fputs($fp, "POST $web[path] HTTP/1.1\r\n"); 
			fputs($fp, "Host: $web[host]\r\n"); 
			fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n"); 
			fputs($fp, "Content-length: ".strlen($postData)."\r\n"); 
			fputs($fp, "Connection: close\r\n\r\n"); 
			fputs($fp, $postData . "\r\n\r\n"); 
		
			// loop through the response from the server 
			$info = array();
			while (!feof($fp)) { 
				$info[] = @fgets($fp, 1024); 
			} 
			
			//close fp - we are done with it 
			fclose($fp); 
			
			// join the results into a string separated by comma
			$info = implode(",", $info); 
			
		}
		
		return $info; 
	
	} 
	
} 
?>