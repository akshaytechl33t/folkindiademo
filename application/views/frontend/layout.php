<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $head;?>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1060174780715724";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page"> 	<div id="loadericon" class="loadericon" style="display: none; z-index: 99999; !important">   <div id="maskicon" class="maskicon">      <style scoped>        #maskicon{           -webkit-box-sizing: content-box;           -moz-box-sizing: content-box;           box-sizing: content-box;           z-index: 999;           width: 100%;           height: 300%;           position: fixed;           opacity: 0.85;           top: 0;           right: 0;           bottom: 0;           left: 0;           display: block !important;           overflow: hidden;           border: none;           font: normal 16px/1 Arial,serif;           color: rgba(255,255,255,1);           text-align: center;           -o-text-overflow: ellipsis;           text-overflow: ellipsis;           background: rgba(10,10,10,0.8);        }        #maskicon{display:none;}      </style>   </div>   <div id="please_wait" class="please_wait">        <style scoped>          .please_wait{           position:fixed;           overflow:hidden;           top:43%;           left:46%;           border:15px solid #F0F4C3;           border-radius:50%;           border-top:9px solid #009688;           border-bottom:9px solid #00BCD4;           width:120px;           height:120px;           z-index:1000;           -webkit-animation:spin 3s linear infinite;           animation:spin 3s linear infinite;          }          @-webkit-keyframes spin{            0% { -webkit-transform: rotate(0deg); }            100% { -webkit-transform: rotate(360deg); }          }          @keyframes spin{            0% { transform: rotate(0deg); }            100% { transform: rotate(360deg); }          }          </style>   </div></div>
	<?php echo $header;?>
	<?php echo $banner;?>
	<?php echo $maincontent; ?>
	<footer class="footer">
		<?php echo $footer1;?>
		<?php echo $footer;?>
	</footer>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</div>
</body>
</html>
