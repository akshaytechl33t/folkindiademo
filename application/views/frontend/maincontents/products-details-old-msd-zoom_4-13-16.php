<div class="bodyContent">
	<div class="container">
		<div class="row">
			<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="main-content container-fluid catalog">
				<?php if(!empty($row)){?>
					<div class="main-contentContainer">
						<div id="mainDivBody" class="main-vertical-body row-fluid">
							<div class="detailsPage">
								<div class="detailsPageTop">
									<div class="ficha_top_navigation clearfix catalog detailsPageBredCum">
										<div class="listHeaderNavContainer">              
											<div class="content_navigation_menu hidden_mobile pull-left" id="barraOrdenacion">
												<span class="hidden_mobile"><a href="<?php echo base_url();?>">Home</a></span>
												<span class="separator">>></span><a href="<?php echo base_url();?>product/<?php echo $row->category_seo_name;?>"><?php echo $row->category_name;?></a>
												<span class="separator active">>><?php echo $row->name;?></span>
											</div>              
										</div>         
									</div>      

								</div>
								<div class="detailsContent">
									<div class="prdgallery span7">
										<div class="row">
											<div class="col-xs-4">
												<div class="product-thumb">
													<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" class="img-responsive margin-top-7 margin-bottom-7" alt="">
				  
													<?php if(!empty($row->image5)){?>
													
													 <img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image5;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image5;?>" class="img-responsive margin-top-7 margin-bottom-7" alt="">
													<?php } ?>
													<?php if(!empty($row->image6)){?>
													<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image6;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image6;?>" class="img-responsive margin-top-7 margin-bottom-7" alt="">
													<?php } ?>
													<?php if(!empty($row->image7)){?>
													<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image7;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image7;?>" class="img-responsive margin-top-7 margin-bottom-7" alt="">
													<?php } ?>
													
												</div>
											</div> 
											<div class="col-xs-2"></div>
												<div class="col-xs-18 zoom-full-wrapper">
													<div class="medium-wrapper">
														<div class="big-preview-wrapper">
															<?php  if(!empty($row->image1)){?>
																<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" class="img-responsive" alt="">
															<?php } else { ?>
																<img src="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview" alt="Preview" />
															<?php } ?>
														</div>
													</div>
													<div id="zoom-preview-pane">
														<div class="preview-container">
															<?php  if(!empty($row->image1)){?>
																<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" class="zoom-preview" alt="Preview" />
															<?php } else { ?>
																<img src="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview" alt="Preview" />
															<?php } ?>
														</div>
													</div>
												</div>
											<div class="col-xs-4"></div>
										</div>
									</div>
									<div class="datos_ficha_producto span5 pull-right">
									<div class="prdDts">
									<div class="cabecera_producto_height_fix">
										<div class="cabecera_producto row-fluid">
											<div class="info_cabecera_producto">
												<div class="nombreProducto row-fluid hidden_mobile">
												<h1><?php echo $row->name;?></h1>
												</div>
												<div class="rating_listing_container">
													<div class="rating_listing">
														<span id="" class="star_font">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-o"></i>
														</span>
													</div>
													<div class="review_listing">
													<a href="#review_list">
													<span>1</span>
													Reviews
													</a>
													</div>
													<div class="write_review_listing">
													<a href="javascript:void();" data-toggle="modal" data-target="#login_model">Write a REVIEW</a>
													</div>
												</div>               
											<div class="referenciaProducto row-fluid"><?php echo $row->bag_no;?></div>
											</div>
											<div class="precio_cabecera_producto">
												<div class="precioProducto row-fluid">
													<span >
														<?php /*?><span class="padding-right3px;" class="erased ficha_precio_venta_entero false">Rs.</span>
														<span style="" class="erased ficha_precio_venta_entero false">
															<?php echo $row->price;?>
														</span>
														<?php */?>
													</span>									
												</div>
											</div>
										</div>
									</div>

									<div class="producto_color row-fluid">
										<div class="producto_color_texto span12">
											<p class="producto_title_color">Colour:</p>
											<p class="producto_actual_color" ><?php echo $row->value;?></p>
										</div>
										<?php /*?><div class="chooseColor">
											<div class="color_div_on">
												<div class="round-border">
													<img src="images/large/color0.png" alt=""> 
												</div>
											</div>
										</div> 
										<?php */?>
									</div>
									<?php /*?><div class="selector_tallas row-fluid">
										<div id="listaTallas" class="span6">
											<select>
												<option >Choose your size</option>
												<option>One size</option>
											</select>
										</div> 
									</div>
									
									<div class="shop_wishlist row-fluid no_loged">
										<a href="contact-us.html" class="srchStore">Add to cart</a>
										<a href="#" class="addWhishlist">Buy now</a> 
									</div>
									<?php */?>
									<div class="shop_wishlist row-fluid no_loged">
										<a href="#" class="srchStore" data-toggle="modal" data-target="#myModal25">Enquiry</a>
										<?php /*?><a href="#" class="addWhishlist">Add to wishlist</a> <?php */?>
									</div>
									<div class="social-Connect row-fluid ">
										<ul>
											<li><a href="<?php echo SITE_FACEBOOK_LINK?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="<?php echo SITE_TWITTER_LINK?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="<?php echo SITE_Linkedin_LINK?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>

									<div class="tabPanels row-fluid">
										<div class="accordion" id="accordion2">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed" aria-expanded="true" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
														Description 		
													</a>
												</div>
												<div class="accordion-body collapse in" id="collapseOne" aria-expanded="true">
													<div class="accordion-inner">
														<?php echo $row->description;?>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
														Material and washing instructions
													</a>
												</div> 
												<div id="collapseTwo" class="accordion-body collapse">
													<div class="accordion-inner">
														<?php echo $row->instruction;?>
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
									</div>
									</div>
									<div id="panelOufitsProducto">
										<h3>Similar Product</h3>
										<ul>
										<?php if(!empty($similar_product_arr)){
										foreach($similar_product_arr as $similar_product_details):
										?>										
											<li> 
												<div class="prdImage">
													<a href="<?php echo base_url();?>product-details/<?php echo $similar_product_details->seo_name?>">
														<?php if(!empty($similar_product_details->image3)){?>
														<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $similar_product_details->image3;?>" alt="">
														<?php }else { ?>
														<img src="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview" alt="Preview" />
														<?php } ?>
													</a>
												</div>
												<div class="prdDesc">
													<h2>  <?php echo $similar_product_details->name;?> </h2>
													<?php /*?><div class="prdPrc">
														<?php echo $similar_product_details->price;?>
													</div><?php */?>
												</div> 
											</li>										
										<?php endforeach; } ?>
										</ul>
									</div><!--panelOufitsProducto-->     

									<div class="review_wrapper">
									<h2><?php echo $row->name;?></h2>
										<div class="contents">
											<div class="row relative">
												<div class="col-lg-16 col-md-16 col-sm-16 col-xs-24 absolute_top_one">
													<div class="review_left">
														<div class="row">
														<div class="col-lg-16 col-md-16 col-sm-16 col-xs-24">
															<div class="review_inner_left">
																<h3>Mini Cotton bag</h3>
																<p>Quality of the lens is extremely good, had no problem with fittings. it matches the image shows in site .. good service provided.</p>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-24">
															<div class="review_inner_right">
																<div class="rating_listing">
																	<div class="star_font">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	</div><!--star_font-->
																</div><!--rating_listing-->
															</div><!--review_inner_right-->
															<div class="rating_user_wrapper">
																<h4>Angelica winston</h4>
																<h5>01-28-2016</h5>
															</div>
														</div>
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 absolute_top_two">
													<div class="review_right">
														<div class="big_star">4</div>
													</div>
												</div>
											</div>
										</div>  
									</div><!--review_wrapper-->
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal25"  class="modal fade " role="dialog">
  <div class="modal-dialog">     
    <!-- Modal content-->
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url();?>assets/frontend/images/closeButton.png" alt=""></button>
      <div class="modal-header">
        <h4 class="modal-title"><img src="<?php echo base_url();?>assets/frontend/images/large/logo.png"/></h4>
      </div>
      <div class="modal-body">
	  <?php echo form_open('',array('method'=>'post'));?>
        <div class="form-group">
			<label>Name <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'name', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('name'); ?>								
		</div>
		<div class="form-group">
			<label>Company <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'company', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('company'); ?>								
		</div>
		<div class="form-group">
			<label>Address <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'address', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('address'); ?>								
		</div>
		<div class="form-group">
			<label>Number <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'number', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('number'); ?>								
		</div>
		<div class="form-group">
			<label>Email <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'email', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('email'); ?>								
		</div>
		<div class="form-group">
			<label>Enquiry <span class="required">*</span></label>
			<?php echo form_textarea(array('name'=>'enquiry', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('enquiry'); ?>								
		</div>
		<div class="form-group">
			<label>How did you hear about us?</label>
			<?php echo form_input(array('name'=>'about_us', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('about_us'); ?>								
		</div>
		<div class="form-group"> 								
			<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-primary')); ?> 
		</div>
		<?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/frontend/js/msd-zoom.js" type="text/javascript"></script>
<script>
$(document).ready(function(e) {
		$('.big-preview-wrapper img').attr('src', $('.product-thumb img').eq(0).attr('data-src'));
		$('#zoom-preview-pane img').attr('src', $('.product-thumb img').eq(0).attr('data-src'));

		$('.product-thumb img').on('mouseover', function(e){
			$('.big-preview-wrapper img').attr('src', $(this).attr('data-src'));
			$('#zoom-preview-pane img').attr('src', $(this).attr('data-src'));
		});		
	});
	$(window).load(function(e) {
		$('.big-preview-wrapper img').msdZoom({
		previewPane: '#zoom-preview-pane',
	});
});
</script>