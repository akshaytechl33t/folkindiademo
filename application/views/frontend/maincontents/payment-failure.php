<div class="main-content container-fluid catalog" style="margin-top:80px;">
	<div class="container">
		<div class="innertop"> 
		<!--cart page start-->
			<div class="cartPage"> 
				<div class="check_out_wr">
					<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
						<div id="notifyMessage">						
							<div class="payment_error_containr">
								<div class="error_inner">
									<div class="pay_top_icon"><i class="fa fa-exclamation-triangle"></i></div>
									<div class="pay_top_thanks">Sorry for inconvenience</div>
								</div>
							</div>						
						</div>
						<div class="payment_container_wr">
							<div class="container">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
										<div class="payment_container">
											<h1>Oops! <!--That page can't be found!--></h1>
											<p><strong>Payment failed due to any of these reasons:</strong> <br><br>Session expired due to inactivity <br>You double-click on a button <br>Our system encountered an obstacle</p>
											<p><strong>We are sorry for the inconvenience.</strong></p>
											<div class="payment-try-again">Please Try Again</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
										<div class="oops_img">
											<img src="<?php echo base_url()?>assets/frontend/images/opps_bg.jpg" alt="" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

