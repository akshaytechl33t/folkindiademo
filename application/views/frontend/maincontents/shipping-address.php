<div class="bodyContent">
	<div class="main-content container-fluid catalog">
		<div class="container">
			<div class="innertop">
			<!--final accountpage start-->
				<div class="finalAccountPage">
					<?php echo $common_account_left_panel;?>
					<div class="finalaccRight">     
						<div id="finalaccTabCnt1" class="finalaccTabContent">
						<div id="notifyMessage">
							<?php if($this->session->flashdata('success_message') !=''){?>
							
								<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('success_message'); ?></b> </div>
							
							<?php } ?>
							<?php if($this->session->flashdata('error_message') !=''){?>
							
								<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('error_message'); ?></b> </div>
							
							<?php } ?>
						</div>						
						<h4>Add a new address</h4>
							<div class="personalInfoForm">
								<?php echo form_open(current_url(),array('method'=>'post','id'=>'shipping_address'));?>
									<div class="form-group">
										<label for="exampleInputName">Name <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'name','class'=>'form-control validate[required]','value'=>$this->session->userdata['is_user_signed_in']['user_name']));
											echo form_error('name');
											?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Street Address<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_textarea(array('name'=>'street_address','class'=>'form-control validate[required]'));
											echo form_error('street_address');
											?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Landmark<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'land_mark','class'=>'form-control validate[required]'));
											echo form_error('land_mark');
											?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Phone number<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'phone_number','class'=>'form-control validate[required]'));
											echo form_error('phone_number');
											?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Country<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_dropdown(array('name'=>'country_id','class'=>'form-control validate[required]','id'=>'country_id'),$country_arr);
											echo form_error('country_id');
											?>
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">State<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_dropdown(array('name'=>'state_id','class'=>'form-control validate[required]','id'=>'state_id'),$state_arr);
											echo form_error('state_id');
											?>
											<div id="location_loader"></div>
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">City<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_dropdown(array('name'=>'city_id','class'=>'form-control validate[required]','id'=>'city_id'),$city_arr);
											echo form_error('city_id');
											?>
											<div id="location_loader"></div>
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Pincode<span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'pincode','class'=>'form-control validate[required]'));
											echo form_error('pincode');
											?> 
										</span>
									</div>
									<div class="popFormSubmit">
										<?php echo form_submit(array('name'=>'submit', 'value'=>'Save Changes', 'class'=>'btn btn-default popFormSubmitBtn')); ?>										
									</div>
								<?php echo form_close(); ?> 
							</div>
						</div>
						<div class="save-shipping-address">
							<h4>Your Saved Addresses</h4>
							<?php if(!empty($get_shipping_address)){
									foreach($get_shipping_address as $address):
							?>
								<div class="col-md-7 view-address" id="<?php echo $address->id?>">
									<div><strong><?php echo $address->name;?></strong></div>
									<div><?php echo $address->street_address;?></div>
									<div><?php echo $address->land_mark;?></div>									
									<div><?php echo $address->country_name;?></div>
									<div><?php echo $address->state_name;?></div>
									<div><?php echo $address->city_name;?></div>
									<div><?php echo $address->pincode;?></div>
									<div>Ph:-<?php echo $address->phone_number;?></div>
									<div><a href="javascript:void(0)" class="delete_shipping_address" id="<?php echo $address->id?>" title="DELETE"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
								</div>
							<?php endforeach;?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>