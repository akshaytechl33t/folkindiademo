<?php if(!empty($rows)){ ?>
<section class="ourServies">
    <div class="container">
      <div class="row">
        <div class="serviceAll">           
          <div class="row">
		  <?php foreach($rows as $feature_details):		
		  ?>
            <div class="col-md-6 col-sm-4 col-xs-12 ">
			<a href="<?php echo $feature_details->link;?>"> <div class="service-box"> <img data-original="<?php echo base_url();?>uploads/images/home/feature_product/<?php echo $feature_details->images?>" alt="" class="lazy">  </a>
              </div>
            </div>
			<?php endforeach;?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php } ?> 
<?php if(!empty($footer_images)){?>
<section class="letsMake">
    <div class="container">
      <div class="row">
        <div class="letsMakeCnt">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 letsMakeBanner">
			<?php foreach($footer_images as $images):
			//print_r($images);
			?>
			<?php ($images->link !='')? $link=$images->link:$link=base_url().$images->seo_name;?>
             <a href="<?php echo $link;?>">  
			 <div class="letsMakeBannerInner">
				<img data-original="<?php echo base_url();?>uploads/images/home/footer_section_image/<?php echo $images->images?>" alt="" class="lazy">
			 </div></a>
            </div>
           <?php endforeach;?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php } ?> 