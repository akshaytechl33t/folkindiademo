 <link href="<?php echo base_url();?>assets/zoomtimeline.css" rel="stylesheet">
    <link rel='stylesheet' type="text/css" href="<?php echo base_url();?>assets/scroller.css"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/responsiveCarousel.js"></script>
<style type="text/css">
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 45px;
  margin: 0 0 20px;
  position: relative;

  /*Font*/
  font-family: Georgia, serif;
  font-size: 10px;
  line-height: 1.3;
  color: #666;
  text-align: justify;

  /*Borders - (Optional)*/
  border-left: 15px solid #c76c0c;
  border-right: 2px solid #c76c0c;

  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\201C"; /*Unicode for Left Double Quote*/

  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;

  /*Positioning*/
  position: absolute;
  left: 10px;
  top:5px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote a{
  text-decoration: none;
  background: #eee;
  cursor: pointer;
  padding: 0 3px;
  color: #c76c0c;
}

blockquote a:hover{
 color: #666;
}

blockquote em{
  font-style: italic;
}
.detail-excerpt{color:black !important;}
</style>
<div class="bodyContent static_page static_pageAbt">
  <div class="container">
    <div class="row">
      <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
		<!--<div class="abtUsPage">-->

		<?php if($this->uri->segment(1) !='our-story' && $this->uri->segment(1) !='media' && $this->uri->segment(1) !='media-section'){?>
			<div class="static_one">
				<h2><?php echo ($row->page_title)? $row->page_title:'Comming soon...;'?></h2>
				<div class="static_page">
					<div class="static_page_container">
						<div class="page_categories">
							<div class="page-details">
								<?php echo ($row->page_content)?$row->page_content:'Comming soon...;'?>
							</div>
						</div>
					</div>
				</div>
			</div>
<?php } else if($this->uri->segment(1) =='media'){?>
<div class="" style="text-align: left">
                    <div style="color: black !important;" class="zoomtimeline mode-3dslider auto-init zoomtimeline0 skin-light circuit-the-timeline-on inited ztm-ready" data-options="{startItem: 1}" id="zoomtimeline5">




                        <div class="items">











                        </div>


                        <!-- the preloader START -->
                        <div class="preloader-wave">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <!-- the preloader END -->

                        <div class="yearlist-con"><div class="yearlist-container"><div class="yearlist-container-inner"><div class="yearlist-line"></div>





                        </div>

                        </div>

                        </div>






                        <div class="details-container" style="height: 651px;"><div class="clear"></div>


                            <input type="radio" name="radio_btn" id="it1" checked="">
                            <label for="it1" class="detail ">


                                <div class="the-year">The Entrepreneur<figure></figure></div>
                                <h3 class="the-heading">The Concept </h3>
<div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(<?php echo base_url();?>assets/3d_1.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                               <p> <blockquote>
                                    At Folk we believe in the distinctive ancestral heritage of India which can give rise to a range of exciting products for the world market. Also as a citizen of a developing nation, I see a larger socio-economic relevance in my business activities to empower and emancipate a section of the society.<a href="https://www.entrepreneur.com/article/290138" target="_blank">More</a>
                                </blockquote></p>
                            </div><div class="clear"></div>


                            </label>



                            <input type="radio" name="radio_btn" id="it2">
                            <label for="it2" class="detail ">


                                <div class="the-year">Press reader<figure></figure></div>
                                <h3 class="the-heading">Drawing Designs</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(<?php echo base_url();?>assets/3d_2.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p><blockquote>
                                   At Folk, we envision and design useful and customisable products from purely natural materials that support the endeavour for an environment friendly and sustainable future.<a href="http://www.pressreader.com/india/financial-chronicle/20160316/281947426960540" target="_blank">More</a>
                                </blockquote></p>
                            </div><div class="clear"></div>


                            </label>


                            <input type="radio" name="radio_btn" id="it3" >
                            <label for="it3" class="detail ">


                                <div class="the-year">Digital FC<figure></figure></div>
                                <h3 class="the-heading">The Perfect City</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(<?php echo base_url();?>assets/3d_3.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div>

                                <div class=" detail-excerpt">


                                  <p><blockquote>
                                        It is very important to be strategic to make a dream come true. Keep on chasing it with all your effort and strength. One must remember it is a continuous roller coaster and there is no easy way to success.<a href="http://www.mydigitalfc.com/views/bmonica-gupta-kanika-tekriwal-manjir-chatterjeeb-ahead-curve-878" target="_blank">More</a>

                                   </blockquote></p>
                                </div>
                                <div class="clear"></div>
                            </label>


                            <input type="radio" name="radio_btn" id="it4">
                            <label for="it4" class="detail ">

                                <div class="the-year">Liverpool Echo<figure></figure></div>

                                <h3 class="the-heading">Concept to Life</h3>
<div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(<?php echo base_url();?>assets/3d_4.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                               <p><blockquote>
                                  I am very excited about this new venture and the launch. We were looking for a place to be based in the UK to operate our whole European market, Liverpool was the obvious choice because of its central location within the UK and its direct transport links to Europe and beyond.<a href="http://www.liverpoolecho.co.uk/news/liverpool-news/natural-folk-choose-liverpool-3429589" target="_blank">More</a>
                                 </blockquote></p>
                            </div><div class="clear"></div>

                            </label>


                            <input type="radio" name="radio_btn" id="it5">
                            <label for="it5" class="detail next-next-item">

                                <div class="the-year">Padrea<figure></figure></div>

                                <h3 class="the-heading">Meeting Room</h3><div class="detail-image-con" style=""><div class="detail-image--border"></div><div class="detail-image" style="background-image:url(<?php echo base_url();?>assets/3d_5.jpg);"></div><div class="detail-image-shadow-con"><div class="detail-image-shadow-left"></div><div class="detail-image-shadow-right"></div></div></div><div class=" detail-excerpt">


                                <p><blockquote>
                                   My advice to you is that please do not get bogged down with someone elses opinion about your life. Dream big, get on the job, be restless until your objectives are met. Success and a fulfilling life beckons!.<a href="http://padrea.com/2017/03/08/nothing-called-woman-entrepreneur/" target="_blank">More</a>
                                </blockquote></p>
                            </div><div class="clear"></div>

                            </label>
















                        </div></div>
                </div>
<?php } else if($this->uri->segment(1) =='media-section'){?>
				<div class="abtUsPage">
					<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 tabVertically">
						<div class="static_one">
							<h2>Media Section</h2>
						</div>
						<div class="row">
						<?php if($rows){?>
							<div class="tabbable">
								<ul class="nav nav-pills nav-stacked col-lg-6 col-md-6 col-sm-6 col-xs-24 tabVerticallyMenu ">
									<?php $i=0; foreach($rows as $details){ $i++;?>
										<li class="<?php echo ($i==1)?'active':'';?>"><a href="#<?php echo $i;?>"  data-toggle="tab"><?php echo ($details->title == 'Team') ? '<div class="team">'.$details->title.'</div>' : $details->title;?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content col-lg-18 col-md-18 col-sm-18 col-xs-24 tabVerticallyCnt ">
									<?php $j=0; foreach($rows as $details){
									$j++;
									?>
										<div class="tab-pane <?php echo ($j==1)?'active':'';?>" id="<?php echo $j;?>">
											<?php echo $details->content;?>
										</div>
									<?php  } ?>
								</div>
							</div>



						<?php } else{ echo 'Comming soon...';}?>
						<!-- /tabs -->
						</div>
					</div>
				</div>

		<?php } else { ?>
				<div class="abtUsPage">
					<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 tabVertically">
						<div class="static_one">
							<h2>Our Story</h2>
						</div>
						<div class="row">
						<?php if($rows){?>
							<div class="tabbable">
								<ul class="nav nav-pills nav-stacked col-lg-6 col-md-6 col-sm-6 col-xs-24 tabVerticallyMenu ">
									<?php $i=0; foreach($rows as $details){ $i++;?>
										<li class="<?php echo ($i==1)?'active':'';?>"><a href="#<?php echo $i;?>"  data-toggle="tab"><?php echo ($details->title == 'Team') ? '<div class="team">'.$details->title.'</div>' : $details->title;?></a></li>
									<?php } ?>
								</ul>

								<div class="tab-content col-lg-18 col-md-18 col-sm-18 col-xs-24 tabVerticallyCnt ">
									<?php $j=0; foreach($rows as $details){
									$j++;
									?>
										<div class="tab-pane <?php echo ($j==1)?'active':'';?>" id="<?php echo $j;?>">
											<?php echo $details->content;?>
										</div>
									<?php  } ?>
								</div>
							</div>



						<?php } else{ echo 'Comming soon...';}?>
						<!-- /tabs -->
						</div>
					</div>
				</div>
		<?php } ?>
		<?php if($this->uri->segment(1) =='bespoke'){?>
		<div class="col-xs-24 col-sm-12 col-md-12 contactCol1">
		<?php
		  		if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b>
					</div>
				<?php } ?>
		<div class="row">
		<div class="contactSectionLeft">
			<h1>YOUR ENQUIRY</h1>
			<p>Please fill in your contact detalis below</p>
				<?php echo form_open('',array('method'=>'post','class'=>'form-horizontal','role'=>'form','id'=>'bespoke'));?>
				<div class="form-group">
					<label class="control-label col-sm-7" for="email">Name:<span class="required">*</span></label>
					<div class="col-sm-17">
					<?php echo form_input(array('name'=>'name', 'class'=>'form-control validate[required]'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="email">Company:<span class="required">*</span></label>
					<div class="col-sm-17">
						<?php echo form_input(array('name'=>'company', 'class'=>'form-control validate[required]'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="email">Telephone:</label>
						<div class="col-sm-17">
							<?php echo form_input(array('name'=>'telephone', 'class'=>'form-control validate[required,custom[number]]'));?>
						</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="email">Email:<span class="required">*</span></label>
					<div class="col-sm-17">
						<?php echo form_input(array('name'=>'email', 'class'=>'form-control validate[required,custom[email]]'));?>
						</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="pwd">Your enquiry:<span class="required">*</span></label>
					<div class="col-sm-17">
					<?php echo form_textarea(array('name'=>'enquiry', 'class'=>'form-control validate[required]'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="pwd">How did you hear about us?:<span class="required">*</span></label>
					<div class="col-sm-17">
							<?php echo form_input(array('name'=>'message', 'class'=>'form-control validate[required]'));?>
					</div>
				</div>
        <div class="form-group">
          <div class="g-recaptcha" data-sitekey="6LciY00UAAAAAGRsxWMRitOERCNDupjyWrqnGPEc"></div>
        </div>
				<div class="form-group">
					<div class="col-sm-offset-8 col-sm-10">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-default send')); ?>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>
		</div>
		<?php } ?>
	  </div>
    </div>
  </div>
</div>
<div class="clear"></div>
 <script>
$(document).ready(function(){

	$("#bespoke").validationEngine(
		'attach', {promptPosition: "bottomLeft"}
	);
	$('.gallery-02').carousel({ visible: 3, itemMinWidth: 300});

})

</script>
