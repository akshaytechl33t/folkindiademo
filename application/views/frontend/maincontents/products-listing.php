<div class="bodyContent">
<div class="container">
    <div class="row">
      <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
		<div class="main-content container-fluid catalog">
    <div class="main-contentContainer">     
        <div id="mainDivBody" class="main-vertical-body row-fluid">
          <div class="ficha_top_navigation clearfix catalog row-fluid">
            <div class="buscador span10">
              <div class="listHeaderNavContainer">
                <div class="content_navigation_menu hidden_mobile pull-left" id="barraOrdenacion"></div>
              </div>
            </div>
          </div>
          <div class="clearfix catalog"> 
            <!--Left sidebar start-->
            <?php echo $product_left_bar;?>
            <!--Left sidebar end-->
            <div class="span10 contents" id="product_search_result">
              <div class="scrollContainer">
                <div class="span12 product__page">							
					<?php					
					if(!empty($product_details)){
					foreach($product_details as $details):	
					?>					
					<a href="<?php echo base_url().'product-details/'.$details->seo_name;?>">
					<div class="span4 products_pictures clear_1">
						<div class="row-fluid contImagenBuscador">
						  <div class="img-product-list">
							<div class="catalog__image">
							  <div class="i1">
								<div class="thumbnail">
								  <div class="caption">
									<div class="captionInner">
									<?php /*?>
										<div class="capButtonArea"> 
										<a href="<?php echo base_url().'product-details/'.$details->seo_name;?>" class="viewAll">View Details</a> 
										<a href="javascript:void(0);" class="addTocart">Add to cart</a>
										</div>
									<?php */?>	
									</div>
								  </div>
								  <?php if(!empty($details->image2)){?>
								  <img class="prodct lazy <?php echo ($details->qty == 0) ?'out_stock_gray':''?>" data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $details->image2;?>" >
								  <?php } else { ?>
								  <img class="prodct lazy <?php echo ($details->qty == 0) ?'out_stock_gray':''?>" data-original="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="out_stock_gray"/>
								  <?php } ?>
								  <?php if($details->qty == 0){?>
									<div class="out_stock"><span class="">Out Of Stock</span></div>
								  <?php } ?>
								</div>
							  </div>
							</div>
						  </div>
						</div>
                    <div class="searchResultContents">					
                      <div class="searchResultData extras"> <span class="hide2Cols"></span>
					  <a href="<?php echo base_url().'product-details/'.$details->seo_name;?>">
                        <h2 class="product_name"><?php echo $details->name?></h2>
                       </a> </div>
					   <?php if($product_details[0]->category_id != 4 && $product_details[0]->category_id != 5){?>
                      <div class="searchResultPrice"> <wbr></wbr>
                        <span style="padding-right: 3px;" class="txtEntero "></span><span style="" class="txtEntero money" data-inr="<?php echo $details->price?>">Rs.<?php echo $details->price?></span>
						</div>				
					   <?php } ?>		
					</div>
					
					</div>
					</a>			
					<?php endforeach; } else { ?>                         
					<div class="span4 products_pictures clear_1 no-product">
						<div class="searchResultContents">
						  <div class="searchResultData extras"> <span class="hide2Cols"></span> 
							<h2 class="product_name">No product found!.</h2>
							 </div>						 
						</div>
					</div>
					<?php } ?>
				</div>
              </div>
            </div>
          </div>
        </div>     
    </div>
  </div>
	</div>
	</div>
</div>
</div>
<script>
$(document).ready(function(){
	var baseUrl = $(".logo").find('a').attr('href');
	$('body').prepend('<div id="black-cover"><div class="loading_text">Please wait...</div></div>');
	$('.feature_value_id').click(function(){
		var featureValueId = $(this).attr('id');
		var categoryId = $(this).attr('rel');
		$('.feature_value_id').removeClass('product_active');
		$('#'+featureValueId).addClass('product_active');
		$('#black-cover').stop().fadeIn(500).css('opacity',0.7);
		$.ajax({
			type:'POST',
			url : baseUrl+'Ajax/product_listing_search',
			data : { feature_value_id : featureValueId, category_id: categoryId },
			success: function (data) {	
				$('#black-cover').stop().fadeOut(500);
				$('#product_search_result').html(data);	
				$("img.lazy").attr('src',baseUrl+'assets/frontend/images/large/loader.gif');
				$("img.lazy").addClass('lazyloader');
				$("img.lazy").lazyload({effect:'fadeIn'});	
                changePrice();
			}		
		});
	})
})
</script>	