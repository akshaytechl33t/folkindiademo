<div class="main-content container-fluid catalog" style="margin-top:80px;">
	<div class="container">
		<div class="innertop"> 
		<!--cart page start-->
			<div class="cartPage"> 
				<div class="check_out_wr payment-success-page">
					<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 ">
						<div id="notifyMessage">
						
						<div class="payment_success_containr">
							<div class="success_inner">
								<div class="pay_top_icon"><i class="fa fa-check"></i></div>
									<div class="pay_top_thanks">Thank You!</div>
									<p class="payment-success">Your order was completed successfully.</p>
									
								</div>
							</div>
						</div>
						
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>
</div>
<script>
$(document).ready(function(){
	$('#cart_details').remove();
})
</script>
