<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<div class="bodyContent">
	<div class="main-content container-fluid catalog">
		<div class="container">
			<div class="innertop">
				<div class="finalAccountPage">
					<?php echo $common_account_left_panel;?>
					<div class="finalaccRight">
						<div id="finalaccTabCnt1" class="finalaccTabContent">
						<div id="notifyMessage">
							<?php if($this->session->flashdata('success_message') !=''){?>
							
								<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('success_message'); ?></b> </div>
							
							<?php } ?>
							<?php if($this->session->flashdata('error_message') !=''){?>
							
								<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('error_message'); ?></b> </div>
							
							<?php } ?>
						</div>
						<h4>Personal Information</h4>						
							<div class="personalInfoForm">
								<?php echo form_open('User/account',array('method'=>'post','id'=>'personal_information'));?>
									<div class="form-group">
										<label for="exampleInputName">Name <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'user_name','class'=>'form-control validate[required]','value'=>$username));?> 
										</span>
									</div>									
									<div class="form-group">
										<label for="exampleInputName">Contact Number <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_input(array('name'=>'contact_number','class'=>'form-control validate[required]','value'=>$contact_number));?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Dob <span class="required">*</span></label>
										<span class="inputBox">										
											<?php echo form_input(array('name'=>'dob','id'=>"date",'value'=>$dob,'class'=>'form-control form_datetime validate[required]','placeholder'=>'YYYY-mm-dd','readonly'=>'readonly')); ?>
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Gender </label>
										<span class="inputBox">
										<?php echo form_dropdown(array('name'=>'gender','class'=>'form-control'),$gender_list,$gender);?>
										</span>
									</div>
									<div class="popFormSubmit">
										<?php echo form_submit(array('name'=>'submit', 'value'=>'Save Changes', 'class'=>'btn btn-default popFormSubmitBtn','id'=>'registration_submit')); ?>
										
									</div>
								<?php echo form_close(); ?> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var date_input=$('input[name="dob"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
})
</script>