<div class="bodyContent">
	<div class="main-content container-fluid catalog">
		<div class="container">
			<div class="innertop">
				<div class="finalAccountPage">					
					<div class="my_order_details">
						<div id="finalaccTabCnt1" class="finalaccTabContent">						
							<h4><a href="<?php echo base_url();?>my-account">My Account</a> / <strong>My Orders</strong></h4>
							<section>							
							<div class="cartStepBody">
								<div class="cartContent"> 
									<div class="cartTable">
										<div class="table-responsive">
										<?php if(!empty($details_arr)){										
										foreach($details_arr as $key=>$details_arrs):									
										?>
											<table class="table">
												<thead>
												<tr>
													<th colspan="4"><div><a href="<?php echo base_url();?>order-details/<?php echo $key;?>"><?php echo $key;?></a></div></th>
												</tr>
												</thead>												
												<tbody>
												<?php foreach($details_arrs as $arr):?>
													<tr>
														<td style="width: 120px !important;">
															<a href="<?php echo base_url();?>product-details/<?php echo $arr->product_seo_name;?>"><img src="<?php echo $arr->product_image ?>" alt="" class="img-responsive" style="width:100px"></a>
														</td>
														<td style="width: 400px !important;">
															<div class="ShpCartCnt">
															<div class="prdcartHeading text-left"> <?php echo $arr->product_name ?> </div>
															<div class="prdDesc text-left"><b>Category :</b><?php echo $arr->category_name ?></div>
															<div class="prdDesc text-left"><b>Color :</b><?php echo $arr->product_color ?></div>
															<div class="prdDesc text-left"><b>Qty :</b><?php echo $arr->product_quantity ?></div>
															</div>
														</td>														
														<td style="float: left;">Rs. <?php echo $arr->product_unit_price ?></td>
														<td>Delivery Free </td>
													</tr>													
													<?php endforeach; ?>
													<tr>
														<td style="float: left; width: 193px;" colspan="4"><small>Date:-</small> <?php echo date('D d Y',strtotime($details_arrs[0]->added_on))?></td>
													</tr>
												</tbody>
												
											</table>
										<?php endforeach; ?>
										<?php } ?>
													
										</div>
									</div>	
								</div>
							</div>
							</section>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
