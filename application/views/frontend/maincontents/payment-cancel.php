<div class="main-content container-fluid catalog" style="margin-top:80px;">
	<div class="container">
		<div class="innertop"> 
		<!--cart page start-->
			<div class="cartPage"> 
				<div class="check_out_wr">
					<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
						<div id="notifyMessage">						
							<div class="payment_error_containr">
								<div class="error_inner">
									<div class="pay_top_icon"><i class="fa fa-exclamation-triangle"></i></div>
									<div class="pay_top_thanks">Thank you visiting us.We hope to see you again with us soon.</div>
								</div>
							</div>						
						</div>
						<div class="payment_container_wr">
							<div class="container">
								<div class="row">
									<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
										<div class="payment_container">
											<h1>Oops! <!--That page can't be found!--></h1>
											<p><strong>Your payment has been canceled</strong> </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

