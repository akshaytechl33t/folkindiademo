<div class="bodyContent static_page static_pageAbt">
	<div class="container">
		<div class="row">
			<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="static_one">
					<h2>Visit Us</h2>
					<div class="col-lg-12 col-md-24 col-sm-24 col-xs-24 ">
						<div class="row">
							<div class="address-details text-center">
								<h3>Registered Office</h3>
								<h4><b>Folk Products And Design Private Limited</b></h4>
								<p>1B & C, Maniktala Industrial Estate,</p>
								<p>Kolkata 700054,</p>
								<p>West Bengal India,</p>
								<p></p>
								<p></p>
								<p class="call_us">Call us:</span> +91 33 2355 4445/6,</p>
								<p class="call_us">+91 98 7475 4445</p>
								<span>Email us: info@folkindia.in</span>
								<div class="uk_office">
									<h3>UK Office</h3>
									<h4><b>Folk Products And Designs Limited</b></h4>
									<p>20-22 Wenlock Road,</p>
									<p>London N1 7GU</p>
									<p></p>
									<p></p>
									<!-- <p class="call_us">Call us:</span> +44 845 272 5920</p> -->
									<span>Email us: info@folkindia.in</span>
									<!-- <p>www.folkindia.in</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-24 col-sm-24 col-xs-24 ">
						<div class="row">
							<?php
							if($success_message){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-check"></i>
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
									<b><?php echo $success_message; ?></b>
								</div>
							<?php }
							if($error_message){?>
								<div class="alert alert-danger alert-dismissable">
									<i class="fa fa-ban"></i>
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
									<b><?php echo $error_message; ?></b>
								</div>
							<?php } ?>
							<div class="contactSectionLeft">
								<h1>YOUR ENQUIRY</h1>
								<p>Please fill in your contact detalis below</p>
									<?php echo form_open('',array('method'=>'post','class'=>'form-horizontal','role'=>'form','id'=>'contact_us'));?>
									<div class="form-group">
										<label class="control-label col-sm-7" for="email">Name:<span class="required">*</span></label>
										<div class="col-sm-17">
										<?php echo form_input(array('name'=>'name', 'class'=>'form-control validate[required]'));?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-7" for="email">Company:<span class="required">*</span></label>
										<div class="col-sm-17">
											<?php echo form_input(array('name'=>'company', 'class'=>'form-control validate[required]'));?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-7" for="email">Telephone:</label>
											<div class="col-sm-17">
												<?php echo form_input(array('name'=>'telephone', 'class'=>'form-control'));?>
											</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-7" for="email">Email:<span class="required">*</span></label>
										<div class="col-sm-17">
											<?php echo form_input(array('name'=>'email', 'class'=>'form-control validate[required,custom[email]]'));?>
											</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-7" for="pwd">Your enquiry:<span class="required">*</span></label>
										<div class="col-sm-17">
										<?php echo form_textarea(array('name'=>'enquiry', 'class'=>'form-control validate[required]'));?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-7" for="pwd">How did you hear about us?:<span class="required">*</span></label>
										<div class="col-sm-17">
												<?php echo form_input(array('name'=>'message', 'class'=>'form-control validate[required]'));?>
										</div>
									</div>
									<div class="form-group">
										<div class="g-recaptcha" data-sitekey="6LciY00UAAAAAGRsxWMRitOERCNDupjyWrqnGPEc"></div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-8 col-sm-10">
											<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-default send')); ?>
										</div>
									</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="static_one">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-24">

					<a href="https://www.google.com/maps/contrib/110649472818546151886/photos/@22.589098,88.391095,17z/data=!3m1!4b1!4m3!8m2!3m1!1e1" target="_blank"><img src="<?php echo base_url();?>assets/frontend/images/contact-us-img.png"></a>
					</div>
					<div class="col-lg-15 col-md-15 col-sm-15 col-xs-24">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3683.73258288581!2d88.38890631427401!3d22.58910293805895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a02767100000001%3A0x7f2b6589818e0a0!2sFolk!5e0!3m2!1sen!2sin!4v1475699403118" width="100%" height="700" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
