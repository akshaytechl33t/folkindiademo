<div class="bodyContent">
	<div class="container">
		<div class="row">
			<div class="main-content container-fluid catalog">
				<div class="container">
					<div class="innertop"> 
					<!--cart page start-->
						<div class="cartPage"> 						
							<section >
								<div class="cartStepHead">
									<h3>Order Summary </h3>
								</div>
								<div class="cartStepBody">
									<div class="cartContent"> 

									<!--cart table start-->
										<div class="cartTable">
											<div class="table-responsive">
												<table class="table">
													<thead>
														<tr>
															<th>Image</th>
															<th class="itemTbl">Item</th>
															<th>Qty</th>
															<th>Price</th>
															<th>Delivery Details</th>
															<th>Subtotal</th>
															<th>Remove</th>
														</tr>
													</thead>
													<tbody>
													<?php if($cartrows ){
														foreach($cartrows as $key=>$rows):	
													?>													
													<tr id="<?php echo $rows['rowid']?>">
														<td><img src="<?php echo $rows['product_image']?>" alt="" class="img-responsive" style="width:100px"></td>
														<td><div class="ShpCartCnt">
														<div class="prdcartHeading"> <?php echo $rows['name']?> </div>
													
														<div class="prdDesc"><b>Category :</b><?php echo $rows['category_name']?></div>
														<div class="prdDesc"><b>Color :</b><?php echo $rows['product_color']?></div>
														</div></td>
														<td> 
															<span class="product-minus">
																<a href="javascript:void(0);" id="<?php echo $rows['rowid']?>" title="Minus">
																	<i class="fa fa-minus" aria-hidden="true"></i>
																</a>
															</span>  
															<input type="text" class="qtyBox" readonly="readonly" value="<?php echo $rows['quantity']?>"> 
															<span class="product-plus">
																<a href="javascript:void(0);" id="<?php echo $rows['rowid']?>" title="Plus" rel="<?php echo $rows['productid']?>">
																	<i class="fa fa-plus" aria-hidden="true"></i>
																</a>
															</span>
														<div id="product_msg_<?php echo $rows['rowid']?>" class="quantity-zero-msg"></div>
														</td>
														<td class="money" data-inr="<?php echo $rows['price']?>">Rs. <?php echo $rows['price']?></td>
														<td>Free </td>
														<!--<div class="deliveryDat">Delivered in 5-6 business days.</div>-->
														<td  class="money" data-inr="<?php echo $rows['subtotal']?>">Rs. <?php echo $rows['subtotal']?></td>
														<td><a href="javascript:void(0);" id="<?php echo $rows['rowid']?>" class="delItem"><img src="<?php echo base_url();?>assets/frontend/images/large/crossIcon.png" alt=""></a></td>
													</tr>
													 <?php  endforeach ; } else {?> 
													 <tr><td colspan="7">Cart is empty.</td></tr>
													 <?php } ?>	
													</tbody>
												</table>
											</div>
										</div>

									<!--cart table end-->
									<?php if($cartrows ){?>	
									<?php if(empty($this->session->userdata('order_coupon_details'))){ ?>
									<div class="orderConfirmationAlert"> <span class="orderConfirmText">Enter your promocode</span>
										<input type="text" class="alertMobNumber" value=""> 
										<span class="promocode">
											<a href="javascript:void(0);" id="applyPromoCode">Apply</a></span>
										<div id="promo_loader"></div>
										<div id="promocode_err"></div>										
									</div>
									<?php } else { ?>
										<div class="orderConfirmationAlert"> <span class="orderConfirmText" style="color:red">You have used this promocode <strong><?php echo $this->session->userdata['order_coupon_details']->code;?></strong></span>
									<?php } } ?>
									<div class="continuePayAmount"> <a href="<?php echo base_url()?>" class="shpConti">< Continue Shopping</a>
									<?php if($cartrows ){?>		
									<?php if(!empty($this->session->userdata['is_user_signed_in']['user_signed_in'])) {?>									
										<a href="<?php echo base_url()?>checkout" class="place-order">Procceed to checkout > </a>
									<?php } else { ?>										
										<a data-target="#loginpopup" data-toggle="modal" title="Sign in" href="javascript:void(0)" class="place-order">Procceed to checkout > </a> 
									<?php } ?>											 	
									<div class="totalAmountPurChs">Amount Payable: 
									<div id="totalPaybelAmount" style="display:none"><?php echo $this->cart->total();?></div>                                    <?php  																		if(!empty($this->session->userdata('order_coupon_details'))){ ?>										<?php $discount_amount =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;										$coupon_discount_amountINR = number_format($this->cart->total() - $discount_amount,2);                                        //echo trim($coupon_discount_amount);										?>									<?php } else { ?>										<?php $coupon_discount_amountINR = trim(number_format($this->cart->total(),2));?>									<?php } ?>
									<strong id="totalAmount" class="money" data-inr="<?php echo trim($coupon_discount_amountINR)?>">RS. 									
									<?php  									
									if(!empty($this->session->userdata('order_coupon_details'))){ ?>
										<?php $discount_amount =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;
										echo $coupon_discount_amount = number_format($this->cart->total() - $discount_amount,2);
										?>
									<?php } else { ?>
										<?php echo number_format($this->cart->total(),2);?>
									<?php } ?>
									</strong></div>
									<div class="total_savings">
										<?php  if(!empty($this->session->userdata('order_coupon_details'))){?>
											Total savings: RS. <?php echo number_format(($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total(),2) ;?>
										<?php } ?>
									</div>
									<?php } ?>
									</div>
									</div>
								</div>
							</section>
						</div>
					<!--editor page end--> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('body').prepend('<div id="black-cover"><div class="loading_text">Please wait...</div></div>');
	$('body').on('click','.delItem',function(){		
		var cartId = $(this).attr('id');		
		var cnf=confirm('Are You Sure . You want to remove this product?');
		if(cnf==true)
		{
			$('#black-cover').stop().fadeIn(500).css('opacity',0.7);
			$.ajax({
				type:'POST',
				url : '<?php echo base_url();?>Cart/remove',
				data : { row_id:cartId},
				dataType : 'json',
				success:function(data){
					setTimeout(function(){
						$('#black-cover').stop().fadeOut(500);
						if(data){
							$('#'+data).slideUp('slow');
							location.reload();
						}
					},1000)			
				}			
			})
		}
		else
		{
			return false;
		}			
	})
	$('body').on('click','.product-minus',function(){				
		var rowId = $(this).children().attr('id');
		var productId = $(this).children().attr('rel');
		var quantityCount = $(this).siblings('.qtyBox').val();
		if(quantityCount > 1){
			$('#black-cover').stop().fadeIn(500).css('opacity',0.7);
			var Quantity = parseInt( quantityCount )-1; 	
			$.post('<?php echo base_url();?>Cart/update',{row_id:rowId,quantity:Quantity,product_id:productId,flag:'minus'},function(data){
				setTimeout(function(){
					$('#black-cover').stop().fadeOut(500);
					if(data){
						location.reload();
					}
				},1000)	
			},'json') 
		}
		else{
			$('#product_msg_'+rowId).html('<span>Quantity cannot be zero</span>');
		}	
	})	
	$('body').on('click','.product-plus',function(){		
		$('#black-cover').stop().fadeIn(500).css('opacity',0.7);
		var rowId = $(this).children().attr('id');
		var productId = $(this).children().attr('rel');
		var quantityCount = $(this).siblings('.qtyBox').val();		
		var Quantity = parseInt( quantityCount )+1; 
		$.post('<?php echo base_url();?>Cart/update',{row_id:rowId,quantity:Quantity,product_id:productId,flag:'plus'},function(data){	
			setTimeout(function(){
				$('#black-cover').stop().fadeOut(500);				
				if(data.message == 'b2b_limit_over'){
					var text = '<div class="fade modal"id=b2b_msg role=dialog><div class=modal-dialog><div class=modal-content><div class=modal-header><button class=close data-dismiss=modal type=button>×</button><h4 class=modal-title>Thank you for your interest</h4></div><div class=modal-body><p>Above 5 units.You are a bulk buyer for us, Please send us a mail at sales@folkindia.in mentioning your requirement.</div></div></div></div>';
					$('body').append(text);
					$('#b2b_msg').modal('show');
				}
				else
				{
					location.reload();
				}
			},1000)	
		},'json') 			
	})
	$('#applyPromoCode').click(function(){
		$('#promo_loader').css('margin-left','21%');
		$('#promo_loader').css('margin-top','10px;');
   		$('#promo_loader').html('<i class="fa fa-clock-o fA-spin" aria-hidden="true"></i>');
		var code = $('.alertMobNumber').val();
		var text = '';
		$.post('<?php echo base_url();?>Ajax/apply_promocode',{code:code},function(data){
			$('#promo_loader').css('display','none');
			if(data.flag == 'success'){
				$('.total_savings').css('display','block');				
				var discountPrice = $('#totalPaybelAmount').html() * data.message/100;
				$('.total_savings').html('Total savings: RS.'+parseFloat(discountPrice).toFixed(2));
				var totalAmt = (parseFloat($('#totalPaybelAmount').html()) - parseFloat(discountPrice)).toFixed(2);
				$('#totalAmount').html('Rs. '+totalAmt);
			}
			else
			{
				$('.total_savings').css('display','none');
				text +='<div class="alert alert-danger">Please enter a valid Promocode or this offer is not available, please try again</div>';
			}
			$('#promocode_err').html(text);
		},'json')
	})
})
</script>