<div class="bodyContent static_page static_pageAbt">
  <div class="container">
    <div class="row">
      <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
	  <?php if(!empty($rows)){?>
		<div class="static_one">			
			<h2>Event</h2>
			<div class="static_page">
			<div class="static_page_container">
				<div class="page_categories">
					<div class="page-details">						
						<?php foreach($rows as $details):?>
						<div class="col-md-12 col-sm-12 col-xs-24  letsMakeBanner">
						 <a href="<?php echo base_url();?>event-details/<?php echo $details->seo_url;?>">  
						 <div class="letsMakeBannerInner">
							<img data-original="<?php echo base_url();?>uploads/images/event/<?php echo $details->image?>" alt="" class="lazy">
						 </div>
						 <h3><?php echo $details->title;?></h3>
						 
						 <?php //echo $details->short_description;?>
						 </a>
						 </div> 
						<?php endforeach;?>						 
					</div>	
				</div>	
			</div>	
		</div>
	  <?php } else { echo 'Comming soon...';}?>		
	  </div>
    </div>
  </div>
</div> 
</div>
<div class="clear"></div>
