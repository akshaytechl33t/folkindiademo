<div class="bodyContent">
	<div class="main-content container-fluid catalog">
		<div class="container">
			<div class="innertop">
				<div class="finalAccountPage my_order_details_container">
				<div class="my_order_details">
					<?php if($this->uri->segment(1) == 'my-account'){?>
					<h4>My Account</h4>
					<?php } ?>
					<?php if($this->uri->segment(1) == 'order-details'){?>
					<h4><a href="<?php echo base_url();?>my-account">My Account</a> / <a href="<?php echo base_url();?>orders">My Orders</a> / <strong><?php echo $transaction_id?></strong></h4>
					<?php } ?>
				</div>
					<div class="my_order_details_inner_container">
					<div class="finalaccLeft">
							<div class="finalaccLeftBottom">
								<div class="finalaccTab">
								<h5>Order Details</h5>
									<ul>
										<li class=""><label>Order ID:</label> <?php echo $get_order_details->txnid;?> </a></li>
										<li class=""><label>Order Date:</label> <?php echo $get_order_details->created;?></li>
										<li class=""><label>Amount Paid:</label> Rs. <?php echo $get_order_details->amount;?>  </li>				
									</ul>
								</div>
							</div>
					</div>
					<div class="finalaccRight">
							<div id="finalaccTabCnt1" class="finalaccTabContent">
							
								<p><strong><?php echo $get_order_details->shipping_name;?></strong> <?php echo $get_order_details->shipping_phone?></p>
								<p><small><?php echo $get_order_details->shipping_street_address;?>, <?php echo $get_order_details->shipping_land_mark;?> , <?php echo $get_order_details->shipping_country;?> , <?php echo $get_order_details->shipping_state;?> , <?php echo $get_order_details->shipping_city;?> , <?php echo $get_order_details->pincode;?></small></p>
									
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
