<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/bootstrap-select.css">
<script src="<?php echo base_url();?>assets/frontend/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/rating.js"></script> 
<div class="bodyContent">
	<div class="container">	
		<div class="row">
			<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="main-content container-fluid catalog">
				<?php if(!empty($row)){?>
				<div id="productId" style="display:none"><?php echo $row->id;?></div>
				<div id="productPrice" style="display:none"><?php echo $row->price;?></div>
				<div id="productName" style="display:none"><?php echo $row->name;?></div>
				<div id="productSeoName" style="display:none"><?php echo $row->seo_name;?></div>
				<div id="categoryName" style="display:none"><?php echo $row->category_name;?></div>			
						
					<div class="main-contentContainer">
						<div id="mainDivBody" class="main-vertical-body row-fluid">
							<div class="detailsPage">
								<div class="detailsPageTop">
									<div class="ficha_top_navigation clearfix catalog detailsPageBredCum">
										<div class="listHeaderNavContainer">              
											<div class="content_navigation_menu hidden_mobile pull-left" id="barraOrdenacion">
												<div class="col-md-3 col-sm-4 col-lg-3 col-xs-24 hidden_mobile"><a href="<?php echo base_url();?>">Home</a></div>
												<div class="col-md-3 col-sm-6 col-lg-6 col-xs-24  hidden_mobile"><span class="separator">>></span><a href="<?php echo base_url();?>product/<?php echo $row->category_seo_name;?>"><?php echo $row->category_name;?></a></div>
												<div class="col-md-3 col-sm-12 col-lg-15 col-xs-24 hidden_mobile"><span class="separator active">>><?php echo $row->name;?></span></div>
											</div>              
										</div>         
									</div>      
								</div>
								<div class="detailsContent" >
									<div class="prdgallery span7" id="att_image">
										<div class="row">
											<div class="col-xs-6">
												<div class="product-thumb">
													<div>
													<img data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" class="img-responsive margin-top-7 margin-bottom-7 lazy" alt="">
													</div>
													<?php if(!empty($row->image5)){?>
													<div>
													 <img data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image5;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image5;?>" class="img-responsive margin-top-7 margin-bottom-7 lazy" alt="">
													 </div>
													<?php } ?>
													<?php if(!empty($row->image6)){?>
													<div>
													<img data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image6;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image6;?>" class="img-responsive margin-top-7 margin-bottom-7 lazy" alt="">
													</div>
													<?php } ?>
													<?php if(!empty($row->image7)){?>
													<div>
													<img data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image7;?>" data-src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image7;?>" class="img-responsive margin-top-7 margin-bottom-7 lazy" alt="">
													</div>
													<?php } ?>
													
												</div>
											</div> 
											<!--<div class="col-xs-2"></div>-->
												<div class="col-xs-18 ">
													<div class="medium-wrapper pd-listing-border">
														<div class="big-preview-wrapper">
															<?php  if(!empty($row->image1)){?>
																<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $row->image1;?>" class="product-image img-responsive" alt="">
															<?php } else { ?>
																<img data-original="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview lazy" alt="Preview" />
															<?php } ?>
														</div>
													</div>
													
												</div>
											<div class="col-xs-4"></div>
										</div>
									</div>									 
									<div class="datos_ficha_producto span5 pull-right">
									<div id="notifyMessage">
										<?php if($this->session->flashdata('success_message') !=''){?>
											<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
											<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
											<b><?php echo $this->session->flashdata('success_message'); ?></b> </div>
										<?php } ?>
										<?php if($this->session->flashdata('error_message') !=''){?>
											<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
											<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
											<b><?php echo $this->session->flashdata('error_message'); ?></b> </div>
										<?php } ?>
									</div>
									<div class="prdDts">
									<div class="cabecera_producto_height_fix">
										<div class="cabecera_producto row-fluid">
											<div class="info_cabecera_producto">
												<div class="nombreProducto row-fluid hidden_mobile">
												<h1><?php echo $row->name;?></h1>
												</div>
												
												<div class="rating_listing_container">
													<div class="rating_listing">
														<span id="" class="star_font">														
														<?php 
														if($ratings_details){		
															$total_rating = 0;	
															$total_user = 0;																	
															foreach($ratings_details as $ratings):	
																$total_rating +=$ratings->count_rating;
																$total_user += count($ratings->user_id);
															endforeach;
															$rating = $total_rating/$total_user;
															$rating_data=round($rating,2);
															$float_chk=is_float($rating_data);
															$decimal_data=0;
															$floor_data=0;
															$indicator=0;
															$floor_data=floor($rating_data);
															$decimal_data=$rating_data-$floor_data;					
															if($decimal_data>=0.2 && $decimal_data <=0.9)
															{
																$indicator=$floor_data+1;
															}
															for($i=1;$i<=5; $i ++)
															{	
																if($i<=$rating_data)
																{
																	$rate_imagepath= '<i class="fa fa-star"></i>';
																}
																else if(!empty($indicator) && ($indicator==$i))
																{
																	$rate_imagepath= '<i class="fa fa-star-half-o"></i>';
																}
																else
																{
																	$rate_imagepath= '<i class="fa fa-star-o"></i>';
																}
																	echo $rate_imagepath;
															} 
														} ?>
														</span>
													</div>
													<div class="review_listing">
													<a href="#review_list">
													<span><?php echo sizeof($ratings_details);?> </span>Reviews
													</a>
													</div>
													<div class="write_review_listing">
													<a href="#write_review">Write a REVIEW</a>
													</div>
													
												</div>               
											<div class="referenciaProducto row-fluid">Ref. <?php echo $row->bag_no;?></div>
											</div>
											<div class="precio_cabecera_producto">
												<div class="precioProducto row-fluid">
												<?php if($row->category_id !=4 && $row->category_id !=5){?>
													<span >
														<span class="padding-right3px;" class="erased ficha_precio_venta_entero false"></span>
														<span style="" class="erased ficha_precio_venta_entero false money" data-inr="<?php echo $row->price;?>">
															Rs. <?php echo $row->price;?>
														</span>
														
													</span>	
												<?php } ?>													
												</div>
											</div>
										</div>
									</div>

									<div class="producto_color row-fluid">
										<div class="producto_color_texto span12">
											<p class="producto_title_color">Colour:</p>
											<!--<p class="producto_actual_color" ><?php //echo $row->value;?></p>-->
											<ul class="product_color">
												<li><a href="javascript:void(0);" id="<?php echo $row->color_id;?>"><i class="fa fa-hand-o-right"></i><?php echo $row->value;?></a></li>
												<div id="productColor" style="display:none"><?php echo $row->value;?></div>
												<?php /* ?><?php 						
												if(!empty($row->value)){
												$explode_color = explode(',',$row->value);
												$explode_color_id = explode(',',$row->color_id);
												$attr_details = array_combine($explode_color_id,$explode_color);
												foreach($attr_details as $key=>$explode_colors):
												?>
													<li><a href="javascript:void(0);" id="<?php echo $key;?>" class="get_attr_image"><i class="fa fa-hand-o-right"></i><?php echo $explode_colors;?></a></li>	
												<?php endforeach; } ?><?php */ ?>
											</ul>
										</div>
										<!--<div class="chooseColor">
											<div class="color_div_on">
												<div class="round-border">
													<img src="images/large/color0.png" alt=""> 
												</div>
											</div>
										</div> -->
									</div>
									<?php if($row->qty == 0 ){?>
									<div class="pd_out_stock_wrapper row-fluid no_loged">
										<div class="pd_out_stock_inner_wrapper">
											<div class="_3FV-Hc">Sold Out</div>
											<div class="_20qTiu">This item is currently out of stock</div>
										</div>
									</div>
									
									<?php } else {?>
									<?php if($row->category_id !=4 && $row->category_id !=5){?>
										<div class="shop_wishlist row-fluid no_loged">
											<a href="javascript:void(0);" class="srchStore" id="addtocart">Add to cart</a>
										</div>
									<?php } ?>
									<?php } ?>
									
									
									<div id="prdct_loader"></div>
									<div class="social-Connect row-fluid ">
										<ul>
											<li><a href="<?php echo SITE_FACEBOOK_LINK?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="<?php echo SITE_Linkedin_LINK?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>

									<div class="tabPanels row-fluid">
										<div class="accordion" id="accordion2">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle collapsed" aria-expanded="true" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
														Description 		
													</a>
												</div>
												<div class="accordion-body collapse in" id="collapseOne" aria-expanded="true">
													<div class="accordion-inner">
														<?php echo $row->description;?>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
														Material and washing instructions
													</a>
												</div> 
												<div id="collapseTwo" class="accordion-body collapse">
													<div class="accordion-inner">
														<?php echo $row->instruction;?>
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
									</div>
									</div>
									<?php if(!empty($similar_product_arr)){?>
									<div id="panelOufitsProducto">
										<h3>Similar Product</h3>
										<ul>
										<?php
										foreach($similar_product_arr as $similar_product_details):
										?>										
											<li> 
												<div class="prdImage">
													<a href="<?php echo base_url();?>product-details/<?php echo $similar_product_details->seo_name?>">
														<?php if(!empty($similar_product_details->image3)){?>
														<img data-original="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $similar_product_details->image3;?>" alt="" class="lazy">
														<?php }else { ?>
														<img data-original="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview lazy" alt="Preview" />
														<?php } ?>
													</a>
												</div>
												<div class="prdDesc">
													<h2>  <?php echo $similar_product_details->name;?> </h2>
													<?php if($similar_product_details->category_id !=4 && $similar_product_details->category_id !=5){?>
														<div class="prdPrc money" data-inr="<?php echo $similar_product_details->price;?>">
															Rs. <?php echo $similar_product_details->price;?>
														</div>
													<?php } ?>	
												</div> 
											</li>										
										<?php endforeach;?>
										</ul>
									</div><!--panelOufitsProducto-->     
									<?php  } ?>
									<div class="row">
										<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24" id="write_review">
											<div class="write_review_container_one">
												<div class="reviews">
												<h2>Reviews</h2>
													<div class="reviews_top">
														<p>Be the first to write a review about this product.</p>
														<?php if($this->session->userdata['is_user_signed_in']['user_signed_in']){?>
														<a role="button" data-toggle="collapse" href="#RatingForm" aria-expanded="false" aria-controls="collapseExample" class="rvw-cont">Rate/Review</a> 
														<?php } else {?>
														<a data-target="#loginpopup" data-toggle="modal" title="Sign in" href="javascript:void(0)" class="rvw-cont">Rate/Review</a> 
														<?php } ?>
													</div>
												</div>
											</div>
											<div id="RatingForm" class="collapse write_review_container_two">
												<div class="row">
													<div class="col-lg-14 col-md-16 col-sm-16 col-xs-24">
													 <?php echo form_open('Products/rating',array('method'=>'post','id'=>'rating_form'));?>
														<div class="row">
															<div class="col-md-24">
																<div class="form-group">
																	<label>Enter Your Rating</label>
																	<div class="stars starrr" data-rating="0"></div>
																	<input type="hidden" value="" class="validate[required]" name="count_rating" id="count_rating">
																	<?php echo form_hidden('product_id',$row->id);
																	echo form_hidden('product_seo_url',$row->seo_name);
																	?>
																</div>
															</div>
															<div class="col-md-24">
																<div class="form-group">
																	<label>Enter Your Subject<span class="required">*</span></label>
																	<?php echo form_input(array('name'=>'subject', 'placeholder'=>"Enter Your Subject",'class'=>'form-control validate[required]'));
																	echo form_error('subject');
																	?> 
																</div>
															</div>
															<div class="col-md-24">
																<div class="form-group">
																	<label>Enter Your Comment<span class="required">*</span></label>
																	<?php echo form_textarea(array('name'=>'comment', 'placeholder'=>"Enter Your Comment",'class'=>'form-control validate[required]'));
																	echo form_error('comment');
																	?>					
																</div>
															</div>
															<div class="col-md-24">
																<div class="form-group">
																	<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'reviews_submit')); ?>
																</div>
															</div>
															<div id="reg_loader"></div> 	
														</div>
														<?php echo form_close(); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="review_wrapper" id="review_list">										 
										<h2><?php echo $row->name;?></h2>
										<div class="contents">
											<div class="row relative">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 absolute_top_two col-lg-push-16 col-md-push-16 col-sm-push-16">
													<?php 
														$total_rating = 0;	
														$total_user = 0; 
														if(!empty($ratings_details)){									
														foreach($ratings_details as $ratings):	
														$total_rating +=$ratings->count_rating;
														$total_user += count($ratings->user_id);
														endforeach;}
													?>
													<div class="review_right">
														<?php if(($total_rating && $total_user)>0) {?>
														<?php $rating= $total_rating/$total_user;?>
														<div class="big_star"><?php echo $rating;?></div>
														<?php } else { ?>
														<div class="big_star">0</div>
														<?php }?>
													</div>
												</div>
												<div class="col-lg-16 col-md-16 col-sm-16 col-xs-24 col-lg-pull-4 col-md-pull-4 col-sm-pull-4">
													<div class="review_left">
													<?php if($ratings_details){														
													foreach($ratings_details as $ratings):	
													?>
														<div class="row">
															<div class="col-lg-16 col-md-16 col-sm-16 col-xs-24">
																<div class="review_inner_left">
																	<h3><?php echo $ratings->subject;?></h3>
																	<p><?php echo $ratings->comment;?></p>
																</div>
															</div>
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-24">
																<div class="review_inner_right">
																	<div class="rating_listing">
																		<div class="star_font">
																		<?php 
																		for($i=1; $i <= $ratings->count_rating;$i++){ ?>
																		<i class="fa fa-star"></i>
																		<?php } ?>
																		</div>
																	</div>
																</div>
																<div class="rating_user_wrapper">
																	<h4><?php echo $ratings->user_name?></h4>
																	<h5><?php echo date('Y-m-d',strtotime($ratings->created));?></h5>
																</div>
															</div>
														</div>
													<?php endforeach; } else {?>														
													<div class="row">
														<div class="col-lg-16 col-md-16 col-sm-16 col-xs-24">
															<div class="review_inner_left">
																<h3>There are no reviews.</h3>
															</div>
														</div>
													</div>
													<?php } ?>
													</div>
												</div>
												
											</div>
										</div>  
									</div><!--review_wrapper-->
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal25"  class="modal fade " role="dialog">
  <div class="modal-dialog">     
    <!-- Modal content-->
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url();?>assets/frontend/images/closeButton.png" alt=""></button>
      <div class="modal-header">
        <h4 class="modal-title"><img src="<?php echo base_url();?>assets/frontend/images/large/logo.png"/></h4>
      </div>
      <div class="modal-body">
	  <?php echo form_open('',array('method'=>'post'));?>
        <div class="form-group">
			<label>Name <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'name', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('name'); ?>								
		</div>
		<div class="form-group">
			<label>Company <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'company', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('company'); ?>								
		</div>
		<div class="form-group">
			<label>Address <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'address', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('address'); ?>								
		</div>
		<div class="form-group">
			<label>Number <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'number', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('number'); ?>								
		</div>
		<div class="form-group">
			<label>Email <span class="required">*</span></label>
			<?php echo form_input(array('name'=>'email', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('email'); ?>								
		</div>
		<div class="form-group">
			<label>Enquiry <span class="required">*</span></label>
			<?php echo form_textarea(array('name'=>'enquiry', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('enquiry'); ?>								
		</div>
		<div class="form-group">
			<label>How did you hear about us?</label>
			<?php echo form_input(array('name'=>'about_us', 'value'=>'', 'class'=>'form-control validate[required]'));?>
			<?php echo form_error('about_us'); ?>								
		</div>
		<div class="form-group"> 								
			<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-primary')); ?> 
		</div>
		<?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/frontend/js/okzoom-1.0.min.js"></script>
<script>
$(document).ready(function(e) {
	$('.big-preview-wrapper img').attr('src', $('.product-thumb img').eq(0).attr('data-src'));

	$('body').on('mouseover','.product-thumb img',function(e){		
		$('.big-preview-wrapper img').attr('src', $(this).attr('data-src'))
	});	
	$('.product-image').okzoom({
	  width: 250,
	  height: 250,
	  round: true,
	  background: "#fff",
	  backgroundRepeat: "repeat",
	  shadow: "0 0 5px #000",
	  border: "1px solid #fff"
	});
	$('body').prepend('<div id="black-cover"><div class="loading_text">Please wait...</div></div>');
	$('.get_attr_image').click(function(){
		$('#black-cover').stop().fadeIn(500).css('opacity',0.7);
		var colorId = $(this).attr('id');
		var productId = <?php echo @$row->id ?>;
		 $.ajax({
				type:'POST',
				url : '<?php echo base_url();?>Ajax/get_attr_image',
				dataType: "json",
				data : { color_id : colorId, product_id: productId },
				success: function (data) {	
				$('#black-cover').stop().fadeOut(500);
				var text = '';
				text+='<div class="row">';
				text+='<div class="col-xs-4">';
				text+='<div class="product-thumb">';
				text+='<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/'+data.image1+'" class="img-responsive margin-top-7 margin-bottom-7" alt="">';
				if(data.image5 !=null){
					text+='<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/'+data.image5+'" class="img-responsive margin-top-7 margin-bottom-7" alt="">';
				}
				if(data.image6 !=null){
					text+='<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/'+data.image6+'" class="img-responsive margin-top-7 margin-bottom-7" alt="">';
				}
				if(data.image7 !=null){
					text+='<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/'+data.image7+'" class="img-responsive margin-top-7 margin-bottom-7" alt="">';
				}
				text+='</div>';
				text+='</div>';
				text+='<div class="col-xs-2"></div>';
				text+='<div class="col-xs-18 ">';
				text+='<div class="medium-wrapper">';
				text+='<div class="big-preview-wrapper">';
				if(data.image1 !=''){
					text+='<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/'+data.image1+'" class="img-responsive" alt="">';
				}else{
					text+='<img src="<?php echo base_url();?>assets/frontend/images/no-image.jpg" class="zoom-preview" alt="Preview" />';
				}
				text+='</div>';
				text+='</div>';
				text+='</div>';
				text+='<div class="col-xs-4"></div>';
				text+='</div>';
				$('#att_image').html(text);						
			}				
		});			
	})	
	
	$('#addtocart').click(function(){
		$('#prdct_loader').css('margin-top','10px');
		$('#prdct_loader').html('<i class="fa fa-clock-o fA-spin" aria-hidden="true"></i>');
		var productId = $('#productId').text();
		var productPrice = $('#productPrice').text();
		var productColor = $('#productColor').text();
		var productName = $('#productName').text();
		var productSeoName = $('#productSeoName').text();
		var categoryName = $('#categoryName').text();
		var productImg = $('.big-preview-wrapper').children("img").attr("src");
		var numberItem = 0;	
		$.post('<?php echo base_url();?>Cart/add',{product_id:productId,product_price:productPrice,product_color:productColor,product_name:productName,category_name:categoryName,product_image:productImg,product_seo_name:productSeoName},function(data){
			$('#prdct_loader').css('display','none');
			$('#addtocart').html('Added');
			$('#addtocart').css('pointer-events','none');
			$.each(data, function (i, j) {
					numberItem++;
			})		
			$('#cart_details').html('<em>'+numberItem+'</em>');
			$('#cart_notification').html('<ul aria-labelledby=dropdownMenu1 class=dropdown-menu style=display:block><div class=cart_on_hover><div class=des_aro><i class="fa fa-sort-asc"></i></div> Your have added this '+productName+' with '+ productColor +'</div></ul>');
			$('.empty_notfy').remove();			
			setTimeout(function(){
				$('#cart_notification').children().slideUp(5000);
			},1000);
			
		},'json');
	});	
});
</script>