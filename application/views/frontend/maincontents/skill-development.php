<link href="<?php echo base_url();?>assets/skilldemo/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/skilldemo/css/style.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>assets/skilldemo/js/jquery.min.js"> </script>
<script src="<?php echo base_url();?>assets/skilldemo/js/script.js"> </script>

<!-- <link href="assets/skilldemo/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="assets/skilldemo/css/style.css" rel="stylesheet" type="text/css"> -->
<!-- <nav class="mobile-nav">
    <div class="bars-img pull-left">
        <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/grey-bars.svg">
    </div>
    <div class="mobil-nav-img pull-left">
        <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/folk-logo.png">
    </div>
    <div class="mobile-login pull-right">
        <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/user.svg">
        <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/ecom.svg">
    </div>
    <div class="mobile-side-menu">
        <div class="mob-nav-close text-right">
            <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/close.svg">
        </div>
        <ul>
            <li> 
                <a href=""> SKILL DEVELOPMENT </a>
            </li>
            <li> 
                <a href=""> CLIENT </a>
            </li>
            <li> 
                <a href=""> BE SPOKE </a>
            </li>
            <li class="mob-sub-menu">
                <a href=""> SELECT A COUNTRY </a>
                <span class="pull-right select-sub-menu">  
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/arrow-down-angle.svg">
                </span>
                <ul >
                    <li> 
                        <a href="<?php echo base_url(); ?>"> INDIA </a>
                    </li>
                    <li> 
                        <a href="<?php echo base_url(); ?>"> CANADA </a>
                    </li>
                    <li> 
                        <a href="<?php echo base_url(); ?>"> USA </a>
                    </li>
                </ul>
            </li>
            <li> 
                <a href="<?php echo base_url(); ?>"> LIFE STYLE </a>
            </li>
            <li> 
                <a href="<?php echo base_url(); ?>"> GIFTING </a>
            </li>
        </ul>
    </div>
</nav> -->
<section class="tag-line">
    It's 100% placement based
</section>
<section class="white-section section-style conatiner-fluid main-page-content no-xs-padding-l no-xs-padding-r">
    <!-- <div class="mob-page-title">
        SKILL DEVELOPMENT
    </div> -->
    <div class="container no-xs-padding-l no-xs-padding-r">
        <div class="col-sm-6 col-xs-12 no-xs-padding-l no-xs-padding-r">
            <div class="clearfix clear">
                <div class="col-xs-9 height-250 no-padding-right bottom-gap height-xs-200 no-xs-padding-l height-sm-160 no-padding-left">
                    <img class="img-full-height" src="<?php echo base_url(); ?>assets/skilldemo/imgs/people-craft.png">
                </div>
                <div class="col-xs-3 height-250 bottom-gap height-xs-200 no-xs-padding-r height-sm-160 no-padding-right">
                    <img class="img-full-height" src="<?php echo base_url(); ?>assets/skilldemo/imgs/made-craft.png">
                </div>
            </div>
            <div class="clear image-thumb-icon-wrapper thumbnails-hide-mobile">
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/4.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/5.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/6.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/7.png">
                </div>
            </div>
            <div class="col-xs-12 no-padding-right thumbnails-hide-mobile no-padding-left">
                <img class="goal-img-size" src="<?php echo base_url(); ?>assets/skilldemo/imgs/goal.png">
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 right-side-sec no-margin-xs-top">
            <h3> OUR PROCESS: </h3>
            <p> At Folk, we believe in making one’s life a little simpler by weaving in thoughtful innovations in all our bags and accessories which we create from natural materials like jute, juco, cotton and canvas. And in line with our larger worldview, our very own skill training program is been conceived to pro- vide holistic training on two levels. These constitute: 
            </p>
            <ul>
                <li style="font-size: 18px;">
                    Teaching them the professional activities practised at Folk as part of basic and advanced apprenticeship training
                </li>
                <li style="font-size: 18px;">
                    Inculcating in them independent thought and imagination in the form of conversations, workshops and recreational pursuits
                </li>
            </ul>
            <p>This in an inclusive training program</p>
        </div>
        <div class="col-xs-12 no-padding-right thumbnails-show-mobile justify-xs-center bottom-xs-gap">
            <img class="goal-img-size" src="<?php echo base_url(); ?>assets/skilldemo/imgs/goal.png">
        </div>
        <div class="clear image-thumb-icon-wrapper thumbnails-show-mobile bottom-xs-gap xs-padding-left xs-padding-right">
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/4.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/5.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/6.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/7.png">
            </div>
        </div>
    </div>
</section>
<section class="sandal-section section-style conatiner-fluid no-xs-padding-l no-xs-padding-r">
    <div class="container">
        <div class="col-xs-12 height-350 no-padding-right no-padding-left bottom-gap height-xs-200 thumbnails-show-mobile margin-xs-top-40">
            <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/coaching.png">
        </div>
        <div class="col-sm-6 col-xs-12 right-side-sec no-xs-padding-l no-xs-padding-r no-margin-xs-top">
            <h3 class="full-xs-width"> 
                OUR INFRASTRUCTURE
            </h3>
            <p> 
                <span> Training hall - 1 room 700 sq ft </span>
                <span> Class room   - 1 room 500 sq ft </span>
                <span>  
                Wholly in-house production 5000 sq ft. Our production center is conveniently located in the Government Industrial Estate. 150 meters from railway station, 7.7 mtrs. from airport and easily accessible through other public transportation.
                </span>
                <span>Bio matrices attendance</span>
                <span>CCTV Camera</span>
                <span>Separate Washrooms for men and women</span>
                <span>Commercial and industrial electric supply of electricity and WI-FI enabled</span>
                <span>Full time supervision of the training</span>
            </p>
            <div class="content-icons">
                <div class="clearfix">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/17.svg">
                    <h5> Bio Metrices </h5>
                </div>
                <div class="clearfix">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/18.svg">
                    <h5> CCTV </h5>
                </div>
                <div class="clearfix">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/19.svg">
                    <h5> Washroom </h5>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 secion-img-content no-xs-padding-l no-xs-padding-r">
            <div class="col-xs-12 height-350 no-padding-right no-padding-left bottom-gap height-xs-200 thumbnails-hide-mobile height-sm-230">
                <img class="img-full-height" src="<?php echo base_url(); ?>assets/skilldemo/imgs/coaching.png">
            </div>
            <div class="image-thumb-icon-wrapper clear">
                <div class="width-30 three-thumbs-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/8.png">
                </div>
                <div class="width-30 three-thumbs-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/9.png">
                </div>
                <div class="width-30 three-thumbs-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/10.png">
                </div>
            </div>
            <div class="col-xs-12 no-padding-right text-right display-xs-flex justify-xs-center no-padding-left">
                <img class="goal-img-size" src="<?php echo base_url(); ?>assets/skilldemo/imgs/goal.png">
            </div>
        </div>
    </div>
</section>
<section class="white-section section-style conatiner-fluid">
    <div class="container no-xs-padding-l no-xs-padding-r">
        <div class="col-sm-6 col-xs-12 no-xs-padding-l no-xs-padding-r margin-xs-top-40">
            <div class="clearfix clear">
                <div class="col-xs-12 height-250 no-padding-right bottom-gap height-xs-150 no-xs-padding-l height-sm-160 no-padding-left">
                    <img class="img-full-height" src="<?php echo base_url(); ?>assets/skilldemo/imgs/products.png">
                </div>
            </div>
            <div class="clear image-thumb-icon-wrapper thumbnails-hide-mobile">
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/12.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/13.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/14.png">
                </div>
                <div class="width-22-perc thumbs-md-height">
                    <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/15.png">
                </div>
            </div>
            <div class="col-xs-12 no-padding-right thumbnails-hide-mobile no-padding-left">
                <img class="goal-img-size" src="<?php echo base_url(); ?>assets/skilldemo/imgs/goal.png">
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 right-side-sec no-xs-padding-l no-xs-padding-r">
            <h3> 
                OUR PROGRAM
            </h3>
            <p>  
                Our skill program includes :
            </p>
            <ul>
                <li style="font-size: 18px;">
                    Arranging one-on-one interactive sessions with each student to evaluate their individual inclinations and needs
                </li>
                <li style="font-size: 18px;">
                    Orienting them in Folk’s areas of speciality such as stitching, printing, quality checking, packaging
                </li>
                <li style="font-size: 18px;">
                    Imparting basic and advance training
                </li>
                <li style="font-size: 18px;">
                    Ensuring placement or livelihood generation through setting up micro enterprises.
                </li>
            </ul>
            <p>
                At its core, this initiative is a humble step to steer trainees towards their own direction in life by means of an interdependent ecosystem of careful guidance and mentorship that would not only equip them with tangible skills and accomplishments but also emancipate their individual selves.
            </p>
        </div>
        <div class="col-xs-12 no-padding-right thumbnails-show-mobile justify-xs-center bottom-xs-gap">
            <img class="goal-img-size" src="<?php echo base_url(); ?>assets/skilldemo/imgs/goal.png">
        </div>
        <div class="clear image-thumb-icon-wrapper thumbnails-show-mobile bottom-xs-gap">
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/12.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/13.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/14.png">
            </div>
            <div class="width-22-perc thumbs-xs-height">
                <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/15.png">
            </div>
        </div>
    </div>
</section>
<section class="footer-class">
    <div class="footer-div-class footer-contact-div">
        <span class="contact-heading-span">CONTACT US</span>
    </div>
    <div class="footer-div-class flex-column">
        <span class="d-flex width-30">
        <span class="footer-img-class" style="padding-top: 0.8rem">
            <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/phone-receiver.svg">
        </span>
        <span style="display: flex; flex-direction: column;">
            <span>+91 33 2355 4445/6</span>
	       <span>+91 98 7475 4445</span>
        </span>
        </span>
        <span class="d-flex width-30">
        <span class="footer-img-class">
            <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/envelope(1).svg">
        </span>
        <span>info@folkindia.in</span>
        </span>
        <span class="d-flex width-40 justify-content-center-web">
        <span class="footer-img-class">
            <img src="<?php echo base_url(); ?>assets/skilldemo/imgs/placeholder(1).svg">
        </span>
        <span class="width-60">Folk Products And Design Private Limited 1B & C, Maniktala Industrial Estate, Kolkata 700054, West Bengal, India</span>
        </span>
    </div>
</section>
