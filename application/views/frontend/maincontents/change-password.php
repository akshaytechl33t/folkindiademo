<div class="bodyContent">
	<div class="main-content container-fluid catalog">
		<div class="container">
			<div class="innertop">
			<!--final accountpage start-->
				<div class="finalAccountPage">
					<?php echo $common_account_left_panel;?>
					<div class="finalaccRight">     
						<div id="finalaccTabCnt1" class="finalaccTabContent">
						<div id="notifyMessage">
							<?php if($this->session->flashdata('success_message') !=''){?>							
								<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('success_message'); ?></b> </div>							
							<?php } ?>
							<?php if($this->session->flashdata('error_message') !=''){?>
							
								<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
								<b><?php echo $this->session->flashdata('error_message'); ?></b> </div>							
							<?php } ?>
						</div>
						<div class="alert alert-danger" id="password_mismatch" style="display:none">New Password Mismatch</div>
						
						<h4>Change Password</h4>
							<div class="personalInfoForm">
								<?php echo form_open('User/change_password',array('method'=>'post','id'=>'change_password'));?>
									<div class="form-group">
									<label for="exampleInputName">Old Password <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_password(array('name'=>'current_password','class'=>'form-control validate[required]'));?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">New Password  <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_password(array('name'=>'new_password','class'=>'form-control validate[required,minSize[8]]','id'=>'new_password'));?> 
										</span>
									</div>
									<div class="form-group">
										<label for="exampleInputName">Confirm Password  <span class="required">*</span></label>
										<span class="inputBox">
											<?php echo form_password(array('name'=>'confirm_password','class'=>'form-control validate[required,minSize[8]]','id'=>'confirm_password'));?> 
										</span>
									</div>
									<div class="popFormSubmit">
										<?php echo form_submit(array('name'=>'submit', 'value'=>'Save Changes', 'class'=>'btn btn-default popFormSubmitBtn')); ?>										
									</div>
								<?php echo form_close(); ?> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(document).on('submit','#change_password',function(e)
	{
		var newPassword = $('#new_password').val();
		var confirmPassword = $('#confirm_password').val();
		if(newPassword!= confirmPassword)
		{
			$('#password_mismatch').css('display','block');
			return false;
		}
		
	});	 
})


</script>