<div class="bodyContent"> 
    <!---->
    <div class="main-content container-fluid catalog">
      <div class="container">
        <div class="innertop"> 
          <!--cart page start-->
          <div class="cartPage"> 
		  
            <!--cartStep1 start-->
            <section id="cartStep1"  >
				<div class="section_margin_bottom">
					<div class="">
						<h3>1. Login Id <strong><?php echo $this->session->userdata['is_user_signed_in']['user_email_id'];?></strong>
							<div class="text-right checkout-change-login"><a href="javascript:void(0);" id="changeLogin">Change login</a></div>
						</h3>
						<div class="tick"></div>
					</div>
			   </div>
              <div class="cartStepBody collapse" id="login_id">
                <div class="cartContent">
                  <div  class="logAs"> Loged in as <strong><?php echo $this->session->userdata['is_user_signed_in']['user_email_id'];?></strong> </div>
                  <div  class="logAlert"> Please note that upon clicking "Sign out" will lose items in your cart and will be redirected to the home page. </div>
                  <div class="cartAccButtonArea"> <a href="<?php echo base_url();?>logout" class="cartSignout">Sign out</a> <a href="javascript:void(0);" class="continueShoping"  >Continue with checkout</a> </div>                
                </div>
              </div>
            </section>
            <!--cartStep1 end-->             
            <!--cartStep2 start-->
            <section id="cartStep1" class="cartstep2">
			<div class="section_margin_bottom">
				 <?php if($this->session->userdata('shipping_address') == ''){?>           
					<div class="cartStepHead"> <h3>2. Delivery Address</h3> </div>
				<?php } else { ?>
				<div class="">
					<h3>2. Delivery Address
					<div class="text-right checkout-change-login" id="chnageAddress"><a href="javascript:void(0);">Change Address</a></div>
					</h3>
					<div class="tick"></div>
				</div>
				<?php } ?>
			 </div> 
              <div class="cartStepBody <?php echo ($this->session->userdata('shipping_address') != '') ? 'collapse':'';?>" id="delivery_details">
                <div class="cartContent clearfix">
				<?php if(!empty($get_shipping_address)){
				foreach($get_shipping_address as $ship_address):		
				?>
                  <div class="deleveryAddArea"  id="<?php echo $ship_address->id?>"> 
					<div class="deleveryAddress">
						<div class="delvAddHead">
						  <h4><?php echo $ship_address->name?></h4>
						</div>
						<div class="shpAddress">
							<p> <?php echo $ship_address->street_address?> <br/>
							<p> <?php echo $ship_address->land_mark?> <br/>
							<p> <?php echo $ship_address->city_name?> - <?php echo $ship_address->pincode?>  </br>	
							<p> <?php echo $ship_address->state_name?></br>
							 <?php echo $ship_address->country_name?> </p>
						</div>
						<div class="phoneNumber"> <?php echo $ship_address->phone_number?> </div>
						<div class="shpContiArea">
						<?php if($ship_address->id == $this->session->userdata['shipping_address']['id']) { ?>
							<a href="javascript:void(0);" class="shpConti choose_shipping_address" id="<?php echo $ship_address->id?>">Continue</a>
						<?php } else { ?>
							<a href="javascript:void(0);" class="shpConti choose_shipping_address" id="<?php echo $ship_address->id?>">Delivery Here</a>
						<?php } ?>
						
						</div>
                    </div>        
					<div class="addressEditArea">
                      <ul>                        
							<li><a href="javascript:void(0);" class="delete_shipping_address" id="<?php echo $ship_address->id?>"><img src="<?php echo base_url();?>assets/frontend/images/large/delIcon.png" alt=""></a></li>
                      </ul>
                    </div>	
					<?php if($ship_address->id == $this->session->userdata['shipping_address']['id']) { ?> <div class="greenRibbon"> <img src="<?php echo base_url();?>assets/frontend/images/large/greenRight.png" alt=""> </div><?php } ?>
                  </div>
				<?php endforeach; } ?>
				<div class="orderConfirmationAlert clearfix addnewaddress text-center">
					<span class="orderConfirmText">
						<a href="javascript:void(0);" data-toggle="modal" data-target="#addNewShipping">Add new address</a>
					</span>
				</div>
				</div>				
              </div>				
		   </section>		   
		    <?php echo $add_new_shipping;?>  			
            <!--cartStep2 end-->             
            <!--cartStep3 start-->
            <section id="cartStep1" class="cartstep3">
			<?php if($this->session->userdata('shipping_address') != ''){?>
				<div class="cartStepHead">
					<h3>3. Order Summary
					<?php if($this->session->userdata['cart_contents']){?>
						<span><?php 
							$cartrows = array(); foreach ($this->cart->contents() as $item)
							{
								$cartrows[] = array('rowid' => $item['rowid']);
							} 
							?>
							<?php echo sizeof($cartrows).' items'?>
						</span>
					<?php } ?>
					</h3>
				</div>
			  <?php  } else { ?>
				<div class="cartStepHeadClose item_box">
					<h3>3. Order Summary 
						<?php if($this->session->userdata['cart_contents']){?>
							<span><?php 
								$cartrows = array(); foreach ($this->cart->contents() as $item)
								{
									$cartrows[] = array('rowid' => $item['rowid']);
								} 
								?>
								<?php echo sizeof($cartrows).' items'?>
							</span>
						<?php } ?>
					</h3>
				</div>
			  <?php } ?>
              <div class="cartStepBody <?php echo ($this->session->userdata('shipping_address') == '') ? 'collapse':'';?>" id="item_details">
                <div class="cartContent"> 
                  <!--cart table start-->
                  <div class="cartTable">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Product Image</th>
                            <th class="itemTbl">Item</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Delivery Details</th>
                            <th>Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
						<?php 
						$cart_product_name = array();
						if($checkout_cart_details){									
							foreach($checkout_cart_details as $key=>$details):	
							array_push($cart_product_name,$details['name']);
						?>	
                          <tr>
                            <td><a href="<?php echo base_url();?>product-details/<?php echo $details['product_seo_name']?>"><img src="<?php echo $details['product_image']?>" alt="" class="img-responsive" style="width:100px"></a></td>
                            <td><div class="ShpCartCnt">
                               <div class="prdcartHeading"> <?php echo $details['name']?> </div>
                               <div class="prdDesc"><b>Category :</b><?php echo $details['category_name']?></div>
								<div class="prdDesc"><b>Color :</b><?php echo $details['product_color']?></div>
                              </div></td>
                            <td><?php echo $details['quantity']?></td>
                           <td class="money" data-inr="<?php echo number_format($details['price'],2)?>">Rs. <?php echo number_format($details['price'],2)?></td>
                           <td>Free</td>
                          <td class="money" data-inr="<?php echo number_format($details['subtotal'],2)?>">Rs. <?php echo number_format($details['subtotal'],2)?></td>                            
                          </tr>
						  <?php  endforeach ; } else {?> 
							<tr><td colspan="7">Cart is empty.</td></tr>
						 <?php } ?>	
                        </tbody>
                      </table>
                    </div>
                  </div> 
                  <div class="continuePayAmount"> <a href="javascript:void(0);" class="shpConti choose_payment">Continue ></a>
                    <?php  									
					if(!empty($this->session->userdata('order_coupon_details'))){ ?>
						<?php $discount_amount =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;
						$coupon_discount_amountINR = number_format($this->cart->total() - $discount_amount,2);
						?>
					<?php } else { ?>
						<?php $coupon_discount_amountINR = number_format($this->session->userdata['cart_contents']['cart_total'],2);?>
					<?php } ?>
                    <div class="totalAmountPurChs">Amount Payable: <strong class="money" data-inr="<?php echo trim($coupon_discount_amountINR); ?>">RS. 
					<?php  									
					if(!empty($this->session->userdata('order_coupon_details'))){ ?>
						<?php $discount_amount =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;
						echo $coupon_discount_amount = number_format($this->cart->total() - $discount_amount,2);
						?>
					<?php } else { ?>
						<?php echo number_format($this->session->userdata['cart_contents']['cart_total'],2);?>
					<?php } ?>
					
					</strong></div>
					<div class="total_savings">
						<?php  if(!empty($this->session->userdata('order_coupon_details'))){?>
							Total savings: RS. <?php echo number_format(($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total(),2) ;?>
						<?php } ?>
					</div>
                  </div>
                </div>
              </div>
            </section>
            <!--cartStep3 end-->             
            <!--cartStep4 start-->
            <section id="cartStep1" class="cartStep4">
				<div class="cartStepHeadClose payment-box">
					<!--<div class="cartStepHeadClose ">-->
					<h3>4. Payment method </h3>
				</div>			  
				<div class="cartStepBody cartStepPaymentBody collapse" id="payment_details">
					                <div class="paymentMethod payment_container"> 
									<!-- only for GST 
									<div class="payment_price">
						<span style="color:red;">Online Shopping is temporarily unavailable due to GST Implementation . We will get back to you soon! </span>
						</div>
							 only for GST -->		
                  <!--payment tab start-->
						<div class="payment_price">
						<span class="you-pay">You pay : Rs. </span>
						<?php  									
					if(!empty($this->session->userdata('order_coupon_details'))){ ?>
						<?php $discount_amount =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;
						echo $coupon_discount_amount = number_format($this->cart->total() - $discount_amount,2);
						?>
					<?php } else { ?>
						<?php echo number_format($this->session->userdata['cart_contents']['cart_total'],2);?>
					<?php } ?>
						</div>


						<div style="margin-bottom: 1%;">
							<label>Click to Pay With</label>
						</div>
						<div class="payment_btn">
							<a href="javascript:void(0);" id="make_payment">
								<img src="<?php echo base_url();?>assets/frontend/images/checkout-payubiz.png"  alt="Buy with PayUBiz">
							</a>
						</div> 
						<br>						
						<div class="payment_btn">
							<label>Or<br/></label>
                            <p>Please select currency for PayPal</p>
                            <select id="paypal_acceptable_currency" name="paypal_acceptable_currency"style="width: 18%; margin-bottom: 2%;">
                                <option value="USD">USD</option>
								<option value="EUR">EUR</option>
                                <option value="GBP">GBP</option>
                                <option value="AUD">AUD</option>
                                <option value="BRL">BRL</option>
                                <option value="CAD">CAD</option>
                                <option value="CZK">CZK</option>
                                <option value="DKK">DKK</option>
                                <option value="HKD">HKD</option>
                                <option value="HUF">HUF</option>
                                <option value="ILS">ILS</option>
                                <option value="JPY">JPY</option>
                                <option value="MYR">MYR</option>
                                <option value="MXN">MXN</option>
                                <option value="NOK">NOK</option>
                                <option value="NZD">NZD</option>
                                <option value="PHP">PHP</option>
                                <option value="PLN">PLN</option>
                                <option value="RUB">RUB</option>
                                <option value="SGD">SGD</option>
                                <option value="SEK">SEK</option>
                                <option value="CHF">CHF</option>
                                <option value="TWD">TWD</option>
                                <option value="THB">THB</option>
                            </select>
							<a href="javascript:void(0);" id="paypal_img">
								<img style="width:24%;" src="<?php echo base_url();?>assets/frontend/images/paypal_buy.png" alt="Buy with PayPal">
							</a>
						</div>               
                  <!--payment tab end--> 
                </div>
				<?php 
				if(!empty($this->session->userdata('order_coupon_details')))
				{
					$net_disount_amt =($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() ;
					
					$net_amt = ceil($this->cart->total() - $discount_amount);
				} else { $net_amt = ceil($this->session->userdata['cart_contents']['cart_total']); } 
				
				$amount = urlencode($net_amt);

				$from_Currency = urlencode('INR');
				$to_Currency = urlencode('USD');
				$get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=" . $from_Currency . "&to=" . $to_Currency);

				$get = explode("<span class=bld>",$get);
				$get = explode("</span>",$get[1]);
				$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
				
				$payPalAmount = money_format($converted_amount, 2);
				$username = $this->session->userdata['is_user_signed_in']['user_name'];	
				$email =  $this->session->userdata['is_user_signed_in']['user_email_id'];











	
				
				$hash_string =  hash("sha512",'jfwlKH|'.$txnid.'|'.@$net_amt.'|'.@$cart_product_name.'|'.@$username.'|'.@$email.'|||||||||||32MycirA');?>
				<!-- test url https://test.payu.in/_payment-->
				<!-- live url https://secure.payu.in/_payment-->
				<form action='https://secure.payu.in/_payment' method="post" name="payuForm">
					<input type="hidden" name="firstname" value="<?php echo @$username;?>" />
					<input type="hidden" name="lastname" value="" />
					<input type="hidden" name="surl" value="<?php echo base_url();?>success/" />
					<input type="hidden" name="phone" value="" />
					<input type="hidden" name="key" value="jfwlKH" />
					<input type="hidden" name="hash" value ="<?php echo $hash_string;?>" />
					<input type="hidden" name="curl" value="<?php echo base_url();?>cancel/" />
					<input type="hidden" name="furl" value="<?php echo base_url();?>error/" />
					<input type="hidden" name="txnid" value="<?php echo $txnid;?>" />
					<input type="hidden" name="productinfo" value="<?php echo $cart_product_name;?>" />
					<input type="hidden" id="" name="amount" value="<?php echo @$net_amt;?>" />
					<input type="hidden" name="email" value="<?php echo @$email?>" />	
					<input type="hidden" name="shipping_firstname" value="<?php echo @$this->session->userdata['shipping_address']['name'];?>" />						
					<input type="hidden" name="shipping_address1" value="<?php echo @$this->session->userdata['shipping_address']['street_address'];?>" />	
					<input type="hidden" name="shipping_address2" value="<?php echo @$this->session->userdata['shipping_address']['land_mark'];?>" />	
					<input type="hidden" name="shipping_country" value="<?php echo @$this->session->userdata['shipping_address']['country_name'];?>" />	
					<input type="hidden" name="shipping_state" value="<?php echo @$this->session->userdata['shipping_address']['state_name']; ?>" />	
					<input type="hidden" name="shipping_city" value="<?php echo $this->session->userdata['shipping_address']['city_name'];?>" />	
					<input type="hidden" name="shipping_zipcode" value="<?php echo $this->session->userdata['shipping_address']['pincode'];?>"/>	
					<input type="hidden" name="shipping_phone" value="<?php echo @$this->session->userdata['shipping_address']['phone_number'];?>" />				
				</form>
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="paypal">
					<!-- Identify your business so that you can collect the payments. -->
					<input type="hidden" name="business" value="manjir@gmail.com">
					<input type="hidden" name="firstname" value="<?php echo @$username;?>" />
					<input type="hidden" name="lastname" value="" />
					<input type="hidden" name="return" value="<?php echo base_url();?>success/" />
					<input type="hidden" name="phone" value="" />
					<!-- Specify a Buy Now button. -->
					<input type="hidden" name="cmd" value="_xclick">
					<!--<input type="hidden" name="quantity" value="<?php //echo $totalQuantity;?>">-->
					<input type="hidden" name="item_name" value="Online Shopping" />
					<input type="hidden" name="txnid" value="<?php echo $txnid;?>" />
					<input type="hidden" name="productinfo" value="<?php echo $cart_product_name;?>" />
					<input type="hidden" id="paypal_amount" name="amount" value="<?php echo @$payPalAmount;?>" />
					<input type="hidden" name="email" value="<?php echo @$email?>" />	
					<input type="hidden" name="shipping_firstname" value="<?php echo @$this->session->userdata['shipping_address']['name'];?>" />						
					<input type="hidden" name="shipping_address1" value="<?php echo @$this->session->userdata['shipping_address']['street_address'];?>" />	
					<input type="hidden" name="shipping_address2" value="<?php echo @$this->session->userdata['shipping_address']['land_mark'];?>" />	
					<input type="hidden" name="shipping_country" value="<?php echo @$this->session->userdata['shipping_address']['country_name'];?>" />	
					<input type="hidden" name="shipping_state" value="<?php echo @$this->session->userdata['shipping_address']['state_name']; ?>" />	
					<input type="hidden" name="shipping_city" value="<?php echo $this->session->userdata['shipping_address']['city_name'];?>" />	
					<input type="hidden" name="shipping_zipcode" value="<?php echo $this->session->userdata['shipping_address']['pincode'];?>"/>	
					<input type="hidden" name="shipping_phone" value="<?php echo @$this->session->userdata['shipping_address']['phone_number'];?>" />				
					
					<input type="hidden" name="currency_code" value="USD" id="frm_currency_code">
					<?php
					if($checkout_cart_details){									
							foreach($checkout_cart_details as $key=>$details):
					?>
					<input type="hidden" name="item_name_<?php echo $key+1;?>" value="<?php echo $details['name']; ?>">
					<input type="hidden" name="amount_<?php echo $key+1;?>" value="<?php echo $details['usdprice']; ?>">
					<input type="hidden" name="quantity_<?php echo $key+1;?>" value="<?php echo $details['qty']; ?>">
					<?php  endforeach ; } else {?> 
						<input type="hidden" name="item_name" value="Purchase Online">
						<input type="hidden" name="amount" value="<?php echo $payPalAmount; ?>">
						<input type="hidden" name="quantity" value="1">
					 <?php } ?>	
					<!-- Display the payment button. -->
				</form>
                <input type="hidden" id="mainamount" name="mainamount" value="<?php echo @$amount;?>" />
					<?php /*?>					
					<div class="cartStepBody cartStepPaymentBody ">
						<div class="paymentMethod"> 
						<!--payment tab start-->
							<div class="">
								<div class=" bhoechie-tab-container">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-24 tabCol bhoechie-tab-menu">
										<div class="list-group"> 
											<a href="#" class="list-group-item active ">Credit Card  </a>
											<a href="#" class="list-group-item ">Debit Card </a> 
											<a href="#" class="list-group-item ">Net Banking</a>
											<a class="payumoney list-group-item" href="#wallet">PayUMoney</a>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24 tabCol bhoechie-tab"> 
									<!-- flight section -->
										<div class="bhoechie-tab-content active">
											<div class="bhoechie-tab-contentInner"> 
											<!--creditCardLeft start-->
											<div class="creditCardLeft">
											<div class="paymentHeader bmargin15"> Pay using Credit Card.
												<ul class="card-logo">
													<li class="visa"></li>
													<li class="master"></li>
													<li class="amex"></li>
													<li class="diners"></li>
													<li class="discover"></li>
												</ul>
											</div>
											<!--credit card form start-->
												<div class="creditcardForm creditcardForm-wrapper clearfix">
													
														<div class="form-group">
															<input type="text" class="form-control" id="" name="ccNumber" autocomplete="off" placeholder="Card Number">
														</div>														
														<div class="form-group">
															<input type="text" class="form-control" name="card_holder" autocomplete="off" placeholder="NAME ON CARD"  id="cardName">
														</div>
														<div class="cvv-inn-wr">
															<div class="cvv-inn-wrInpt">
																<input type="text"  class="form-control" name="cvvNumber" autocomplete="off" maxlength="3" placeholder="CVV">
															</div>
															<div class="cus-tooltip cvv-logo-wr dis-inl-bk"> 
															<span class="cvv-logo dis-inl-bk only-number">&nbsp;</span> </div>
															<div class="clear">&nbsp;</div>
														</div>
														
														<div class="expdateCard">
															<div class="exp-dt-wr txt-el-wr">
															<input type="text" class="form-control" name="month" autocomplete="off" maxlength="10" placeholder="MM / YY"  id="date">
															</div>														
														</div>
														
														<div class="creditFormSubmit">
														<button class="btn btn-default send creditFormSubmitSend" type="button">Continue</button>
														</div>
												</div>
												<!--credit card form end-->
											<!--boxBotomIcon"--> 
											</div>
											<!--creditCardLeft end--> 
											</div>
										</div>								
										<div class="bhoechie-tab-content">
											<center>
												<div class="creditCardLeft">
													<div class="paymentHeader bmargin15"> Pay using Credit Card.
													<ul class="card-logo">
														<li class="visa"></li>
														<li class="master"></li>
														<li class="amex"></li>
														<li class="diners"></li>
														<li class="discover"></li>
													</ul>
													</div>
													<div class="creditcardForm creditcardForm-wrapper clearfix">
													<form role="form">
														<div class="allBanks">
															<div class="form-group">
															<label for="sel1">Select debit card</label>
																<select class="form-control" id="sel1">
																	<option value="" selected="selected">Select Debit Card Type</option>
																	<option value="debitcard_MAST_1">MasterCard Debit Cards (All Banks)</option>
																	<option value="debitcard_VISA_1">Visa Debit Cards (All Banks)</option>
																	<option value="debitcard_SMAE_1">State Bank Maestro Cards</option>
																	<option value="debitcard_MAES_1">Other Maestro Cards</option>
																	<option value="debitcard_CITD_1">CITI Debit Card</option>
																	<option value="debitcard_SMAST_1">State Bank MasterCard debit cards</option>
															</select>
															</div>												
														</div>
														<div id="CarddetailSection" style="display:none">
															<div class="form-group">
																<input type="text" class="form-control" name="pan" autocomplete="off" maxlength="19" placeholder="Card Number"  id="cardNumb">
															</div>
															<div class="form-group">
																<input type="text" class="form-control" name="pan" autocomplete="off" maxlength="19" placeholder="NAME ON CARD"  id="cardName">
															</div>
															<div class="cvv-inn-wr">
																<div class="cvv-inn-wrInpt">
																<input type="text"  id="cvv-no"  class="form-control frm-el frm-el-tx cvv-no-el only-number plc-hd auto-for" name="cvv" autocomplete="off" maxlength="3" placeholder="CVV">
																</div>
																<div class="cus-tooltip cvv-logo-wr dis-inl-bk"> <span class="cvv-logo dis-inl-bk only-number">&nbsp;</span> </div>
																<div class="clear">&nbsp;</div>
															</div>
															<div class="frm-el-wr exp-cvv-wr clearfix expdateCard">
																<div class="exp-dt-wr txt-el-wr">
																<input type="text" class="form-control" name="month" autocomplete="off" maxlength="10" placeholder="MM / YY"  id="date">
																</div>	
															</div>
														</div>
														<div class="netbankSub">
																<button type="submit" class="btn btn-default send netBnkk">Continue</button>
														</div>
													</form>	
													</div>	
												</div>
											</center>
										</div>
										<div class="bhoechie-tab-content">
											<div class="netBankingArea">
												<div class="paymentHeader bmargin15"> Pay using Credit Card.
													<ul class="card-logo">
														<li class="visa"></li>
														<li class="master"></li>
														<li class="amex"></li>
														<li class="diners"></li>
														<li class="discover"></li>
													</ul>
												</div>
												<div class="creditcardForm creditcardForm-wrapper clearfix">
													<form> 
													<h5>Select one of the popular banks:</h5>
														<div class="bankList">
															<ul>
																<li>
																	<input id="hdfc" name="hdfcbank" value="hdfc" type="radio"><label for="hdfc">&nbsp;</label> 
																	<div class="bank-name-wr HDFC">&nbsp;</div>
																</li>
																<li>
																	<input id="icicici" name="icicicibank" value="icicici" type="radio"> <label for="icicici">&nbsp;</label>
																	<div class="bank-name-wr ICICI">&nbsp;</div>
																</li>
																<li>
																	<input id="cityban" name="citybank" value="cityban" type="radio">
																	<label for="cityban">&nbsp;</label> 
																	<div class="bank-name-wr CITIBANK">&nbsp;</div>
																</li>
																<li>
																	<input id="sbi" name="sbibank" value="sbi" type="radio"> <label for="sbi">&nbsp;</label> 
																	<div class="bank-name-wr SBI">&nbsp;</div>
																</li>
																<li>
																	<input id="axis" name="axisbank" value="axis" type="radio">  <label for="axis">&nbsp;</label> 
																	<div class="bank-name-wr AXIS">&nbsp;</div>
																</li>
																<li>
																	<input id="kotak" name="kotakBank" value="kotak" type="radio">  <label for="kotak">&nbsp;</label>  <div class="bank-name-wr KOTAK">&nbsp;</div>
																</li>
															</ul>
														</div>
														<div class="allBanks">
															<div class="form-group">
																<label for="sel1">or select any other bank:</label>
																<select class="form-control" id="sel1">
																	<option value="" selected="selected" gateway_status="1">Select Bank </option>
																	<option value="PSBNB" gateway_status="1">Punjab And Sind Bank</option>
																	<option value="CPNB" gateway_status="1">Punjab National Bank - Corporate Banking</option>
																	<option value="PNBB" gateway_status="1">Punjab National Bank - Retail Banking</option>
																	<option value="SRSWT" gateway_status="1">Saraswat Bank</option>
																	<option value="PMNB" gateway_status="1">Punjab And Maharashtra Co-operative Bank Limited</option>
																	<option value="OBCB" gateway_status="1">Oriental Bank of Commerce</option>
																	<option value="KRVB" gateway_status="1">Karur Vysya - Retail Netbanking</option>
																	<option value="162B" gateway_status="1">Kotak Mahindra Bank</option>
																	<option value="LVCB" gateway_status="1">Lakshmi Vilas Bank - Corporate Netbanking</option>
																	<option value="LVRB" gateway_status="1">Lakshmi Vilas Bank - Retail Netbanking</option>
																	<option value="SVCNB" gateway_status="1">Shamrao Vithal Co-operative Bank Ltd.</option>
																	<option value="SOIB" gateway_status="1">South Indian Bank</option>
																	<option value="UBIB" gateway_status="1">Union Bank - Retail Netbanking</option>
																	<option value="UNIB" gateway_status="1">United Bank Of India</option>
																	<option value="VJYB" gateway_status="1">Vijaya Bank</option>
																	<option value="YESB" gateway_status="1">Yes Bank</option>
																	<option value="UBIBC" gateway_status="1">Union Bank - Corporate Netbanking</option>
																	<option value="UCOB" gateway_status="1">UCO Bank</option>
																	<option value="SYNDB" gateway_status="1">Syndicate Bank</option>
																	<option value="TMBB" gateway_status="1">Tamilnad Mercantile Bank</option>
																	<option value="TBON" gateway_status="1">The Nainital Bank</option>
																	<option value="KRVBC" gateway_status="1">Karur Vysya - Corporate Netbanking</option>
																	<option value="KRKB" gateway_status="1">Karnataka Bank</option>
																	<option value="CRPB" gateway_status="1">Corporation Bank</option>
																	<option value="CSMSNB" gateway_status="1">Cosmos Bank</option>
																	<option value="DCBCORP" gateway_status="1">DCB Bank - Corporate Netbanking </option>
																	<option value="DCBB" gateway_status="1">DCB Bank Limited</option>
																	<option value="CITNB" gateway_status="1">Citibank Netbanking</option>
																	<option value="CBIB" gateway_status="1">Central Bank Of India</option>
																	<option value="BOIB" gateway_status="1">Bank of India</option>
																	<option value="BOMB" gateway_status="1">Bank of Maharashtra</option>
																	<option value="CABB" gateway_status="1">Canara Bank</option>
																	<option value="CSBN" gateway_status="1">Catholic Syrian Bank</option>
																	<option value="DENN" gateway_status="1">Dena Bank</option>
																	<option value="DSHB" gateway_status="1">Deutsche Bank</option>
																	<option value="INOB" gateway_status="1">Indian Overseas Bank</option>
																	<option value="INIB" gateway_status="1">IndusInd Bank</option>
																	<option value="JAKB" gateway_status="1">Jammu and Kashmir Bank</option>
																	<option value="JSBNB" gateway_status="1">Janata Sahakari Bank Pune</option>
																	<option value="INDB" gateway_status="1">Indian Bank </option>
																	<option value="IDBB" gateway_status="1">IDBI Bank</option>
																	<option value="DLSB" gateway_status="1">Dhanlaxmi Bank</option>
																	<option value="FEDB" gateway_status="1">Federal Bank</option>
																	<option value="HDFB" gateway_status="1">HDFC Bank</option>
																	<option value="ADBB" gateway_status="1">Andhra Bank</option>
																</select>
															</div>
															<div class="netbankSub">
																<button type="submit" class="btn btn-default send netBnkk">Continue</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="bhoechie-tab-content">
											<center>
												<div class="creditCardLeft">
													<div class="paymentHeader bmargin15"> 
														<span class="branding_info"></span>
													</div>
													<div class="creditcardForm creditcardForm-wrapper clearfix paubiz">
														<h1>Pay using PayUmoney</h1>
														<div class="card_image"></div>
														<div class="card_support">
															<div class="wallet_info"><span></span>Supports all Credit Cards, Debit Cards and Netbanking options
															</div>
														</div>
														<div class="button_container">
															<span>Note: In the next step you will be redirected to PayUmoney site. Please enter card or netbanking details after sign-in.</span>
															<input value="Pay Using PayUmoney" onclick="processWallet();" class="button " type="button">
														</div>
													</div>	
												</div>
											</center>
										</div>
										<ul class="boxBotomIcon">
											<li><img src="<?php echo base_url()?>assets/frontend/images/icon1.jpg"  alt=""/></li>
											<li><img src="<?php echo base_url()?>assets/frontend/images/icon2.jpg"  alt=""/></li>
											<li><img src="<?php echo base_url()?>assets/frontend/images/icon3.jpg"  alt=""/></li>
											<li><img src="<?php echo base_url()?>assets/frontend/images/icon4.jpg"  alt=""/></li>
										</ul>
									</div>
								<!---->

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 tabCol cartGrandtotal">
									<div class="creditCardRight">
										<div class="paymentTop">
											<p class="text_left">Total</p>
											<p class="text_right">
												<strong class="ng-binding"> 
												Rs.<?php if(!empty($this->session->userdata('order_coupon_details'))){?>
												<?php echo $coupon_discount_amount = number_format(($this->session->userdata['cart_contents']['cart_total']) - ($this->session->userdata['order_coupon_details']->discount_amount),2);?>
												<?php } else { ?>
												<?php echo number_format($this->session->userdata['cart_contents']['cart_total'],2); ?>
												<?php } ?>
												</strong>
											</p>
										</div>
									<!--paymentTop-->

										<div class="paymentBottom">
											<h2>Amount Payable</h2>
											<p>Rs. <?php if(!empty($this->session->userdata('order_coupon_details'))){?>
												<?php echo $coupon_discount_amount = number_format(($this->session->userdata['cart_contents']['cart_total']) - ($this->session->userdata['order_coupon_details']->discount_amount),2);?>
												<?php } else { ?>
												<?php echo number_format($this->session->userdata['cart_contents']['cart_total'],2); ?>
												<?php } ?>
											</p>
										</div>
									</div>
								<!--creditCardRight--> 

								</div>
								</div>
							</div>

						<!--payment tab end--> 
						</div>
					</div> 
					<?php */?>					
				<!--payment tab end--> 
				</div>
              </div>
            </section>
            <!--cartStep4 end-->             
          </div>
          <!--editor page end--> 
        </div>		
      </div>
    </div>
  </div>
<script>
$(document).ready(function(){
	$('.choose_shipping_address').click(function(){
		var setShippingAddrress = $(this).attr('id');
		$.post('<?php echo base_url()?>Ajax/set_shipping_address',{address_id : setShippingAddrress},function(data){
			if(data){
				location.reload();
			}
		})
	})
    $('#paypal_acceptable_currency').change(
        function()
        {
			$('#loadericon').show();
            $.ajax(
                {
                    url: '/currency-paypal',
                    type: "POST",
                    data: 'reqCurrency=' + $('#paypal_acceptable_currency').val()+'&mainamount='+$('#mainamount').val(),
                    success: function(result){
                        $('#frm_currency_code').val($('#paypal_acceptable_currency').val());
						$('#paypal_amount').val(result);
						$('#loadericon').hide();
                    }
                }
            );
        }
    )
	
	$('#changeLogin').click(function(){ 
		$('#login_id').removeClass('collapse');		
		$('.section_margin_bottom').children().addClass('cartStepHead');
		$('.section_margin_bottom').find('h3').siblings().remove()
		$('.section_margin_bottom').find('h3').children('strong').remove()
		$('#delivery_details').addClass('collapse');
		$('.checkout-change-login').remove();
		$('.cartstep2').children('.section_margin_bottom').addClass('cartStepHeadClose');
		$('.cartstep2').children('.section_margin_bottom').addClass('item_box');
		$('.cartstep2').children('.section_margin_bottom').removeClass('cartStepHead');
		$('.cartstep2').children('.section_margin_bottom').children().removeClass('cartStepHead');
		$('.cartstep3').children().addClass('cartStepHeadClose');
		$('.cartstep3').children().addClass('payment-box');
		$('.cartstep3').children().removeClass('cartStepHead');
		$('#item_details').addClass('collapse');
		$('.cartStep4').children('.payment-box').removeClass('cartStepHead');
		$('.cartStep4').children('.payment-box').addClass('cartStepHeadClose');
		$('#payment_details').addClass('collapse');
		$('#payment_details').removeClass('in');

	})
	$('.choose_payment').click(function(){
		$('.payment-box').removeClass('cartStepHeadClose');
		$('.payment-box').addClass('cartStepHead');
		$('#payment_details').addClass('in');
		$('.payment-box').css('box-shadow','none');
	})
	$('#make_payment').click(function(){
		var payuForm = document.forms.payuForm;
		payuForm.submit();
	})
	$('#paypal_img').click(function(){
		var paypalForm = document.forms.paypal;
		paypalForm.submit();
	})
	$('.continueShoping').click(function(){
		location.reload();
	})
	$('#chnageAddress').click(function(){
		$('.cartstep2').children('.section_margin_bottom').addClass('cartStepHead');
		$('.cartstep2').children('.section_margin_bottom').find('h3').siblings().remove()
		$('.cartstep2').children('.section_margin_bottom').find('h3').children('strong').remove()
		$('.cartstep2').children('#delivery_details').addClass('collapse');
		$('#chnageAddress').remove();
		$('#delivery_details').removeClass('collapse');
		$('.cartstep3').children().addClass('cartStepHeadClose');
		$('.cartstep3').children().addClass('payment-box');
		$('.cartstep3').children().removeClass('cartStepHead');
		$('#item_details').addClass('collapse');
		$('.cartStep4').children('.payment-box').removeClass('cartStepHead');
		$('.cartStep4').children('.payment-box').addClass('cartStepHeadClose');
		$('#payment_details').addClass('collapse');
		$('#payment_details').removeClass('in');
	})
	 $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });

})
</script>