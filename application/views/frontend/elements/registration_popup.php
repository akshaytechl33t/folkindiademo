<div class="modal fade mypoup" id="Signuppopup" role="dialog" style="display:none;">
    <div class="modal-dialog">
	<!-- Modal content-->
		<div class="modal-content">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<section class="logSec logSgnUp">
					<div class="logformSEc">
					<h4 class="modal-title">Signup</h4>
					 <div id="error_regmsg"></div>
					 <div id="succe_msg"></div>
						<?php echo form_open('User/registration',array('method'=>'post','id'=>'registration'));?>
							<div class="form-group">
								<?php echo form_input(array('name'=>'email_id', 'placeholder'=>"Email (*)",'class'=>'form-control validate[required,custom[email]]','id'=>'email'));?>
							</div>
							<div class="form-group">
								<?php echo form_input(array('name'=>'fname', 'placeholder'=>" First name(*)",'class'=>'form-control validate[required]'));?>
							</div>
							<div class="form-group">
								<?php echo form_input(array('name'=>'lname', 'placeholder'=>" Last name(*)",'class'=>'form-control validate[required]'));?>
							</div>
							<div class="form-group">
								<?php echo form_password(array('name'=>'password', 'placeholder'=>" Password(*)",'class'=>'form-control validate[required]'));?>
							</div>
							<div class="form-group">
								<div class="g-recaptcha" data-sitekey="6LciY00UAAAAAGRsxWMRitOERCNDupjyWrqnGPEc"></div>
							</div>
							<div class="logPopupSubmit">
							 <?php echo form_submit(array('name'=>'submit', 'value'=>'Sign up', 'class'=>'btn btn-default poplogSubmitBtn','id'=>'registration_submit')); ?>
							 <div id="reg_loader"></div>
							</div>
							<!--inside the form tag-->
							<!--this custom_captcha id is used in js file,you can change in js and html both-->

							<!--inside the form tag-->
						<?php echo form_close(); ?>
						<div class="newUser">
							<div class="newHere">Already have an account ?</div>
							<a href="javascript:void(0);" class="logPopSignUp" id="loginPopUp">Login</a>
						</div>
					</div>
					<div class="logoPopSignUpSec">
						<div class="enagage-social-div fk-inline-block">
							<div class="line">
								<div class="fk-inline-block engage-icons manage-orders-icon"></div>
								<div class="fk-inline-block lmargin10">
									<p class="fk-font-14 bpadding5">Manage Your Orders</p>
									<p class="fk-font-12 bpadding5 fk-text-center">Easily Track Orders, Create Returns</p>
								</div>
							</div>
							<div class="line tmargin30">
								<div class="fk-inline-block engage-icons informed-decisions-icon"></div>
								<div class="fk-inline-block lmargin10">
									<p class="fk-font-14 bpadding5">Make Informed Decisions</p>
									<p class="fk-font-12 bpadding5 fk-text-center">Get Relevant Alerts And Recommendations</p>
								</div>
							</div>
							<div class="line tmargin30">
								<div class="fk-inline-block engage-icons enagage-socially-icon"></div>
								<div class="fk-inline-block lmargin10">
									<p class="fk-font-14 bpadding5">Engage Socially</p>
									<p class="fk-font-12 bpadding5 fk-text-center">With Wishlists, Reviews, Ratings</p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
    </div>
</div>

