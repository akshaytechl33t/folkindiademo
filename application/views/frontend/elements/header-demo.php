<header class="header header1">
    <div class="container">
        <div class="row">
            <div class="headerMain">
                <div class="row">
                    <div class="col-md-3 col-sm-2 col-xs-12 no-padRight logoArea">
                        <div class="logo"> <a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>assets/frontend/images/large/logo.png"> </a> </div>
                    </div>
                    <div class="col-md-9 col-sm-10 col-xs-12 mainMenu">
                        <?php if(!empty($promocode)){?>
                        <div class="header-promocode-adv blink"> New Year Discount of <?php echo $promocode->discount_amount; ?>% . Use <span><?php echo $promocode->code;?></span> promo code to avail the offer till <?php echo date('F d Y',strtotime($promocode->expire));?></div>
                        <?php  } ?>
                        <ul class="topNav">
                            <li style="background:none;">
                                <a href="https://www.folkindia.in/bespoke">
                                    <MARQUEE WIDTH=500 onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 6, 0);">For Bulk Orders, We personalise as per your need. GST applicable as per product make</MARQUEE>
                                </a>
                            </li>
                            <li style="background:none;">
                                <div class="fb-like" data-href="https://www.facebook.com/folkproducts/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                            </li>
                            <?php if(!empty($this->session->userdata['is_user_signed_in']['user_id'])) {?>
                            <li><a href="<?php echo base_url();?>logout" title="Logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo base_url();?>my-account" title="My account"><i class="fa fa-user" aria-hidden="true"></i> &nbsp; Hi <?php echo $this->session->userdata['is_user_signed_in']['user_name'] ?></a></li>
                            <?php } else { ?>
                            <li><a href="javascript:void(0)" title="Sign in" data-toggle="modal" data-target="#loginpopup"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0)" title="Sign up" data-toggle="modal" data-target="#Signuppopup"><i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
                            <?php } ?>
                            <li class="cart_wr dropdown">
                                <a href="<?php echo base_url();?>viewcart">
                                    <i class="fa fa-cart-arrow-down"></i>
                                    <div id="cart_details"></div>
                                    <?php if($this->session->userdata['cart_contents']){?>
                                    <em id="cart_details"><?php
                                        $cartrows = array(); foreach ($this->cart->contents() as $item)
                                        {
                                        	$cartrows[] = array('rowid' => $item['rowid']);
                                        }
                                        ?>
                                    <?php echo sizeof($cartrows);?>
                                    </em>
                                    <?php } ?>
                                </a>
                                <div id="cart_notification"></div>
                                <?php if($this->session->userdata['cart_contents'] == ''){?>
                                <ul class="dropdown-menu empty_notfy"  aria-labelledby="dropdownMenu1">
                                    <div class="cart_on_hover">
                                        <div class="des_aro">
                                            <i class="fa fa-sort-asc"></i>
                                        </div>
                                        YOUR SHOPPING CART IS EMPTY.
                                    </div>
                                </ul>
                                <?php } ?>
                            </li>
                            <li class="social-media"><a target="_blank" href="<?php echo SITE_FACEBOOK_LINK;?>" > <i class="fa fa-facebook"></i> </a></li>
                            <li class="social-media"><a target="_blank" href="<?php echo SITE_Linkedin_LINK;?>"> <i class="fa fa-linkedin"></i></a></li>
                            <li><a role="button" href="javascript:void(0);" class="menu-search-icon" ><i class="fa fa-search"></i></a></li>
                        </ul>
                        <div class="menu_search col-md-8" style="display:none">
                            <input type="text" class="form-control" id="product_search" placeholder="Search Your product">
                            <div id="loader"></div>
                            <div id="search_result">
                            </div>
                        </div>
                        <nav class="navbar navbar-default navbar-static-top">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                <a class="navbar-brand" href="javascript:void(0)">Menu</a> 
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="musicMenu new-li">
                                        <label>Pick a Currency</label>				
                                        <select id="currencydrop" class="currency-picker" name="currencies">
                                            <option value="INR">INR</option>
                                            <option value="EUR">EUR</option>
                                            <option value="GBP">GBP</option>
                                            <option value="USD">USD</option>
                                        </select>
                                    </li>
                                    <!-- Demo menu link -->
                                    <li class="musicMenu"><a href="<?php echo base_url().'skill-development'; ?>">Skill Development</a>
                                    </li>
                                    <?php if(!empty($rows)){?>
                                    <?php foreach($rows as $menu):?>
                                    <li class="musicMenu"><a href="<?php echo base_url().'product/'.$menu->seo_name ?>"><?php echo $menu->name ?></a></li>
                                    <?php endforeach; ?>
                                    <?php } ?>
                                    <?php if(!empty($static_page)){?>
                                    <?php foreach($static_page as $pages):?>
                                    <li class="musicMenu"><a href="<?php echo base_url().$pages->page_seo_url;?>"><?php echo $pages->page_title;?></a></li>
                                    <?php endforeach; ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <?php //echo $login_popup;?>
            <?php $this->load->view('frontend/elements/login_popup'); ?>
            <?php $this->load->view('frontend/elements/registration_popup'); ?>
            <?php //echo $registration_popup;?>
        </div>
    </div>
    <input type="hidden" value="" id="currenciesdata">
</header>
<script>
    $(document).ready(function(){
    	$('.menu-search-icon').click(function(){
    		$('.menu_search').toggle();
    	});
    
    	var ajaxReq = $.ajax({
    		type:'GET',
    		url : 'http://data.fixer.io/api/latest?access_key=51517c7b9b314440f7cdac9cbea1e93b&base=INR',
    		dataType: "json",
    		success: function (data)
    		{
    			if(data !=''){
    				console.log(data);
    				$('#currenciesdata').val(JSON.stringify(data));
                    changePrice();
    			}
    			else
    			{
    				$('#currenciesdata').val('');
    			}
    
    		},
    		error:function(){
    		}
    	});
    	var baseUrl = $(".logo").find('a').attr('href');
    	$( "#product_search" ).keyup(function() {
    		var text = '';
    		var search_value = $(this).val();
    		if(search_value !=''){
    				if ((search_value.length) >= 3 ) {
    					$('#loader').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
    					$('#loader').show();
    				if (ajaxReq != null) ajaxReq.abort();
    				var ajaxReq = $.ajax({
    					type:'POST',
    					url : baseUrl+'Ajax/search',
    					dataType: "json",
    					data : { value : search_value },
    					success: function (data) {
    					$('#loader').hide();
    					text +='<ul class="top_search_result_ul content">';
    					if(data !=''){
    						$.each( data, function( key, value ) {
    							text +='<li><a href="'+baseUrl+'product-details/'+value.seo_name+'">'+value.name+' in <strong>'+ value.category_name +'</strong></a></li>';
    						});
    					}
    					else
    					{
    						text +='<li>No Item Found</li>';
    					}
    					text +='</ul>';
    					$('#search_result').html(text);
    					$('#search_result').show();
    				},
    				error:function(){
    					ajaxReq = null;
    					}
    				});
    			}
    		}
    		else
    		{
    			$('#search_result').hide();
    		}
    	});
    })
    
</script>