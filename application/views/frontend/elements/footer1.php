<?php if(!empty($static_page_all)){?>
<div class="footerWrap">
  <div class="footerMenu">
	<ul>
<li><a href="<?php echo base_url().'media';?>">Media</a></li>
	<li><a href="<?php echo base_url().'our-story';?>">Our Story</a></li>
	<?php foreach($static_page_all as $pages):?>
    <?php if($pages->page_name != 'Careers Sales' && $pages->page_name != 'Careers Design'): ?>
	     <li><a href="<?php echo base_url().$pages->page_seo_url;?>"><?php echo $pages->page_name;?></a></li>
	  <?php endif;?>
  <?php endforeach;?>
	<li><a href="<?php echo base_url();?>contact-us">Contact Us </a></li>
	</ul>
  </div>
</div>
<?php } ?>
