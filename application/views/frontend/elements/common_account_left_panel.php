<div class="finalaccLeft">
	<h4>My Account</h4>
	<div class="finalaccLeftTop">
		<h5>Orders</h5>
		<p><a href="<?php echo base_url();?>orders">My Orders</a></p>
	</div>
	<div class="finalaccLeftBottom">
		<div class="finalaccTab">
		<h5>Settings</h5>
			<ul>
				<li class="<?php echo ($this->uri->segment(1) == 'my-account') ? 'active':'';?>"><a href="<?php echo base_url();?>my-account">Personal Information</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'change-password') ? 'active':'';?>"><a href="<?php echo base_url();?>change-password">change Password</a></li>
				<li class="<?php echo ($this->uri->segment(1) == 'address') ? 'active':'';?>"><a href="<?php echo base_url();?>address">Addresses</a></li>				
			</ul>
		</div>
	</div>
</div>