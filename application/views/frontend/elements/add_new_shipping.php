<div class="modal fade mypoup" id="addNewShipping" role="dialog" style="display:none;">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-body">
          <section class="finalaccTabContent add_new_shipping">          
			<h4>Add a new address</h4>
				<div class="personalInfoForm">
					<?php echo form_open('Shipping/index',array('method'=>'post','id'=>'shipping_address'));?>
						<div class="form-group">
							<label for="exampleInputName">Name <span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_input(array('name'=>'name','class'=>'form-control validate[required]','value'=>$this->session->userdata['is_user_signed_in']['user_name']));
								echo form_error('name');
								echo form_hidden('shipping_url','checkout');
								?> 
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">Street Address<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_textarea(array('name'=>'street_address','class'=>'form-control validate[required]'));
								echo form_error('street_address');
								?> 
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">Landmark<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_input(array('name'=>'land_mark','class'=>'form-control validate[required]'));
								echo form_error('land_mark');
								?> 
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">Phone number<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_input(array('name'=>'phone_number','class'=>'form-control validate[required]'));
								echo form_error('phone_number');
								?> 
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">Country<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_dropdown(array('name'=>'country_id','class'=>'form-control validate[required]','id'=>'country_id'),$country_arr);
								echo form_error('country_id');
								?>
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">State<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_dropdown(array('name'=>'state_id','class'=>'form-control validate[required]','id'=>'state_id'),$state_arr);
								echo form_error('state_id');
								?>
								<div id="location_loader"></div>
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">City<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_dropdown(array('name'=>'city_id','class'=>'form-control validate[required]','id'=>'city_id'),$city_arr);
								echo form_error('city_id');
								?>
								<div id="location_loader"></div>
							</span>
						</div>
						<div class="form-group">
							<label for="exampleInputName">Pincode<span class="required">*</span></label>
							<span class="inputBox">
								<?php echo form_input(array('name'=>'pincode','class'=>'form-control validate[required]'));
								echo form_error('pincode');
								?> 
							</span>
						</div>
						<div class="popFormSubmit">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save Changes', 'class'=>'btn btn-default popFormSubmitBtn')); ?>										
						</div>
					<?php echo form_close(); ?> 
				</div>                                 
          </section>
        </div>        
      </div>      
    </div>
</div>