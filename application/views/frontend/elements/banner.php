<?php if(!empty($rows)){?>
<section id="banner">
	<div id="hero-wrapper">
    <div class="carousel-wrapper">
      <div id="hero-carousel" class="carousel slide carousel-fade">       
        <div class="carousel-inner">
		<?php foreach($rows as $key=>$row):?>
		<div class="item <?php if($key == 0 ){ echo 'active';}?>">
           <a href="<?php echo $row->link;?>"><img src="<?php echo base_url();?>uploads/images/home/banner/<?php echo $row->images?>" class="first-slide" alt="" /></a>
        </div>
		<?php endforeach; ?>
         
        </div>
        <a class="left carousel-control" href="#hero-carousel" data-slide="prev">
          <i class="fa fa-chevron-left left"></i>
        </a>
        <a class="right carousel-control" href="#hero-carousel" data-slide="next">
          <i class="fa fa-chevron-right right"></i>
        </a>
      </div>
    </div>
  </div>
  </section>
 <?php } ?>
 <script>
 $('.carousel').carousel({
  interval: 5000,
  pause: true
})
 </script>