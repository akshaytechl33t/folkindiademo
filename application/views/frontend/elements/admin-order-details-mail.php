<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width"/>
<title>WELCOME TO <?php echo SITE_NAME; ?></title>
</head>
<body style="">
<table class="body-wrap" style="margin: 0; padding: 0; font-size: 100%; line-height: 1.65; font-family:Arial, Helvetica, sans-serif; width: 580px !important; margin:0px auto; height: 100%; background: #efefef; -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none; ">
  <tr>
    <td class="mail_container" style="display: block !important; clear: both !important; margin: 0 auto !important; width: 580px !important;"><table>
        <tr>
          <td align="center" class="masthead" style="padding:20px 0; background:#131313; color: white;"><a href="#" style="text-decoration:none;outline:none;color:#ffffff;font-size:13px"> <img style="max-width:100%; margin: 0 auto; display: block;" alt="logo" src="<?php echo base_url();?>assets/frontend/images/large/logo.png"> </a> </td>
        </tr>
        <tr>
          <td class="content" style="background: white; padding: 30px 35px;"><h2 style="font-size: 21px; margin-bottom: 20px; line-height: 1.25">
		  Hello Admin,</h2>
            <p style="font-size: 14px; font-weight: normal;  margin-bottom: 20px; "><?php echo $rows['user_name']?> &nbsp;  has placed an order and Order No & transaction number  is &nbsp; <?php echo $rows['mihpayid']?> & <?php echo $rows['txnid']?> ,</p>
            <table cellpadding="0" cellspacing="0" style="width:100%; border:1px solid #dddddb; border-collapse: collapse; font-size:12px; color:#333;">
              <thead style="background:#eaeae7; color:#333;">
                <tr colspan="2">
                  <td width="184" cellpadding="0" cellspacing="0" colspan="2" style="width:100%; border:1px solid #dddddb; padding:0px 10px 0px 10px;">Product Details</td>    
                </tr>
				
              </thead>
              <tbody>                
                <?php if(!empty($rows['product_details'])){
					$sum = 0;
					foreach($rows['product_details'] as $pd_details){
					$sum = $sum+$pd_details->amount;	
					?>
						<tr>				
							<td width="184" cellpadding="0" colspan="2" cellspacing="0" style="border:1px solid #dddddb; padding:10px;">
							  <table>
								  <tr>
									<td width="100" cellpadding="0" cellspacing="0"><img width="60" src="<?php echo $pd_details->product_image;?>" alt="" /></td>
									<td width="100%" cellpadding="0" cellspacing="0"><?php echo $pd_details->product_name;?></td>
								  </tr>
								</table>
							</td>                  
						</tr>				
					<?php } } ?> 
                <tr>
					<td cellpadding="0" cellspacing="0" style="width:50%; border:1px solid #dddddb; padding:0px 10px 0px 10px; font-family:arial, helvetica, sans-serif;font-size:13px;font-weight:normal;text-decoration:none;color:#4a4a4a;padding:10px 10px 10px 10px;">
					<strong>Shipping Address:</strong><br>
						<?php echo $rows['shipping_name']?><br>
						<?php echo $rows['shipping_street_address']?><br>
						<?php echo $rows['shipping_land_mark']?><br>
						<?php echo $rows['shipping_country'].' - '.$rows['shipping_state']?>,<br>
						<?php echo $rows['shipping_city'].' - '.$rows['shipping_pincode']?><br>
						<?php if($rows['shipping_phone'] !='') { ?> Mobile No.: <?php echo $rows['shipping_phone'];}?>
					</td>
				</tr>
                
              </tbody>
            </table>
            
            <p style="font-size: 14px; font-weight: normal; margin-bottom: 20px; ">For any further assistance or queries, please email us at <a href="mailtio:<?php echo SITE_SENDER_EMAIL;?>"><?php echo SITE_SENDER_EMAIL;?></a></p></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="mail_container"><table width="100%">
        <tr>
          <td class="content footer" style="background: none;" align="center"><p style="margin-bottom: 0; color: #888; text-align: center; font-size: 14px; "> © Folk <?php echo date(Y);?>. All Rights Reserved. </p>
            </td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>