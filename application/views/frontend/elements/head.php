<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo ($title)?strip_html_tags($title):SITE_TITLE; ?> </title>
<meta name="keywords" content="<?php echo ($keywords)?strip_html_tags($keywords):SITE_META_KEYWORDS; ?>" />
<meta name="description" content="<?php echo ($description)?strip_html_tags($description):SITE_META_DESCRIPTION; ?>" />
<link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
<link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,400italic,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/font-awesome-4.5.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
<?php if(uri_string() != 'skill-development') { ?>
	<link href="<?php echo base_url()?>assets/frontend/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<?php } ?>
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link href="<?php echo base_url()?>assets/frontend/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>assets/frontend/css/responsive.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url()?>assets/frontend/js/jquery-1.11.3-min.js"></script>
<script src="<?php echo base_url()?>assets/frontend/js/jquery.cookie.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.lazyload.js"></script>
<script src="<?php echo base_url()?>assets/frontend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/frontend/js/custom.js"></script>
<link href="<?php echo base_url();?>assets/frontend/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>assets/frontend/js/jquery.validationEngine-en.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.validationEngine.js"></script>

<script src="<?php echo base_url()?>assets/frontend/js/common.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var baseUrl = $(".logo").find('a').attr('href');
	$("img.lazy").attr('src',baseUrl+'assets/frontend/images/large/loader.gif');
	$("img.lazy").addClass('lazyloader');
	$("img.lazy").lazyload({effect:'fadeIn'});		
})

</script>