<div class="modal fade mypoup" id="loginpopup" role="dialog" style="display:none;">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-body">
          <section class="logSec">
            <div class="logformSEc">
				<h4 class="modal-title">Login</h4>
				<div id="login_check" style="display:none"> <div class="alert alert-danger"> Your given credetial is wrong.Please put valid username and password.</div> </div>
				<?php echo form_open('User/login',array('method'=>'post','id'=>'login'));?>
					<div class="form-group">
						<?php echo form_input(array('name'=>'email_id', 'placeholder'=>"Email(*)",'class'=>'form-control validate[required,custom[email]]'));?>
					</div>
				  <div class="form-group">
				   <?php echo form_password(array('name'=>'password', 'placeholder'=>"Password(*)",'class'=>'form-control validate[required]'));
				   echo form_hidden('page_url',$this->router->fetch_method());
				   ?>

				  </div>
          <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LciY00UAAAAAGRsxWMRitOERCNDupjyWrqnGPEc"></div>
          </div>
				  <div class="logPopupSubmit">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Login', 'class'=>'btn btn-default poplogSubmitBtn','id'=>'registration_submit')); ?>
						<a href="javascript:void(0);" class="forgotPass" id="ForgotPopUp"> Forgot Password?</a>
				  </div>
				<div id="reg_loader"></div>
				<?php echo form_close(); ?>
				  <div class="newUser">
					<div class="newHere">New user ?</div>
					<a  class="logPopSignUp" id="SignupPopup">Signup</a>
				  </div>
            </div>
          </section>
        </div>
      </div>
    </div>
</div>
