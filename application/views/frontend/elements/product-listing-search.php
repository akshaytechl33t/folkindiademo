<div class="scrollContainer">
	<div class="span12 product__page">	
		<?php if(!empty($product_details)){
		foreach($product_details as $details):	
		?>		
		<a href="<?php echo base_url().'product-details/'.$details->seo_name;?>">
		<div class="span4 products_pictures clear_1">
			<div class="row-fluid contImagenBuscador">
			  <div class="img-product-list">
				<div class="catalog__image">
				  <div class="i1">
					<div class="thumbnail">
					  <div class="caption">
						<div class="captionInner">
						<?php /*?>
							<div class="capButtonArea"> 
							<a href="<?php echo base_url().'product-details/'.$details->seo_name;?>" class="viewAll">View Details</a> 
							<a href="javascript:void(0);" class="addTocart">Add to cart</a>
							</div>
						<?php */?>	
						</div>
					  </div>
					  <?php if(!empty($details->image2)){?>
					  <img class="prodct <?php echo ($details->qty == 0) ?'out_stock_gray':''?>" src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $details->image2;?>">
					  <?php } else { ?>
					  <img class="prodct <?php echo ($details->qty == 0) ?'out_stock_gray':''?>" src="<?php echo base_url();?>assets/frontend/images/no-image.jpg"/>
					  <?php } ?>
					   <?php if($details->qty == 0){?>
						<div class="out_stock"><span class="">Out Of Stock</span></div>
					  <?php } ?>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		<div class="searchResultContents">
		  <div class="searchResultData extras"> <span class="hide2Cols"></span>
		  <a href="<?php echo base_url().'product-details/'.$details->seo_name;?>">
			<h2 class="product_name"><?php echo $details->name?></h2>
		   </a> </div>
			  <?php if($product_details[0]->category_id != 4 && $product_details[0]->category_id != 5){?>
			<div class="searchResultPrice"> <wbr></wbr>
			<span style="padding-right: 3px;" class="txtEntero "></span><span style="" class="txtEntero money" data-inr="<?php echo $details->price?>">Rs.<?php echo $details->price?></span>
			</div>
			  <?php } ?>			
		</div>
		</div>
		</a>			
		<?php endforeach; } else { ?>                         
		<div class="span4 products_pictures clear_1 no-product">
			<div class="searchResultContents">
			  <div class="searchResultData extras"> <span class="hide2Cols"></span> 
				<h2 class="product_name">No product found!.</h2>
				 </div>						 
			</div>
		</div>
		<?php } ?>
	</div>
</div>