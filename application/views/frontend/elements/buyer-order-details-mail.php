<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width"/>
<title>WELCOME TO <?php echo SITE_NAME; ?></title>
</head>
<body style="">
<div style="width:100%;height:100%;margin:10px auto;padding:0;background-color:#ffffff;font-family:Arial,Tahoma,Verdana,sans-serif;font-weight:299px;font-size:13px;text-align:center" bgcolor="#ffffff">   
	<table class="" style="max-width:600px" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			<tr> 
			<td class="" style="width:10px;background-color:#FFFFFF" width="10" bgcolor="#027cd5">&nbsp;</td> 
			<td class="" style="background:#FFFFFF; color: white;padding:0;margin:0" valign="middle" bgcolor="#027cd5" align="left"> 
			<a href="<?php echo base_url();?>" style="text-decoration:none;outline:none;color:#ffffff;font-size:13px"> <img style="max-width:100%; margin: 0 auto; display: block;" alt="logo" src="<?php echo base_url();?>assets/frontend/images/large/logo.png"> </a> 
			</td> 			
			</tr> 
		</tbody>
	</table> 	  
	<table class="" style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6; border-top: 1px solid #e6e6e6;" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody>
			<tr> 
				<td class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px" valign="top" bgcolor="#F9F9F9" align="left"> 
				<p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> Hi <?php echo $rows['user_name']?>, </p>
				<br> 
				<p style="padding:0;margin:0;color:#565656;font-size:13px"> Thank you for your order!</p>		
				<br> 
				</td> 
			</tr> 
		</tbody>
	</table> 
	<table class="" style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody> 
			<tr> 
				<td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" valign="top" bgcolor="" align="left"> 
					<table style="margin:0" width="100%" cellspacing="0" cellpadding="0" border="0"> 
						<tbody>
							<tr> 
								<td colspan="4" width="100%" valign="top" align="left"> 

									<p style="padding:0;margin:0;color:#565656;line-height:22px;font-size:13px"> 
									 Please find below, the summary of your order and your order no & transaction no is 
									 <span style="color:#565656;font-size:13px"><?php echo $rows['mihpayid'] .'&nbsp;&nbsp;&&nbsp;&nbsp;'. $rows['txnid']?></span>			
									</p>
									<br> 
								</td> 
							</tr> 
						 </tbody>
					</table> 
				</td> 
			</tr> 
		</tbody> 
	</table>	   
	<?php if(!empty($rows['product_details'])){
		foreach($rows['product_details'] as $details):
	?>
	<table class="" style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody>
			<tr> 
			<td class="" style="background-color:#ffffff" width="350" valign="top" align="center"> 
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr> 
							<td style="padding-left:20px;text-align:center" width="40%" valign="middle" align="center"> 								
								<img src="<?php echo $details->product_image?>" class="" border="0" width="100"> 
							</td> 
							<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px" width="60%" valign="top" align="center"> 
								<p style="padding:0;margin:0;color:#848484;font-size:12px">Product name</p> 
								<p style="padding:0;margin:0">
									<span style="color:#565656;font-size:13px"><?php echo $details->product_name?></span>
								</p> 							  
							</td> 
						</tr>
					</tbody>
				</table> 
			</td> 
			<td class="" style="background-color:#ffffff" width="250" valign="top" align="center"> 
				<table width="100%" cellspacing="0" cellpadding="0" border="0"> <tbody>
				<tr> 			
				<td style="padding:12px 10px 0 10px;margin:0;text-align:center" width="33%" valign="top" align="center"> 

				<p style="padding:0;margin:0;color:#848484;font-size:12px">Attribute</p> 
					<p style="padding:7px 0 0 0;margin:0;color:#565656;font-size:13px"> <?php echo $details->product_color?><br><?php echo $details->category_name?> </p> 
				</td> 
				<td style="padding:12px 20px 0 10px;margin:0;text-align:center" width="33%" valign="top" align="center"> 

				<p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">Subtotal </p> 

				<p style="white-space:nowrap;padding:7px 0 0 0;margin:0;color:#565656;font-size:13px"> 
				 Rs. <?php echo $details->product_unit_price?>  
				 </p> 
				</td> 
				</tr> </tbody>
				</table> 
			</td> 
			</tr> 
		</tbody>
	</table> 
	<?php endforeach; } ?>	
 	<table class="" style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody>
			<tr> 
				<td class="" style="clear:both;display:block;margin:0 auto;padding:10px 20px 0 20px;background-color:#ffffff" valign="top" bgcolor="" align="right"> 
					<table width="100%" cellspacing="0" cellpadding="0"> 
						<tbody>
							<tr> 
							<td style="border-top:2px solid #565656;border-bottom:1px solid #e6e6e6;padding:15px 0;margin:0;background-color:#f9f9f9" valign="top" align="right"> 
							<p style="padding:0;margin:0;text-align:right;color:#565656;line-height:22px;white-space:nowrap;font-size:21px"> 
							 Total 

							<span style="font-size:21px">Rs. <?php echo number_format($rows['net_amount_debit'],2);?>
							</span> </p> 
							</td> 
							</tr> 
						</tbody>
					</table> 
				</td> 
			</tr> 
		</tbody>
	</table>	
	<table class="" style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody> 
			<tr> 
				<td class="" style="background-color:#ffffff;color:#565656;display:block;font-weight:300;margin:0;padding:0;clear:both" valign="top" bgcolor="#ffffff" align="left"> 
					<table width="100%" cellspacing="0" cellpadding="0">
							<tbody>
								<tr> 
									<td style="padding:20px 20px 0 20px;margin:0" valign="top" align="left"> 
										<p style="margin:0;padding:0;color:#565656;font-size:13px"> 
										DELIVERY ADDRESS 
										</p> 
										<p style="padding:0;margin:15px 0 10px 0;font-size:18px;color:#333333"> 
										<?php echo $rows['shipping_name']?>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $rows['shipping_phone']?> 
										</p> 
										<p style="line-height:18px;padding:0;margin:0;color:#565656;font-size:13px"> 
										 <?php echo $rows['shipping_street_address']?>
										<br><?php echo $rows['shipping_land_mark']?>
										<br><?php echo $rows['shipping_city']?>&nbsp;&nbsp; - &nbsp;&nbsp;<?php echo $rows['shipping_pincode']?>
										<br><?php echo $rows['shipping_state']?>&nbsp;&nbsp;,&nbsp;&nbsp;<?php echo $rows['shipping_country']?>
										</p> 
									</td> 
								</tr> 
							</tbody>
					</table> 
				</td> 
			</tr> 
		</tbody> 
	</table> 
	<table class="" style="max-width:600px;border:solid 1px #e6e6e6;border-top:none" width="100%" cellspacing="0" cellpadding="0"> 
		<tbody> 
			<tr> 
				<td style="text-align:center;background-color:#f9f9f9;display:block;margin:0 auto;clear:both;padding:15px 40px" valign="top" bgcolor="#F9F9F9" align="center">
					<p style="padding:0;margin:0 0 7px 0"> <a title="Folkindia .in" style="text-decoration:none;color:#565656" href="<?php echo base_url();?>"><span style="color:#565656;font-size:13px"><span class="">Folkindia</span>.in</span></a> </p> 
					<p style="padding:10px 0 0 0;margin:0;border-top:solid 1px #cccccc;font-size:11px;color:#565656"> 
					</p> 
				</td> 
			</tr> 
		</tbody> 
	</table>  
</div>
</body>
</html>