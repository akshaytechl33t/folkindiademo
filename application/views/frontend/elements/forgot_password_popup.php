<div class="modal fade mypoup" id="forgotpop" role="dialog" style="display:none;">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-body">
          <section class="logSec">          
            <div class="logformSEc">
				<h4 class="modal-title">Forgot Password</h4>
				<div id="forgot_check" style="display:none"> <div class="alert alert-danger">This email id is not registered!!</div> </div>
				<div id="forgot_s_msg" style="display:none"><div class="alert alert-success">Please check your mail for password.</div></div>
				<?php echo form_open('User/forgot',array('method'=>'post','id'=>'forgot'));?>
					<div class="form-group">
						<?php echo form_input(array('name'=>'email_id', 'placeholder'=>"Email(*)",'class'=>'form-control validate[required,custom[email]]'));?> 
					</div>				  				  				  
				 				  
				  <div class="logPopupSubmit">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-default poplogSubmitBtn','id'=>'forgot_submit')); ?>						
												
				  </div>	
				<div id="reg_loader"></div> 	
				<?php echo form_close(); ?> 
				  <div class="newUser">
					<div class="newHere">New user ?</div> 
					<a href="javascript:void(0);" class="logPopSignUp" id="SignupPopup">Signup</a>
					<a href="javascript:void(0);" class="logPopSignUp" id="loginPopUp">Login</a>
				  </div>
            </div>                                  
          </section>
        </div>        
      </div>      
    </div>
</div>