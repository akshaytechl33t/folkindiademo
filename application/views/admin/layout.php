<!DOCTYPE html>
<html>
    <!-- LOADING HEAD ELEMENT -->
    <?php echo $head; ?>
	<!-- LOADING HEAD ELEMENT -->
	
	<body class="skin-blue">
	
		<!-- LOADING HEADER ELEMENT -->
		<?php echo $header; ?>
		<!-- LOADING HEADER ELEMENT -->
		
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- LOADING LEFT SIDEBAR ELEMENT -->
			<?php echo $sidebar; ?>
			<!-- LOADING LEFT SIDEBAR ELEMENT -->
			
			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
				<!-- LOADING MAINCONTENT -->                
               <?php echo $maincontent; ?>
			   <!-- LOADING MAINCONTENT -->   
            </aside><!-- /.right-side -->
			
		</div><!-- ./wrapper -->
		
		<!-- LOADING FOOTER ELEMENT -->   
		<?php echo $footer; ?>
		<!-- LOADING FOOTER ELEMENT -->   

    </body>
</html>