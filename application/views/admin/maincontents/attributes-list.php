<section class="content-header">
  <h1><?php echo $page_title; ?><small></small></h1>
  <ol class="breadcrumb">
    <li>
      <?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
      <?php echo anchor('admin',$home_anchor); ?> </li>
    <li class="active"><?php echo $page_title; ?></li>
  </ol>
</section>
<div id="notifyMessage">
  <?php if($success_message){?>
  <section class="content gapp">
    <div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
      <b><?php echo $success_message; ?></b> </div>
  </section>
  <?php } ?>
  <?php if($error_message){?>
  <section class="content">
    <div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
      <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
      <b><?php echo $error_message; ?></b> </div>
  </section>
  <?php } ?>
</div>
<?php /*?><div class="box_horizontal"><?php echo anchor('admin/Attributes/add','Add New', array('class'=>'btn btn-primary btn-flat')); ?></div><*/?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body table-responsive">
          <table <?php if($rows){ echo 'id="data_table"'; } ?> class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Attributes Name</th>
                <th class="sorting_disabled">Status</th>
                <th class="sorting_disabled">Options</th>
              </tr>
            </thead>
            <tbody>
              <?php if(count($rows)>0){ ?>
              <?php foreach($rows as $row){ ?>
              <tr>
                <td><?php echo $row->title; ?></td>
                <td><?php
			   if( $row->published == '1')
			   { 
					$status = '<i class="fa fa-check-circle fa-lg" title="Click to unpublish"></i>'; 
					$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'0');
			   }
			   elseif( $row->published == '0')
			   { 
					 $status = '<i class="fa fa-times-circle fa-lg" title="Click to publish"></i>';
					$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'1');
			   } 
			   echo anchor('admin/Attributes/change_status/',$status, $attr);
			  ?>
                </td>
                <td align="left"><?php
						echo anchor('admin/Attributes/edit/'.$row->id,'<i class="fa fa-pencil"></i>', array('title'=>'Click to edit'));
				  ?>
                </td>
              </tr>
              <?php } ?>
              <?php }else{ ?>
              <tr>
                <td colspan="6" align="center">No record found !</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <!----- modal content --->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Attribute List</h4>
            </div>
			<?php echo form_open(current_url().'/edit_attributes',array('id'=>'attrFrm')); ?>
            <div class="modal-body">
				
			</div>
            <div class="modal-footer">
			<?php echo form_input(array('type'=>'submit','class'=>'btn btn-success','value'=>'Save'))?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
			<?php echo form_close();?>
          </div>
        </div>
      </div>
      <!---------- modal ------->
    </div>
  </div>
</section>
<script type="text/javascript">
	$(function() {
		$("#data_table").dataTable(
			{
				"aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
				"iDisplayLength": <?php echo DISPLAY_NUM_RESULTS;?> //Pagination limit
			}
		);
    	$("#example1").dataTable();
        $('#example2').dataTable({
        	"bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>