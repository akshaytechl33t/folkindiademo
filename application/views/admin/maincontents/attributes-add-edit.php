<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>		
	 <ol class="breadcrumb">
		<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?>
		</li>
			<li><?php echo anchor('admin/attributes/','Manage Attributes');?></li>						
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-11">
			<div class="box box-primary">
				<div class="box-header"></div>
				 <?php if($success_message){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
					<b><?php echo $success_message; ?></b>
				</div>
			<?php }	if($error_message){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
					<b><?php echo $error_message; ?></b>
				</div>
			<?php }	
				echo form_open('admin/Attributes/'.$action.'/'.$id,array('method'=>'post'));
				echo form_hidden('action',$action);
				echo form_hidden('id',$id);
			?>
				<div class="box-body">
					<div class="form-group">
						<?php
						echo form_label('Title<span class="required">*</span>','title');
						echo form_input(array('name'=>'title', 'value'=>$field['title'], 'class'=>'form-control','readonly'=>'readonly'));
						echo form_error('title');
						?>
					</div>
					<?php 
					if($this->uri->segment(2) != 'add'){
						if( $field['atr_value'] !=''){ 
							echo form_label('Values<span class="required">*</span>','value');
							$i = 0; 
							foreach($field['atr_value'] as $key=>$atr_values):
							$i++;					
								foreach($atr_values as $key=>$values):									
									echo '<div class="form-group ctr_form " id='.$i.' >';
									echo form_label('','');
									echo form_input(array('name'=>'value[]', 'value'=>$values,'id'=>$i,'class'=>"form-control form_width pull-left"));
									echo form_hidden('value_id[]',$key);	
									if(sizeof($field['atr_value']) <= $i){
										echo '<i title="Add more" class="fa fa-plus fea_icons add_attr_value"></i>';
									}	
									 echo '<i class="fa fa-minus-circle fea_icons delete_attr" title="Remove"  id='.$key.'></i>';	
									echo '</div>';
								endforeach;
							endforeach;
						}
					}
					else
					{
						
						echo '<div class="form-group ctr_form" id='.$i.'>';
						echo form_label('Value<span class="required">*</span>','value');
						echo form_input(array('name'=>'value[]', 'value'=>$values, 'id'=>$i,'class'=>"form-control form_width pull-left"));							
						echo '<i title="Add more" class="fa fa-plus fea_icons add_attr_value"></i> &nbsp <i class="fa fa-minus-circle fea_icons remove_attr_value" title="Remove"  id='.$i.'></i>';
						echo '</div>';
					}
					?>
					<div class="add_more_field"></div>		
										
					<div class="form-group">
						<?php echo form_label('Status','published'); ?><br />
						<?php
						echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
						echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';								
						?>
					</div>
				</div>
				<div class="box-footer">
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
					<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
					<?php echo anchor('admin/attributes','Back',array('class'=>'btn btn-primary'));?>
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>		
	</div>
</section>
<script>
$(document).ready(function(){
	$("input[type='checkbox'], input[type='radio']").iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal'
    });
	$('input[type="checkbox"][name="add_value"]').on('ifChecked',function(event){		
		if($(this).is('iCheck') != 1){			
			var text ='';		
			text +='<div class="form-group ctr_form" id="'+i+'">';
			//text +='<label for="title">Value<span class="required">*</span></label>';
			text +='<input type="text" class="form-control form_width pull-left" id="'+i+'" name="value[]" value="">';	
			text +='<input type="hidden" value="" id="'+i+'" name="value_id[]">';
			text +='<i title="Add more" class="fa fa-plus fea_icons add_attr_value"></i> &nbsp <i class="fa fa-minus-circle fea_icons remove_attr_value" title="Remove" id="'+i+'"></i>';
			text +='<div id="field'+i+'"></div>';
			text +='</div>';			
		i++;
		$('.add_more_field').append(text);
			$('.ctr_form').show();
			$('label[for=value]').show();
			$('input[type="submit"]').addClass('disabled');			
		}
		else
		{			
			$('input[type="submit"]').removeClass('disabled');			
		}		
	})	
	$('input[type="checkbox"][name="add_value"]').on('ifUnchecked',function(event){	
		$('.ctr_form').hide();
		$('label[for=value]').hide();
	})	
	var i=1;	
	$(document).on('click','.add_attr_value',function(){		
			var text ='';		
			text +='<div class="form-group ctr_form" id="'+i+'">';
			//text +='<label for="title">Value<span class="required">*</span></label>';
			text +='<input type="text" class="form-control form_width pull-left" id="'+i+'" name="value[]" value="">';	
			text +='<input type="hidden" value="" id="'+i+'" name="value_id[]">';
			text +='<i title="Add more" class="fa fa-plus fea_icons add_attr_value"></i> &nbsp <i class="fa fa-minus-circle fea_icons remove_attr_value" title="Remove" id="'+i+'"></i>';
			text +='<div id="field'+i+'"></div>';
			text +='</div>';			
		i++;
		$('.add_more_field').append(text);
	})
	$(document).on('click','.remove_attr_value',function(){
		var id = $(this).attr('id');
		$('#'+id).remove();
		if($('.ctr_form').length == 0){
			$('input[type="checkbox"][name="add_value"]').iCheck('enable');
			$('input[type="checkbox"][name="add_value"]').iCheck('uncheck');
			$('input[type="submit"]').removeClass('disabled');
		}		
	})
	$(document).on('keyup','.pull-left',function(){
		$(this).each(function(){
			var id = $(this).attr('id');
			var value = $(this).val();
			if( value == '' ){
				$('#field'+id).html('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">Value field cannot be blank</i></label></div>');
				$('input[type="submit"]').addClass('disabled');
				//$('input[type="checkbox"][name="add_value"]').iCheck('disable');
			} 
			else
			{
				//$('input[type="checkbox"][name="add_value"]').iCheck('enable');
				$('#field'+id).remove();
				$('input[type="submit"]').removeClass('disabled');	
			}			
		})
	})	
	$(document).on('click','.delete_attr',function(){
		var id = $(this).attr('id');
		$.post( "<?php echo base_url();?>admin/Attributes/delete_attribute", { id : id }, function( data ) {
			if(data !=''){
				location.reload();
			}
		}, "json");
	})
})
</script>