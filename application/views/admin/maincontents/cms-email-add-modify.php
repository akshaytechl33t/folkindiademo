          <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1><?php echo $action;?> Email Template <small></small></h1>
                        
                       
                    
                    <ol class="breadcrumb">
                        <li>
							<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            				<?php echo anchor('admin',$home_anchor); ?>
						</li>
                        <li><?php echo anchor('admin/cms_email','Manage CMS Email Templates');?></li>
						<li class="active"><?php echo $action; ?> Email Template </li>
                    </ol>
                </section>
                <!-- Main content -->
				<section class="content">
					<div class="row">
						<!-- left column -->
						<div class="col-md-11">
							<!-- general form elements -->
							<?php if($success_message){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-check"></i>
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
									<b><?php echo $success_message; ?></b>
								</div>
							<?php } ?>
							<?php if($error_message){?>
								<div class="alert alert-danger alert-dismissable">
									<i class="fa fa-ban"></i>
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
									<b><?php echo $error_message; ?></b>
								</div>
							<?php } ?>
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Email Variables</h3>
									<br /><br />
									<div>
									<table width="50%" border="1" bordercolor="#3C8DBC" align="left">
									<th>Variable</th>
									<th>Usage</th>
									<tbody>
									<tr><td>[LOGO]</td><td>Display site Logo</td></tr>
									<tr><td>[USERNAME]</td><td>Display User's Name(mail receiver's name)</td></tr>
									<tr><td>[NEWPASSWORD]</td><td>Display the new reset password</td></tr>
									<tr><td>[SITESIGNATURE]</td><td>Display site signature in the email</td></tr>
									<tr><td>[ITEMLIST]</td><td>Display the purchased item(s) in the mail</td></tr>
									<tr><td>[PASSWORD]</td><td>Display the user password in the mail.</td></tr>
									<tr>
									<td colspan="2">
									<font size="-1">
									<strong>Note:-</strong> 
									Do not change the spelling/name of the variable only change the position and email content and styling as per your need.
									</font>
									</td>
									</tr>
									</tbody>
									</table>
									</div>
								</div>
								<!-- form start -->
								<?php 
									echo form_open(current_url(), array('method'=>'post','name'=>'Form', 'accept-charset'=>'utf-8')); 
									echo form_hidden('id',$id);
									echo form_hidden('action',$action);
								?>
									<div class="box-body">
									<div class="form-group">
											<label for="exampleInputEmail1">Email Subject<span class="required">*</span></label>
											<?php echo  form_input(array('name'=>'email_templates_subject','value'=>$email_templates_subject,'placeholder'=>'Enter Subject','class'=>"form-control"))?>
											<?php echo form_error('email_templates_subject'); ?>
										</div>
										
										<div class="form-group">
											<label for="exampleInputEmail1">Email Content<span class="required">*</span></label>
											<?php echo call_editor('email_templates_content',html_entity_decode($email_templates_content),'','','','#E4ECEF'); ?>
                                            <!--$feilddata['email_templates_content'];-->
											<?php echo form_error('email_templates_content'); ?>
										</div>
																			
										<div class="radio">
											<label>
												<?php echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y));?> Publish  
												&nbsp;&nbsp;
												<?php echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n));?> Unpublish  
											</label>
										</div>
									</div>

									<div class="box-footer">
										<?php echo form_submit(array('name'=>'save', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
										<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
										<?php if($action=='Edit')echo anchor('admin/cms_email','Back',array('class'=>'btn btn-primary'));?>
									</div>
								<?php echo form_close(); ?>
							</div>


						</div><!--/.col (left) -->
						<!-- right column -->
						
					</div>   <!-- /.row -->
				</section><!-- /.content -->