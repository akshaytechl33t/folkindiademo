<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
	<ol class="breadcrumb">
		<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?> 
		</li>
		<li><?php echo anchor('admin/event/','Event');?></li>
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row"> 
	<!-- left column -->
		<div class="col-md-11"> 
		<!-- general form elements -->
			<div class="box box-primary">
					<div class="box-header"> 
					</div>
				<?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b> 
					</div>
				<?php }
				?>				
				<?php 
					echo form_open_multipart('admin/Event/'.$action.'/'.$id,array('method'=>'post'));
					echo form_hidden('action',$action);
					echo form_hidden('id',$id);
				?>
					<div class="box-body">	
						<div class="form-group"> 					
							<?php
							echo form_label('Title<span class="required">* </span>','title'); 
							echo form_input(array('type'=>'text','name'=>'title','class'=>'form-control','value'=>$field['title']));	
							echo form_error('title');
							?>
						</div>	
						<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/event/<?php echo $field['image'];?>" style="width:50%"/>
						</div>
						<div class="form-group"> 					 
						<?php
						echo form_label('Upload Image<span class="required">*  ( Image size must be 578 X 375 )</span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image[]','id'=>'event_image'));	
						echo form_input(array('name'=>'old_image[]','type'=>'hidden','value'=>$field['image']));
						?>
					</div>
					<?php } else { ?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image<span class="required">* ( Image size must be 578 X 375 )</span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image[]','id'=>'event_image'));	
						?>
					</div>
					<?php } ?>								
					<div class="form-group">
						<?php echo form_label('Short Description ','short_description');
						echo call_editor('short_description',$field['short_description'],'','','','#E4ECEF'); 
						echo form_error('short_description');
						?>																		
					</div>
					<div class="form-group">
						<?php echo form_label('Long Description ','long_description');
						echo call_editor('long_description',$field['long_description'],'','','','#E4ECEF'); 
						echo form_error('long_description');
						?>																		
					</div>
					<div class="form-group"> 					
						<?php
						echo form_label('Meta Title','meta_title'); 
						echo form_input(array('type'=>'text','name'=>'meta_title','class'=>'form-control','value'=>$field['meta_title']));	
						?>
					</div>	
					<div class="form-group">
						<?php echo form_label('Meta keywords ','meta_keywords');
						echo call_editor('meta_keywords',$field['meta_keywords'],'','','','#E4ECEF'); 
						echo form_error('meta_keywords');
						?>																		
					</div>
					<div class="form-group">
						<?php echo form_label('Meta Description ','meta_keywords');
						echo call_editor('meta_description',$field['meta_description'],'','','','#E4ECEF'); 
						echo form_error('meta_description');
						?>																		
					</div>
					<div class="form-group">
					<?php echo form_label('Status','published'); ?><br />
					<?php
					echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y)).'&nbsp; Publish &nbsp;&nbsp;';
					echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n)).'&nbsp;Unpublish';		
					?>
					</div>
				</div> 
				<div class="box-footer"> 
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?> 
					<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?> <?php echo anchor('admin/event','Back',array('class'=>'btn btn-primary'));?> 
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>
	</div>
<!-- /.row --> 
</section>
<script>
$(document).ready(function(){
	
var _URL = window.URL;
$("#event_image").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 578 || $h < 375){
					alert('Image size should be  578 X 375 .');
					$('#event_image').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
})
</script>