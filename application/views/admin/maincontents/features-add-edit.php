<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
	 <ol class="breadcrumb">
		<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?>
		</li>			
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-11">
			<div class="box box-primary">
				<div class="box-header">
				</div>
			<?php
			if($success_message){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
					<b><?php echo $success_message; ?></b>
				</div>
			<?php }
			if($error_message){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
					<b><?php echo $error_message; ?></b>
				</div>
			<?php }
				echo form_open('admin/Features/'.$action.'/'.$id,array('method'=>'post'));
				echo form_hidden('action',$action);
				echo form_hidden('id',$id);
			?>
				<div class="box-body">
					<div class="form-group">
						<?php
						echo form_label('Title<span class="required">*</span>','title');
						echo form_input(array('name'=>'title', 'value'=>$field['title'], 'class'=>'form-control'));
						echo form_error('title');
						?>
					</div>	
					
					<?php if($action == 'edit'){
						if(!empty($field['features_value'])){ $i = 0; ?>
							<div class="form-group ctr_form" id="<?php echo $i;?>">
							<?php echo form_label('Value <span class="required">*</span>','value');?>
								<?php foreach($field['features_value'] as $features_value): $i++;
										foreach($features_value as $key=>$values):?>
									<div id="<?php echo $key;?>">
										<?php echo form_label('','');
										echo form_input(array('name'=>'value[]','value'=>$values,'id'=>'value'.$key.'','class'=>'form-control form_width pull-left'));
										if(sizeof($field['features_value'])>1 && $i>1)
										{
											echo '<i class="fa fa-minus-circle fea_icons edit_new_feature_remove" title="Remove" id='.$key.'></i>';
										}
										if(sizeof($field['features_value']) <= $i){
										echo '<i class="fa fa-plus fea_icons new_feature_field" title="Add more"></i>';
										}				
										echo form_hidden('value_id[]',$key);
										?>
									</div>	
									<?php endforeach;
								endforeach; ?>
							</div>
					<?php }	} else { ?> 					
					<div class="form-group ctr_form" id="0">						
						<?php 
						echo form_label('Value <span class="required">*</span>','value');
						echo form_input(array('name'=>'value[]','value'=>$field['value'][0],'id'=>'value0','class'=>'form-control form_width pull-left')).'<i class="fa fa-plus fea_icons new_feature_field" title="Add more"></i>';
						echo form_error('value[]');
						?>
					</div>
					<?php } ?>
					<div class="add_more_field"></div>	
					<div class="form-group">
						<?php echo form_label('Status','published'); ?><br />
						<?php
						echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
						echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';								
						?>
					</div>
				</div>
				<div class="box-footer">
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
					<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
					<?php echo anchor('admin/features','Back',array('class'=>'btn btn-primary'));?>
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>		
	</div>
</section>
<script>
$(document).ready(function(){
	var i=1;
	$(document).on('click','.new_feature_field',function(){		
		var text ='';
		text +='<div class="form-group ctr_form" id="'+i+'">';
		//text +='<label for="title">Value</label>';
		text +='<input type="text" class="form-control form_width pull-left" id="value'+i+'" name="value[]">';
		text +='<input type="hidden" value="" name="value_id[]">';
		text +='<i class="fa fa-minus-circle fea_icons new_feature_remove" title="Remove" id="'+i+'"></i> &nbsp <i title="Add more" class="fa fa-plus fea_icons new_feature_field"></i>';
		text +='</div>';
		i++;
		$('.add_more_field').append(text);
	})
	$(document).on('click','.new_feature_remove',function(){		
		var id = $(this).attr('id');
		$('#'+id).remove();
	})
	$('.edit_new_feature_remove').click(function()
	{
			var id = $(this).attr('id');
			$.ajax({
				type: 'post',
				url : '<?php echo base_url();?>admin/Ajax/edit_new_feature_remove',
				data:{ id: $(this).attr('id')},
				dataType : 'json',
				success:function(data){
					if(data.flag =='success'){

						location.reload();
					}
				}
			});
	});
	
})
</script>