<section class="content-header">
	<h1><?php echo $page_title; ?> <small></small></h1>
    <ol class="breadcrumb">
        <li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            <?php echo anchor('admin',$home_anchor); ?>
		</li>        
        <li class="active"><?php echo $page_title; ?></li>
    </ol>
</section>
<div id="notifyMessage">
	<?php if($success_message){?>
		<section class="content">
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $success_message; ?></b>
			</div>
		</section>
	<?php } ?>
	<?php if($error_message){?>
		<section class="content">
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $error_message; ?></b>
			</div>
		</section>
	<?php } ?>
</div>
<!-- Main content -->
<section class="content">
	<div class="row">
    	<div class="col-xs-12">
        	<div class="box">
            	<!-- /.box-header -->
                <div class="box-body table-responsive">
				<?php if(!empty($rows)){?>
                	<table id="data_table" class="table table-bordered table-striped">
                    	<thead>
                        	<tr>
								<th width="25%">Email Subject</th>
								<th>Email Content</th>
                                <th class="sorting_disabled">Status</th>
                                <th class="sorting_disabled">Options</th>
                             </tr>
                          </thead><tbody>
						  <?php  foreach($rows as $row){?>
                          
                             <tr>
                                <td><?php echo $row->email_templates_subject;?></td>
                                <td><?php echo $row->email_templates_content;?></td>
								<td>
								<?php
                                       if( $row->published == '1')
									   {   $status = '<i class="fa fa-check-circle fa-lg" title="Click to unpublish"></i>'; 
                                           $attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'0');
                                       }
									   elseif( $row->published == '0')
									   {    $status = '<i class="fa fa-times-circle fa-lg" title="Click to publish"></i>';
                                            $attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'1');
                                       } 
									   echo anchor('admin/cms_email/change_status/',$status, $attr);
                                      ?>
								</td>
								<td><?php echo anchor('admin/cms_email/edit/'.$row->id,'<i class="fa fa-pencil"></i>','title="click to edit"');?></td>
                             </tr> 
						 <?php }?> 
						 </tbody>
                    </table>
					<?php }else{?>No Records Found.<?php }?>
                 </div><!-- /.box-body -->
             </div><!-- /.box -->
         </div>
     </div>
</section><!-- /.content -->
<script type="text/javascript">
	$(function() {
		$("#data_table").dataTable(
			{
				"aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
				"iDisplayLength": <?php echo DISPLAY_NUM_RESULTS;?> //Pagination limit
			}
		);
    	$("#example1").dataTable();
        $('#example2').dataTable({
        	"bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
	$(document).ready(function(){
		$("#del").click(function(e){
			return confirm('Are You Sure Want to Delete ?');
		});
	});
</script>