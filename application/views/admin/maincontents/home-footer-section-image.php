<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
    <ol class="breadcrumb">
        <li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
		</li>
        <li class="active"><?php echo $page_title; ?></li>
    </ol>
</section>
<div id="notifyMessage">
	<?php if($success_message){?>
		<section class="content gapp">
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $success_message; ?></b>
			</div>
		</section>
	<?php } ?>
	<?php if($error_message){?>
		<section class="content">
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $error_message; ?></b>
			</div>
		</section>
	<?php } ?>
</div>
<div class="box_horizontal"><?php echo anchor('admin/Home_footer_section_image/add','Add New', array('class'=>'btn btn-primary btn-flat')); ?></div>
<section class="content">
	<div class="row">
    	<div class="col-xs-12">
        	<div class="box">
                <div class="box-body table-responsive">
                	<table <?php if($rows){ echo 'id="data_table"'; } ?> class="table table-bordered table-striped">
                    	<thead>
                        	<tr>	
								<th>Image</th>
								                            	
                                <th>Link</th>   								
                                <th class="sorting_disabled">Status</th>
                                <th class="sorting_disabled">Options</th>
                             </tr>
                          </thead>
						  <tbody>
                            <?php if(count($rows)>0){ ?>
                                <?php foreach($rows as $row){ ?>
                                    <tr>
										<td><img src="<?php echo base_url();?>uploads/images/home/footer_section_image/<?php echo $row->images;?>" style="width:50%"></td> 									
										<td><?php echo $row->link; ?></td>
										<td>
											<?php
											if( $row->published == '1')
											{ 
												$status = '<i class="fa fa-check-circle fa-lg" title="Click to unpublish"></i>'; 
												$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'0');
											}
											elseif( $row->published == '0')
											{ 
												$status = '<i class="fa fa-times-circle fa-lg" title="Click to publish"></i>';
												$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'1');
											} 
											echo anchor('admin/Home_footer_section_image/change_status/',$status, $attr);
											?>
										</td>
										<td align="left">
											<?php
											echo anchor('admin/Home_footer_section_image/edit/'.$row->id,'<i class="fa fa-pencil"></i>', array('title'=>'Click to edit')).nbs(3);
											echo anchor('admin/Home_footer_section_image/delete/'.$row->id,'<i class="fa fa-trash-o"></i>', array('title'=>'Click to delete', 'onclick'=>"return confirmDelete('','');"));
											?>
										</td>
                                    </tr>
                                    <?php } ?>
                                <?php }else{ ?>
                                	<tr><td colspan="6" align="center">No record found !</td></tr>
                                <?php } ?>
                          </tbody>
                    </table>
                 </div>
             </div>
         </div>
     </div>
</section>
<script type="text/javascript">
	$(function() {
		$("#data_table").dataTable(
			{
				"aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
				"iDisplayLength": <?php echo DISPLAY_NUM_RESULTS;?> //Pagination limit
			}
		);
    	$("#example1").dataTable();
        $('#example2').dataTable({
        	"bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>