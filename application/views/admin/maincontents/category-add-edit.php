<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $page_title; ?><small></small></h1>
			
		 <ol class="breadcrumb">
			<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
			</li>
			<li><?php echo anchor('admin/category/','Manage Category');?></li>			
			<li class="active"><?php echo $page_title; ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-11">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<!--<h3 class="box-title">Quick Example</h3>-->
					</div>
					 <?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php foreach($error_message as $get_error_msg):	 echo $get_error_msg.'</br>'; endforeach;?></b>
					</div>
				<?php }
				?>
					<!-- form start -->
					<?php 
						echo form_open_multipart('admin/Category/'.$action.'/'.$id,array('method'=>'post'));
						echo form_hidden('action',$action);
						echo form_hidden('id',$id);
					?>
						<div class="box-body">                          	                                                    
							<div class="form-group">
								<?php
								echo form_label('Name<span class="required">*</span>','name');
								echo form_input(array('name'=>'name', 'value'=>$field['name'], 'class'=>'form-control','id'=>'name'));
								echo form_error('name');
								?>
							</div>  
							<div class="form-group"> 
							<?php
								echo form_label('Meta title','meta_title'); 
								echo form_textarea(array('name'=>'meta_title','class'=>'form-control','id'=>'meta_title','value'=>$field['meta_title']));	
								?>
							</div>
							<div class="form-group"> 
							<?php
								echo form_label('Meta keywords','meta_keywords'); 
								echo form_textarea(array('name'=>'meta_keywords','class'=>'form-control','id'=>'meta_keywords','value'=>$field['meta_keywords']));	
								?>
							</div>
							<div class="form-group"> 
							<?php
								echo form_label('Meta description','meta_description'); 
								echo form_textarea(array('name'=>'meta_description','class'=>'form-control','id'=>'meta_description','value'=>$field['meta_description']));	
								?>
							</div>
							<div class="form-group">
								<?php echo form_label('Status','published'); ?><br />
								<?php
								echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
								echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';								
								?>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
							<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
							<?php echo anchor('admin/category','Back',array('class'=>'btn btn-primary'));?>
						</div>
					<?php echo form_close(); ?>
				</div><!-- /.box -->
			</div><!--/.col (left) -->
			<!-- right column -->			
		</div>   <!-- /.row -->
	</section><!-- /.content -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('#name').keyup(function(){
			$('#meta_title').html($(this).val());
		})		
		/* var editorShortDescription = CKEDITOR.instances.short_description; //reference to instance		
		var editorSpecification = CKEDITOR.instances.specification; //reference to instance	
		
		if (editorShortDescription.name === 'short_description') { 		
			editorShortDescription.on('blur', function(evt) {
				$('#meta_keywords').html(editorShortDescription.getData());
			});
		}
		if (editorSpecification.name === 'specification') { 		
			editorSpecification.on('blur', function(evt) {
				$('#meta_description').html(editorSpecification.getData());
			});
		}	 */	
	})
	</script>