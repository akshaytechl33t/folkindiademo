<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $page_title; ?><small></small></h1>
			
		 <ol class="breadcrumb">
			<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
			</li>			
			<li><?php echo anchor('admin/country/','Manage Country');?></li>						
			<li class="active"><?php echo $page_title; ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-11">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<!--<h3 class="box-title">Quick Example</h3>-->
					</div>
					 <?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b>
					</div>
				<?php }
				?>
					<!-- form start -->
					<?php 
						echo form_open('admin/Country/'.$action.'/'.$id,array('method'=>'post'));
						echo form_hidden('action',$action);
						echo form_hidden('id',$id);
					?>
						<div class="box-body">     
							<div class="form-group">
								<?php
								echo form_label('Name<span class="required">*</span>','state_name');
								echo form_input(array('name'=>'name', 'value'=>$field['name'], 'class'=>'form-control'));
								echo form_error('name');
								?>
							</div>
							<div class="form-group">
								<?php
								echo form_label('Code<span class="required">*</span>','state_name');
								echo form_input(array('name'=>'code', 'value'=>$field['code'], 'class'=>'form-control'));
								echo form_error('code');
								?>
							</div>
							<div class="form-group">
								<?php echo form_label('Status','published'); ?><br />
								<?php
								echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
								echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';								
								?>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
							<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
							<?php echo anchor('admin/country','Back',array('class'=>'btn btn-primary'));?>
						</div>
					<?php echo form_close(); ?>
				</div><!-- /.box -->
			</div><!--/.col (left) -->
			<!-- right column -->			
		</div>   <!-- /.row -->
	</section><!-- /.content -->
