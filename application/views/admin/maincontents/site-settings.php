<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Site Settings <small></small></h1>
   
	<ol class="breadcrumb">
    	<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            <?php echo anchor('admin',$home_anchor); ?>
        </li>
        <li class="active">Site settings</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

	<?php if($this->session->flashdata('success_message')){?>
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<b><?php echo $this->session->flashdata('success_message'); ?></b>
		</div>
	<?php } ?>
	<?php if($this->session->flashdata('error_message')){?>
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<b><?php echo $this->session->flashdata('error_message'); ?></b>
		</div>
	<?php } ?>
	<div class="row">
		<!-- left column -->
		<div class="col-md-11">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<!--<h3 class="box-title">Quick Example</h3>-->
				</div>
				<!-- form start -->
				<?php 
					echo form_open_multipart(current_url(), array('method'=>'post','name'=>'Form', 'accept-charset'=>'utf-8')); 
					echo form_hidden('id',1);
				?>
					<div class="box-body">
                    
						<div class="form-group">
							<label for="exampleInputEmail1">Site Name<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_name', 'value'=>$row->site_name, 'class'=>'form-control','id'=>'exampleInputEmail1','placeholder'=>"Enter Site Name")); ?>
							<?php echo form_error('site_name'); ?>
						</div>
										
						<div class="form-group">
							<label for="exampleInputEmail1">Site Sender Name<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_sender_name', 'value'=>$row->site_sender_name, 'class'=>'form-control','placeholder'=>"Enter Sender Name")); ?>
							<?php echo form_error('site_sender_name'); ?>
						</div>
						
						<div class="form-group">
							<label for="exampleInputEmail1">Site Sender Email ID<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_sender_email', 'value'=>$row->site_sender_email, 'class'=>'form-control','placeholder'=>"Enter Sender Email ID")); ?>
							<?php echo form_error('site_sender_email'); ?>
						</div>				
										
						<div class="form-group">
							<label for="exampleInputEmail1">Site Receiver Email ID<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_receiver_email', 'value'=>$row->site_receiver_email, 'class'=>'form-control','placeholder'=>"Enter Receiver Email ID")); ?>
							<?php echo form_error('site_receiver_email'); ?>
						</div>			
						<div class="form-group">
							<label for="exampleInputEmail1">Site Receiver Email ID two</label>
							<?php echo form_input(array('name'=>'site_receiver_email2', 'value'=>$row->site_receiver_email2, 'class'=>'form-control','placeholder'=>"Enter Receiver Email ID two")); ?>
							<?php echo form_error('site_receiver_email2'); ?>
						</div>
						<div class="form-group">
							<label>Site Address<span style="color:red">*</span></label>
							<?php echo form_textarea(array('name'=>'site_address', 'value'=>$row->site_address, 'class'=>'form-control','placeholder'=>"Site Address",'rows'=>3)); ?>							                           
							<?php echo form_error('site_address'); ?>
						</div>						
						<div class="form-group">
							<label>Site Contact Details<span style="color:red">*</span></label>
							<?php echo form_textarea(array('name'=>'site_contact_detail', 'value'=>$row->site_contact_detail, 'class'=>'form-control','placeholder'=>"Site Contact detail",'rows'=>3)); ?>							
							<?php echo form_error('site_contact_detail'); ?>
						</div>	
						<div class="form-group">
							<label>Site Contact Details1<span style="color:red">*</span></label>
							<?php echo form_textarea(array('name'=>'site_contact_detail1', 'value'=>$row->site_contact_detail1, 'class'=>'form-control','placeholder'=>"Site Contact detail 1",'rows'=>3)); ?>							
							<?php echo form_error('site_contact_detail1'); ?>
						</div>	
						<div class="form-group">
							<label for="exampleInputEmail1">Site Phone 1<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_phone_1', 'value'=>$row->site_phone_1, 'class'=>'form-control','placeholder'=>"Enter Phone 1")); ?>
							<?php echo form_error('site_phone_1'); ?>
						</div>
                        
                        <div class="form-group">
							<label for="exampleInputEmail1">Site Phone 2</label>
							<?php echo form_input(array('name'=>'site_phone_2', 'value'=>$row->site_phone_2, 'class'=>'form-control','placeholder'=>"Enter Phone 2")); ?>
						</div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Site Fax number</label>
							<?php echo form_input(array('name'=>'site_fax_number', 'value'=>$row->site_fax_number, 'class'=>'form-control','placeholder'=>"Site Fax numer")); ?>
						</div>
                        <div class="form-group">
							<label for="exampleInputEmail1">Site First Set Of Results ( Pagination Value )<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_first_set_of_results', 'value'=>$row->site_first_set_of_results, 'class'=>'form-control','placeholder'=>"Enter First Set Of Results ( Pagination Value )")); ?>
							<?php echo form_error('site_first_set_of_results'); ?>
						</div>
                        
                        <div class="form-group">
							<label for="exampleInputEmail1">Linkedin Link<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_linkedin', 'value'=>$row->site_linkedin, 'class'=>'form-control','placeholder'=>"Enter Linkedin Link")); ?>
							<?php echo form_error('site_linkedin'); ?>
						</div>
                        
                        <div class="form-group">
							<label for="exampleInputEmail1">Facebook Link<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_facebook_link', 'value'=>$row->site_facebook_link, 'class'=>'form-control','placeholder'=>"Enter Facebook Link")); ?>
							<?php echo form_error('site_facebook_link'); ?>
						</div>
                        
                        <div class="form-group">
							<label for="exampleInputEmail1">Twitter Link<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_twitter_link', 'value'=>$row->site_twitter_link, 'class'=>'form-control','placeholder'=>"Enter Twitter Link")); ?>
							<?php echo form_error('site_twitter_link'); ?>
						</div>			
						
						<div class="form-group">
							<label for="exampleInputEmail1">Paypal Payment Mode<span style="color:red">*</span></label></br>
							<?php echo form_radio(array('name'=> 'site_paypal_mode','value'=> 'S','checked'=>$sandbox,'class'=>'paypal_mode')).'&nbsp; Sandbox &nbsp;&nbsp;';
							echo form_radio(array('name'=> 'site_paypal_mode','value'=> 'L','checked'=>$live,'class'=>'paypal_mode')).'&nbsp;Live';?>
							<?php echo form_error('site_paypal_mode'); ?>
						</div>
						<div class="form-group">
							<label>Paypal url<span style="color:red">*</span></label>
                            <?php echo form_input(array('name'=>'site_paypal_url', 'id'=>'paypal_url', 'value'=>$row->site_paypal_url, 'class'=>'form-control','placeholder'=>"Enter Paypal url",'readonly'=>'readonly')); ?>
							<?php echo form_error('site_paypal_url '); ?>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Paypal merchant email id<span style="color:red">*</span></label>
							<?php echo form_input(array('name'=>'site_paypal_merchant_email_id', 'value'=>$row->site_paypal_merchant_email_id, 'class'=>'form-control','placeholder'=>"Paypal merchant email id")); ?>
							<?php echo form_error('site_paypal_merchant_email_id'); ?>
						</div>
                        <div class="form-group">
							<label>Site Title<span style="color:red">*</span></label>
                            <?php echo form_input(array('name'=>'site_title', 'value'=>$row->site_title, 'class'=>'form-control','placeholder'=>"Enter Site Title",'rows'=>3)); ?>
							<?php echo form_error('site_title'); ?>
						</div>
                        
                        <div class="form-group">
							<label>Site Meta Keywords<span style="color:red">*</span></label>
                            <?php echo form_textarea(array('name'=>'site_meta_keywords', 'value'=>$row->site_meta_keywords, 'class'=>'form-control','placeholder'=>"Enter Meta Keywords",'rows'=>3)); ?>
							<?php echo form_error('site_meta_keywords'); ?>
						</div>
                        
                        <div class="form-group">
							<label>Site Meta Description<span style="color:red">*</span></label>
                            <?php echo form_textarea(array('name'=>'site_meta_description', 'value'=>$row->site_meta_description, 'class'=>'form-control','placeholder'=>"Enter Meta Description",'rows'=>3)); ?>
							<?php echo form_error('site_meta_description'); ?>
						</div>
                        
					</div><!-- /.box-body -->

					<div class="box-footer">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
						<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
						<?php echo anchor('admin','Back',array('class'=>'btn btn-primary'));?>
					</div>
				<?php echo form_close(); ?>
			</div><!-- /.box -->
		</div><!--/.col (left) -->
		<!-- right column -->
						
	</div>   <!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function(){
	$('.paypal_mode').on('ifChecked',function() { 	
		if($(this).val() == 'S'){
			$('#paypal_url').val('https://www.sandbox.paypal.com/cgi-bin/webscr/');
		}
		if($(this).val() == 'L'){
			$('#paypal_url').val('https://www.paypal.com/cgi-bin/webscr/');
		}
	})
})
</script>