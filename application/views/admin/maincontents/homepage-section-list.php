<section class="content-header">
	<h1>Manage Homepage Section<small></small></h1>
    <ol class="breadcrumb">
        <li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            <?php echo anchor('admin',$home_anchor); ?>
		</li>
        <li class="active">Manage Homepage Section</li>
    </ol>
</section>
<div id="notifyMessage">
	<?php if($this->session->flashdata('success_message')){?>
		<section class="content gapp">
			<div class="alert alert-success alert-dismissable" style="margin-bottom:0px;">
				<i class="fa fa-check"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $this->session->flashdata('success_message'); ?></b>
			</div>
		</section>
	<?php } ?>
	<?php if($this->session->flashdata('error_message')){?>
		<section class="content">
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $this->session->flashdata('error_message'); ?></b>
			</div>
		</section>
	<?php } ?>
</div>
<section class="content">
	<div class="row">
    	<div class="col-xs-12">
        	<div class="box">
                <div class="box-body table-responsive">
                	<table <?php if($rows){ echo 'id="data_table"'; } ?> class="table table-bordered table-striped">
                    	<thead>
                        	<tr>
                            	<th class="sorting_disabled">Section #</th>
								<th>Section Name</th>
                                <th class="sorting_disabled">Status</th>
                                <th class="sorting_disabled">Options</th>
                             </tr>
                          </thead>
						  <tbody>
						  	<?php if(!empty($rows)){?>
						  		<?php foreach($rows as $row){?>
									 <tr>
										<td><?php echo $row->order;?></td>
										<td>
										<?php echo $row->label;?>													
										</td>
										<td>
											<?php
												   if( $row->published == '1')
												   { 
														$status ='<i class="fa fa-check-circle fa-lg" title="Click to unpublish"></i>';
														$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'0');
												   }
												   elseif ( $row->published == '0')
												   { 
														$status ='<i class="fa fa-times-circle fa-lg" title="Click to publish"></i>';
														$attr = array('rel'=>$row->id,'class'=>'change_status','id'=>'change_status_'.$row->id,'data-rel'=>'1');
												   } 
												   echo anchor('admin/homepage_section/change_status/',$status, $attr);			
											 ?>
										</td>
										<td>											
											<?php
											if($row->id==1) { 
												echo anchor('admin/banner','Configure','title="Click to Configure" target="_blank"').nbs(3);
											} 
											if($row->id==2)
											{ 
												echo anchor('admin/home_featue_product','Configure','title="Click to Configure" target="_blank"').nbs(3);												
											} 
											if($row->id==3)
											{ 
												echo anchor('admin/home_footer_section_image','Configure','title="Click to Configure" target="_blank"').nbs(3);												
											}
											?>		
										</td>
									 </tr> 
						 	<?php }?> 
							<?php }else{ ?>
								<tr><td colspan="6" align="center">No record found !</td></tr>
							<?php }?>
						 </tbody>
                    </table>
                 </div>
             </div>
         </div>
     </div>
</section>
<script type="text/javascript">
	$(function() {
		$("#data_table").dataTable(
			{
				"aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
				"iDisplayLength": <?php echo DISPLAY_NUM_RESULTS;?> //Pagination limit
			}
		);
    	$("#example1").dataTable();
        $('#example2').dataTable({
        	"bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>