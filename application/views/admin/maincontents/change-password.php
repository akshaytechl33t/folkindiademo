<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Change Password <small></small></h1>
	<ol class="breadcrumb">
    	<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            <?php echo anchor('admin',$home_anchor); ?>
        </li>
        <li class="active">Change Password</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">

	<?php if($this->session->flashdata('success_message')){?>
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<b><?php echo $this->session->flashdata('success_message'); ?></b>
		</div>
	<?php } ?>
	<?php if($this->session->flashdata('error_message')){?>
		<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			<b><?php echo $this->session->flashdata('error_message'); ?></b>
		</div>
	<?php } ?>
	
	<div class="row">
		<!-- left column -->
		<div class="col-md-11">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<!--<h3 class="box-title">Quick Example</h3>-->
				</div>
				<!-- form start -->
				<?php 
					echo form_open(current_url(), array('method'=>'post','name'=>'Form', 'accept-charset'=>'utf-8')); 
					//echo form_hidden('id',$id);
				?>
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Old Password</label>
							<?php echo form_password(array('name'=>'user_old_password','class'=>'form-control','placeholder'=>"Enter Old Password")); ?>
							<?php echo form_error('user_old_password'); ?>
						</div>
										
						<div class="form-group">
							<label for="exampleInputEmail1">New Password</label>
							<?php echo form_password(array('name'=>'user_new_password', 'class'=>'form-control','placeholder'=>"Enter New Password")); ?>
							<?php echo form_error('user_new_password'); ?>
						</div>
										
										
						<div class="form-group">
							<label for="exampleInputEmail1">Confirm Password</label>
							<?php echo form_password(array('name'=>'user_confirm_password', 'class'=>'form-control','placeholder'=>"Confirm New Password")); ?>
							<?php echo form_error('user_confirm_password'); ?>
						</div>
					</div><!-- /.box-body -->

					<div class="box-footer">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
						<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
						<?php echo anchor('admin','Back',array('class'=>'btn btn-primary'));?>
					</div>
				<?php echo form_close(); ?>
			</div><!-- /.box -->
		</div><!--/.col (left) -->
		<!-- right column -->
						
	</div>   <!-- /.row -->
</section><!-- /.content -->