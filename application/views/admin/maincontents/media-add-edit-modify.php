<section class="content-header">
	<h1><?php echo $view_page_title; ?><small></small></h1>
		
	 <ol class="breadcrumb">
		<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?>
		</li>
		<li><?php echo anchor('admin/static_page','Manage Static Pages'); ?></li>
		<li class="active"><?php echo $view_page_title; ?></li>
	</ol>
</section>
	
<section class="content">
	<div class="row">
		<div class="col-md-11">
			<div class="box box-primary">
				<div class="box-header"></div>
				<?php 
					$form_attr = array('method'=>'post');
					echo form_open('admin/Static_page/media_edit/'.$id,$form_attr);
					echo form_hidden('id',$id);
					echo form_hidden('action',$action);
				?>
					<div class="box-body">
						<div class="form-group">
							<?php echo form_label('Title','title'); ?><span class="required">*</span>
							<?php
								echo form_input(array('name'=>'title', 'id'=>'title', 'value'=>$title, 'class'=>'form-control'));
								echo form_error('title');
							?>
						</div>													
						<div class="form-group">
							<?php echo form_label('Content','content'); ?><span class="required">*</span>
							<?php echo call_editor('content',($content),'','','','#E4ECEF'); ?>
							<?php echo form_error('content'); ?>
						</div>
						
						<div class="form-group">
							<?php echo form_label('Status','published');?>
								<p>
									<?php echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y));?>&nbsp;&nbsp;Publish &nbsp;&nbsp;&nbsp;
									<?php echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n));?>&nbsp;&nbsp;Unpublish
								</p>
							<?php echo form_error('published'); ?>
						</div>
						
					</div>

					<div class="box-footer">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
						<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>						
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>


