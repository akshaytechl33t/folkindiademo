<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
    <ol class="breadcrumb">
        <li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
		</li>
        <li class="active"><?php echo $page_title; ?></li>
    </ol>
</section>
<div id="notifyMessage">
	<?php if($success_message){?>
		<section class="content gapp">
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $success_message; ?></b>
			</div>
		</section>
	<?php } ?>
	<?php if($error_message){?>
		<section class="content">
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				<b><?php echo $error_message; ?></b>
			</div>
		</section>
	<?php } ?>
</div>
<section class="content">
	<div class="row">
    	<div class="col-xs-12">
        	<div class="box">
                <div class="box-body table-responsive">
                	<table <?php if($rows){ echo 'id="data_table"'; } ?> class="table table-bordered table-striped">
                    	<thead>
                        	<tr>								
                            	<th>Name</th> 
 								<th>Company</th> 
								<th>Telephone</th> 
								<th>Email</th>
								<th>Enquiriy</th>
								<th>Message</th>
								<th>Date</th>
								<th class="sorting_disabled">Action</th>
                             </tr>
                          </thead>
						  <tbody>
                            <?php if(count($rows)>0){ ?>
                                <?php foreach($rows as $row){ ?>
                                    <tr>
									<td><?php echo $row->name?></td>
									<td><?php echo $row->company?></td>
									<td><?php echo $row->telephone?></td>
									<td><?php echo $row->email?></td>
									<td><?php echo $row->enquiry?></td>
									<td><?php echo $row->message?></td>
									<td><?php echo change_date_time_format($row->created); ?></td>  <td align="left">
                                      <?php
									  
                                      echo anchor('admin/Contact_us/delete/'.$row->id,'<i class="fa fa-trash-o"></i>', array('title'=>'Click to delete', 'onclick'=>"return confirmToDelete('Enquiry','');"));
                                      ?>
                                      </td>
                                    </tr>
                                    <?php } ?>
                                <?php }else{ ?>
                                	<tr><td colspan="7" align="center">No Enquiries found !</td></tr>
                                <?php } ?>
                          </tbody>
                    </table>
                 </div>
             </div>
         </div>
     </div>
</section>


	  
	  
	  

<script type="text/javascript">
	$(function() {
		$("#data_table").dataTable(
			{
				"aoColumnDefs" : [ { "bSortable" : false, "aTargets" : [ "sorting_disabled" ] } ],
				"iDisplayLength": <?php echo DISPLAY_NUM_RESULTS;?> //Pagination limit
			}
		);
    	$("#example1").dataTable();
        $('#example2').dataTable({
        	"bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>

<script>
	$(document).ready(function(e)
	{
		$(document).on('click','.show_details',function(e)
		{
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);
			var id = $(this).attr('rel');
			var hyper_link = $(this).attr('href');
			$.ajax
			({
				type:'POST',
				url:hyper_link,
				data: {id :id },
				success: function(data)
				{				
					$('#mbody').html(data);
					$('#myModal').modal('show');
				}
			});
		});
		$(document).on('click','.replyTo',function(e)
		{
			e.preventDefault ? e.preventDefault() : (e.returnValue = false);
			var id = $(this).attr('id');
			var replyto_name_subject = $(this).attr('rel');
			var split_arr = replyto_name_subject.split("_");
			$('#to').val(split_arr[0]);
			$('#name').val(split_arr[1]);
			$('#subject').val(split_arr[2]);
			$('#myModal').modal('hide');
			$('#myModal1').modal('show');
		});
		
	});
</script>