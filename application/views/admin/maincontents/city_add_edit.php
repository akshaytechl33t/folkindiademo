<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $page_title; ?> <small></small></h1>
			
		 <ol class="breadcrumb">
			<li>
				<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
			</li>
			<li><?php echo anchor('admin/city/listing/','Manage City'); ?></li>
			<li class="active"><?php echo $page_title; ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-11">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
					 <?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b>
					</div>
				<?php }
				?>
						<!--<h3 class="box-title">Quick Example</h3>-->
					</div>
					<!-- form start -->
					<?php 
						echo form_open('admin/city/'.$action.'/'.$id,array('method'=>'post'));
						echo form_hidden('action',$action);
                     	echo form_hidden('id',$id);
                     ?>
						<div class="box-body">
							<div class="form-group">
								<?php
								echo form_label('Country<span class="required">*</span>','country_name');
								echo form_dropdown(array('name'=>'country_id','class'=>'form-control country_id'),$country_list,$country_id);
								echo form_error('country_id');
								?>
							</div>
							<div class="form-group" id="state">
								<?php
								echo form_label('State<span class="required">*</span>','state_id');
								echo form_dropdown('state_id',$state_arr, $state_id, 'id="state_id" class="form-control state_id"');
								echo form_error('state_id');
								?>
							</div>							
							
							<div class="form-group">
								<?php
								echo form_label('City<span class="required">*</span>','city_name');
								echo form_input(array('name'=>'city_name', 'id'=>'city_name', 'value'=>$city_name, 'class'=>'form-control'));
								echo form_error('city_name');
								?>
							</div>
														
							<div class="form-group">
								<?php echo form_label('Status','published'); ?>
								<br />
								<?php 
									echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y)).'&nbsp;&nbsp;&nbsp;Publish &nbsp;&nbsp;&nbsp;';
									echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n)).'&nbsp;&nbsp;&nbsp;Unpublish';
									echo form_error('published');
								?>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
                            <?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));	?>	
							<?php echo "&nbsp;".anchor(base_url().'admin/city/listing/'.$id, 'Back',array('title'=>"Back",'class'=>'btn btn-primary'));?>
						</div>
					<?php echo form_close(); ?>
				</div><!-- /.box -->


			</div><!--/.col (left) -->
			<!-- right column -->
			
		</div>   <!-- /.row -->
	</section><!-- /.content -->
	