<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $page_title; ?><small></small></h1>
			
		 <ol class="breadcrumb">
			<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
			</li>
			<li><?php echo anchor('admin/state/','Manage State');?></li>
			<li class="active"><?php echo $page_title; ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-11">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<!--<h3 class="box-title">Quick Example</h3>-->
					</div>
					 <?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b>
					</div>
				<?php }
				?>
					<!-- form start -->
					<?php 
						$form_attr = array('method'=>'post');
						echo form_open('admin/state/'.$action.'/'.$id,$form_attr);
						echo form_hidden('action',$action);
						echo form_hidden('id',$id);
					?>
						<div class="box-body">
						<div class="form-group">
								<?php
								echo form_label('Country<span class="required">*</span>','country_id');
								echo form_dropdown(array('name'=>'country_id', 'id'=>'country_id','class'=>'form-control'),$country_list,$country_id);
								echo form_error('country_id');
								?>
							</div>						
							<div class="form-group">
								<?php
								echo form_label('State<span class="required">*</span>','state_name');
								echo form_input(array('name'=>'state_name', 'id'=>'state_name', 'value'=>$state_name,'placeholder'=>'Enter State Name...', 'class'=>'form-control'));
								echo form_error('state_name');
								?>
							</div>						
							
														
							<div class="form-group">
								<?php echo form_label('Status','published'); ?><br />
								<?php
								echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y)).'&nbsp; Publish &nbsp;&nbsp;';
								echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n)).'&nbsp;Unpublish';
								//echo form_error('published');
								?>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
							<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
							<?php echo anchor('admin/state','Back',array('class'=>'btn btn-primary'));?>
						</div>
					<?php echo form_close(); ?>
				</div><!-- /.box -->


			</div><!--/.col (left) -->
			<!-- right column -->
			
		</div>   <!-- /.row -->
	</section><!-- /.content -->