<section class="content-header">
	<h1><?php echo $view_page_title; ?><small></small></h1>
		
	 <ol class="breadcrumb">
		<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?>
		</li>
		<li><?php echo anchor('admin/static_page','Manage Static Pages'); ?></li>
		<li class="active"><?php echo $view_page_title; ?></li>
	</ol>
</section>
	
<section class="content">
	<div class="row">
		<div class="col-md-11">
			<div class="box box-primary">
				<div class="box-header"></div>
				<?php 
					$form_attr = array('method'=>'post');
					echo form_open('admin/static_page/'.$action.'/'.$id,$form_attr);
					echo form_hidden('id',$id);
					echo form_hidden('action',$action);
				?>
					<div class="box-body">
						<div class="form-group">
							<?php echo form_label('Name','page_name'); ?><span class="required">*</span>
							<?php
								echo form_input(array('name'=>'page_name', 'id'=>'page_name', 'value'=>$page_name, 'class'=>'form-control'));
								echo form_error('page_name');
							?>
						</div>						
						<div class="form-group">
							<?php echo form_label('Title','page_title'); ?><span class="required">*</span>
							<?php 
								echo form_input(array('name'=>'page_title', 'id'=>'page_title', 'value'=>$page_title, 'class'=>'form-control'));
								echo form_error('page_title');
							?>
						</div>						
						<div class="form-group">
							<?php echo form_label('Content','page_content'); ?><span class="required">*</span>
							<?php echo call_editor('page_content',($page_content),'','','','#E4ECEF'); ?>
							<?php echo form_error('page_content'); ?>
						</div>
						<div class="form-group">
							<?php echo form_label('Display into top menu navigation',''); ?>
							<?php echo form_checkbox(array('name'=>'top_menu', 'id'=>'top_menu', 'value'=>1,'checked'=>$checked_menu));?>
							<?php echo form_error('top_menu'); ?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Meta Title','meta_title');
							echo form_input(array('name'=>'meta_title', 'id'=>'meta_title', 'value'=>$page_meta_title, 'class'=>'form-control'));
							echo form_error('meta_title');
							?>
						</div>
						
						<div class="form-group">
							<?php
							echo form_label('Meta Keywords','meta_keyword');
							echo form_input(array('name'=>'meta_keyword', 'id'=>'meta_keyword', 'value'=>$page_meta_keyword, 'class'=>'form-control'));
							echo form_error('meta_keyword');
							?>
						</div>
						<div class="form-group">
							<?php
							echo form_label('Meta Description','meta_description');
							echo form_textarea(array('name'=>'meta_description', 'id'=>'meta_description', 'value'=>$page_meta_description, 'class'=>'form-control'));
							echo form_error('meta_description');
							?>
						</div>
						<div class="form-group">
							<?php echo form_label('Status','published');?>
								<p>
									<?php echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y));?>&nbsp;&nbsp;Publish &nbsp;&nbsp;&nbsp;
									<?php echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n));?>&nbsp;&nbsp;Unpublish
								</p>
							<?php echo form_error('published'); ?>
						</div>
						
					</div>

					<div class="box-footer">
						<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
						<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
						<?php echo anchor('admin/static_page','Back', array('title'=>'Back','class'=>'btn btn-primary btn-flat'));?>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>


