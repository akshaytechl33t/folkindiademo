<link href="<?php echo base_url();?>assets/frontend/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>assets/frontend/js/jquery.validationEngine-en.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/jquery.validationEngine.js"></script>
<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>			
	 <ol class="breadcrumb">
		<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?>
		</li>			
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-11">
						 <?php if($success_message){ ?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }	if($error_message){ ?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b>
					</div>
				<?php }	
					echo form_open_multipart('admin/Products/'.$action.'/'.$id,array('method'=>'post','id'=>'adminproduct'));
					echo form_hidden('action',$action);
					echo form_hidden('id',$id);
				?>
			<div class="box box-primary">
				<div class="box-header">
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
									
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Category<span class="required">*</span>','brand_id');?>
										<?php echo form_dropdown(array('name'=>'category_id','class'=>'form-control'),$category_arr,$field['category_id']);?>
										<?php echo form_error('category_id'); ?>								
									</div>
								</div>	
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Name<span class="required">*</span>','name');?>
										<?php echo form_input(array('name'=>'name', 'value'=>$field['name'], 'class'=>'form-control validate[required]'));?>
										<?php echo form_error('name'); ?>								
									</div>
								</div>	
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Bag no<span class="required">*</span>','bag_no');?>
										<?php echo form_input(array('name'=>'bag_no', 'value'=>$field['bag_no'], 'class'=>'form-control validate[required]'));?>
										<?php echo form_error('bag_no'); ?>								
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Qty <span class="required">*</span>','qty');?>
										<?php echo form_input(array('name'=>'qty', 'value'=>$field['qty'], 'class'=>'form-control validate[required,custom[number]]'));?>
										<?php echo form_error('qty'); ?>								
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('B2b qty limit <span class="required">*</span>','qty');?>
										<?php echo form_input(array('name'=>'b2b_qty_limit', 'value'=>$field['b2b_qty_limit'], 'class'=>'form-control validate[required,custom[number]]'));?>
										<?php echo form_error('b2b_qty_limit'); ?>								
									</div>
								</div>								
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Price <span class="required">*</span>','price');?>
										<?php echo form_input(array('name'=>'price', 'value'=>$field['price'], 'class'=>'form-control validate[required,custom[number]]'));?>
										<?php echo form_error('price'); ?>								
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Description <span class="required">*</span>','description');
										echo call_editor('description',$field['description'],'validate[required]','','','#E4ECEF'); 
										echo form_error('description');
										?>																		
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Material And washing instruction <span class="required">*</span>','description');
										echo call_editor('instruction',$field['instruction'],'validate[required]','','','#E4ECEF'); 
										echo form_error('instruction');
										?>																		
									</div>
								</div>									
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Weight<span class="required">*</span>','weight');?>
										<?php echo form_input(array('name'=>'weight', 'value'=>$field['weight'], 'class'=>'form-control validate[required]'));?>
										<?php echo form_error('weight'); ?>								
									</div>
								</div>								
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php echo form_label('Manufacture warranty<span class="required">*</span>','warranty');?>
												<?php echo form_input(array('name'=>'warranty', 'value'=>$field['warranty'], 'class'=>'form-control validate[required]'));?>
												<?php echo form_error('warranty'); ?>															
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo form_label('&nbsp');?>
												<?php echo form_dropdown('warranty_month', $warranty_arr, $field['warranty_month'],"class='form-control select-box validate[required]'");?>
												<?php echo form_error('warranty_month'); ?>			
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12 attr_inline">
									<div id="pd_attr_value"></div>	
									<div class="attr_feature">
										<div class="attribute_static">											
											<input type="checkbox" value="<?php echo ($field['check_attribute'] == 1)?1:''?>"  <?php echo ($field['check_attribute'] == 1)?'checked=checked':'';?> name="check_attribute" class="checkbox attribute_checkbox validate[required]">	
											<div id="check_attr_checkbox"><?php echo ($field['check_attribute'] == 1)?'<input class="checkbox attribute_checkbox" type="hidden" checked="" name="attr_checkbox" value="1">':''; ?></div>											
											<label for="attribute">Attribute <span class="required">* </span></label>
											<div class="popover bottom">
												<div class="arrow"></div>
												<?php if($action == 'add'){?>
													<div class="popover-content" id="attribute_checkbox_box">
														<div class="row">
															<div class="col-md-6 margin_btt">
																<div class="media-body">
																	Select attribute checkbox.
																</div>
															</div>										
														</div>
													</div>
												<?php } ?>
												<?php if($action == 'add'){	$id = "attribute"; $style = "display:none";	} else{ $id = '';$style ='';}?>
												<div class="popover-content" id="<?php echo $id;?>" style="<?php echo $style;?>">
													<div class="row">
														<div class="col-md-12 margin_btt">
															<div class="form-group pull-top ser_cont">
																<div class="">
																<?php if(!empty($attribute_array)){
																	$i=0;
																foreach($attribute_array as $key=>$attribute):
																foreach($attribute as $attribute_values):
																	if(!empty($field['edit_attribute_value_id']) && in_array($attribute_values->attribute_value_id,$field['edit_attribute_value_id'])){ $checked_marked = 'checked=checked;';} else { $checked_marked='';};
																	?>
																	<div class="aatr_container">
																		<div class="col-md-6">
																				<label><?php echo $key?></label>
																					<input type="checkbox" class="validate[minCheckbox[1]]" value="<?php echo $attribute_values->attribute_value_id?>" id="<?php echo $i;?>" <?php echo $checked_marked;?> name="attribute_value_id[]">
																					<span><?php echo $attribute_values->value;?></span>
																		</div>
																	
																	</div>
																<?php $i++;endforeach; endforeach; } ?>										
																</div>
															</div>
														</div>
													</div>
												</div>												
											</div>	
											
										</div>	
										<div class="feature_static">										
											<input type="checkbox" value="<?php echo ($field['check_feature'] == 1)?1:''?>"  <?php echo ($field['check_feature'] == 1)?'checked=checked':'';?> name="check_feature" class="checkbox feature_checkbox">
											<div id="check_feature_checkbox"><?php echo ($field['check_feature'] == 1)?'<input class="checkbox attribute_checkbox" type="hidden" checked="" name="feature_checkbox" value="1">':''; ?></div>									
											<label for="attribute">Feature</label>
											<div class="popover bottom">
												<div class="arrow"></div>
												<?php if($action == 'add'){?>
												<div class="popover-content" id="feature_checkbox_box">
													<div class="row">
														<div class="col-md-6 margin_btt">
															<div class="media-body">
																Select product category.
															</div>
														</div>										
													</div>
												</div>
												<?php } ?>
												<?php if($action == 'add'){	$id = "feature"; $style = "display:none";	} else{ $id = '';$style =''; } ?>
												<div class="popover-content" id="<?php echo $id;?>" style="<?php echo $style;?>">
													<div class="row">
														<div class="col-md-12 margin_btt">
														<?php if(!empty($feature_array)){
														$feature_prdct_count = 0;
														foreach($feature_array as $key=>$features):$feature_prdct_count++;	?>
															<div class="form-group pull-top ser_cont">
																<label><?php echo $key;?></label>
																<?php foreach($features as $key=>$features_value):
																
																if(!empty($field['edit_feature_value_id']) && in_array($features_value->features_value_id,$field['edit_feature_value_id'])){ $checked_marked = 'checked'; }else{ $checked_marked = ''; }
																?>
																<div class="row">
																	<div class="aatr_container">
																		<div class="col-md-4"><input type="radio" value="<?php echo $features_value->features_value_id;?>" name="features_value_id[<?php echo $feature_prdct_count;?>]"  <?php echo $checked_marked;?> ><span><?php echo $features_value->value;?></span>
																		</div>
																	</div>
																</div>
															<?php endforeach; ?>	
															</div>
														<?php endforeach; } ?>	
														</div>
													</div>
												</div>
											</div>																		
										</div>
										<div class="similar_product_static">										
											<input type="checkbox" value="<?php echo ($field['check_similar_product'] == 1)?1:''?>"  <?php echo ($field['check_similar_product'] == 1)?'checked=checked':'';?> name="check_similar_product" class="checkbox similar_product_checkbox_checked">
											
											<div id="similar_product_checkbox"><?php echo ($field['check_similar_product'] == 1)?'<input class="checkbox attribute_checkbox" type="hidden" checked="" name="similar_product_checkbox" value="1">':''; ?></div>
											<label for="attribute">Similar Product </label>
											<div class="popover bottom">
												<div class="arrow"></div>
												<div class="popover-content" id="similar-product-checkbox" style="<?php echo ($field['check_similar_product'] == 1)?'display:none':'display:block'?>" >
													<div class="row">
														<div class="col-md-6 margin_btt">
															<div class="media-body">
																Select product category.
															</div>
														</div>										
													</div>
												</div>
												<div class="popover-content" style="<?php echo ($field['check_similar_product'] == 1)?'display:block':'display:none'?>" id="similar-product">
													<div class="row">
														<div class="col-md-12 margin_btt">
														<?php if(!empty($similar_product_arr)){
														foreach($similar_product_arr as $similar_product):	
														if(!empty($field['edit_similar_product_id']) && in_array($similar_product->id,$field['edit_similar_product_id'])){ $checked_marked = 'checked'; }else{ $checked_marked = ''; }
														?>
															<div class="form-group pull-top ser_cont similar-product">
																<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-12">
																			<div class="media media_img">
																				<input type="checkbox" value="<?php echo $similar_product->id?>" <?php echo $checked_marked;?> name="similar_product_id[]">	
																				<div class="media-left pull-left">
																					&nbsp;</div>
																				<div class="media-body"><?php echo $similar_product->name?></div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php endforeach;} ?>	
														</div>
													</div>
												</div>
											</div>																		
										</div>
									</div>
								</div>	
							</div> 
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">

										<?php echo form_label('Meta tag <span class="required">*</span>','meta_tag');
										 echo form_input(array('name'=>'meta_tag', 'value'=>$field['meta_tag'], 'class'=>'form-control validate[required]'));
										 echo form_error('meta_tag');
										 ?>				
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Meta keywords <span class="required">*</span>','meta_keywords');
										echo call_editor('meta_keywords',$field['meta_keywords'],'validate[required]','','','#E4ECEF'); 
										echo form_error('meta_keywords');
										?>																		
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<?php echo form_label('Meta description <span class="required">*</span>','meta_description');
										echo call_editor('meta_description',$field['meta_description'],'validate[required]','','','#E4ECEF'); 
										echo form_error('meta_description');
										?>																		
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo form_label('Status','published'); ?><br />
                                        <?php
                                        echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
                                        echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';
                                        ?>
                                    </div>
                                </div>	
                                <div class="col-md-12">
                                    <div class="box-footer">
                                    <?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
                                    <?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
                                    <?php echo anchor('admin/products','Back',array('class'=>'btn btn-primary'));?>
                                    </div>
                                </div>	
							</div>	
						</div>									
					</div>	
				</div>				
			<?php echo form_close(); ?>
			</div>
		</div>			
	</div>
</section>
<script>
$(document).ready(function(){
	$(document).on('click',".attribute_checkbox",function(){	
		var isChecked = $(this).prop('checked');	
		if(isChecked == true){
			var text ='<input type="hidden" class="checkbox attribute_checkbox" value="1" name="attr_checkbox" checked>';
			$('#attribute_checkbox_box').css('display','none');
			$('#attribute').css('display','block');
			$('#check_attr_checkbox').html(text);
		}
		else
		{
			$('#attribute_checkbox_box').css('display','block');
			$('#attribute').css('display','none');
			$('#check_attr_checkbox').children().remove();
		}
	})
	$(document).on('click',".feature_checkbox",function(){	
		var isChecked = $(this).prop('checked');	
		if(isChecked == true){
			var text ='<input type="hidden" class="checkbox attribute_checkbox" value="1" name="feature_checkbox" checked>';
			$('#feature_checkbox_box').css('display','none');
			$('#feature').css('display','block');
			$('#check_feature_checkbox').html(text);
			
		}
		else
		{
			$('#feature_checkbox_box').css('display','block');
			$('#feature').css('display','none');
			$('#check_feature_checkbox').children().remove();
			
		}
	})
	$(document).on('click','.similar_product_checkbox_checked',function(){
		var isChecked = $(this).prop('checked');	
		if(isChecked == true){
			var text ='<input type="hidden" class="checkbox attribute_checkbox" value="1" name="similar_product_checkbox" checked>';
			$('#similar-product-checkbox').css('display','none');
			$('#similar-product').css('display','block');
			$('#similar_product_checkbox').html(text);
			
		}
		else
		{
			$('#similar-product-checkbox').css('display','block');
			$('#similar-product').css('display','none');
			$('#similar_product_checkbox').children().remove();
			
		}
	})
	$("#adminproduct").validationEngine(
		'attach', {promptPosition: "bottomLeft"}		
	);
})
</script>