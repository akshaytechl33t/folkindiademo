<?php if($this->session->flashdata('success_message')){?>
<script type="text/javascript">
$("#formLogin").effect("shake");	
</script>
<?php }?>
<div class="form-box" id="login-box">
	<?php echo form_open(current_url(), array('method'=>'post','id'=>'formLogin')); ?>
		<?php //echo form_hidden('user_type','SA'); ?>
		<div class="header">Sign In</div>
        <div class="body bg-gray">
		
			<?php if($this->session->flashdata('error_message')){ ?>
				<div class="form-group has-error" id="errorMessageShake">
					<label class="control-label" for="inputError">
						<i class="fa fa-times-circle-o"></i><?php echo $this->session->flashdata('error_message'); ?>
					</label>
				</div>
			<?php } ?>
			<?php if($this->session->flashdata('success_message')){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
					<b><?php echo $this->session->flashdata('success_message'); ?></b>
				</div>
			<?php } ?>
			
            <div class="form-group">
				<?php echo form_input(array('name'=>'user_email_id', 'value'=>get_cookie("usermail_cookie"), 'class'=>'form-control', 'placeholder'=>'User Email')); ?>
				<?php echo form_error('user_email_id'); ?>
            </div>
            <div class="form-group">
				<?php echo form_password(array('name'=>'user_password', 'value'=>get_cookie("password_cookie"), 'class'=>'form-control', 'placeholder'=>'Password')); ?>
				<?php echo form_error('user_password'); ?>
            </div>          
            <div class="form-group">
				<?php 
					if(get_cookie("usermail_cookie"))
					{ 
						echo form_input(array('type'=>"checkbox",'name'=>"remember_me",'value'=>1,'title'=>"Remember Me",'checked'=>TRUE));
					}
					else
					{ 
						echo form_input(array('type'=>"checkbox",'name'=>"remember_me",'value'=>1,'title'=>"Remember Me"));  
					} 
				?>
				&nbsp;Remember Me
            </div>
        </div>
        <div class="footer"> 
			<?php echo form_submit(array('name'=>'login', 'value'=>'Sign in', 'class'=>'btn bg-olive btn-block')); ?>
            <p><?php //echo anchor('admin/forgot-password','I forgot my password'); ?></p> 
        </div>
   <?php echo form_close(); ?>
</div>
