<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo $page_title; ?><small></small></h1>
			
		 <ol class="breadcrumb">
			<li><?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
            	<?php echo anchor('admin',$home_anchor); ?>
			</li>
			<li><?php echo anchor('admin/faq/','Manage Faq');?></li>			
			<li class="active"><?php echo $page_title; ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-11">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<!--<h3 class="box-title">Quick Example</h3>-->
					</div>
					 <?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php foreach($error_message as $get_error_msg):	 echo $get_error_msg.'</br>'; endforeach;?></b>
					</div>
				<?php }
				?>
					<!-- form start -->
					<?php 
						echo form_open_multipart('admin/Faq/'.$action.'/'.$id,array('method'=>'post'));
						echo form_hidden('action',$action);
						echo form_hidden('id',$id);
					?>
						<div class="box-body">                          	                                                    
							<div class="form-group">
								<?php
								echo form_label('question<span class="required">*</span>','question');
								echo form_input(array('name'=>'question', 'value'=>$field['question'], 'class'=>'form-control','id'=>'question'));
								echo form_error('question');
								?>
							</div>  
							<div class="form-group"> 
							<?php
								echo form_label('Answer','answer'); 
								echo form_textarea(array('name'=>'answer','class'=>'form-control','id'=>'answer','value'=>$field['answer']));	
								?>
							</div>							
							<div class="form-group">
								<?php echo form_label('Status','published'); ?><br />
								<?php
								echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$field['checked_y'])).'&nbsp; Publish &nbsp;&nbsp;';
								echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$field['checked_n'])).'&nbsp;Unpublish';								
								?>
							</div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?>
							<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?>
							<?php echo anchor('admin/faq','Back',array('class'=>'btn btn-primary'));?>
						</div>
					<?php echo form_close(); ?>
				</div><!-- /.box -->
			</div><!--/.col (left) -->
			<!-- right column -->			
		</div>   <!-- /.row -->
	</section><!-- /.content -->	