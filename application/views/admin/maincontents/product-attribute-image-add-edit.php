<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
	<ol class="breadcrumb">
		<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?> 
		</li>
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row"> 
	<!-- left column -->
		<div class="col-md-11"> 
		<!-- general form elements -->
			<div class="box box-primary">
					<div class="box-header"> 
					</div>
				<?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b> 
					</div>
				<?php }
				?>				
				<?php 
				if($action == 'add'){
					echo form_open_multipart('admin/Product_attribute_image/'.$action.'/'.$id,array('method'=>'post'));
				}
				else
				{
					echo form_open_multipart('admin/Product_attribute_image/'.$action.'/'.$product_id.'/'.$id,array('method'=>'post'));
				}					
					echo form_hidden('action',$action);
					echo form_hidden('id',$id);
				?>
					<div class="box-body">	
						<div class="form-group">
							<?php echo form_label('Color<span class="required">*</span>','color_id');?>
							<select name="color_id" class="form-control">
								<option value="">Select</option>
								<?php if(!empty($color_dropdown_arr)){
								foreach($color_dropdown_arr as $colors):?>
								<option value="<?php echo $colors->attribute_value_id?>" <?php echo ($colors->attribute_value_id == $field['attibute_value_id'])?'selected=selected':'';?>><?php echo $colors->value?></option>
								<?php endforeach;?>
								<?php } ?>
							</select>
							
							<?php echo form_error('color_id'); ?>								
						</div>
						
						<div class="form-group">
							<?php echo form_label('Sku<span class="required">*</span>','sku');?>
							<?php echo form_input(array('name'=>'sku', 'value'=>$field['sku'], 'class'=>'form-control validate[required]'));?>
							<?php echo form_error('sku'); ?>								
						</div>
						
						<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image1'];?>" style="width:10%"/>
						</div>
						<div class="form-group"> 					
						<?php
						echo form_label('Image1<span class="required">* ( Image size must be 1000 X 1399 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image1[]','id'=>'image1'));	
						echo form_input(array('name'=>'old_image1[]','type'=>'hidden','value'=>$field['image1']));
						?>
					</div>
					<?php } else { ?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image1<span class="required">* ( Image size must be 1000 X 1399 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image1[]','id'=>'image1'));	
						?>
					</div>
					<?php } ?>	
					<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image2'];?>" style="width:10%"/>
						</div>
						<div class="form-group"> 					
						<?php
						echo form_label('Image2<span class="required">* ( Image size must be 407 X 569 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image2[]','id'=>'image2'));	
						echo form_input(array('name'=>'old_image2[]','type'=>'hidden','value'=>$field['image2']));
						?>
					</div>
					<?php } else { ?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image2<span class="required">*  ( Image size must be 407 X 569 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image2[]','id'=>'image2'));	
						?>
					</div>
					<?php } ?>
					<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image3'];?>" style="width:10%"/>
						</div>
						<div class="form-group"> 					
						<?php
						echo form_label('Image3<span class="required">* ( Image size must be 293 X 334 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image3[]','id'=>'image3'));	
						echo form_input(array('name'=>'old_image3[]','type'=>'hidden','value'=>$field['image3']));
						?>
					</div>
					<?php } else { ?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image3<span class="required">* ( Image size must be 293 X 334 ) </span>','image3'); 
						echo form_input(array('type'=>'file','name'=>'image3[]','id'=>'image3'));	
						?>
					</div>
					<?php } ?>
					<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image4'];?>" style="width:10%"/>
						</div>
						<div class="form-group"> 					
						<?php
						echo form_label('Image4<span class="required">* ( Image size must be 99 X 100 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image4[]','id'=>'image4'));	
						echo form_input(array('name'=>'old_image4[]','type'=>'hidden','value'=>$field['image4']));
						?>
					</div>
					<?php } else {?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image4<span class="required">* ( Image size must be 99 X 100 )  </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image4[]','id'=>'image4'));	
						?>
					</div>		
					<?php } ?>
					<div class="form-group"> 	
						<h4><?php echo form_label('Different angles of product images'); ?></h4>
					</div>
					<?php if($this->uri->segment(3) == 'edit'){?>
					<?php if($field['image5']){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image5'];?>" style="width:10%"/>
						</div>
						<div><a href="javascript:void(0);" class="product-img-delete" data-image-place="image5" id="<?php echo $id;?>"><i class="fa fa-trash-o"></i></a></div>
					<?php } ?>
						<div class="form-group"> 					
						<?php
						echo form_label('Image5<span class="required"> ( Image size must be 1000 X 1399 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image5[]','id'=>'image5'));	
						echo form_input(array('name'=>'old_image5[]','type'=>'hidden','value'=>$field['image5']));
						?>
					</div>
					<?php } else {?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image5<span class="required"> ( Image size must be 1000 X 1399 )  </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image5[]','id'=>'image5'));	
						?>
					</div>		
					<?php } ?>
					<?php if($this->uri->segment(3) == 'edit'){?>
					<?php if($field['image6']){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image6'];?>" style="width:10%"/>
						</div>
						<div><a href="javascript:void(0);" class="product-img-delete" data-image-place="image6" id="<?php echo $id?>"><i class="fa fa-trash-o"></i></a></div>
					<?php } ?>
						<div class="form-group"> 					
						<?php
						echo form_label('Image6<span class="required"> ( Image size must be 1000 X 1399 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image6[]','id'=>'image6'));	
						echo form_input(array('name'=>'old_image6[]','type'=>'hidden','value'=>$field['image6']));
						?>
					</div>
					<?php } else {?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image6<span class="required"> ( Image size must be 1000 X 1399 )  </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image6[]','id'=>'image6'));	
						?>
					</div>		
					<?php } ?>	
					<?php if($this->uri->segment(3) == 'edit'){?>
					<?php if($field['image7']){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/product/product_attribute_image/<?php echo $field['image7'];?>" style="width:10%"/>
							<div><a href="javascript:void(0);" class="product-img-delete" data-image-place="image7" id="<?php echo $id?>"><i class="fa fa-trash-o"></i></a></div>
						</div>
					<?php } ?>
						<div class="form-group"> 					
						<?php
						echo form_label('Image7<span class="required"> ( Image size must be 1000 X 1399 ) </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image7[]','id'=>'image7'));	
						echo form_input(array('name'=>'old_image7[]','type'=>'hidden','value'=>$field['image7']));
						?>
					</div>
					<?php } else {?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image7<span class="required"> ( Image size must be 1000 X 1399 )  </span>','image'); 
						echo form_input(array('type'=>'file','name'=>'image7[]','id'=>'image7'));	
						?>
					</div>		
					<?php } ?>	
					<div class="form-group">
					<?php echo form_label('Status','published'); ?><br />
					<?php
					echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y)).'&nbsp; Publish &nbsp;&nbsp;';
					echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n)).'&nbsp;Unpublish';		
					?>
					</div>
				</div>
				<div class="box-footer"> 
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?> 
					<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?> 
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>
	</div>
<!-- /.row --> 
</section>
<script>
$(document).ready(function(){
	
var _URL = window.URL;
$("#image1").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 1000 || $h < 1399){
					alert('Image size should be  1000 X 1399.');
					$('#image1').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image2").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 407 || $h < 569){
					alert('Image size should be  407 X 569 .');
					$('#image2').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image3").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 293 || $h < 334){
					alert('Image size should be 293 X 334 .');
					$('#image3').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image4").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 99 || $h < 100){
					alert('Image size should be 99 X 100 .');
					$('#image3').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image5").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 1000 || $h < 1399){
					alert('Image size should be 1000 X 1399 .');
					$('#image5').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image6").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 1000 || $h < 1399){
					alert('Image size should be 1000 X 1399 .');
					$('#image6').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
var _URL = window.URL;
$("#image7").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 1000 || $h < 1399){
					alert('Image size should be 1000 X 1399 .');
					$('#image7').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})

$('.product-img-delete').click(function(){
	var imageID = $(this).attr('id');
	var imageNo = $(this).attr('data-image-place');
	$.post('<?php echo base_url();?>admin/Ajax/product_arttribute_image_delete',{id:imageID,image_no:imageNo},function(data){
		location.reload();		
	},'json');
})	
	
	
	
})
</script>