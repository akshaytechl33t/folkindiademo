<section class="content-header">
	<h1><?php echo $page_title; ?><small></small></h1>
	<ol class="breadcrumb">
		<li>
			<?php $home_anchor = "<i class='fa fa-dashboard'></i> Home"; ?>
			<?php echo anchor('admin',$home_anchor); ?> 
		</li>
		<li><?php echo anchor('admin/banner/','Manage Banner');?></li>
		<li class="active"><?php echo $page_title; ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row"> 
	<!-- left column -->
		<div class="col-md-11"> 
		<!-- general form elements -->
			<div class="box box-primary">
					<div class="box-header"> 
					</div>
				<?php
				if($success_message){?>
					<div class="alert alert-success alert-dismissable"> <i class="fa fa-check"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $success_message; ?></b>
					</div>
				<?php }
				if($error_message){?>
					<div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>
						<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
						<b><?php echo $error_message; ?></b> 
					</div>
				<?php }
				?>				
				<?php 
					echo form_open_multipart('admin/Banner/'.$action.'/'.$id,array('method'=>'post'));
					echo form_hidden('action',$action);
					echo form_hidden('id',$id);
				?>
					<div class="box-body">		
						<?php if($this->uri->segment(3) == 'edit'){?>
						<div class="form-group">						
							<img src="<?php echo base_url();?>uploads/images/home/banner/<?php echo $field['images'];?>" style="width:50%"/>
						</div>
						<div class="form-group"> 					
						<?php
						echo form_label('Upload Image<span class="required">*  ( Image size must be 1920 X 720 )</span>','image'); 
						echo form_input(array('type'=>'file','name'=>'images[]','id'=>'banner_image'));	
						echo form_input(array('name'=>'old_image[]','type'=>'hidden','value'=>$field['images']));
						?>
					</div>
					<?php } else { ?>
					<div class="form-group"> 					
						<?php
						echo form_label('Image<span class="required">* ( Image size must be 1920 X 720 )</span>','image'); 
						echo form_input(array('type'=>'file','name'=>'images[]','id'=>'banner_image'));	
						?>
					</div>
					<?php } ?>								
					<div class="form-group"> 					
						<?php
						echo form_label('Target page link','image'); 
						echo form_input(array('name'=>'link','class'=>'form-control','value'=>$field['link']));	
						echo form_error('link');
						?>
					</div>
					<div class="form-group">
					<?php echo form_label('Status','published'); ?><br />
					<?php
					echo form_radio(array('name'=> 'published','value'=> 1,'checked' =>$checked_y)).'&nbsp; Publish &nbsp;&nbsp;';
					echo form_radio(array('name'=> 'published','value'=> 0,'checked' =>$checked_n)).'&nbsp;Unpublish';		
					?>
					</div>
				</div>
				<div class="box-footer"> 
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary')); ?> 
					<?php echo form_input(array('type'=>"reset",'value'=>"Reset",'name'=>"reset",'class'=>"btn btn-primary"));?> <?php echo anchor('admin/banner','Back',array('class'=>'btn btn-primary'));?> 
				</div>
			<?php echo form_close(); ?>
			</div>
		</div>
	</div>
<!-- /.row --> 
</section>
<script>
$(document).ready(function(){
	
var _URL = window.URL;
$("#banner_image").change(function (e) {	
	var file, img;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {  			
				$w = this.width,
				$h = this.height;	
				if($w > 1920 || $h < 720){
					alert('Image size should be  1920 X 720 .');
					$('#banner_image').val('');
				} 
			};
			img.src = _URL.createObjectURL(file);
		}
	})
})
</script>