<!DOCTYPE html>
<html class="bg-black">
    <?php echo $head; ?>
    <body class="bg-black">
        <div class="form-box" id="login-box">
            <?php echo $maincontent; ?>
        </div>
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min-2.0.2.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>        
    </body>
</html>