<aside class="left-side sidebar-offcanvas"> 
    <section class="sidebar">
        <div class="user-panel">
        	<div class="pull-left image">
            	<?php echo img(array('src'=>'assets/admin/images/avatarnew.gif','class'=>'img-circle','alt'=>'User Image')); ?>
            </div>
            <div class="pull-left info">
            	<p>Hello, <?php echo $this->session->userdata('user_name');?></p>		
                <?php echo anchor('javascript:void(0);',"<i class='fa fa-circle text-success'></i> Online"); ?>
            </div>
        </div>       	
        <ul class="sidebar-menu">
			<li class="treeview">				
                <?php echo anchor('javascript:void(0);',"<i class='fa fa-location-arrow'></i><span> CMS</span><i class='fa fa-angle-left pull-right'></i>"); ?>
				<ul class="treeview-menu">
					<li><?php echo anchor('admin/static_page','<i class="fa fa-list-alt"></i> <span> Static Pages</span>'); ?></li>			
					<li><?php echo anchor('admin/cms_email','<i class="fa fa-envelope"></i> <span> CMS Email</span>'); ?>
					<li><?php echo anchor('admin/Static_page/about_us','<i class="fa fa-envelope"></i> <span> About us</span>'); ?></li>
<li><?php echo anchor('admin/Static_page/media','<i class="fa fa-envelope"></i> <span> Media</span>'); ?></li>			
				</ul>
            </li>						
			<li><?php echo anchor('admin/customers','<i class="fa fa-users"></i> Customers'); ?></li>
			<li><?php echo anchor('admin/category','<i class="fa fa-outdent"></i> Category'); ?></li>
			<li><?php echo anchor('admin/attributes','<i class="fa fa-list-alt"></i> Attributes'); ?></li>
			<li><?php echo anchor('admin/features',"<i class='fa fa-tasks'></i> Features"); ?></li>
            <li><?php echo anchor('admin/products',"<i class='fa fa-tasks'></i> Products"); ?></li>
            <li><?php echo anchor('admin/homepage_section',"<i class='fa fa-cogs'></i><span> Home page Configuration</span>");?></li>
			<li><?php echo anchor('admin/event',"<i class='fa fa-tasks'></i> Event"); ?></li>
			<li><?php echo anchor('admin/faq',"<i class='fa fa-tasks'></i> Faq"); ?></li>
			<li><?php echo anchor('admin/promocode',"<i class='fa fa-file-code-o'></i> Promocode"); ?></li>
			<?php /*?><li><?php echo anchor('admin/orders','<i class="fa fa-table"></i> Orders'); ?></li> 
			<li><?php echo anchor('admin/customers','<i class="fa fa-users"></i> Customers'); ?></li> <?php */?>
			
            <li class="treeview">
				<?php echo anchor('javascript:void(0);',"<i class='fa fa-globe'></i> <span> Locations</span><i class='fa fa-angle-left pull-right'></i>"); ?>
				<ul class="treeview-menu">
					<li><?php echo anchor('admin/country','<i class="fa fa-location-arrow"></i>Country'); ?></li>
					<li><?php echo anchor('admin/state',"<i class='fa fa-location-arrow'></i> States"); ?></li>					
					<li><?php echo anchor('admin/city/listing',"<i class='fa fa-location-arrow'></i> City"); ?></li>					
				</ul>
			</li>
			<li class="treeview">				
				<?php echo anchor('javascript:void(0);',"<i class='fa fa-user'></i> <span> Web Admin</span><i class='fa fa-angle-left pull-right'></i>"); ?>
				<ul class="treeview-menu">					
					<li><?php echo anchor('admin/settings','<i class="fa fa-user"></i> Site Settings'); ?></li>
					<li><?php echo anchor('admin/contact_us','<i class="fa fa-question-circle"></i> Contact us'); ?></li>
					<li><?php echo anchor('admin/change_password','<i class="fa fa-key"></i> Change Password'); ?></li>
				</ul>
			</li>
		</ul>
	</section>
</aside>