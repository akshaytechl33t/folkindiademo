<head>
    <meta charset="UTF-8">
	<?php /*?><link href="<?php echo base_url();?>assets/icon/favicon.ico" type="image/x-icon" rel="shortcut icon" /><?php */?>
    <title><?php echo SITE_NAME; ?> | Admin Panel</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>  
	 <?php echo link_tag(array('href'=>'assets/admin/css/bootstrap.min.css','rel'=>'stylesheet','type'=>'text/css')); ?>
	 <?php echo link_tag(array('href'=>'assets/admin/css/font-awesome.min.css','rel'=>'stylesheet','type'=>'text/css')); ?>
	 
	 <?php echo link_tag(array('href'=>'assets/admin/css/ionicons.min.css','rel'=>'stylesheet','type'=>'text/css')); ?>
	 <?php echo link_tag(array('href'=>'assets/admin/css/AdminLTE.css','rel'=>'stylesheet','type'=>'text/css')); ?>

	 <?php echo link_tag(array('href'=>'assets/admin/css/datatables/dataTables.bootstrap.css','rel'=>'stylesheet','type'=>'text/css'));?>
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	  <script language="javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery-1.8.2.min.js"></script>
	  <script language="javascript" src="<?php echo base_url(); ?>assets/admin/js/php.default.min.js"></script>
		<script language="javascript" src="<?php echo base_url(); ?>assets/admin/js/admin_common_function.js"></script>
		
	<!--<link rel="apple-touch-icon" sizes="57x57" href="assets/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/icon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">	-->

<script type="text/javascript">
$(document).ready(function(e) {
    var page_uRlx = document.URL;
	$(".sidebar-menu").find("a[href='"+page_uRlx+"']").parent().addClass("activeleft");
	
	if($(".sidebar-menu").find("a[href='"+page_uRlx+"']").parent().parent().attr("class")=="treeview-menu"){
		$(".sidebar-menu").find("a[href='"+page_uRlx+"']").closest(".treeview").addClass("active");
	}
});
</script>	
</head>