<!-- header logo: style can be found in header.less -->
<header class="header">
	<?php echo anchor('admin',SITE_NAME,'class="logo"'); ?>
	<!-- Add the class icon to your logo image or logo icon to add the margining -->
	<a href="<?php echo base_url();?>" class="link"></a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a class="dropdown-toggle" data-toggle="dropdown" style="hover:none;">
						<span>Time: <?php echo date("d-m-Y  h:i A").nbs(3).date_default_timezone_get(); ?></span>
						<span id="user_online" style="display:none;"><?php echo $this->session->userdata('online');?></span>
							<span id="user_id" style="display:none;"><?php echo $this->session->userdata('userid');?></span>
					</a>
				</li>
				<li class="dropdown user user-menu">
					<?php echo anchor('','Visit Website', 'target="_blank"'); ?>
				</li>
				<?php if($this->session->userdata('user_type') == 'SA'){?>				
				<li class="dropdown user user-menu">
					<?php echo anchor('admin/settings',img(array('src'=>'assets/admin/images/icons/settings.png','alt'=>'Settings')).' Settings',array('title'=>'Settings')); ?>
				</li>
				<?php }?>
				<li class="dropdown user user-menu">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?php echo $this->session->userdata('user_name');?></span>
					</a>
				</li>
				<li class="dropdown user user-menu">
					<?php $admin_logout_anchor = "<span>Logout</span>"; ?>
					<?php echo anchor('admin/logout',$admin_logout_anchor,'class="dropdown-toggle"',array('data-toggle'=>'dropdown'));?>
					<!--<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span>Logout</span>
					</a>-->
				</li>
			</ul>
		</div>
	</nav>
</header>