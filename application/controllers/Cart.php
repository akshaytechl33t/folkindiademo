<?php
class Cart extends CI_Controller
{
    var $data;
    function  __construct() 
	{
        parent::__construct();
        $this->public_init_elements->init_elements();		
    }

    function cart()
	{	//$this->cart->destroy();		
		//print_r($this->session->unset_userdata('order_coupon_details') );
		
        $cartrows = array();
        foreach ($this->cart->contents() as $item)
        {
            $product_options = $this->cart->product_options($item['rowid']);
			
			$cartrows[] = array(
								'rowid' => $item['rowid'],
								'productid' => $item['id'],
								'name' => $item['name'],
								'price' => $item['price'],
								'product_color' => $product_options['color'],
								'category_name' => $product_options['category_name'],
								'product_seo_name'  =>$product_options['product_seo_name'],
								'product_image' =>$product_options['product_image'],
								'quantity' => $item['qty'],
								'subtotal' => $item['subtotal']
							);
							
        }		
		//print_r( $cartrows );die;
		$data['cartrows'] = $cartrows;
		$data['cart_total'] = $this->cart->total();
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/cart-details', $data, true);
		$this->load->view('frontend/layout', $this->data);
    }
    function find_item($sku) 
    {
		if($this->cart->total_items()>0){
			foreach ($this->cart->contents() as $item)
			{ 
				if ($item['id'] == $productid)
				{
					return array('rowid' => $item['rowid'], 'qty' => $item['qty']);
				}
				if($item['id'] == $lense_id){
					return array('rowid' => $item['rowid'], 'qty' => $item['qty']);
				}
			}
		}
		else
		{ 
			return FALSE;
		}
    }
    function add()
	{ 
		
		$result = $this->find_item($this->input->post('product_id'));      
		if ($result == FALSE)
		{
			$data = array(
					'id' =>$this->input->post('product_id'),
					'qty' => 1,
					'price'=>$this->input->post('product_price'),
					'name'=>$this->input->post('product_name'),			
					'options' =>array(
						'color'=>$this->input->post('product_color'),
						'product_image' =>$this->input->post('product_image'),						
						'category_name'=>$this->input->post('category_name'),						
						'product_seo_name'=>$this->input->post('product_seo_name')
					)
					
				 );	
			$this->cart->insert($data);
		}
		else
		{
			$data = array(
					'rowid' => $result['rowid'],
					'qty' => $result['qty'] + 1
				 );                    
			$this->cart->update($data);
		}    
		echo json_encode($this->cart->contents());
		exit;
    }
    function update()
	{
		$rowid=$this->input->post('row_id');
		$product_id = $this->input->post('product_id');
		$quantity=$this->input->post('quantity');
		if($this->input->post('flag') == 'minus'){
			$data=array('rowid'=>$rowid,'qty'=>$quantity);
			$this->cart->update($data);			
			foreach ($this->cart->contents() as $item)
			{
				if($item['rowid'] == $rowid){
					$cartrows = array(
						'rowid' => $item['rowid'],
						'qty'=>$item['qty'],
						'subtotal'=>$item['subtotal'],
						'price' => $item['price'],
					);
				}
			} 
			$response['message'] =$cartrows;	
		}
		else
		{
			$table['name'] = 'folk_products';
			$quantity_limit = $this->Common_model->find_data($table,'row','',array('id'=>$product_id));
			
			if($quantity > $quantity_limit->b2b_qty_limit){
				$response['message'] = 'b2b_limit_over';			
			}
			else{			
				$data=array('rowid'=>$rowid,'qty'=>$quantity);
				$this->cart->update($data);			
				foreach ($this->cart->contents() as $item)
				{
					if($item['rowid'] == $rowid){
						$cartrows = array(
							'rowid' => $item['rowid'],
							'qty'=>$item['qty'],
							'subtotal'=>$item['subtotal'],
							'price' => $item['price'],
						);
					}
				} 
				$response['message'] =$cartrows;				
			}
		}		
		echo json_encode($response);
		exit;
		
	}
	function remove()
	{
		
	 
		$cartrows = array(); 
		foreach ($this->cart->contents() as $item)
		{
			$cartrows[] = array('rowid' => $item['rowid']);
		}
		(sizeof($cartrows) <= 1) ? $this->session->unset_userdata('order_coupon_details') : '';
							
					
		$row_id=$this->input->post('row_id');
		if($row_id!=NULL)
		{		
			$data=array('rowid'=>$row_id,'qty'=>0);
			$this->cart->update($data);
			echo json_encode($row_id);exit;
		}
    }
	function delete()
	{
		$this->session->unset_userdata('shipping_address');
		$this->cart->destroy();		
		$this->session->unset_userdata('order_coupon_details');	
		redirect('cart-details');
	}
	
}
?>