<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	
	var $data;
	function __construct()
	{				
		parent::__construct();
		$this->public_init_elements->init_elements();	
	}	 
    function search(){	
		$search_keyword = $this->input->post('value');
		$table['name']="folk_products";
		$table['alias_name']='p';		
		$select = 'p.name, p.seo_name, c.name AS category_name';				
		$join[0] = array('table'=>'folk_categories','table_alias'=>'c','field'=>'id','table_master'=>'p','field_table_master'=>'category_id','type'=>'LEFT');		
		$group_by[0] = 'p.id';
		$row = $this->Common_model->find_data($table,'array','',array('p.published'=>1),$select,$join,$group_by,'','','','','',array('p.name'=>$search_keyword),'','');	
		echo json_encode($row);
		die;
	}
	function product_listing_search(){
		$feature_value_id = $this->input->post('feature_value_id');
		$category_id = $this->input->post('category_id');
		$table['name']="folk_assign_features";
		$table['alias_name']='af';		
		$select = 'p.name, p.seo_name,p.price,pai.image2,p.qty,p.category_id,p.published';				
		$join[0] = array('table'=>'folk_products','table_alias'=>'p','field'=>'id','table_master'=>'af','field_table_master'=>'product_id','type'=>'LEFT');		
		$join[1] = array('table'=>'folk_products_attribute_images','table_alias'=>'pai','field'=>'product_id','table_master'=>'p','field_table_master'=>'id','type'=>'LEFT');
		$group_by[0] = 'p.id';
		$order_by[0]=array('field'=>'p.id','type'=>'DESC');
		$this->data['product_details'] = $this->Common_model->find_data($table,'array','',array('p.category_id'=>$category_id,'p.published'=>1,'af.feature_value_id'=>$feature_value_id,'p.published'=>1),$select,$join,$group_by,$order_by);
		$this->load->view('frontend/elements/product-listing-search', $this->data,FALSE);
	}
	function get_attr_image(){
		$color_id = trim($this->input->post('color_id'));
		$product_id = trim($this->input->post('product_id'));
		$table['name'] = 'folk_products_attribute_images';
		$row = $this->Common_model->find_data($table,'row','',array('product_id'=>$product_id,'color_id'=>$color_id));
		echo json_encode($row);
	}
	function state_drop_down(){
		$country_id = $this->input->post('country_id');
		$table['name'] = 'folk_states';
		$state_arr = $this->Common_model->find_data($table,'array','',array('published'=>1,'country_id'=>$country_id));
		echo json_encode($state_arr);
		exit;
	}
	function city_drop_down(){
		$state_id = $this->input->post('state_id');
		$table['name'] = 'folk_cities';
		$city_arr = $this->Common_model->find_data($table,'array','',array('published'=>1,'state_id'=>$state_id));
		echo json_encode($city_arr);
		exit;
	}
	function delete_shipping_address(){
		$address_id = $this->input->post('address_id');
		$table['name'] = 'folk_shipping_addresses';
		$res = $this->Common_model->delete_data($table,$address_id);
		echo json_encode($res);
		exit;
	}
	function apply_promocode(){
		$promocode = $this->input->post('code');
		$table['name'] = 'folk_promocodes';
		$get_discount_amt = $this->Common_model->find_data($table,'row','',array('code'=>$promocode,'expire >='=>date('Y-m-d')));
		if($get_discount_amt){	
			$this->session->unset_userdata('order_coupon_details');
			$this->session->set_userdata('order_coupon_details', $get_discount_amt);
			//print_r($this->session->userdata('order_coupon_details'));die;
			$response['flag'] = 'success';
			$response['message'] = $get_discount_amt->discount_amount;
		}
		else
		{
			$response['flag'] = 'error';
		}
		echo json_encode($response);
		exit;
	}
	function set_shipping_address(){
		$address_id = $this->input->post('address_id');
		
		$table['name']="folk_shipping_addresses";
		$table['alias_name']='s';				
		$select = 's.*,c.name as country_name,st.state_name,ct.city_name';				
		$join[0] = array('table'=>'folk_countries','table_alias'=>'c','field'=>'id','table_master'=>'s','field_table_master'=>'country_id','type'=>'LEFT');
		$join[1] = array('table'=>'folk_states','table_alias'=>'st','field'=>'id','table_master'=>'s','field_table_master'=>'state_id','type'=>'LEFT');
		$join[2] = array('table'=>'folk_cities','table_alias'=>'ct','field'=>'id','table_master'=>'s','field_table_master'=>'city_id','type'=>'LEFT');
		
		$res=$this->Common_model->find_data($table,'row','',array('s.id'=>$address_id),$select,$join);	
		if($res){
			$shipping_address = array(
				'id'=>$res->id,
				'name'=>$res->name,
				'street_address'=>$res->street_address,
				'land_mark'=>$res->land_mark,
				'phone_number'=>$res->phone_number,
				'pincode'=>$res->pincode,
				'default'=>$res->default,
				'country_name'=>$res->country_name,
				'state_name'=>$res->state_name,
				'city_name'=>$res->city_name
			);
			$this->session->set_userdata('shipping_address', $shipping_address);
			$response['flag'] = 'success';
		}
		else
		{
			$response['flag'] = 'error';
		}
		echo json_encode($response);
		exit;
	}
}
?>






