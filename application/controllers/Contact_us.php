<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller
{
	var $data;
	function __construct()
	{
		parent::__construct();
		$this->public_init_elements->init_elements();
		$this->table['name'] = 'folk_static_pages';
	}

    function index(){
			if($this->contact_us_validation() == true){
					$ch = curl_init();
					$request_body = http_build_query(array(
						'secret' => '6LciY00UAAAAALymL9tVxwcRiSyoLF15YdkYCLiN',
						'response' => $_REQUEST['g-recaptcha-response'],
						'remoteip' => $_SERVER['REMOTE_ADDR'],
					));
					$request_url = 'https://www.google.com/recaptcha/api/siteverify';
					// exit;
			    curl_setopt($ch, CURLOPT_URL, $request_url);
			    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type' => 'application/x-www-form-urlencoded'));
			    curl_setopt($ch, CURLOPT_HEADER, 1);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_body);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			    $output = curl_exec($ch);
			    $sfdc_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE );
			    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			    $sfdc_response_body = substr($output, $header_size);
			    curl_close($ch);
			    $response = json_decode($sfdc_response_body, true);
			    if (!$response['success']) {
			    	echo json_encode('Invlaid Captcha');
						exit;
			    }
					$postdata= array(
							'name'	=>$this->input->post('name'),
							'company'			=>$this->input->post('company'),
							'telephone'	=>$this->input->post('telephone'),
							'email'		=>$this->input->post('email'),
							'enquiry'		=>$this->input->post('enquiry'),
							'message'		=>$this->input->post('message'),
							'published' 	=>1
						);
					$res = $this->Common_model->save_data(array('name'=>'folk_contact_us'),$postdata);
					if($res)
					{
						$tokens['LOGO'] = base_url().'assets/frontend/images/large/logo.jpg';
						$tokens['NAME'] = ucfirst($this->input->post('name'));
						$tokens['COMPANY'] = $this->input->post('company');
						$tokens['TELEPHONE'] = $this->input->post('telephone');
						$tokens['EMAIL'] = $this->input->post('email');
						$tokens['ENQUIRY'] = $this->input->post('enquiry');
						$tokens['MESSAGE'] = $this->input->post('message');
						$tokens['URL'] = base_url();

						$table_mail['name']='folk_email_templates';
						$select = 'id,email_templates_subject,email_templates_content,published';
						$conditions = array('id' => 2,'published' => 1);
						$content=$this->Common_model->find_data($table_mail,'row','',$conditions,$select);

						$email_content = $content->email_templates_content;
						$subject = $content->email_templates_subject;
						//$to = SITE_RECEIVER_EMAIL.',manjir@gmail.com,manjir@folkindia.in';
						$to = 'soumyajit.pati@gmail.com';
						//$from = SITE_SENDER_EMAIL;
						$from = 'info@folkindia.in';
						
						$mail_flag = send_template_mail($tokens, $email_content, $subject, $to, $from);
						if($mail_flag){
							$this->data['success_message'] = 'Message Sent Successfully';
						}
							
						
					}
					else
					{
						$this->data['error_message'] = 'Some Error occoured! Please Try Again';

					}
			}

		$this->data['maincontent'] = $this->load->view('frontend/maincontents/contact-us',$this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	 function contact_us_validation()
	{
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('name', 'Name', 'strip_tags|trim|required');
		$this->form_validation->set_rules('company', 'Company', 'strip_tags|trim|required');
		$this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email');
		$this->form_validation->set_rules('enquiry', 'Enquiry', 'strip_tags|trim|required');
		$this->form_validation->set_rules('message', 'message ', 'strip_tags|trim|required');
        if($this->form_validation->run() == TRUE)
		{
            return TRUE;
        } else {
            return FALSE;
        }

	}

}
?>
