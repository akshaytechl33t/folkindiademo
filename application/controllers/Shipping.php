<?php
class Shipping extends CI_Controller
{
    var $data;
    function  __construct() 
	{
        parent::__construct();		
        $this->public_init_elements->init_elements();
		$this->table['name']='folk_shipping_addresses';			
    }
	function index(){
		$this->public_init_elements->is_user_logged_in();
		$table['name'] = "folk_countries";
		$order_by[0]['field'] = 'name';
		$order_by[0]['type'] = 'ASC';
		$this->data['country_arr'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);
		$this->data['state_arr'] = array(''=>'Select state');
		$this->data['city_arr'] = array(''=>'Select city');
		if($this->input->post('submit'))
		{
			if ($this->validate_shipping_address_form() == TRUE) 
			{
				$postdata = array(
					'user_id'=>$this->session->userdata['is_user_signed_in']['user_id'],
					'name' => $this->input->post('name'),	
					'street_address' => $this->input->post('street_address'),
					'land_mark' => $this->input->post('land_mark'),
					'phone_number' => $this->input->post('phone_number'),
					'country_id' => $this->input->post('country_id'),
					'state_id' => $this->input->post('state_id'),
					'city_id' => $this->input->post('city_id'),
					'pincode' => $this->input->post('pincode'),
					'published' => 1
				);
				$returnval=$this->Common_model->save_data($this->table,$postdata); 
				if($returnval){
					$this->session->set_flashdata('success_message', 'You have add a new shipping address');
					if($this->input->post('shipping_url') == 'checkout'){
						redirect(checkout);
					}else
					{						
						redirect('address');
					}
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Some Error occoured! Please Try Again');
				}
			}
			else
			{						
				$this->data['field']['name']=set_value('name');	
				$this->data['field']['street_address']=set_value('street_address');	
				$this->data['field']['land_mark']=set_value('land_mark');
				$this->data['field']['phone_number']=set_value('phone_number');
				$this->data['field']['country_id']=set_value('country_id');				
				$this->data['field']['state_id']=set_value('state_id');				
				$this->data['field']['city_id']=set_value('city_id');				
				$this->data['field']['pincode']=set_value('pincode');				
			}
		}
		$table['name']="folk_shipping_addresses";
		$table['alias_name']='s';		
		
		$select = 's.*,c.name as country_name,st.state_name,ct.city_name';	
			
		$join[0] = array('table'=>'folk_countries','table_alias'=>'c','field'=>'id','table_master'=>'s','field_table_master'=>'country_id','type'=>'LEFT');
		$join[1] = array('table'=>'folk_states','table_alias'=>'st','field'=>'id','table_master'=>'s','field_table_master'=>'state_id','type'=>'LEFT');
		$join[2] = array('table'=>'folk_cities','table_alias'=>'ct','field'=>'id','table_master'=>'s','field_table_master'=>'city_id','type'=>'LEFT');
		
		$this->data['get_shipping_address']=$this->Common_model->find_data($table,'array','',array('user_id'=>$this->session->userdata['is_user_signed_in']['user_id']),$select,$join);		
		
		$this->data['common_account_left_panel'] = $this->load->view('frontend/elements/common_account_left_panel',$this->data, true);	
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/shipping-address', $this->data, true);
        $this->load->view('frontend/layout', $this->data);
	}
	function validate_shipping_address_form(){
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><div class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></div></div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('street_address', 'street_address', 'required');
		$this->form_validation->set_rules('land_mark', 'land_mark', 'required');		
		$this->form_validation->set_rules('phone_number', 'phone number', 'required');	
		$this->form_validation->set_rules('country_id', 'country', 'required');	
		$this->form_validation->set_rules('state_id', 'state', 'required');	
		$this->form_validation->set_rules('city_id', 'city', 'required');	
		$this->form_validation->set_rules('pincode', 'pincode', 'required');			
        if ($this->form_validation->run() == TRUE)
		{			
            return TRUE;
        } 
		else 
		{
            return FALSE;
        }
	}

}
?>