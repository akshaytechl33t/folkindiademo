<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller
{
	var $data;
	function __construct()
	{				
		parent::__construct();
		$this->public_init_elements->init_elements();
		$this->public_init_elements->is_user_logged_in();
		$this->load->library('Paypal_inc');	
		require_once dirname( __FILE__ ) . '../../third_party/Payubiz_inc.php';
	}	
    function index(){	
		$table['name']="folk_shipping_addresses";
		$table['alias_name']='s';		
		
		$select = 's.*,c.name as country_name,st.state_name,ct.city_name';	
			
		$join[0] = array('table'=>'folk_countries','table_alias'=>'c','field'=>'id','table_master'=>'s','field_table_master'=>'country_id','type'=>'LEFT');
		$join[1] = array('table'=>'folk_states','table_alias'=>'st','field'=>'id','table_master'=>'s','field_table_master'=>'state_id','type'=>'LEFT');
		$join[2] = array('table'=>'folk_cities','table_alias'=>'ct','field'=>'id','table_master'=>'s','field_table_master'=>'city_id','type'=>'LEFT');
		
		$this->data['get_shipping_address']=$this->Common_model->find_data($table,'array','',array('user_id'=>$this->session->userdata['is_user_signed_in']['user_id']),$select,$join); 
		
		$this->data['country_arr'] = $this->Common_model->find_data(array('name'=>'folk_countries'),'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);			
		
		$cartrows = array();
		$totalQuantity = 0;
        foreach ($this->cart->contents() as $item)
        {
            $product_options = $this->cart->product_options($item['rowid']);
			
			$cartrows[] = array(
				'rowid' => $item['rowid'],
				'productid' => $item['id'],
				'name' => $item['name'],
				'price' => $item['price'],
				'usdprice' => $this->currency('INR','USD',$item['price']),
				'product_color' => $product_options['color'],
				'category_name' => $product_options['category_name'],
				'product_seo_name'  =>$product_options['product_seo_name'],
				'product_image' =>$product_options['product_image'],
				'quantity' => $item['qty'],
				'subtotal' => $item['subtotal']
			);	
			$totalQuantity = $totalQuantity+$item['qty'];
        }	
		
		$this->data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		
		//$this->data['paypal']=$this->paypal_inc->index();
		$this->data['paypal']['amount'] = $this->currency('INR','USD',$this->cart->total()); //number_format(,2); 
		if($this->cart->contents()){
			$cartqty = array();
			foreach($this->cart->contents() as $cart_details):
				$cartqty[] = $cart_details['qty'];				
			endforeach;
		}	
		
		/*$this->data['paypal']['discount'] = $this->session->userdata['order_coupon_details']->discount_amount;
		$this->data['paypal']['shipping'] = '';
		$this->data['paypal']['name'] = $this->session->userdata['shipping_address']['name'];
		$this->data['paypal']['street_address'] = $this->session->userdata['shipping_address']['street_address'];
		$this->data['paypal']['land_mark'] = $this->session->userdata['shipping_address']['land_mark'];
		$this->data['paypal']['phone_number'] = $this->session->userdata['shipping_address']['phone_number'];
		$this->data['paypal']['pincode'] = $this->session->userdata['shipping_address']['pincode'];
		$this->data['paypal']['country_name'] = $this->session->userdata['shipping_address']['country_name'];
		$this->data['paypal']['state_name'] = $this->session->userdata['shipping_address']['state_name'];
		$this->data['paypal']['city_name'] = $this->session->userdata['shipping_address']['city_name'];
		$this->data['paypal']['quantity'] = array_sum($cartqty); 
		$this->data['paypal']['item_name']=SITE_NAME.' Product details';*/		
		
		$this->data['checkout_cart_details']  = $cartrows;
		$this->data['totalQuantity']  = $totalQuantity;
		$this->data['add_new_shipping'] = $this->load->view('frontend/elements/add_new_shipping', $this->data, true);		
		//$this->data['checkout_cart_details'] = $this->session->userdata['cart_contents'];
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/checkout-details', $this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	
	function currency($from_Currency,$to_Currency,$amount) {		
		$amount = urlencode($amount);

		$from_Currency = urlencode($from_Currency);
		$to_Currency = urlencode($to_Currency);
		$get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=" . $from_Currency . "&to=" . $to_Currency);

		$get = explode("<span class=bld>",$get);
		$get = explode("</span>",$get[1]);
		$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
		
		return money_format($converted_amount, 2);
	}
    function currencyPaypal ()
    {
		if ($_POST) {
			$convertedMoney = $this->currency('INR', $_POST['reqCurrency'], $_POST['mainamount']);
			echo $convertedMoney;
		} else {
			echo 0;
		} exit;
    }
    
    
	function success(){			
		$cartrows = array();	
		if(!empty($_POST)){			
			if($_POST['status'] == 'success'){
				$postdata = array(
					'user_id'=>$this->session->userdata['is_user_signed_in']['user_id'],
					'mihpayid'=>$_POST['mihpayid'],
					'mode'=>$_POST['mode'],
					'txnid'=>$_POST['txnid'],
					'amount'=>$_POST['amount'],
					'card_category'=>$_POST['cardCategory'],
					'discount'=>(!empty($this->session->userdata('order_coupon_details'))) ? ($this->session->userdata['order_coupon_details']->discount_amount/100) * $this->cart->total() : '',
					'net_amount_debit'=>$_POST['net_amount_debit'],
					'hash'=>$_POST['hash'],
					'field1'=>$_POST['field1'],
					'field2'=>$_POST['field2'],
					'field3'=>$_POST['field3'],
					'field4'=>$_POST['field4'],
					'field5'=>$_POST['field5'],
					'field6'=>$_POST['field6'],
					'field7'=>$_POST['field7'],
					'field8'=>$_POST['field8'],
					'field9'=>$_POST['field9'],
					'payment_source'=>$_POST['payment_source'],
					'pg_type'=>$_POST['PG_TYPE'],
					'bank_ref_num'=>$_POST['bank_ref_num'],
					'bank_code'=>$_POST['bankcode'],
					'name_on_card'=>$_POST['name_on_card'],
					'card_num'=>$_POST['cardnum'],
					'card_hash'=>$_POST['cardhash'],
					//'issuing_bank'=>$_POST['issuing_bank'],
					'card_type'=>$_POST['card_type'],
					'added_on'=>$_POST['addedon'],
					'shipping_name'=>$this->session->userdata['shipping_address']['name'],
					'shipping_street_address'=>$this->session->userdata['shipping_address']['street_address'],
					'shipping_land_mark'=>$this->session->userdata['shipping_address']['land_mark'],
					'shipping_phone'=>$this->session->userdata['shipping_address']['phone_number'],
					'shipping_country'=>$this->session->userdata['shipping_address']['country_name'],
					'shipping_state'=>$this->session->userdata['shipping_address']['state_name'],
					'shipping_city'=>$this->session->userdata['shipping_address']['city_name'],
					'shipping_pincode'=>$this->session->userdata['shipping_address']['pincode'],
					'published'=>1				
				);
				$order_res = $this->Common_model->save_data(array('name'=>'folk_orders'),$postdata);
				$order_id = $this->db->insert_id();
				if($order_res){	
					foreach ($this->cart->contents() as $item)
					{
						$product_options = $this->cart->product_options($item['rowid']);					
						$cartrows[] = array(
							'order_id'=>$order_id,
							'product_id' => $item['id'],
							'product_name' => $item['name'],
							'product_seo_name'  =>$product_options['product_seo_name'],
							'product_image' =>$product_options['product_image'],
							'product_unit_price' => $item['price'],
							'product_color' => $product_options['color'],
							'category_name' => $product_options['category_name'],					
							'product_quantity' => $item['qty'],
							'product_sub_total' => $item['subtotal']
						);	
						
						$get_product_qty = $this->Common_model->find_data(array('name'=>'folk_products'),'row','',array('id'=>$item['id']));	
						if($get_product_qty){							
							$outstock = $get_product_qty->qty - $item['qty'];
							$update_stock_data =array(								
								'qty'=>$outstock
							); 
							$update_product_qty = $this->Common_model->save_data(array('name'=>'folk_products'),$update_stock_data,$item['id']);
						}						
					}
					$order_detail_res = $this->Common_model->save_data(array('name'=>'folk_order_details'),$cartrows,'0','1');
					$this->session->unset_userdata('order_coupon_details');	
					$this->session->unset_userdata('shipping_address');
					$this->cart->destroy();			
					if($order_detail_res){
						// admin notification mail					
						$this->admin_notification_mail($_POST['txnid']);
						// buyer notification mail		
						$this->buyer_notification_mail($_POST['txnid']);
					}
				}
			}
			$this->data['maincontent'] = $this->load->view('frontend/maincontents/payment-succcess', $this->data, true);
			$this->load->view('frontend/layout', $this->data);
		}
		else{
			redirect('/');
		}
			
	}
	function failure(){
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/payment-failure', $this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	function cancel(){
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/payment-cancel', $this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	function admin_notification_mail($transaction_id){	
		if($transaction_id){
			$get_order = $this->Common_model->find_data(array('name'=>'folk_orders'),'row','',array('txnid'=>$transaction_id));		
			if($get_order){
				$table_order_details['name'] = 'folk_orders';
				$table_order_details['alias_name']='od';	
				$select = 'od.*,ods.order_id,ods.product_id,ods.product_name,ods.product_image,ods.product_unit_price,ods.product_color,ods.category_name,u.user_name,u.user_email_id,u.contact_number';
				$join[0] = array('table'=>'folk_order_details','table_alias'=>'ods','field'=>'order_id','table_master'=>'od','field_table_master'=>'id','type'=>'LEFT');
				$join[1] = array('table'=>'folk_users','table_alias'=>'u','field'=>'id','table_master'=>'od','field_table_master'=>'user_id','type'=>'LEFT');
				
				$get_details = $this->Common_model->find_data($table_order_details,'array','',array('od.txnid'=>$get_order->txnid),$select,$join);
				
				if($get_details){					
						$order_arr = array(
							'mihpayid'=>$get_details[0]->mihpayid,
							'txnid'=>$get_details[0]->txnid,
							'amount'=>$get_details[0]->amount,							
							'discount'=>$get_details[0]->discount,
							'net_amount_debit'=>$get_details[0]->net_amount_debit,
							'shipping_name'=>$get_details[0]->shipping_name,
							'shipping_street_address'=>$get_details[0]->shipping_street_address,
							'shipping_land_mark'=>$get_details[0]->shipping_land_mark,
							'shipping_phone'=>$get_details[0]->shipping_phone,
							'shipping_country'=>$get_details[0]->shipping_country,
							'shipping_state'=>$get_details[0]->shipping_state,
							'shipping_city'=>$get_details[0]->shipping_city,
							'shipping_pincode'=>$get_details[0]->shipping_pincode,
							'user_name'=>$get_details[0]->user_name,
							'user_email_id'=>$get_details[0]->user_email_id,
							'contact_number'=>$get_details[0]->contact_number,							
							'product_details'=>$get_details
						);
				}
				
				$admin_subject= 'Order Details';
				$admin_data['rows'] = $order_arr;					
				$admin_email_content = $this->load->view('frontend/elements/admin-order-details-mail',$admin_data,true);
				
				//$to = SITE_RECEIVER_EMAIL;
				$to = 'debobrata1305@gmail.com,indrajitsen60@gmail.com';
				$from = SITE_SENDER_EMAIL;
				$send_to_admin = send_template_mail($tokens,$admin_email_content,$admin_subject,$to,$from); 
				
				return $send_to_admin;
			}
		}
				
	}
	function buyer_notification_mail($transaction_id){
		$get_order = $this->Common_model->find_data(array('name'=>'folk_orders'),'row','',array('txnid'=>$transaction_id));	
		$buyer_details = array();	
		if($get_order){
			$table_order_details['name'] = 'folk_orders';
			$table_order_details['alias_name']='od';	
			$select = 'od.*,ods.order_id,ods.product_id,ods.product_name,ods.product_image,ods.product_unit_price,ods.product_color,ods.category_name,u.user_name,u.user_email_id,u.contact_number';
			$join[0] = array('table'=>'folk_order_details','table_alias'=>'ods','field'=>'order_id','table_master'=>'od','field_table_master'=>'id','type'=>'LEFT');
			$join[1] = array('table'=>'folk_users','table_alias'=>'u','field'=>'id','table_master'=>'od','field_table_master'=>'user_id','type'=>'LEFT');
			
			$get_details = $this->Common_model->find_data($table_order_details,'array','',array('od.txnid'=>$get_order->txnid),$select,$join);				
			if($get_details){					
					$buyer_details = array(
						'mihpayid'=>$get_details[0]->mihpayid,
						'txnid'=>$get_details[0]->txnid,
						'amount'=>$get_details[0]->amount,							
						'discount'=>$get_details[0]->discount,
						'net_amount_debit'=>$get_details[0]->net_amount_debit,
						'shipping_name'=>$get_details[0]->shipping_name,
						'shipping_street_address'=>$get_details[0]->shipping_street_address,
						'shipping_land_mark'=>$get_details[0]->shipping_land_mark,
						'shipping_phone'=>$get_details[0]->shipping_phone,
						'shipping_country'=>$get_details[0]->shipping_country,
						'shipping_state'=>$get_details[0]->shipping_state,
						'shipping_city'=>$get_details[0]->shipping_city,
						'shipping_pincode'=>$get_details[0]->shipping_pincode,
						'user_name'=>$get_details[0]->user_name,
						'user_email_id'=>$get_details[0]->user_email_id,
						'contact_number'=>$get_details[0]->contact_number,							
						'product_details'=>$get_details
					);
					
			}
			$admin_subject= 'Order Confirmation - Your Order with Folkindia.in ['.$buyer_details['txnid'].'] has been successfully placed!';
			$buyer_detail['rows'] = $buyer_details;					
			$buyer_email_content = $this->load->view('frontend/elements/buyer-order-details-mail',$buyer_detail,true);				
			$to = $buyer_details['user_email_id'];
			$from = SITE_SENDER_EMAIL;
			$send_to_buyer = send_template_mail($tokens,$buyer_email_content,$admin_subject,$to,$from); 
			
			return $send_to_buyer;				
		}
	}
}
?>