<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_page extends CI_Controller
{
	var $data;
	function __construct()
	{
		parent::__construct();
		$this->public_init_elements->init_elements();
		$this->table['name'] = 'folk_static_pages';
	}

    function index($seo_name= NULL)
	{

		$this->data['row'] = $this->Common_model->find_data($this->table,'row','',array('page_seo_url'=>$seo_name,'published'=>1));


if($seo_name =='our-story'){

			$table['name'] = 'folk_our_story';
			$this->data['rows'] = $this->Common_model->find_data($table,'array','',array('published'=>1));
			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		}
if($seo_name =='media-section'){

			$table['name'] = 'folk_media_section';
			$this->data['rows'] = $this->Common_model->find_data($table,'array','',array('published'=>1));
			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		}
else if($seo_name =='media'){


			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		}
		else if($this->uri->segment(1) =='bespoke'){

			//echo 'jhjk';die;
			if($this->input->post('name'))
			{
				$ch = curl_init();
				$request_body = http_build_query(array(
					'secret' => '6LciY00UAAAAALymL9tVxwcRiSyoLF15YdkYCLiN',
					'response' => $_REQUEST['g-recaptcha-response'],
					'remoteip' => $_SERVER['REMOTE_ADDR'],
				));
				$request_url = 'https://www.google.com/recaptcha/api/siteverify';
				// exit;
		    curl_setopt($ch, CURLOPT_URL, $request_url);
		    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type' => 'application/x-www-form-urlencoded'));
		    curl_setopt($ch, CURLOPT_HEADER, 1);
		    curl_setopt($ch, CURLOPT_POST, 1);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_body);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    $output = curl_exec($ch);
		    $sfdc_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE );
		    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		    $sfdc_response_body = substr($output, $header_size);
		    curl_close($ch);
		    $response = json_decode($sfdc_response_body, true);
		    if (!$response['success']) {
		    	echo json_encode('Invlaid Captcha');
					exit;
		    }
				$postdata= array(
						'name'	=>$this->input->post('name'),
						'company'			=>$this->input->post('company'),
						'telephone'	=>$this->input->post('telephone'),
						'email'		=>$this->input->post('email'),
						'enquiry'		=>$this->input->post('enquiry'),
						'message'		=>$this->input->post('message'),
						'published' 	=>1
					);
				$res = $this->Common_model->save_data(array('name'=>'folk_contact_us'),$postdata);
				if($res)
				{
					$tokens['LOGO'] = base_url().'assets/frontend/images/large/logo.jpg';
					$tokens['NAME'] = ucfirst($this->input->post('name'));
					$tokens['COMPANY'] = $this->input->post('company');
					$tokens['TELEPHONE'] = $this->input->post('telephone');
					$tokens['EMAIL'] = $this->input->post('email');
					$tokens['ENQUIRY'] = $this->input->post('enquiry');
					$tokens['MESSAGE'] = $this->input->post('message');
					$tokens['URL'] = base_url();

					$table_mail['name']='folk_email_templates';
					$select = 'id,email_templates_subject,email_templates_content,published';
					$conditions = array('id' => 2,'published' => 1);
					$content=$this->Common_model->find_data($table_mail,'row','',$conditions,$select);

					$email_content = $content->email_templates_content;
					$subject = $content->email_templates_subject;
					$to = SITE_RECEIVER_EMAIL;
					$from = SITE_SENDER_EMAIL;
					$mail_flag = send_template_mail($tokens, $email_content, $subject, $to, $from);
					if($mail_flag){
						$this->data['success_message'] = 'Message Sent Successfully';
					}
				}
				else
				{
					$this->data['error_message'] = 'Some Error occoured! Please Try Again';

				}
			}

			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		}
		else if( $this->uri->segment(1) =='waste'){
			$table['name'] = 'folk_footer_images';
			$this->data['row'] = $this->Common_model->find_data($table,'row','',array('seo_name'=>$seo_name,'published'=>1),'description as page_content,title as page_title');
			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		}
		else if( $this->uri->segment(1) =='events'){
			$table['name'] = 'folk_events';
			$order_by[0]=array('field'=>'id','type'=>'DESC');
			$this->data['rows'] = $this->Common_model->find_data($table,'array','',array('published'=>1),'','','',$order_by);

			$this->data['maincontent'] = $this->load->view('frontend/maincontents/event-list',$this->data, true);
		}
		else{

			$this->data['maincontent'] = $this->load->view('frontend/maincontents/static-content',$this->data, true);
		$this->data['meta']['meta_tag']=$this->data['row']->meta_title;
		$this->data['meta']['meta_keywords']=$this->data['row']->meta_keyword;
		$this->data['meta']['meta_description']=$this->data['row']->meta_description;
		$this->load->view('frontend/elements/head',$this->data['meta']);
}
		$this->load->view('frontend/layout', $this->data);
	}

	function event_detail($seo_url){
		$table['name'] = 'folk_events';
		$this->data['row'] = $this->Common_model->find_data($table,'row','',array('seo_url'=>$seo_url,'published'=>1));
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/event-detail',$this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	function team(){

		 $table['name'] = 'folk_our_story';
		$id 	= 	$this->input->post('id');
		$res = $this->Common_model->find_data($table,'row','',array('id'=>$id));
		echo json_encode($res->content);
		die;
	}
}
?>
