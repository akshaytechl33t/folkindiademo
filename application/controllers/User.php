<?php
class User extends CI_Controller
{
    var $data;
    function  __construct()
	{
        parent::__construct();
        $this->public_init_elements->init_elements();
		$this->table['name']='folk_users';
    }

	function login(){

		if ($this->validate_login_form() == TRUE)
		{
				$ch = curl_init();
				$request_body = http_build_query(array(
					'secret' => '6LciY00UAAAAALymL9tVxwcRiSyoLF15YdkYCLiN',
					'response' => $_REQUEST['g-recaptcha-response'],
					'remoteip' => $_SERVER['REMOTE_ADDR'],
				));
				$request_url = 'https://www.google.com/recaptcha/api/siteverify';
				// exit;
		    curl_setopt($ch, CURLOPT_URL, $request_url);
		    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type' => 'application/x-www-form-urlencoded'));
		    curl_setopt($ch, CURLOPT_HEADER, 1);
		    curl_setopt($ch, CURLOPT_POST, 1);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_body);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    $output = curl_exec($ch);
		    $sfdc_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE );
		    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		    $sfdc_response_body = substr($output, $header_size);
		    curl_close($ch);
		    $response = json_decode($sfdc_response_body, true);
		    if (!$response['success']) {
		    	echo json_encode('Invlaid Captcha');
					exit;
		    }
			$conditions = array(
				'user_email_id' => $this->input->post('email_id'),
				'user_password' => md5($this->input->post('password')),
				'published' => 1
			);
			$result=$this->Common_model->find_data($this->table,'row','',$conditions);

			if(!empty($result))
			{
				$userdata = array(
						'user_id' => $result->id,
						'user_name' => $result->user_name,
						'user_seo_url' => $result->user_seo_url,
						'user_email_id' => $result->user_email_id,
						'user_signed_in' => true
				);

				$this->session->set_userdata('is_user_signed_in', $userdata);
				$this->session->set_userdata('previous_url', $this->input->post('page_url'));
				if($this->session->userdata('previous_url') == 'product_details' || $this->session->userdata('previous_url') == 'cart'){
					$msg['success_message'] = $this->session->userdata('previous_url');
					echo json_encode($msg);
					exit;
				}
				else
				{
					$msg1['message'] = 'home';
					echo json_encode($msg1);
					exit;
				}
			}
			else
			{
				$msg1['error_message'] = 'error';
				echo json_encode($msg1);
				exit;
			}
		}
		else
		{
			echo "0";
		}
	}
function forgot(){
		if ($this->validate_forgot_form() == TRUE)
		{
			$conditions = array(
				'user_email_id' => $this->input->post('email_id'),
				'published' => 1
			);
			$result=$this->Common_model->find_data($this->table,'row','',$conditions);
		if(!empty($result))
			{
				$id=uniqid();
				$tokens['LOGO'] = base_url().'assets/frontend/images/large/logo.png';
					$tokens['SITE_URL'] = base_url();
					$tokens['YEAR'] = date('Y');
					$tokens['TEMPPASS'] = $id;
					$table_mail['name']='folk_email_templates';
					$select = 'id,email_templates_subject,email_templates_content,published';
					$conditions = array('id' => 4,'published' => 1);
					$content=$this->Common_model->find_data($table_mail,'row','',$conditions,$select);

					$email_content = $content->email_templates_content;
					$subject = $content->email_templates_subject;
					$to = $this->input->post('email_id');
					$from = SITE_SENDER_EMAIL;

					$mail_flag = send_template_mail($tokens, $email_content, $subject, $to, $from);
					$this->db->set('user_password', md5($id)); //value that used to update column
$this->db->where('user_email_id', $this->input->post('email_id')); //which row want to upgrade
$this->db->update('folk_users');  //table name
					echo 1;
					exit;
			}
		else
			{
				$msg1['error_message'] = 'error';
				echo json_encode($msg1);
				exit;
			}
		}
		else
		{
			echo "0";
		}
	}
	function registration(){
		if($this->input->is_ajax_request() == true){
			if ($this->validate_registration_form() == TRUE)
			{
				$ch = curl_init();
				$request_body = http_build_query(array(
					'secret' => '6LciY00UAAAAALymL9tVxwcRiSyoLF15YdkYCLiN',
					'response' => $_REQUEST['g-recaptcha-response'],
					'remoteip' => $_SERVER['REMOTE_ADDR'],
				));
				$request_url = 'https://www.google.com/recaptcha/api/siteverify';
				// exit;
		    curl_setopt($ch, CURLOPT_URL, $request_url);
		    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type' => 'application/x-www-form-urlencoded'));
		    curl_setopt($ch, CURLOPT_HEADER, 1);
		    curl_setopt($ch, CURLOPT_POST, 1);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_body);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    $output = curl_exec($ch);
		    $sfdc_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE );
		    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		    $sfdc_response_body = substr($output, $header_size);
		    curl_close($ch);
		    $response = json_decode($sfdc_response_body, true);
		    if (!$response['success']) {
		    	echo json_encode('Invlaid Captcha');
					exit;
		    }

				$user_activation_code = md5($this->input->post('email_id'));
				$user_seo_url = strtolower(create_unique_slug($this->input->post('fname').''.$this->input->post('lname'), 'folk_users', 'user_email_id', $key = NULL, $value = NULL));

				$postdata = array(
					'user_name' => $this->input->post('fname').' '.$this->input->post('lname'),
					'user_email_id' => $this->input->post('email_id'),
					'user_password' => md5($this->input->post('password')),
					'user_seo_url' => $user_seo_url,
					'created'=>date('Y-m-d H:i:s'),
					'user_activation_code' => $user_activation_code,
					'published' => 0
				);
				$returnval=$this->Common_model->save_data($this->table,$postdata);
				if($returnval)
				{
					$tokens['LOGO'] = base_url().'assets/frontend/images/large/logo.png';
					$tokens['USERNAME'] = ucfirst($this->input->post('fname').' '.$this->input->post('lname'));
					$tokens['SITE_URL'] = base_url();
					$tokens['YEAR'] = date('Y');
					$tokens['CLICK HERE'] = base_url().'User/activation/'.$user_activation_code;
					$table_mail['name']='folk_email_templates';
					$select = 'id,email_templates_subject,email_templates_content,published';
					$conditions = array('id' => 1,'published' => 1);
					$content=$this->Common_model->find_data($table_mail,'row','',$conditions,$select);

					$email_content = $content->email_templates_content;
					$subject = $content->email_templates_subject;
					$to = $this->input->post('email_id');
					$from = SITE_SENDER_EMAIL;

					//echo SITE_SENDER_EMAIL;
					$mail_flag = send_template_mail($tokens, $email_content, $subject, $to, $from);
					//echo '<pre>'; print_r($mail_flag); exit;
					echo 1;
					exit;
				}
			}
			else
			{
				$msg['err_msg'] = validation_errors();
				echo json_encode($msg);
				exit;
			}
		}
	}
	function validate_registration_form() {
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('fname', 'first name', 'required');
		$this->form_validation->set_rules('lname', 'last name', 'required');
		$this->form_validation->set_rules('email_id', 'email address', 'trim|required|callback_is_chk_unique');
		$this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == TRUE)
		{
            return TRUE;
        }
		else
		{
            return FALSE;
        }
    }
	function validate_login_form(){
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><div class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></div></div>');
		$this->form_validation->set_rules('email_id', 'email address', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == TRUE)
		{

            return TRUE;
        }
		else
		{
            return FALSE;
        }
	}
	function is_chk_unique($str) {
		$conditions = array('user_email_id' => $str);
		$select = 'user_email_id';
		$count_email=$this->Common_model->find_data($this->table,'count','',$conditions,$select);
		if ($count_email > 0) {
		   $this->form_validation->set_error_delimiters('<div class="form-group has-error errmsg"><div class="control-label" for="inputError"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>','</div></div>');
			$this->form_validation->set_message('is_chk_unique', 'The email id you&rsquo;ve given is already registered with us.');
			return false;
		} else {
			return true;
		}
    }

function validate_forgot_form(){
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><div class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></div></div>');
		$this->form_validation->set_rules('email_id', 'email address', 'trim|required');
        if ($this->form_validation->run() == TRUE)
		{

            return TRUE;
        }
		else
		{
            return FALSE;
        }
	}
	function activation($activation_code = NULL){
		if($activation_code ==''){
			$this->session->set_flashdata('error_message', 'Access not allowed.');
            redirect(base_url());
		}
		else
		{
			$this->load->model('User_model');
			$conditions = array('user_activation_code' => $activation_code);
            $check_in_database = $this->User_model->find_data('count', $conditions);
			if($check_in_database > 0)
			{
                $conditions = array('user_activation_code' => $activation_code);
                $user = $this->User_model->find_data('row', $conditions);

                $is_user_active = $this->User_model->activate_user($activation_code);

               	if($is_user_active)
				{
                    $this->session->set_flashdata('success_message', 'Your account has been activated. Please login to access.');

                    $get_user_details = $this->User_model->find_data('row', array('id'=>$user->id));
					$userdata = array(
						'user_id' => $get_user_details->id,
						'user_name' => $get_user_details->user_name,
						'user_seo_url' => $get_user_details->user_seo_url,
						'user_email_id' => $get_user_details->user_email_id,
						'user_signed_in' => true
					);

					$this->session->set_userdata('is_user_signed_in', $userdata);
					redirect('my-account');

                }
				else
				{
                    $this->session->set_flashdata('error_message', 'Some problem occurred. Please contact site admin.');
                    redirect(base_url());
                }
            }
			else
			{
                $this->session->set_flashdata('error_message', 'Access not allowed.');
                redirect(base_url());
            }
		}
	}
	function account(){
		$this->public_init_elements->is_user_logged_in();
		$this->data['gender_list'] = array(''=>'Select','male'=>'Male','female'=>'Female');
		$conditions = array('id' =>$this->session->userdata['is_user_signed_in']['user_id']);
		$get_user_data = $this->Common_model->find_data($this->table,'row','',$conditions);
		if($get_user_data){
			$this->data['username'] = $get_user_data->user_name;
			$this->data['contact_number'] = $get_user_data->contact_number;
			$this->data['dob'] = ($get_user_data->dob !='0000-00-00') ? $get_user_data->dob : '';
			$this->data['gender'] = $get_user_data->gender;
		}


		if($this->input->post('submit'))
		{
			$user_seo_url = strtolower(create_unique_slug($this->input->post('user_name'), 'folk_users', 'user_email_id','id',$this->session->userdata['is_user_signed_in']['user_id']));
				$postdata = array(
					'user_name' => $this->input->post('user_name'),
					'dob'		=> $this->input->post('dob'),
					'gender'	=> $this->input->post('gender'),
					'contact_number'=>$this->input->post('contact_number'),
					'user_seo_url'=> $user_seo_url,
					'modified' => date('Y-m-d H:i:s')
				);
			$result = $this->Common_model->save_data($this->table,$postdata,$this->session->userdata['is_user_signed_in']['user_id']);
			if($result){
				$this->session->set_flashdata('success_message', 'You have successfully change your personal information');
				redirect('my-account');
			}
			else
			{
				$this->session->set_flashdata('error_message', 'Some Error occoured! Please Try Again');
			}
		}

		$this->data['common_account_left_panel'] = $this->load->view('frontend/elements/common_account_left_panel',$this->data, true);
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/my-account',$this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	function change_password(){
		$this->public_init_elements->is_user_logged_in();
		if($this->input->post('submit'))
		{
			$check_old_pwd = $this->Common_model->find_data($this->table,'row','',array('id'=>$this->session->userdata['is_user_signed_in']['user_id'],'user_password'=>md5($this->input->post('current_password'))));
			if(!empty($check_old_pwd)){
				$postdata = array(
				'user_password' => md5($this->input->post('new_password')),
				);
				$result = $this->Common_model->save_data($this->table,$postdata,$this->session->userdata['is_user_signed_in']['user_id']);
				if($result)
				{
					$this->session->set_flashdata('success_message', 'Password Successfully Changed');
					redirect('change-password');
				}
				else
				{
					$this->session->set_flashdata('error_message', 'Some Error occoured! Please Try Again');
					redirect('change-password');
				}
			}
			else
			{
					$this->session->set_flashdata('error_message', 'Invalid your old password.');
					redirect('change-password');
			}
		}
		$this->data['common_account_left_panel'] = $this->load->view('frontend/elements/common_account_left_panel',$this->data, true);
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/change-password', $this->data, true);
        $this->load->view('frontend/layout', $this->data);
	}
	function logout() {
		$user_id = $this->session->userdata['is_user_signed_in']['user_id'];
		$this->session->unset_userdata('order_coupon_details');
		$this->session->unset_userdata('shipping_address');
		$this->cart->destroy();
		$postData = array('last_logged_in' => date('Y-m-d H:i:s'));
		$result=$this->Common_model->save_data($this->table,$postData,$user_id);
		$this->session->unset_userdata('is_user_signed_in');
        redirect('/');
    }
	function orders(){
		$details_arr = array();
		$table['name']="folk_orders";
		$table['alias_name']='o';

		$select = 'o.id,o.user_id,o.txnid,o.amount,o.added_on,ods.order_id,ods.id as order_details_id,ods.product_name,ods.product_seo_name,ods.product_image,ods.product_unit_price,ods.product_color,ods.category_name,ods.product_quantity';
		$join[0] = array('table'=>'folk_order_details','table_alias'=>'ods','field'=>'order_id','table_master'=>'o','field_table_master'=>'id','type'=>'LEFT');
		$order_details=$this->Common_model->find_data($table,'array','',array('o.user_id'=>$this->session->userdata['is_user_signed_in']['user_id']),$select,$join);
		if($order_details){
			foreach($order_details as $details){
				$details_arr[$details->txnid][] = $details;
			}
		}
		//echo '<pre>';
		//print_r($details_arr);die;
		$this->data['details_arr'] = $details_arr;
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/orders',$this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
	function order_details($transaction_id){

		$get_order_details = $this->Common_model->find_data(array('name'=>'folk_orders'),'row','',array('txnid'=>$transaction_id,'user_id'=>$this->session->userdata['is_user_signed_in']['user_id']));
		$this->data['get_order_details'] = $get_order_details;
		$this->data['transaction_id'] = $transaction_id;
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/order-details',$this->data, true);
		$this->load->view('frontend/layout', $this->data);
	}
}
?>
