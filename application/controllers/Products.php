<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller
{
	var $data;
	function __construct()
	{				
		parent::__construct();
		$this->public_init_elements->init_elements();	
	}
	 
    function index($seo_name){			
		$table['name'] = 'folk_categories';
		$get_category = $this->Common_model->find_data($table,'row','',array('published'=>1,'seo_name'=>$seo_name));		
		if($get_category){
			$table1['name']="folk_products";
			$table1['alias_name']='p';	
			
			$select1 = 'p.id,p.category_id,p.name,p.seo_name,p.price,p.published,pai.image2,c.name as category_namep,p.qty,c.published as cat_pub';	
			
			$join1[0]['table']="folk_products_attribute_images";
			$join1[0]['table_alias']="pai";			
			$join1[0]['field']="product_id";
			$join1[0]['table_master']="p";
			$join1[0]['field_table_master']="id";			
			$join1[0]['type']="LEFT";
			
			$join1[1] = array('table'=>'folk_categories','table_alias'=>'c','field'=>'id','table_master'=>'p','field_table_master'=>'category_id','type'=>'LEFT');
			
			$group_by[0]='p.id';
			$order_by[0]=array('field'=>'id','type'=>'DESC');
			$this->data['product_details'] = $this->Common_model->find_data($table1,'array','',array('p.category_id'=>$get_category->id,'p.published'=>1),$select1,$join1,$group_by,$order_by);
			//echo '<pre>';
			//print_r($this->data['product_details']);die;
			$this->common_product_left_bar($this->data['product_details'],$get_category);	
		}
		$this->data['meta']['meta_tag']=$get_category->meta_title;
		$this->data['meta']['meta_keywords']=$get_category->meta_keywords;
		$this->data['meta']['meta_description']=$get_category->meta_description;
		$this->load->view('frontend/elements/head',$this->data['meta']);
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/products-listing',$this->data, true);	
		$this->load->view('frontend/layout', $this->data);
	}
	function common_product_left_bar($all_product_arr=NULL,$get_category=NULL){	
		if($all_product_arr){
			$table_cmn_left['name']="folk_products";
			$table_cmn_left['alias_name']='p';				
			$select_cmn_left = 'p.category_id,af.feature_value_id,fv.value,f.title';			
			$join[0] = array('table'=>'folk_assign_features','table_alias'=>'af','field'=>'product_id','table_master'=>'p','field_table_master'=>'id','type'=>'LEFT');
			$join[1] = array('table'=>'folk_features_values','table_alias'=>'fv','field'=>'id','table_master'=>'af','field_table_master'=>'feature_value_id','type'=>'LEFT');
			$join[2] = array('table'=>'folk_features','table_alias'=>'f','field'=>'id','table_master'=>'fv','field_table_master'=>'features_id','type'=>'LEFT');
			$group_by[0] = 'fv.id';
			$order_by[0]=array('field'=>'f.id','type'=>'ASC');
			$all_features = $this->Common_model->find_data($table_cmn_left,'array','',array('p.category_id'=>$get_category->id),$select_cmn_left,$join,$group_by,$order_by);
			
			$this->data['category_id']= $get_category->id;
			if($all_features){
				$feature_arr = array();
				foreach($all_features as $feature):
					$feature_arr[$feature->title][] = $feature;
				endforeach;
			}
			
			$this->data['feature_arr'] = $feature_arr;			
		}
		$this->data['product_left_bar'] = $this->load->view('frontend/elements/common_product_left_bar',$this->data, true);	
	}
	function product_details($seo_name){
		
		$table['name']="folk_products";
		$table['alias_name']='p';		
		
		$select = 'p.*,group_concat(DISTINCT aa.attribute_value_id) as attribute_value_id,group_concat(DISTINCT av.attribute_id) as attribute_id,av.value as value,av.id as color_id,a.title,ai.image1,c.name as category_name,c.seo_name as category_seo_name,ai.image5,ai.image6,ai.image7';	
		
		$join[0] = array('table'=>'folk_assign_attributes','table_alias'=>'aa','field'=>'product_id','table_master'=>'p','field_table_master'=>'id','type'=>'LEFT');
		$join[1] = array('table'=>'folk_attribute_values','table_alias'=>'av','field'=>'id','table_master'=>'aa','field_table_master'=>'attribute_value_id','type'=>'LEFT');
		$join[2] = array('table'=>'folk_attributes','table_alias'=>'a','field'=>'id','table_master'=>'av','field_table_master'=>'attribute_id','type'=>'LEFT');
		$join[3] = array('table'=>'folk_products_attribute_images','table_alias'=>'ai','field'=>'product_id','table_master'=>'p','field_table_master'=>'id','type'=>'LEFT'/* ,'and'=>' AND ai.product_id = 23' */);
		$join[4] = array('table'=>'folk_categories','table_alias'=>'c','field'=>'id','table_master'=>'p','field_table_master'=>'category_id','type'=>'LEFT');
		
		$this->data['row'] = $this->Common_model->find_data($table,'row','',array('p.seo_name'=>$seo_name),$select,$join);		
	
		$table1['name']="folk_similar_products";
		$table1['alias_name']='sp';		
		$select2 = 'sp.*, p.name,p.price,p.seo_name,pai.image3,p.category_id';		
		$join1[0] = array('table'=>'folk_products','table_alias'=>'p','field'=>'id','table_master'=>'sp','field_table_master'=>'similar_product_id','type'=>'LEFT');
		$join1[1] = array('table'=>'folk_products_attribute_images','table_alias'=>'pai','field'=>'product_id','table_master'=>'p','field_table_master'=>'id','type'=>'LEFT');	
		
		$this->data['similar_product_arr'] = $this->Common_model->find_data($table1,'array','',array('sp.product_id'=>$this->data['row']->id),$select2,$join1,'','','','','','','','',1);
		
		$table2['name']="folk_ratings";
		$table2['alias_name']='r';		
		$select3 = 'r.*,u.user_name';		
		$join3[0] = array('table'=>'folk_products','table_alias'=>'p','field'=>'id','table_master'=>'r','field_table_master'=>'product_id','type'=>'LEFT');
		$join3[1] = array('table'=>'folk_users','table_alias'=>'u','field'=>'id','table_master'=>'r','field_table_master'=>'user_id','type'=>'LEFT');
		$this->data['ratings_details'] = $this->Common_model->find_data($table2,'array','',array('r.product_id'=>$this->data['row']->id),$select3,$join3);
		
		$this->load->view('frontend/elements/head',$this->data['row']);
		$this->data['maincontent'] = $this->load->view('frontend/maincontents/product-details',$this->data, true);	
		$this->load->view('frontend/layout', $this->data);
	}
	function rating(){
		if ($this->validate_product_rating() == TRUE){	
			$table['name'] = 'folk_ratings';
			$postdata = array(
				'product_id' => $this->input->post('product_id'),
				'user_id' => $this->session->userdata['is_user_signed_in']['user_id'],
				'count_rating' => $this->input->post('count_rating'),	
				'subject' => $this->input->post('subject'),								
				'comment' => $this->input->post('comment'),										
				'published' =>1
			);	
			$returnval=$this->Common_model->save_data($table,$postdata);   				
			if($returnval) 
			{
				$this->session->set_flashdata('success_message', 'You have successfully added your review.');
				redirect('product-details/'.$this->input->post('product_seo_url'));
			}
		}
	}
	function validate_product_rating(){
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('comment', 'comment', 'required');
        if ($this->form_validation->run() == TRUE)
		{			
            return TRUE;
        } 
		else 
		{
            return FALSE;
        }	
	}	

}
?>