<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller
{
    var $data;
	

    function  __construct() 
	{
        parent::__construct();
			
		$this->load->library('admin_init_elements');
		$this->load->model('User_model');
    }

	function index()
	{ 
		$this->admin_init_elements->init_elements();
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/home',$data,true);        
        $this->load->view('admin/layout', $this->data);
	}	
    function login()
	{ 	
        if($this->session->userdata('is_admin_logged_in') == true)
        {
            redirect('admin/home');
        }
		
		$this->admin_init_elements->init_head();
		
		if($this->input->post('login'))
		{ 	
			if(($this->input->post('is_cookie') == 1) && ($this->input->post('user_password') == $this->input->post('oripassword')))
			{
				$password=$this->input->post('user_password');
			}
			else
			{
				$password=md5($this->input->post('user_password'));
			}
			$condition=array(
				'user_email_id'=>$this->input->post('user_email_id'),
				'user_type'=>$this->input->post('user_type'),
				'user_password'=>$password,
				'user_type'=>'SA'
				);
			$row=$this->User_model->find_data('row',$condition);	
			
			if(count($row) >0)
			{
				$data = array(
							'userid'			=> $row->id, 
							'user_email_id' 	=> $row->user_email_id,
							'user_name' 		=> $row->user_name,
							'last_logged_in'	=> $row->last_logged_in,
						);
            	$this->session->set_userdata($data);
				$remember_me = $this->input->post("remember_me"); 
				if($remember_me == 1)
				{
					$this->input->set_cookie('usermail_cookie', $this->input->post("user_email_id"), 36000);
					$this->input->set_cookie('password_cookie', $this->input->post("user_password"), 36000);
				}
				else
				{
					delete_cookie("usermail_cookie");
					delete_cookie("password_cookie");
				}					
				$data = array (
								'is_admin_logged_in' => true,
								'user_email_id' => $this->input->post('user_email_id')
								);
				$this->session->set_userdata($data);
				$this->session->set_flashdata('success_message','Logged in Successfully');
				redirect('admin/home');
			}
			else
			{ 
				$this->session->set_flashdata('error_message','Invalid Login');
				redirect(current_url());
			}
		}
       $this->data['maincontent'] = $this->load->view('admin/maincontents/login', $data, true);
       $this->load->view('admin/layout-login', $this->data);
    }
    function password_change()
	{
		$this->admin_init_elements->init_elements();
		
        if($this->input->post('submit'))
        {       
            if ($this->validate_form_data() == TRUE)
            {
				 $postdata = array(
						 'user_password' 	=> md5($this->input->post('user_new_password')),
						 'modified'			=> date('Y-m-d H:i:s')
						);
				if($this->User_model->save_data($postdata,$this->session->userdata('userid'))>0)
				{
					$this->session->set_flashdata('success_message','Password has been changed successfully');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some error occured.Please try again.');
				}
                redirect(current_url());                
            }
            else
            {
				$data = validation_errors();
            }
        }
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/change-password', $data, true);
        $this->load->view('admin/layout', $this->data);
    }
    function logout()
    {
		$this->admin_init_elements->init_elements();
		$postData=array(
			'last_logged_in' => $this->session->userdata('admin_last_loggedin'),
			'modified'=>date('Y-m-d H:i:s')
		);
		$update_logged_out=$this->User_model->save_data($postData,$this->session->userdata('userid'));
		if($update_logged_out > 0)
		{	
			$this->session->unset_userdata('userid');
			$this->session->unset_userdata('user_email_id');
			$this->session->unset_userdata('user_fname');
			$this->session->unset_userdata('user_lname');
			$this->session->unset_userdata('last_logged_in');
			$this->session->unset_userdata('is_admin_logged_in');
			
			$this->session->set_flashdata('success_message','Logged out successfully');
			redirect('admin/login');
		}
		
    }
    function validate_form_data()
    {
     	$this->load->library('form_validation');		
	
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		
        $this->form_validation->set_rules('user_old_password', 'old password', 'required|callback_is_matches_password');
		$this->form_validation->set_rules('user_new_password', 'new password', 'required|min_length[6]');
		$this->form_validation->set_rules('user_confirm_password', 'confirm password', 'required|matches[user_new_password]');

        if ($this->form_validation->run() == true)
        {
         	return true;
        }
        else
        {
            return false;
        }
    }	

    function is_matches_password($str)
    {
		$this->admin_init_elements->init_elements();
		$record = $this->User_model->find_data('row',array('folk_users.id'=>$this->session->userdata('userid'),'user_type'=>'SA'));
		$user_password = $record->user_password;
		$form_old_password = md5($this->input->post('user_old_password')); 
		if($user_password == $form_old_password)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_matches_password', 'Wrong old password given.');
			return false;
		}
    }
}
?>