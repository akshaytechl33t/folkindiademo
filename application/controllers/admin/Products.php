<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_products';	
    }

	function index(){      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		$table['name']="folk_products";
		$table['alias_name']='p';
		
		$select = 'p.*,c.name as category_name ';		
		$join[0]['table']="folk_categories";
		$join[0]['table_alias']="c";		
		$join[0]['field']="id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="category_id";			
		$join[0]['type']="LEFT";		
		
		$this->data['rows'] = $this->Common_model->find_data($table,'array','','',$select,$join);	
				
		$this->data['page_title']  = "Product list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/products-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Products";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;
		$field['checked_n'] = false;
		
		$table['name'] = 'folk_categories';
		$this->data['category_arr']=$this->Common_model->find_data($table,'list',array('key'=>'id','value'=>'name','empty_name'=>' category'),$condition=array('published'=>'1'));
			
		$this->data['warranty_arr'] = array(''=>'Select Month / Year','year'=>'Year','month'=>'Month');
		
		$table_attribute['name']="folk_attributes";
		$table_attribute['alias_name']='a';		
		$select = 'a.*,av.value,av.id as attribute_value_id';		
		$join[0]['table']="folk_attribute_values";
		$join[0]['table_alias']="av";		
		$join[0]['field']="attribute_id";
		$join[0]['table_master']="a";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";		
		
		$attribute_arr = $this->Common_model->find_data($table_attribute,'array','',array('a.published'=>1),$select,$join);
		if(!empty($attribute_arr)){
			$attribute_array = array();
			foreach($attribute_arr as $attr_details):
				$attribute_array[$attr_details->title][] = $attr_details;
			endforeach;
		}
		$this->data['attribute_array'] = $attribute_array;
		
		$table_feature['name']="folk_features";
		$table_feature['alias_name']='f';		
		$select = 'f.*,fv.value,fv.id as features_value_id';		
		$join[0]['table']="folk_features_values";
		$join[0]['table_alias']="fv";		
		$join[0]['field']="features_id";
		$join[0]['table_master']="f";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";		
		
		$feature_arr = $this->Common_model->find_data($table_feature,'array','',array('f.published'=>1),$select,$join);
		if(!empty($feature_arr)){
			$feature_array = array();
			foreach($feature_arr as $feature_details):
				$feature_array[$feature_details->title][] = $feature_details;
			endforeach;
		}
		$this->data['feature_array'] = $feature_array;
		
		$table_smlr_prdct['name'] = 'folk_products';
		$this->data['similar_product_arr'] = $this->Common_model->find_data($table_smlr_prdct,'array','',array('published'=>1),'id,name');
		
		if($this->input->post('action') == 'add')
		{
			$get_error=array();	
			if ($this->validate_product_form() == TRUE)
			{	
					
				$postdata = array(
					'category_id'        => $this->input->post('category_id'),
					'name'        => $this->input->post('name'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('name'), $this->table, 'seo_name', $key = NULL, $value = NULL)),
					'bag_no'        => $this->input->post('bag_no'),
					'price'      	  => $this->input->post('price'),
					'qty'      	 	 => $this->input->post('qty'),
					'b2b_qty_limit'      	  => $this->input->post('b2b_qty_limit'),
					'weight'		=>$this->input->post('weight'),
					'description'	 => $_REQUEST['description'],
					'instruction' 	=> $_REQUEST['instruction'],
					'warranty'       => $this->input->post('warranty').'-'.$this->input->post('warranty_month'),
					'check_attribute'=>$this->input->post('attr_checkbox'),
					'check_feature'=>$this->input->post('feature_checkbox'),
					'check_similar_product'=>$this->input->post('similar_product_checkbox'),	
					'meta_tag'=>$this->input->post('meta_tag'),	
					'meta_keywords'=>$this->input->post('meta_keywords'),	
					'meta_description'=>$this->input->post('meta_description'),	
					'published'   => $this->input->post('published')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){					
					$lastInsertId = $this->db->insert_id();						
				################################## assign attribute value insert ##################################	
									
					$table2['name'] = 'folk_assign_attributes';			
					$attribute_value_id_arr= $this->input->post('attribute_value_id');
					
					if($attribute_value_id_arr){
						foreach($attribute_value_id_arr as $attr_value_id):
						$postData2[]=array(
										'product_id'=>$lastInsertId,
										'attribute_value_id'=>$attr_value_id,
										'published'	=> $this->input->post('published')
									);
						endforeach; 					
						$resultAttrValue = $this->Common_model->save_data($table2,$postData2,'',1);
					}
					
				################################## assign features value insert ##################################	
				
					$table3['name'] = 'folk_assign_features';			
					$features_values= $this->input->post('features_value_id');
					
					if($features_values){
						foreach($features_values as $feature_values):
						$postData3[]=array(
										'product_id'=>$lastInsertId,
										'feature_value_id'=>$feature_values,
										'published'	=> $this->input->post('published')
									);						
						endforeach;
						$resultfeatureValue=$this->Common_model->save_data($table3,$postData3,'',1);
					}					
					
				################################## similar product inseted ##################################	
				
					$table4['name'] = 'folk_similar_products';			
					$similar_product_value_id= $this->input->post('similar_product_id');
					if($similar_product_value_id){
						foreach($similar_product_value_id as $similar_prdct_id):
						$postData4[]=array(
										'product_id'=>$lastInsertId,
										'similar_product_id'=>$similar_prdct_id,
										'published'	=> $this->input->post('published')
									);						
						endforeach;
						$resultsmlrPrdct=$this->Common_model->save_data($table4,$postData4,'',1);
					}
					
					$this->session->set_flashdata('success_message','Products has been added successfully.');
					redirect('admin/products');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
				}								
			}
			else
			{						
				$this->data['field']['category_id']=set_value('category_id');	
				$this->data['field']['name']=set_value('name');	
				$this->data['field']['bag_no']=set_value('bag_no');
				$this->data['field']['price']=set_value('price');
				$this->data['field']['qty']=set_value('qty');
				$this->data['field']['b2b_qty_limit']=set_value('b2b_qty_limit');				
				$this->data['field']['bag_no']=set_value('bag_no');
				$this->data['field']['weight']=set_value('weight');
				$this->data['field']['description']=set_value('description');
				$this->data['field']['instruction']=set_value('instruction');
				$this->data['field']['warranty']=set_value('warranty');
				$this->data['field']['warranty_month']=set_value('warranty_month');
				$this->data['field']['published']=set_value('published');				
			}
		}
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/products-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit product";
		
		$table['name'] = 'folk_categories';
		$this->data['category_arr']=$this->Common_model->find_data($table,'list',array('key'=>'id','value'=>'name','empty_name'=>' category'),$condition=array('published'=>'1'));
			
		$this->data['warranty_arr'] = array(''=>'Select Month / Year','year'=>'Year','month'=>'Month');
		
		$table_attribute['name']="folk_attributes";
		$table_attribute['alias_name']='a';		
		$select = 'a.*,av.value,av.id as attribute_value_id';		
		$join[0]['table']="folk_attribute_values";
		$join[0]['table_alias']="av";		
		$join[0]['field']="attribute_id";
		$join[0]['table_master']="a";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";		
		
		$attribute_arr = $this->Common_model->find_data($table_attribute,'array','',array('a.published'=>1),$select,$join);
		if(!empty($attribute_arr)){
			$attribute_array = array();
			foreach($attribute_arr as $attr_details):
				$attribute_array[$attr_details->title][] = $attr_details;
			endforeach;
		}
		$this->data['attribute_array'] = $attribute_array;
		
		$table_feature['name']="folk_features";
		$table_feature['alias_name']='f';		
		$select = 'f.*,fv.value,fv.id as features_value_id';		
		$join[0]['table']="folk_features_values";
		$join[0]['table_alias']="fv";		
		$join[0]['field']="features_id";
		$join[0]['table_master']="f";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";		
		
		$feature_arr = $this->Common_model->find_data($table_feature,'array','',array('f.published'=>1),$select,$join);
		
		$table_smlr_prdct['name'] = 'folk_products';
		$this->data['similar_product_arr'] = $this->Common_model->find_data($table_smlr_prdct,'array','',array('published'=>1),'id,name');
		
		if(!empty($feature_arr)){
			$feature_array = array();
			foreach($feature_arr as $feature_details):
				$feature_array[$feature_details->title][] = $feature_details;
			endforeach;
		}
		$this->data['feature_array'] = $feature_array;
		
		$table1['name']="folk_products";
		$table1['alias_name']='p';							
		
		$select = 'p.*,c.id as category_id,c.name as category_name,group_concat(DISTINCT aa.attribute_value_id) as attribute_value_id,group_concat(DISTINCT af.feature_value_id) as feature_value_id,group_concat(DISTINCT sp.similar_product_id) as similar_product_id';	
		
		$join[0]['table']="folk_categories";
		$join[0]['table_alias']="c";
		
		$join[0]['field']="id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="category_id";		
		$join[0]['type']="LEFT";	
		
		$join[1]['table']="folk_assign_attributes";
		$join[1]['table_alias']="aa";
		
		$join[1]['field']="product_id";
		$join[1]['table_master']="p";
		$join[1]['field_table_master']="id";		
		$join[1]['type']="LEFT";
		
		$join[2]['table']="folk_assign_features";
		$join[2]['table_alias']="af";
		
		$join[2]['field']="product_id";
		$join[2]['table_master']="p";
		$join[2]['field_table_master']="id";		
		$join[2]['type']="LEFT";
		
		$join[3]['table']="folk_similar_products";
		$join[3]['table_alias']="sp";
		
		$join[3]['field']="product_id";
		$join[3]['table_master']="p";
		$join[3]['field_table_master']="id";		
		$join[3]['type']="LEFT";
		
		$row = $this->Common_model->find_data($table1,'row','',array('p.id'=>$id),$select,$join);
		/* echo '<pre>';
		print_r($row);die; */
		if($row){
			$this->data['id']=$row->id;	
			$this->data['field']['category_id']=$row->category_id;
			$this->data['field']['name']=$row->name;			
			$this->data['field']['bag_no'] = $row->bag_no;			
			$this->data['field']['price'] = $row->price;
			$this->data['field']['description'] = $row->description;
			$this->data['field']['instruction'] = $row->instruction;
			$this->data['field']['weight'] = $row->weight;
			$waranty = explode('-',$row->warranty);
			$this->data['field']['warranty'] = $waranty[0];
			$this->data['field']['warranty_month'] = $waranty[1];
			$this->data['field']['check_attribute'] = $row->check_attribute;
			$this->data['field']['check_feature'] = $row->check_feature;
			$this->data['field']['check_similar_product'] = $row->check_similar_product;
			$this->data['field']['attribute_value_id'] = $row->attribute_value_id;
			$this->data['field']['feature_value_id'] = $row->feature_value_id;
			$this->data['field']['similar_product_id'] = $row->similar_product_id;
			$this->data['field']['meta_tag'] = $row->meta_tag;
			$this->data['field']['meta_keywords'] = $row->meta_keywords;
			$this->data['field']['meta_description'] = $row->meta_description;
			$this->data['field']['qty'] = $row->qty;
			$this->data['field']['b2b_qty_limit'] = $row->b2b_qty_limit;			
			
			if($row->published ==1){
				$this->data['field']['checked_y']=true;
				$this->data['field']['checked_n']=false;
			}
			else
			{
				$this->data['field']['checked_n']=true;
				$this->data['field']['checked_y']=false;
			}
			if($this->data['field']['attribute_value_id']){ $this->data['field']['edit_attribute_value_id'] = explode(',',$this->data['field']['attribute_value_id']);}
			
			if($this->data['field']['feature_value_id']){$this->data['field']['edit_feature_value_id'] = explode(',',$this->data['field']['feature_value_id']);} 	
			
			if($this->data['field']['similar_product_id']){$this->data['field']['edit_similar_product_id'] = explode(',',$this->data['field']['similar_product_id']);} 	
			
		}
		
        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_product_form() == TRUE)
			{	
				$postdata = array(
					'category_id'        => $this->input->post('category_id'),
					'name'        		=> $this->input->post('name'),
					'seo_name'   	 => strtolower(create_unique_slug($this->input->post('name'), $this->table, 'seo_name', 'id',$id)),
					'bag_no'        => $this->input->post('bag_no'),
					'price'      	  => $this->input->post('price'),
					'qty'      	  => $this->input->post('qty'),
					'b2b_qty_limit'      	  => $this->input->post('b2b_qty_limit'),
					'weight'		=>$this->input->post('weight'),
					'description'	 => $_REQUEST['description'],
					'instruction' 	=> $_REQUEST['instruction'],
					'warranty'       => $this->input->post('warranty').'-'.$this->input->post('warranty_month'),
					'check_attribute'=>$this->input->post('attr_checkbox'),
					'check_feature'=>$this->input->post('feature_checkbox'),
					'check_similar_product'=>$this->input->post('similar_product_checkbox'),							
					'meta_tag'=>$this->input->post('meta_tag'),	
					'meta_keywords'=>$this->input->post('meta_keywords'),	
					'meta_description'=>$this->input->post('meta_description'),
					'modified'    => date('Y-m-d H:i:s'),	
					'published'   => $this->input->post('published')
				);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{	
					################# delete assign attribute #####################
					
					 $table_attribute['name'] = 'folk_assign_attributes';
						$get_product_attribute = $this->Common_model->find_data($table_attribute,'array','',array('product_id'=>$id));
						if(!empty($get_product_attribute)){
							foreach($get_product_attribute as $get_product_attributes):
								$deleteAttribute = $this->Common_model->delete_data($table_attribute,$get_product_attributes->id);
							endforeach;
						} 					
					################ insert assign attribute ######################
					$table2['name'] = 'folk_assign_attributes';			
					$attribute_value_id_arr= $this->input->post('attribute_value_id');
					
					foreach($attribute_value_id_arr as $attr_value_id):
					$postData2[]=array(
								'product_id'=>$id,
								'attribute_value_id'=>$attr_value_id,
								'published'	=> $this->input->post('published')
							);
					endforeach; 			
					$resultAttrValue = $this->Common_model->save_data($table2,$postData2,'',1);	
					
					################# delete assign feature #######################
						$table_feature['name'] = 'folk_assign_features';
						$get_product_feature = $this->Common_model->find_data($table_feature,'array','',array('product_id'=>$id));
						if(!empty($get_product_feature)){
							foreach($get_product_feature as $get_product_feature):
								$deleteFeature = $this->Common_model->delete_data($table_feature,$get_product_feature->id);
							endforeach;
						}
					
					################ insert assign feature ########################
					$table3['name'] = 'folk_assign_features';			
					$features_values= $this->input->post('features_value_id');
					
					foreach($features_values as $feature_values):
					$postData3[]=array(
									'product_id'=>$id,
									'feature_value_id'=>$feature_values,
									'published'	=> $this->input->post('published'),
									'modified'    => date('Y-m-d H:i:s')
								);
						
					endforeach;
					$resultfeatureValue=$this->Common_model->save_data($table3,$postData3,'',1);
					
					################# delete similar product #######################
						 $table_smlr_prdct['name'] = 'folk_similar_products';
						$get_smlr_product = $this->Common_model->find_data($table_smlr_prdct,'array','',array('product_id'=>$id));
						if(!empty($get_smlr_product)){
							foreach($get_smlr_product as $get_smlr_products):
								$deleteFeature = $this->Common_model->delete_data($table_smlr_prdct,$get_smlr_products->id);
							endforeach;
						} 
					################################## similar product inseted ##################################	
				
					$table4['name'] = 'folk_similar_products';			
					$similar_product_value_id= $this->input->post('similar_product_id');
					//print_r($similar_product_value_id);die;
					if($similar_product_value_id){
						foreach($similar_product_value_id as $similar_prdct_id):
						$postData4[]=array(
										'product_id'=>$id,
										'similar_product_id'=>$similar_prdct_id,
										'published'	=> $this->input->post('published'),
										'modified'    => date('Y-m-d H:i:s')
									);						
						endforeach;
						$resultsmlrPrdct=$this->Common_model->save_data($table4,$postData4,'',1);
					}
					
					$this->session->set_flashdata('success_message','Product has been updated successfully.');
					redirect('admin/products');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.');  					
				}	
			}
			else
			{
				$this->data['field']['title']=set_value('title');	
				$this->data['field']['published']=set_value('published');			
			}
		}			
		$this->data['maincontent'] = $this->load->view('admin/maincontents/products-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_product_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');			
		if($this->uri->segment(3) == 'add'){
			$this->form_validation->set_rules('name', 'name', 'trim|required|callback_is_unique_name');
		}		
		
		$this->form_validation->set_rules('bag_no', 'bag_no', 'required');			
		$this->form_validation->set_rules('price', 'price', 'required');	
		$this->form_validation->set_rules('qty', 'qty', 'required');	
		$this->form_validation->set_rules('b2b_qty_limit', 'b2b qty limit', 'required');			
		$this->form_validation->set_rules('weight', 'weight', 'required');			
		$this->form_validation->set_rules('description', 'description', 'required');			
		$this->form_validation->set_rules('instruction', 'instruction', 'required');
		$this->form_validation->set_rules('warranty', 'warranty', 'required');
		$this->form_validation->set_rules('warranty_month', 'warranty_month', 'required');	
		
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function delete()
	{			
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	function is_unique_name($str)
    {    
		$conditions = array('name'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('name'=>$str,'id !='=>$this->input->post('id'));
		}
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	

		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_unique_name', 'The name must contain a unique value.');
			return false;
		}       
    }
	function delete_attribute(){		
		$table['name'] = 'folk_attribute_values';
		$returnVal = $this->Common_model->delete_data($table,$this->input->post('id'));
		echo json_encode($returnVal);
	}
	####################################
	function edit_attributes()
	{
		$id = $this->input->post('value_id');
		if($this->input->post('value'))
		{
			foreach($this->input->post('value') as $index=>$val)
			{
				$res = $this->Common_model->save_data(array('name'=>'folk_attribute_values'),array('value'=>$val),$id[$index]);
			}
		}
		$this->session->set_flashdata('success_message','Attributes has been updated successfully.');
		redirect('admin/attributes');
	}
}
?>