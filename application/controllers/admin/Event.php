<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller
{
    var $data;
    function  __construct() 
	{
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_events';
    }

	function index()
	{ 	
		$this->data['page_title'] = 'Event listing';
			$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		$this->data['rows'] = $this->Common_model->find_data($this->table,'array');		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/event-listing',$this->data,true);        
        $this->load->view('admin/layout', $this->data);
	}
    function add()
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = "add";
		
		$this->data['checked_y']=true;
		$this->data['checked_n']=false;		
        if($this->input->post('action') == 'add')
		{			            	
			if ($this->validate_event_form() == TRUE)
			{ 
				if($_FILES['image']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image']['name']))
					{
						$path='uploads/images/event/';					
						$upload=upload_image('image',$path); 	
						$img = $upload;						
						if($img[0] !='error-upload')
						{
							$postData=array(
								'title'=>$this->input->post('title'),
								'seo_url'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_url', $key = NULL, $value = NULL)),
								'image'=>$img[1][0],
								'short_description'=>$_REQUEST['short_description'],
								'long_description'=>$_REQUEST['long_description'],
								'meta_title'=>$_REQUEST['meta_title'],
								'meta_description'=>$_REQUEST['meta_description'],
								'meta_keywords'=>$_REQUEST['meta_keywords'],
								'published'	=> $this->input->post('published')
							);			
							$result=$this->Common_model->save_data($this->table,$postData);		
							if($result)
							{
								$this->session->set_flashdata('success_message','Event has been added successfully.');
							}
							else
							{
								$this->session->set_flashdata('error_message','Addition process was failed.');
							}	
							redirect('admin/event');	
						}
						else
						{							
							$this->data['error_message']=$img[1];	
							$this->data['field']['title']=set_value('title');	
							$this->data['field']['short_description']=set_value('short_description');	
							$this->data['field']['long_description']=set_value('long_description');	
							$this->data['field']['meta_title']=set_value('meta_title');	
							$this->data['field']['meta_description']=set_value('meta_description');	
							$this->data['field']['meta_keywords']=set_value('meta_keywords');	
							($this->input->post('published')==1)? $data['field']['checked_y']=true: $data['field']['checked_n']=true;
						}
					}
					
				}
				else
				{	
					$get_error = 'Image cannot be blank';	
					$this->data['error_message'] = $get_error;	
					$this->data['field']['title']=set_value('title');	
					$this->data['field']['short_description']=set_value('short_description');	
					$this->data['field']['long_description']=set_value('long_description');	
					$this->data['field']['meta_title']=set_value('meta_title');	
					$this->data['field']['meta_description']=set_value('meta_description');	
					$this->data['field']['meta_keywords']=set_value('meta_keywords');
					$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
					($this->input->post('published')==1)? $data['field']['checked_y']=true: $data['field']['checked_n']=true;		
				}	
			}			            
        }		
		$this->data['page_title']  = "Add Event";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/event-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function edit($id)
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "Edit";

		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);

		$this->data['id']=$row->id; 
		$this->data['field']['title']=$row->title;
		$this->data['field']['image']=$row->image;
		$this->data['field']['short_description']=$row->short_description;
		$this->data['field']['long_description']=$row->long_description;
		$this->data['field']['meta_title']=$row->meta_title;
		$this->data['field']['meta_keywords']=$row->meta_keywords;
		$this->data['field']['meta_description']=$row->meta_description;
		
		if($row->published ==1){
			$this->data['checked_y']=true;
			$this->data['checked_n']=false;
		}
		else
		{
			$this->data['checked_n']=true;
			$this->data['checked_y']=false;
		}
		 		
        if($this->input->post('action') == 'Edit')
		{
           if ($this->validate_event_form() == TRUE)
			{						
				$exist_image=$this->input->post('old_image');
				if(array_filter($_FILES['image']['name']))
				{		
					$path='uploads/images/event/';					
					$upload=upload_image('image',$path); 	
					$img = $upload;																		
					$upload =$img[1][0];;
					@unlink($path.$exist_image[0]);
				}
				else
				{
					$upload = $exist_image[0];
				}
				if($upload[0]!='error-upload')
				{
					$postdata=array(
							'title'=>$this->input->post('title'),
							'seo_url'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_url', 'id',$id)),
							'image'=>$upload,
							'short_description'=>$_REQUEST['short_description'],
							'long_description'=>$_REQUEST['long_description'],
							'meta_title'=>$_REQUEST['meta_title'],
							'meta_description'=>$_REQUEST['meta_description'],
							'meta_keywords'=>$_REQUEST['meta_keywords'],
							'published'	=> $this->input->post('published'),
							'modified'=>date('Y-m-d H:i:s')
						);
					$result=$this->Common_model->save_data($this->table,$postdata,$id);								
					if($result){						
						$this->session->set_flashdata('success_message','Event has been successfully updated.');
						redirect('admin/event');						
					}
					else
					{				
						$this->session->set_flashdata('error_message','Update process was failed.');  
					}
				}
				else
				{
					$this->data['error_message']=$upload[1];
					$this->data['field']['title']=set_value('title');	
					$this->data['field']['short_description']=set_value('short_description');	
					$this->data['field']['long_description']=set_value('long_description');	
					$this->data['field']['meta_title']=set_value('meta_title');	
					$this->data['field']['meta_description']=set_value('meta_description');	
					$this->data['field']['meta_keywords']=set_value('meta_keywords');	
					($this->input->post('published')==1)? $data['field']['checked_y']=true: $data['field']['checked_n']=true;
				}	
           }
        }
        				
		$this->data['page_title']  = "Edit Event";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/event-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function validate_event_form()
	{
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		
		$this->form_validation->set_rules('title', 'title', 'required|callback_is_unique_title');
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
    }
      
	function delete()
	{		
		$row = $this->Common_model->find_data($this->table,'row','',array('id'=>$this->uri->segment(4)));
		
		if($row->image !=''){
			$path1 = 'uploads/images/event/'.$row->image;			
			@unlink($path1);
		}
		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Event has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }

	function change_status(){
		
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));						
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	
	function is_unique_title($str)
    {    
		$conditions = array('title'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('title'=>$str,'id !='=>$this->input->post('id'));
		}
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	

		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_unique_title', 'The title must contain a unique value.');
			return false;
		}       
    }
}
?>