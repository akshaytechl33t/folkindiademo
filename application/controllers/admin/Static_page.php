<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_page extends CI_Controller{
    var $data;

    function  __construct(){
		parent::__construct();
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->load->model('static_page_model');		
	}
    function index(){
       
	   	$data['success_message'] = '';
		$data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){ 
			$data['error_message'] = $this->session->flashdata('error_message');
		}
		$data['rows'] = $this->static_page_model->find_data();
		
		$data['page_title']  = "Manage Static Pages"; 
        $this->data['maincontent'] = $this->load->view('admin/maincontents/static-page-list',$data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add()
	{
        $data['success_message'] = '';
		$data['error_message'] = '';
	
		$data['checked_y']	= true;
		$data['checked_n']	= false;  
		
		$data['checked_menu']	= false;
		$data['checked_no_menu'] = false;  
		
		$data['view_page_title']  = "Add Static Page"; 
		$data['action'] = "add";
		
        if($this->input->post('action') == 'add')
		{  
            if($this->validate_static_page_form() == TRUE)
			{  
				$post_data = array(
									'page_name' 		=> $this->input->post('page_name'),
									'page_title' 		=> $this->input->post('page_title'),
									'page_seo_url' 		=> url_title(strtolower($this->input->post('page_name'))),
									'top_menu' 			=> ($this->input->post('top_menu') == '')?0:$this->input->post('top_menu'),
									'meta_title' 		=> $this->input->post('meta_title'),
									'meta_keyword' 		=> $this->input->post('meta_keyword'),
									'meta_description' 	=> $this->input->post('meta_description'),
									'page_content' 		=> $_REQUEST['page_content'],
									'published' 		=> $this->input->post('published')
								 );
								
                $return_val = $this->static_page_model->save_data($post_data,$id);
				if($return_val >= 0)
				{
                	$this->session->set_flashdata('success_message','Static page has been updated successfully.');
              	}
				else
				{
                   	$this->session->set_flashdata('error_message','Update process was failed.');                                   
                }
				redirect('admin/static_page');
            }
			else
			{ 
				$data['page_name'] = set_value('page_name',$this->input->post('page_name'));
				$data['page_title'] = set_value('page_title',$this->input->post('page_title'));		
				$data['top_menu'] = set_value('top_menu',$this->input->post('top_menu'));				
				$data['page_meta_title'] = set_value('meta_title',$this->input->post('meta_title'));
				$data['page_meta_keyword'] = set_value('meta_keyword',$this->input->post('meta_keyword'));
				$data['page_meta_description'] = set_value('meta_description',$this->input->post('meta_description'));
				$data['page_content'] = set_value('page_content',$this->input->post('page_content'));
				($this->input->post('published')==1)? $data['checked_y'] = true: $data['checked_n'] = true;	
				($this->input->post('top_menu')==1)? $data['checked_menu'] = true: $data['checked_no_menu'] = true;	
			}
        }		
       
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/static-page-add-modify',$data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function edit($id)
	{
        $data['success_message'] = '';
		$data['error_message'] = '';	
		
        if($this->input->post('action') == 'edit')
		{  
            if ($this->validate_static_page_form() == TRUE)
			{ 
				$post_data = array(
									'page_name' 		=> $this->input->post('page_name'),
									'page_title' 		=> $this->input->post('page_title'),
									'page_seo_url' 		=> url_title(strtolower($this->input->post('page_name'))),
									'top_menu' 		=> $this->input->post('top_menu'),
									'meta_title' 		=> $this->input->post('meta_title'),
									'meta_keyword' 		=> $this->input->post('meta_keyword'),
									'meta_description' 	=> $this->input->post('meta_description'),
									'page_content' 		=> $_REQUEST['page_content'],
									'published' 		=> $this->input->post('published'),
									'modified'			=>date('Y-m-d H:i:s')
								 ); 
                $return_val = $this->static_page_model->save_data($post_data,$id);
				if($return_val >= 0)
				{
                	$this->session->set_flashdata('success_message','Static page has been updated successfully.');
              	}
				else
				{
                   	$this->session->set_flashdata('error_message','Update process was failed.');                                   
                }
				redirect('admin/static_page');
                
            }
        }
		$condition = array('id'=>$id);
		$row = $this->static_page_model->find_data('row',$condition);		
		$data['id'] 					= $row->id;
		$data['page_name'] 				= $row->page_name;
		$data['page_title'] 			= $row->page_title;		
		$data['page_meta_title'] 		= $row->meta_title;
		$data['page_meta_keyword'] 		= $row->meta_keyword;
		$data['page_meta_description']	= $row->meta_description;
		$data['page_content'] 			= $row->page_content;
		$data['published'] 				= $row->published;
		$data['top_menu'] 				= $row->top_menu;
		
		($row->published==1)? $data['checked_y']= true: $data['checked_n']=true;  
		($row->top_menu==1)? $data['checked_menu']	= true: $data['checked_no_menu']=true;  
		
		$data['view_page_title']  = "Edit Static Page"; // here the variable name is changed because the database field name is using 'page_title'
		$data['action'] = "edit";
       
	   	//populate view, specific to this function
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/static-page-add-modify',$data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_static_page_form() {
		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('page_name', 'name', 'required');
		$this->form_validation->set_rules('page_title', 'title', 'required');
		$this->form_validation->set_rules('page_content', 'content', 'required');
		$this->form_validation->set_rules('published', 'status', 'required');		
		
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function change_status(){	
	
		if($this->input->post('action')=='_ajax_change_status')
		{
			$id 	= 	$this->input->post('id');
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			$returnVal = $this->static_page_model->save_data($postData,$id);
			
			if($returnVal)
			{
				echo 'success';
			}
		}		
	}
	function delete()
	{			
		$table['name'] = 'folk_static_pages';
		$returnVal = $this->Common_model->delete_data($table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	
	function about_us(){
		$table['name']='folk_our_story';
		$data['rows']=$this->Common_model->find_data($table,'array');
		$data['page_title'] = "Manage about us Templates";	
		$this->data['maincontent'] = $this->load->view('admin/maincontents/about-us-list', $data, true);
		$this->load->view('admin/layout', $this->data);	
	}
	function about_us_edit($id){
		
		$data['success_message'] = '';
		$data['error_message'] = '';	
		
		$condition = array('id'=>$id);
		$table['name'] = 'folk_our_story';
		$row = $this->Common_model->find_data($table,'row','',$condition);		
		$data['id'] 					= $row->id;
		$data['title'] 				= $row->title;
		$data['content'] 			= $row->content;	
		$data['published'] 				= $row->published;
		
		($row->published==1)? $data['checked_y']= true: $data['checked_n']=true;  
		
		$data['view_page_title']  = "Edit About us Page"; 
		
		if($_POST)
		{  
			$post_data = array(
					'title' 		=> $this->input->post('title'),					
					'content' 		=> $_REQUEST['content'],
					'published' 		=> $this->input->post('published'),
					'modified'			=>date('Y-m-d H:i:s')
				); 
			$return_val = $this->Common_model->save_data($table,$post_data,$id);
			if($return_val >= 0)
			{
				$this->session->set_flashdata('success_message','About us page has been updated successfully.');
			}
			else
			{
				$this->session->set_flashdata('error_message','Update process was failed.');                                   
			}
			redirect('admin/Static_page/about_us');
        }
		
		$data['page_title'] = "Edit about us Templates";	
		$this->data['maincontent'] = $this->load->view('admin/maincontents/about-us-add-edit-modify', $data, true);
		$this->load->view('admin/layout', $this->data);	
	}
	function about_us_change_status(){
		if($this->input->post('action')=='_ajax_change_status')
		{
			$table['name'] = 'folk_our_story';
			$id 	= 	$this->input->post('id');
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			$returnVal = $this->Common_model->save_data($table,$postData,$id);
			
			if($returnVal)
			{
				echo 'success';
			}
		}	
	}
	function about_us_delete()
	{			
		$table['name'] = 'folk_our_story';
		$returnVal = $this->Common_model->delete_data($table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
function media(){
		$table['name']='folk_media_section';
		$data['rows']=$this->Common_model->find_data($table,'array');
		$data['page_title'] = "Manage Media Templates";	
		$this->data['maincontent'] = $this->load->view('admin/maincontents/media-list', $data, true);
		$this->load->view('admin/layout', $this->data);	
	}
function media_edit($id){
		
		$data['success_message'] = '';
		$data['error_message'] = '';	
		
		$condition = array('id'=>$id);
		$table['name'] = 'folk_media_section';
		$row = $this->Common_model->find_data($table,'row','',$condition);		
		$data['id'] 					= $row->id;
		$data['title'] 				= $row->title;
		$data['content'] 			= $row->content;	
		$data['published'] 				= $row->published;
		
		($row->published==1)? $data['checked_y']= true: $data['checked_n']=true;  
		
		$data['view_page_title']  = "Edit Media Page"; 
		
		if($_POST)
		{  
			$post_data = array(
					'title' 		=> $this->input->post('title'),					
					'content' 		=> $_REQUEST['content'],
					'published' 		=> $this->input->post('published'),
					'modified'			=>date('Y-m-d H:i:s')
				); 
			$return_val = $this->Common_model->save_data($table,$post_data,$id);
			if($return_val >= 0)
			{
				$this->session->set_flashdata('success_message','Media page has been updated successfully.');
			}
			else
			{
				$this->session->set_flashdata('error_message','Update process was failed.');                                   
			}
			redirect('admin/Static_page/media');
        }
		
		$data['page_title'] = "Edit Media Templates";	
		$this->data['maincontent'] = $this->load->view('admin/maincontents/media-add-edit-modify', $data, true);
		$this->load->view('admin/layout', $this->data);	
	}
function media_change_status(){
		if($this->input->post('action')=='_ajax_change_status')
		{
			$table['name'] = 'folk_media_section';
			$id 	= 	$this->input->post('id');
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			$returnVal = $this->Common_model->save_data($table,$postData,$id);
			
			if($returnVal)
			{
				echo 'success';
			}
		}	
	}
function media_delete()
	{			
		$table['name'] = 'folk_media_section';
		$returnVal = $this->Common_model->delete_data($table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	
	
}
?>
