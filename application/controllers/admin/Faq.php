<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_faqs';	
    }

	function index(){      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		
		$this->data['rows']=$this->Common_model->find_data($this->table,'array');	
		$this->data['page_title']  = "Faq list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/faq-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Category";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;		
		
		if($this->input->post('action') == 'add')
		{
			if ($this->validate_faq_form() == TRUE)
			{
				$get_error='';
				$get_errors = array();
				
				$postdata = array(
						'question'        => $this->input->post('question'),
						'seo_url'    => strtolower(create_unique_slug($this->input->post('question'), $this->table, 'seo_url', $key = NULL, $value = NULL)),	
						'answer' => $_REQUEST['answer'],	
						'published'   => $this->input->post('published')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){
					$this->session->set_flashdata('success_message','Faq has been added successfully.');
					redirect('admin/faq');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
				}	
			}	
			else
			{				
				$this->data['field']['name']=set_value('question');					
				$this->data['field']['meta_title']=set_value('answer');
				$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
				($this->input->post('published')==1)? $this->data['field']['checked_y']=true: $this->data['field']['checked_n']=true;
			}
		}	
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/faq-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit Faq";
		
		$result_arr=array();
		$row = $this->Common_model->find_data($this->table,'row','',array('id'=>$id));	
		$this->data['id'] = $row->id;
		$this->data['field']['question'] = $row->question;
		$this->data['field']['answer'] = $row->answer;
		if($row->published ==1){
			$this->data['field']['checked_y']=true;
			$this->data['field']['checked_n']=false;
		}
		else
		{
			$this->data['field']['checked_n']=true;
			$this->data['field']['checked_y']=false;
		}	
		
        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_faq_form() == TRUE)
			{		
				$get_error='';
				$get_errors = array();
				
				$postdata = array(
					'question'        => $this->input->post('question'),
					'seo_url'    => strtolower(create_unique_slug($this->input->post('question'), $this->table, 'seo_url', $key = NULL, $value = NULL)),	
					'answer' => $_REQUEST['answer'],	
					'modified'    => date('Y-m-d H:i:s'),
					'published'   => $this->input->post('published')
				);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{	
					$this->session->set_flashdata('success_message','Faq has been updated successfully.');
					redirect('admin/faq');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.'); 						
					$this->data['field']['question']=set_value('question');							
					$this->data['field']['answer']=set_value('answer');
					$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
					($this->input->post('published')==1)? $this->data['field']['checked_y']=true: $this->data['field']['checked_n']=true;
				}	
				          				
            }else
			{
				$this->data['error_message']=$get_errors;
				$this->data['field']['answer']=set_value('answer',$this->data['field']['answer']);				
				$this->data['field']['question']=set_value('question',$this->data['field']['question']);
				$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
				($this->input->post('published')==1)? $this->data['field']['checked_y']=true: $this->data['field']['checked_n']=true;
			}
        }		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/faq-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_faq_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');			
		if($this->uri->segment(3) != 'edit'){
			$this->form_validation->set_rules('question', 'question', 'required|callback_is_chk_unique_question');
		}		
		else
		{			
			$this->form_validation->set_rules('question', 'question', 'required');			
		}				
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function delete()
	{
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	function is_chk_unique_question($str)
    {    
		$conditions = array('question'=>$str,'id'=>$this->input->post('id'));
		if($this->input->post('id'))
		{          
			$conditions = array('question'=>$str,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_name', 'The question field must contain a unique value.');
			return false;
		}       
    }

}
?>