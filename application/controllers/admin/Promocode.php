<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocode extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_promocodes';	
    }

	function index(){
      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}	
		
		$this->data['rows']=$this->Common_model->find_data($this->table,'array');		
		$this->data['page_title']  = "Promocode list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/promocode-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }

	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Country";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;
		
		if($this->input->post('action') == 'add')
		{
			if ($this->validate_promocode_form() == TRUE)
			{						
				$postdata = array(
					'name'   => $this->input->post('name'),
					'code' => $this->input->post('code'),
					'discount_amount' => $this->input->post('discount_amount'),
					'expire' => $this->input->post('expire'),
					'published'   => $this->input->post('published')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){					
					$this->session->set_flashdata('success_message','Promocode has been added successfully.');
					redirect('admin/promocode');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
					$this->data['field']['name']=set_value('name');	
					$this->data['field']['code']=set_value('code');	
					$this->data['field']['discount_amount']=set_value('discount_amount');	
					$this->data['field']['expire']=set_value('expire');	
					$this->data['field']['published']=set_value('published');
				}								
			}
			else
			{
				$this->data['field']['name']=set_value('name');	
				$this->data['field']['code']=set_value('code');	
				$this->data['field']['discount_amount']=set_value('discount_amount');	
				$this->data['field']['expire']=set_value('expire');	
				$this->data['field']['published']=set_value('published');
			}
		}
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/promocode-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit Country";
		
		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);		
		
		$this->data['id']=$row->id; 
		$this->data['field']['name']=$row->name;
		$this->data['field']['code']=$row->code;
		$this->data['field']['expire']=$row->expire;
		$this->data['field']['discount_amount']=$row->discount_amount;
		
		if($row->published ==1){
			$this->data['field']['checked_y']=true;
			$this->data['field']['checked_n']=false;
		}
		else
		{
			$this->data['field']['checked_n']=true;
			$this->data['field']['checked_y']=false;
		}

        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_promocode_form() == TRUE)
			{						
				$postdata = array(				
					'name'   => $this->input->post('name'),
					'code' => $this->input->post('code'),
					'expire' => $this->input->post('expire'),
					'discount_amount' => $this->input->post('discount_amount'),
					'modified'    => date('Y-m-d H:i:s'),
					'published'   => $this->input->post('published')					
				);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{					
					$this->session->set_flashdata('success_message','Promocode	has been updated successfully.');
					redirect('admin/promocode');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.');  
					$this->data['field']['name']=set_value('name');	
					$this->data['field']['code']=set_value('code');	
					$this->data['field']['discount_amount']=set_value('discount_amount');	
					$this->data['field']['expire']=set_value('expire');	
					$this->data['field']['published']=set_value('published');
				}					             				
            }
        }		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/promocode-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function validate_promocode_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');	
		
		if($this->uri->segment(3) == 'edit')
		{
			$this->form_validation->set_rules('name', 'name', 'strip_tags|trim|required');
			$this->form_validation->set_rules('code', 'code', 'strip_tags|trim|required');
			$this->form_validation->set_rules('discount_amount', 'discount amount', 'strip_tags|trim|required|numeric');
			$this->form_validation->set_rules('expire', 'expire', 'strip_tags|trim|required');
		}
		else
		{
			$this->form_validation->set_rules('name', 'name', 'strip_tags|trim|is_unique['.$this->table['name'].'.name]|required');
			$this->form_validation->set_rules('code', 'code', 'strip_tags|trim|required|is_unique['.$this->table['name'].'.code]');
			$this->form_validation->set_rules('discount_amount', 'discount amount', 'strip_tags|trim|required|numeric');
			$this->form_validation->set_rules('expire', 'expire', 'strip_tags|trim|required');
		}	
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }

	function delete()
	{		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }

	function change_status(){			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}

	function is_chk_unique_name($str)
	{    
		$conditions = array('name'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('name'=>$str,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_name', 'The name field must contain a unique value..');
			return false;
		}
	}
	function is_chk_unique_code($code)
	{    
		$conditions = array('code'=>$code);
		if($this->input->post('id'))
		{          
			$conditions = array('code'=>$code,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_code', 'The code field must contain a unique value..');
			return false;
		}
	}
	/*-----end ----*/
}
?>