<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_contact_us';	
		//$this->table['alias_name'] = 'enquiries';	
    }

	function index($product_id=0)
	{
      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		$this->data['page_title'] = "Contact us List";
		$this->data['rows']=$this->Common_model->find_data($this->table,'array','');
		$this->data['maincontent'] = $this->load->view('admin/maincontents/contact-us-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
		
	function delete()
	{		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
}
?>