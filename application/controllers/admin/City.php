<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City extends CI_Controller
{
   
    var $data;

    function  __construct(){
        parent::__construct();				
        $this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_states';	      
    }

	function listing()
	{ 		
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		
		$table['name']='folk_cities';
		$table['alias_name']='c';		
		
		$select = 'c.city_name,c.id,c.published,s.state_name,s.id as state_id';
		
		$join[0]['table']="folk_states";
		$join[0]['table_alias']="s";
		
		$join[0]['field']="id";
		$join[0]['table_master']="c";
		$join[0]['field_table_master']="state_id";		
		$join[0]['type']="left";
		
		$this->data['rows']=$this->Common_model->find_data($table,'array','','',$select,$join);		
		
		$this->data['page_title']  = "Manage City";				
        $this->data['maincontent'] = $this->load->view('admin/maincontents/city_list',$this->data,TRUE);		
		$this->load->view('admin/layout', $this->data);
    }	
	function add()
	{		
		$this->data['action'] = "add";
		/*-----------Load countries -------------*/
		$table['name'] = "folk_countries";
		$order_by[0]['field'] = 'name';
		$order_by[0]['type'] = 'ASC';		
		$this->data['country_list'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);
		$this->data['country_id']='';
		
		/*---------------------------------------------*/
		$this->data['id']='';
		$this->data['state_arr']=array(''=>'Select state');
		$this->data['city_name']='';
		$this->data['page_title']  = "Add City";
		$this->data['checked_y']=true;
		$this->data['checked_n']=false;
		
        if($this->input->post('action') == 'add')
		{
            if ($this->validate_city_form() == TRUE)
			{		
				$table1['name']="folk_cities";
				$postData = array(
						'country_id' => $this->input->post('country_id'),
						'state_id' => $this->input->post('state_id'),
						'city_name' => $this->input->post('city_name'),
						'city_seo_url' => create_unique_slug($this->input->post('city_name'), $this->db->dbprefix('cities'), 'city_seo_url'),
						'created' => date('Y-m-d H:i:s'),
						'modified' => date('Y-m-d H:i:s'),
						'published' => $this->input->post('published')
					);
				$returnVal = $this->Common_model->save_data($table1,$postData);
				if($returnVal)
				{
					$this->session->set_flashdata('success_message','City has been added successfully.');
					redirect('admin/city/listing');
				}
				else
				{
					
					$this->session->set_flashdata('error_message','Addition process was failed.');                                   
				}
            }
			else
			{	$this->data['country_id']=set_value('country_id',$this->input->post('country_id')); 
				$this->data['state_id']=set_value('state_id',$this->input->post('state_id')); 
				$this->data['city_name']=set_value('city_name',$this->input->post('city_name')); 
				$table['name']="folk_states";
				$select = '*'; $list=array('empty_name'=>' State','key'=>'id','value'=>'state_name'); $order_by[0]['field'] = 'state_name'; $order_by[0]['type'] = 'ASC';
		
				$this->data['state_arr'] = $this->Common_model->find_data($table,'list',$list,array('published'=>1,'country_id'=>$this->data['country_id']),$select,'','',$order_by);
				($this->input->post('published')==1)? $this->data['checked_y']=true: $this->data['checked_n']=true;
			}
        }
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/city_add_edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
	}
	function edit($id)
	{		
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		$this->data['action'] = "edit";	
		
		$table['name'] = "folk_countries";
		$order_by[0]['field'] = 'name';
		$order_by[0]['type'] = 'ASC';
		$this->data['country_list'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);
		
		$table1['name']="folk_cities";
		$conditions = array('id'=>$id);
		$row = $this->Common_model->find_data($table1,'row','',$conditions);
		
		if($row->published==1){
			$this->data['checked_y']=true;
			$this->data['checked_n']=false;
		}
		else
		{
			$this->data['checked_y']=false;
			$this->data['checked_n']=true;
		}
		
		$this->data['id'] = $id;		
		$this->data['country_id']=$row->country_id;
		$this->data['state_id']=$row->state_id;
		$this->data['city_name']=$row->city_name;
		$this->data['page_title']  = "Edit City";		
		
		$table['name']="folk_states";
		$select = '*';
		$list=array('empty_name'=>' State','key'=>'id','value'=>'state_name');		
		$order_by[0]['field'] = 'state_name';
		$order_by[0]['type'] = 'ASC';
		
		$this->data['state_arr'] = $this->Common_model->find_data($table,'list',$list,array('published'=>1,'country_id'=>$row->country_id),$select,'','',$order_by);
		
        if($this->input->post('action') == 'edit')
		{
			
            if ($this->validate_city_form() == TRUE)
			{						
				$postData = array(
							'country_id' => $this->input->post('country_id'),
							'state_id' => $this->input->post('state_id'),
							'city_name' => $this->input->post('city_name'),
							'city_seo_url' => create_unique_slug($this->input->post('city_name'), $this->db->dbprefix('cities'), 'city_seo_url'),
							'created' => date('Y-m-d H:i:s'),
							'modified' => date('Y-m-d H:i:s'),
							'published' => $this->input->post('published')
								);
                $returnVal = $this->Common_model->save_data($table1,$postData,$id);
				if($returnVal)
				{
                	$this->session->set_flashdata('success_message','City has been updated successfully.');
              	}
				else
				{
                   	$this->session->set_flashdata('error_message','Update process was failed.');                              
                }
				redirect('admin/city/listing/');
            }
        }
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/city_add_edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
	}
	function delete()
	{
		$table['name']='folk_cities';
		$returnVal = $this->Common_model->delete_data($table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','City has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function validate_city_form() 
	{
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('country_id', 'country', 'required');
		$this->form_validation->set_rules('state_id', 'state', 'required');
		$this->form_validation->set_rules('city_name', 'city', 'required|callback_is_chk_unique_city_name');
		$this->form_validation->set_rules('published', 'City status', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }	
	function change_status(){
		
		if($this->input->post('action')=='_ajax_change_status'){
			$id 	= 	$this->input->post('id');
			
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));			
			$table['name']='folk_cities';			
		
			$returnVal = $this->Common_model->save_data($table,$postData,$id);
			
			if($returnVal){
				echo 'success';
			}
		}
		
	}
	function is_chk_unique_city_name($str){    
       $conditions = array('city_name'=>$str);
       if($this->input->post('id'))
	   {          
          $conditions = array('city_name'=>$str,'id !='=>$this->input->post('id'));
       }
	   
		$table['name']='folk_cities';
		$select = '*';
		$query=$this->Common_model->find_data($table,'count','',$conditions,$select);	
      
       if($query == 0)
	   {
           return true;
       }
	   else
	   {
          $this->form_validation->set_message('is_chk_unique_city_name', 'The city field must contain a unique value.');
           return false;
       }
       
    }
	function loadStatebyCountryID()
	{	
		$table['name']="folk_states";
		$select = '*';
		$list=array('key'=>'id','value'=>'state_name');
		$order_by[0]['field'] = 'state_name';
		$order_by[0]['type'] = 'ASC';
		$this->data['state_arr'] = $this->Common_model->find_data($table,'list',$list,array('published'=>1),$select,'','',$order_by);
	}
}
