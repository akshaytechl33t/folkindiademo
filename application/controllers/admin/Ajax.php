<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{   
    var $data;
    function  __construct(){
        parent::__construct();			
		$this->admin_init_elements->init_elements();		
    }	
	function state_drop_down(){ 
		$country_id = $this->input->post('country_id');
		$table['name'] = 'folk_states';		
		$this->data['state_list'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' State','key'=>'id','value'=>'state_name'),array('published'=>1,'country_id'=>$country_id));
		$this->load->view('admin/elements/state_drop_down', $this->data,FALSE);
	}
	function edit_new_feature_remove(){
		$table2['name'] = 'folk_features_values';
		$id = $this->input->post('id');
		$result = $this->Common_model->delete_data($table2,$id);
		if($result){
			$respone['flag'] = 'success';
		}
		else
		{
			$respone['flag'] = 'error';
		}
		echo json_encode($respone);
	}
	function product_arttribute_image_delete(){	
	
		$res = $this->Common_model->save_data(array('name'=>'folk_products_attribute_images'),array($this->input->post('image_no')=>''),$this->input->post('id'));
		//echo $this->db->last_query();die;
		$respone['flag'] = 'success';
		echo json_encode($respone);
	}
}
?>