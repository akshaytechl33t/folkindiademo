<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_email extends CI_Controller
{
    var $data;

    function  __construct() 
	{
        parent::__construct();   
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
        $this->load->model('Cms_email_model');
    }
	
    function index()
	{
		$data['success_message'] = '';
		$data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$data['error_message'] = $this->session->flashdata('error_message');
		}	
		$data['rows']=$this->Cms_email_model->find_data('array');
		$data['page_title'] = "Manage CMS Email Templates";		
		
		$this->data['maincontent'] = $this->load->view('admin/maincontents/cms-email-list', $data, true);	
	
		$this->load->view('admin/layout', $this->data);
	}
    function edit($id)
	{
        if($this->input->post('save'))
        {			
            if ($this->validate_form_data() == true)
            {
				$postdata=array(
								'email_templates_subject'=>$this->input->post('email_templates_subject'),
								'email_templates_content'=>$_REQUEST['email_templates_content'],
								'modified'=>date('Y-m-d H:i:s'),
								'published'=>$this->input->post('published')
								);							
                $save=$this->Cms_email_model->save_data($postdata,$id);
				if($save > 0)
				{
					$this->session->set_flashdata('success_message','Email Template successfully updated.');
					redirect('admin/cms_email');
				}else
				{
					$this->session->set_flashdata('error_message','Some Error occurred. Please try again');
					redirect('admin/cms_email/edit/'.$id);
				} 
            }
        }
		$data['action'] = "Edit";
		$row=$this->Cms_email_model->find_data('row',array('id'=>$id));		
		$data['id']=$row->id;
		$data['email_templates_subject']=$row->email_templates_subject;
		$data['email_templates_content']=$row->email_templates_content;
        ($row->published ==1)? $data['checked_y']=true: $data['checked_n']=true;	
        $this->data['maincontent'] = $this->load->view('admin/maincontents/cms-email-add-modify', $data, true);
		
        $this->load->view('admin/layout', $this->data);
    }

	function add($action=NULL)
	{
		if($action!='superadmin'){redirect('admin/cms_email');}
		if($this->input->post('save'))
		{
			if($this->validate_form_data()==true)
			{
				$postData=array(
						'email_templates_subject'=>$this->input->post('email_templates_subject'),
						'email_templates_content'=>$_REQUEST['email_templates_content'],
						'created'=>date('Y-m-d H:i:s'),
						'modified'=>date('Y-m-d H:i:s'),
						'published'=>$this->input->post('published')
				);
				$insert=$this->Cms_email_model->save_data($postData);
				if($insert > 0)
				{
					$this->session->set_flashdata('success_message','Email Template is successfully created.');
					redirect('admin/cms_email');
				}else
				{
					$this->session->set_flashdata('error_message','Some Error occurred. Please try again');
					redirect('admin/cms_email');
				}
			}
		}
		$data['action']='Add';
		$this->data['maincontent'] = $this->load->view('admin/maincontents/cms-email-add-modify', $data, true);
    
        $this->load->view('admin/layout', $this->data);
	}

    function delete($id,$action=NULL)
	{		
		$delete=$this->Cms_email_model->delete($id);
		if($delete > 0)
		{
			$this->session->set_flashdata('success_message','Email Template is successfully deleted');
			redirect('admin/cms_email');
		}else
		{
			$this->session->set_flashdata('error_message','Some error occurred! Please try again');
			redirect('admin/cms_email');
		}
    }

	function change_status()
	{
		if($this->input->post('action')=='_ajax_change_status')
		{
			$id 	= 	$this->input->post('id');
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			$returnVal = $this->Cms_email_model->save_data($postData,$id);
			if($returnVal)
			{
				echo 'success';
			}
		}	
	}
	
    function validate_form_data()
    {
     	$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
        $this->form_validation->set_rules('email_templates_subject', 'e-mail subject', 'trim|required|callback_is_unique');
		$this->form_validation->set_rules('email_templates_content', 'e-mail content', 'trim|required');
			
        if ($this->form_validation->run() == true)
        {
         	return true;
        }
        else
        {
            return false;
        }
    }

    function is_unique($str)
    {
        if($this->input->post('action')=='Edit')
		{
			$condition=array('id !='=>$this->input->post('id'),'email_templates_subject'=>$str);
		}else
		{
			$condition=array('email_templates_subject'=>$str);
		}
		$row=$this->Cms_email_model->find_data('row',$condition);
		if(count($row)>0)
		{
			$this->form_validation->set_message('is_unique','Subject Already Exists. Please Try Different Subject');
			return false;
		}else
		{
			return true;
		}
    }
	
}
?>