<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Features extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_features';	
    }

	function index(){
      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}	
		
		$this->data['rows']=$this->Common_model->find_data($this->table,'array');		
		$this->data['page_title']  = "Features list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/features-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Features";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;
		if($this->input->post('action') == 'add')
		{
			if ($this->validate_features_form() == TRUE)
			{				
				$postdata = array(					
					'title'        => $this->input->post('title'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_name', $key = NULL, $value = NULL)),								
					'published'   => $this->input->post('published')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){
					$lastInsertId = $this->db->insert_id();	
					
					$table2['name'] = 'folk_features_values';			
					$value = $this->input->post('value');
					
					foreach($value as $values):
					$postData2=array(
									'value'	=>$values,
									'features_id'=>$lastInsertId,
									'published'	=> $this->input->post('published')
								);
					$result_asgn_cat=$this->Common_model->save_data($table2,$postData2);	
					endforeach;
					
					$this->session->set_flashdata('success_message','Features has been added successfully.');
					redirect('admin/features');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
				}								
			}
			else
			{
				$this->data['field']['title']=set_value('title');	
				$this->data['field']['description']=set_value('description');	
				$this->data['field']['value']=set_value('value');
				$this->data['field']['published']=set_value('published');
			}
		}
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/features-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit Frame Fetures";	
		
		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);			
		$result_arr=array();		
		if($row)
		{	
			$table1['name']="folk_features";
			$table1['alias_name']='f';							
			
			$select = 'fea.value,fea.id as value_id';	
			$join[0]['table']="folk_features_values";
			$join[0]['table_alias']="fea";
			
			$join[0]['field']="features_id";
			$join[0]['table_master']="f";
			$join[0]['field_table_master']="id";		
			$join[0]['type']="LEFT";	

			$ftr_value = $this->Common_model->find_data($table1,'array','',array('fea.features_id'=>$id),$select,$join);	
			
			$result_arr['id']=$row->id;	
			$result_arr['title']=$row->title;		
			$result_arr['published']=$row->published;
			$result_arr['value'] = $ftr_value;
			
		}
		if($result_arr){
			$this->data['id']=$row->id; 
			$this->data['field']['title']=$result_arr['title'];
					
			foreach($result_arr['value'] as $values){
				$fea_value = array($values->value_id=>$values->value);					
				$this->data['field']['features_value'][] = $fea_value;
			}
			
			if($result_arr['published'] ==1){
				$this->data['field']['checked_y']=true;
				$this->data['field']['checked_n']=false;
			}
			else
			{
				$this->data['field']['checked_n']=true;
				$this->data['field']['checked_y']=false;
			}
		}
		
        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_features_form() == TRUE)
			{						
				
				$postdata = array(	
						'title'        => $this->input->post('title'),
						'seo_name'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_name','id',$id)),				
						'modified'    => date('Y-m-d H:i:s'),	
						'published'   => $this->input->post('published')
					);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{	
				
				####################### value ########################			
						$table2['name'] = 'folk_features_values';			
						$value = $this->input->post('value');
						$value_id = $this->input->post('value_id');
						$mrg_value = array_combine($value_id,$value);
						foreach($value_id as $index=>$index_id):
						$postData2=array(
										'value'	=>$value[$index],
										'features_id'=>$id,
										'published'	=> $this->input->post('published')
									);
						$result_asgn_cat=$this->Common_model->save_data($table2,$postData2,$index_id);	
						endforeach;	
			
					$this->session->set_flashdata('success_message','Features has been updated successfully.');
					redirect('admin/features');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.');  
				}					             				
            }
			else
			{
				
				$this->data['field']['title']=set_value('title');		
				$this->data['field']['value']=set_value('value');
				$this->data['field']['published']=set_value('published');
			}
        }		
       	
		$this->data['maincontent'] = $this->load->view('admin/maincontents/features-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_features_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');		
		if($this->uri->segment(3) != 'edit')			
		{			
			$this->form_validation->set_rules('title', 'title', 'required|callback_is_chk_unique_title');
		}		
		else
		{			
			$this->form_validation->set_rules('title', 'title', 'required');
		} 	
			
		$this->form_validation->set_rules('value[]', 'value', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function delete()
	{		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	function is_chk_unique_title($str)
    {  
		$conditions = array('title'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('title'=>$str,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_title', 'Duplicate Entry');
			return false;
		}       
    }
#################################################################	
	function values($id)
	{
		if($id>0)
		{
			$frame = $this->Common_model->find_data(array('name'=>'folk_features'),'row','',array('id'=>$id));
			$this->data['rows'] = $this->Common_model->find_data(array('name'=>'folk_features_values'),'','',array('features_id'=>$id));
			$this->data['page_title']  = "Feature values for '".$frame->title."'";	
			$this->data['prev_url'] = 'features';
			$this->data['prev_page_title'] = 'Frame Features';
		  	$this->data['maincontent'] = $this->load->view('admin/maincontents/features-values',$this->data,TRUE);
	       	$this->load->view('admin/layout', $this->data);
		}
		else
		{
			redirect('admin/features');
		}
	}
}
?>