<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_attributes';	
    }

	function index(){      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
				
		$this->data['rows']=$this->Common_model->find_data($this->table,'array');		
		$this->data['page_title']  = "Attributes list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/attributes-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Attributes";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;
		$field['checked_n'] = false;
		
		if($this->input->post('action') == 'add')
		{
			if ($this->validate_attributes_form() == TRUE)
			{
				$postdata = array(
					'title'        => $this->input->post('title'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_name', $key = NULL, $value = NULL)),
					'description' => $_REQUEST['description'],
					'published'   => $this->input->post('published'),
					'add_value'=>$this->input->post('add_value')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){					
					$lastInsertId = $this->db->insert_id();					
			################################## assign attribute group insert ##################################
			
					$table1['name'] = 'eye_assign_attribute_group';			
					$fet_grp_id = $this->input->post('group_id');
					
					foreach($fet_grp_id as $fet_grp_ids):
					$postData1=array(
									'group_id'	=>$fet_grp_ids,
									'attribute_id'=>$lastInsertId,
									'published'	=> $this->input->post('published')
								);
					$result_asgn_cat=$this->Common_model->save_data($table1,$postData1);	
					endforeach;
					
			################################## assign attribute value insert ##################################	
									
					$table2['name'] = 'eye_attribute_values';			
					$fet_grp_value= $this->input->post('value');
					
					foreach($fet_grp_value as $fet_grp_values):
					$postData2=array(
									'value'	=>$fet_grp_values,
									'attribute_id'=>$lastInsertId,
									'published'	=> $this->input->post('published')
								);
					$result_asgn_cat=$this->Common_model->save_data($table2,$postData2);	
					endforeach;
					$this->session->set_flashdata('success_message','Attributes has been added successfully.');
					redirect('admin/attributes');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
				}								
			}
			else
			{						
				$this->data['field']['group_id']=set_value('group_id');	
				$this->data['field']['title']=set_value('title');	
				$this->data['field']['description']=set_value('description');
				$this->data['field']['add_value']=set_value($this->input->post('add_value'));
				$this->data['field']['published']=set_value('published');				
			}
		}
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/attributes-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit attributes";	
		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);
		
		$result_arr=array();		
		if($row)
		{
			$table1['name']="folk_attributes";
			$table1['alias_name']='a';							
			
			$select = 'av.value,av.id as value_id';	
			$join[0]['table']="folk_attribute_values";
			$join[0]['table_alias']="av";
			
			$join[0]['field']="attribute_id";
			$join[0]['table_master']="a";
			$join[0]['field_table_master']="id";		
			$join[0]['type']="LEFT";	

			$atr_value = $this->Common_model->find_data($table1,'array','',array('av.attribute_id'=>$id),$select,$join);
			//echo $this->db->last_query();die;
			//print_r($atr_value);die;
			$result_arr['id']=$row->id;	
			$result_arr['title']=$row->title;
			$result_arr['published']=$row->published;			
			$result_arr['value'] = $atr_value;			
		}	
		if($result_arr){
			$this->data['id']=$row->id; 
			$this->data['field']['title']=$result_arr['title'];			
						
			foreach($result_arr['value'] as $values){
				$attr_value = array($values->value_id=>$values->value);					
				$this->data['field']['atr_value'][] = $attr_value;
			}
			//print_r($this->data['field']['atr_value']);die;
			if($result_arr['published'] ==1){
				$this->data['field']['checked_y']=true;
				$this->data['field']['checked_n']=false;
			}
			else
			{
				$this->data['field']['checked_n']=true;
				$this->data['field']['checked_y']=false;
			}
		}

        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_attributes_form() == TRUE)
			{	
				$postdata = array(
					'title'        => $this->input->post('title'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('title'), $this->table, 'seo_name','id',$id)),					
					'modified'    => date('Y-m-d H:i:s'),	
					'published'   => $this->input->post('published')
				);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{	
					####################### value delete ########################						
						 /* if($this->input->post('add_value') == ''){
						/*$value_id =  $this->input->post('value_id');					
						 if(!empty($value_id)){
							 //echo 'sd';die;
							$table['name'] = 'folk_attribute_values';
							foreach($value_id as $values):
								$returnVal = $this->Common_model->delete_data($table,$values);
							endforeach;
						} 
					} 
					else 
					{ */
						$table2['name'] = 'folk_attribute_values';			
						$value = $this->input->post('value');
						$value_id = $this->input->post('value_id');
						$mrg_value = array_combine($value_id,$value);
						//print_r($mrg_value);					die;
						foreach($mrg_value as $key=>$mrg_values):
						$postData2=array(
									'value'	=>$mrg_values,
									'attribute_id'=>$id,
									'published'	=> $this->input->post('published'),
									'modified'    => date('Y-m-d H:i:s')									
								); 	
						$result_asgn_cat=$this->Common_model->save_data($table2,$postData2,$key);	
						endforeach;	
					/*}	*/
					$this->session->set_flashdata('success_message','Attributes has been updated successfully.');
					redirect('admin/attributes');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.');  					
				}	
			}
			else
			{
				$this->data['field']['title']=set_value('title');	
				$this->data['field']['published']=set_value('published');			
			}
		}			
		$this->data['maincontent'] = $this->load->view('admin/maincontents/attributes-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_attributes_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');			
		if($this->uri->segment(3) != 'edit'){			
			$this->form_validation->set_rules('title', 'title', 'required|callback_is_chk_unique_title');
		}		
		else
		{					
			$this->form_validation->set_rules('title', 'title', 'required');			
		}	
		
		
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function delete()
	{			
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	function is_chk_unique_title($str)
    {    
		$conditions = array('title'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('title'=>$str,'id !='=>$this->input->post('id'));
		}
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	

		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_title', 'The title must contain a unique value.');
			return false;
		}       
    }
	function delete_attribute(){		
		$table['name'] = 'folk_attribute_values';
		$returnVal = $this->Common_model->delete_data($table,$this->input->post('id'));
		echo json_encode($returnVal);
	}
	####################################
	function edit_attributes()
	{
		$id = $this->input->post('value_id');
		if($this->input->post('value'))
		{
			foreach($this->input->post('value') as $index=>$val)
			{
				$res = $this->Common_model->save_data(array('name'=>'folk_attribute_values'),array('value'=>$val),$id[$index]);
			}
		}
		$this->session->set_flashdata('success_message','Attributes has been updated successfully.');
		redirect('admin/attributes');
	}
}
?>