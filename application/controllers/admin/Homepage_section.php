<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage_section extends CI_Controller
{
    var $data;	
    function  __construct() 
	{
        parent::__construct();		
        $this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_home_sections';	
	}

    function index()
	{	  
		$order[0] = array('field'=>'order','type'=>'ASC');
		$this->data['rows'] = $this->Common_model->find_data($this->table,'array','','','','','',$order); 

		$this->data['maincontent'] = $this->load->view('admin/maincontents/homepage-section-list', $this->data, true);	
		$this->load->view('admin/layout', $this->data);
    }

	function change_status()
	{
		if($this->input->post('action')=='_ajax_change_status')
		{
			$id 	= 	$this->input->post('id');
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);
			if($returnVal)
			{
				echo 'success';
			}
		}	
	}
}