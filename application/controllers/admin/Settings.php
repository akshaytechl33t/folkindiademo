<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller
{
    var $data;
    function  __construct() 
	{
        parent::__construct();
		
		if(	$this->session->userdata('userid')){
			$this->load->library('admin_init_elements');
			$this->admin_init_elements->init_elements();	
		}
		else{
			redirect('admin/login');
		}
		
		$this->load->model('Settings_model');
    }
	
    function index()
	{
        if ($this->input->post('submit'))
        {          
            if ($this->validate_form_data() == TRUE)
            {
				
				$exist_image=$this->input->post('old_frame_size_image');		
				if(array_filter($_FILES['frame_size_image']['name']))
				{		
					$path='uploads/images/frame_size/';								
					$dimensions[0]=array('width'=>371,'height'=>339);	
					$img=upload_image('frame_size_image',$path,$dimensions,$exist_image); 											
					$upload =$img[1][0];	
				}
				else
				{
					$upload = $exist_image[0];
				}	
				
				if($img[0]!='error-upload'){
					
					$postdata = array(
						'site_name'						=> $this->input->post('site_name'),
						'site_sender_name'				=> $this->input->post('site_sender_name'),
						'site_sender_email' 			=> $this->input->post('site_sender_email'),
						'site_receiver_email'			=> $this->input->post('site_receiver_email'),							
						'site_address'					=> $this->input->post('site_address'),
						'site_contact_detail'			=> $this->input->post('site_contact_detail'),
						'site_contact_detail1'			=> $this->input->post('site_contact_detail1'),
						'site_phone_1' 					=> $this->input->post('site_phone_1'),
						'site_phone_2' 					=> $this->input->post('site_phone_2'),
						'site_fax_number'				=> $this->input->post('site_fax_number'),
						'site_first_set_of_results' 	=> $this->input->post('site_first_set_of_results'),						
						'site_linkedin' 				=> $this->input->post('site_linkedin'),
						'site_facebook_link' 			=> $this->input->post('site_facebook_link'),
						'site_twitter_link' 			=> $this->input->post('site_twitter_link'),
						'site_paypal_mode'			=> $this->input->post('site_paypal_mode'),
						'site_paypal_url'			=> $this->input->post('site_paypal_url'),
						'site_paypal_merchant_email_id'			=> $this->input->post('site_paypal_merchant_email_id'),
						'site_title' 					=> $this->input->post('site_title'),
						'site_meta_description' 		=> $this->input->post('site_meta_description'),
						'site_meta_keywords'			=> $this->input->post('site_meta_keywords'),
						'site_receiver_email2'			=> $this->input->post('site_receiver_email2'),
						'modified'						=> date('Y-m-d H:i:s')						
					);  
					if($this->Settings_model->save_data($postdata,$this->input->post('id')) > 0)
					{
						$this->session->set_flashdata('success_message','Site settings have been updated successfully.');
						redirect(current_url());
					}
					else
					{
						$this->session->set_flashdata('error_message','Failed to proceed. Some error occured.');
						redirect(current_url());
					} 
				}
				else
				{
					$this->session->set_flashdata('error_message',$img[1]);
				}
            }
        }
	  
		$data['row'] = $this->Settings_model->find_data('row',array('id'=>1));	 
		if($data['row']->site_paypal_mode=='L')
		{
			$data['live'] = true;
		}
		else
		{
			$data['sandbox']=true;
		}
       $this->data['maincontent'] = $this->load->view('admin/maincontents/site-settings', $data, true);		      
       $this->load->view('admin/layout', $this->data);
    }


    function validate_form_data()
    {
     	$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
			
		$this->form_validation->set_rules('site_name', 'site name', 'required');
		$this->form_validation->set_rules('site_sender_name', 'site sender name', 'required');
		$this->form_validation->set_rules('site_sender_email', 'site sender email', 'required|valid_email');
		$this->form_validation->set_rules('site_receiver_email', 'site receiver email', 'required|valid_email');		
		$this->form_validation->set_rules('site_address', 'site address', 'required');
		$this->form_validation->set_rules('site_contact_detail', 'Site Contact Details', 'required');
		$this->form_validation->set_rules('site_phone_1', 'site Phone 1', 'required');
		//$this->form_validation->set_rules('site_fax_number', 'Site Fax Number', 'required');		
		$this->form_validation->set_rules('site_first_set_of_results', 'site first set of results', 'required|is_natural_no_zero');		
		$this->form_validation->set_rules('site_linkedin', 'site linkedin link', 'required|callback_valid_url_format');
		$this->form_validation->set_rules('site_facebook_link', 'site facebook link', 'required|callback_valid_url_format');
		$this->form_validation->set_rules('site_twitter_link', 'site twitter link', 'required|callback_valid_url_format');
		$this->form_validation->set_rules('site_paypal_mode', 'paypal mode', 'required');
		$this->form_validation->set_rules('site_paypal_url', 'paypal url', 'required');
		$this->form_validation->set_rules('site_paypal_merchant_email_id', 'paypal merchant id', 'required');
		$this->form_validation->set_rules('site_title', 'site title', 'required');
		$this->form_validation->set_rules('site_meta_description', 'site meta description', 'required');
		$this->form_validation->set_rules('site_meta_keywords', 'site meta keywords', 'required');	
		$this->form_validation->set_rules('site_receiver_email2', 'site receiver email two', 'valid_email');
		
        if($this->form_validation->run() == true)
        {
         	return true;
        }
        else
        {
            return false;
        }
    }	
	function valid_url_format($str)
	{
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		if(!preg_match($pattern, $str))
		{
			$this->form_validation->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
			return FALSE;
		}
		else
		{
			return true;
		}
	}       
	
}