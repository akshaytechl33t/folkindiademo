<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller
{
    var $data;
    function  __construct() 
	{
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_banners';
    }

	function index()
	{ 	
		$this->data['page_title'] = 'Banner listing';
			$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		$this->data['rows'] = $this->Common_model->find_data($this->table,'array');		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/banner-listing',$this->data,true);        
        $this->load->view('admin/layout', $this->data);
	}
    function add()
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = "add";
		
		$this->data['checked_y']=true;
		$this->data['checked_n']=false;		
        if($this->input->post('action') == 'add')
		{			            	
			if ($this->validate_banner_form() == TRUE)
			{ 
				if($_FILES['images']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['images']['name']))
					{
						$path='uploads/images/home/banner/';					
						$upload=upload_image('images',$path); 	
						$img = $upload;						
						if($img[0] !='error-upload')
						{
							$postData=array(
								'section_id'=>1,
								'images'=>$img[1][0],
								'link'=>$this->input->post('link'),
								'published'	=> $this->input->post('published')
							);			
							$result=$this->Common_model->save_data($this->table,$postData);		
							if($result)
							{
								$this->session->set_flashdata('success_message','Banner has been added successfully.');
							}
							else
							{
								$this->session->set_flashdata('error_message','Addition process was failed.');
							}	
							redirect('admin/banner');	
						}
						else
						{							
							$this->data['error_message']=$img[1];	
							$this->data['field']['link']=set_value('link');	
							($this->input->post('published')==1)? $data['field']['checked_y']=true: $data['field']['checked_n']=true;
						}
					}
					
				}
				else
				{	
					$get_error = 'Image cannot be blank';	
					$this->data['error_message'] = $get_error;	
					$this->data['field']['link']=set_value('link');	
					$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
					($this->input->post('published')==1)? $data['field']['checked_y']=true: $data['field']['checked_n']=true;		
				}	
			 }
			else
			{	
				$this->data['field']['link']=set_value('link');	
				$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
				($this->input->post('published')==1)? $this->data['field']['checked_y']=true: $this->data['field']['checked_n']=true;
			}             
        }
		
		$this->data['page_title']  = "Add Banner";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/banner-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function edit($id)
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "Edit";

		$this->data['position_arr'] = array(''=>'Select position','1'=>'Top Left','2'=>'Top Middle','3'=>'Top Right','4'=>'Middle Left','5'=>'Center','6'=>'Middle Right','7'=>'Bottom Left','8'=>'Bottom Middle','9'=>'Bottom Right');
		
		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);

		$this->data['id']=$row->id; 
		$this->data['field']['images']=$row->images;
		$this->data['field']['link']=$row->link;
		
		if($row->published ==1){
			$this->data['checked_y']=true;
			$this->data['checked_n']=false;
		}
		else
		{
			$this->data['checked_n']=true;
			$this->data['checked_y']=false;
		}
		 		
        if($this->input->post('action') == 'Edit')
		{
           if ($this->validate_banner_form() == TRUE)
			{						
				$exist_image=$this->input->post('old_image');
				if(array_filter($_FILES['images']['name']))
				{		
					$path='uploads/images/home/banner/';					
					$upload=upload_image('images',$path); 	
					$img = $upload;																		
					$upload =$img[1][0];;
					@unlink($path.$exist_image[0]);
				}
				else
				{
					$upload = $exist_image[0];
				}
				if($upload[0]!='error-upload')
				{
					$postdata=array(
							'link'=>$this->input->post('link'),
							'images'=>$upload,
							'published'	=> $this->input->post('published'),
							'modified'=>date('Y-m-d H:i:s')
						);
					$result=$this->Common_model->save_data($this->table,$postdata,$id);								
					if($result){						
						$this->session->set_flashdata('success_message','Banner image has been successfully updated.');
						redirect('admin/banner');						
					}
					else
					{				
						$this->session->set_flashdata('error_message','Update process was failed.');  
					}
				}
				else
				{
					$this->data['error_message']=$upload[1];
					$this->data['field']['link']=set_value('link');	
					$this->data['field']['checked_y'] = $this->data['field']['checked_n'] = false;
					($this->input->post('published')==1)? $this->data['field']['checked_y']=true: $this->data['field']['checked_n']=true;
				}	
           }
        }
        				
		$this->data['page_title']  = "Edit Banner image";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/banner-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function validate_banner_form()
	{
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		if($this->input->post('link') !=''){
			$this->form_validation->set_rules('link', 'link', 'callback_valid_url_format');
			if ($this->form_validation->run() == TRUE)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
    }
	
	
	function valid_url_format($str)
	{
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		if(!preg_match($pattern, $str))
		{
			$this->form_validation->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
			return FALSE;
		}
		else
		{
			return true;
		}
	}      
	function delete()
	{		
		$row = $this->Common_model->find_data($this->table,'row','',array('id'=>$this->uri->segment(4)));
		
		if($row->images !=''){
			$path1 = 'uploads/images/home/banner/'.$row->images;			
			@unlink($path1);
		}
		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Banner has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }

	function change_status(){
		
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));						
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
}
?>