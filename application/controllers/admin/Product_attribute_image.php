<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_attribute_image extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_products_attribute_images';	
    }

	function index($id){      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		
		$this->data['id'] = $id;
		$this->data['page_title']  = "Product attribute image";
		
		$table['name']="folk_products";
		$table['alias_name']='p';
		
		$select = 'p.id,p.published,p.name,pai.*,av.value';
		
		$join[0]['table']="folk_products_attribute_images";
		$join[0]['table_alias']="pai";		
		$join[0]['field']="product_id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";
		
		$join[1]['table']="folk_attribute_values";
		$join[1]['table_alias']="av";		
		$join[1]['field']="id";
		$join[1]['table_master']="pai";
		$join[1]['field_table_master']="color_id";			
		$join[1]['type']="LEFT";
			
		$this->data['rows'] = $this->Common_model->find_data($table,'array','',array('pai.product_id'=>$id),$select,$join);
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/product-attribute-image',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
	function add($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Products attribute image";
		
		$this->data['checked_y']=true;
		$this->data['checked_n']=false;
		
		
		$table['name']="folk_products";
		$table['alias_name']='p';
		
		$select = 'p.id,p.published,p.name,av.value,av.attribute_id,aa.attribute_value_id';	
		
		$join[0]['table']="folk_assign_attributes";
		$join[0]['table_alias']="aa";		
		$join[0]['field']="product_id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";
		
		$join[1]['table']="folk_attribute_values";
		$join[1]['table_alias']="av";		
		$join[1]['field']="id";
		$join[1]['table_master']="aa";
		$join[1]['field_table_master']="attribute_value_id";			
		$join[1]['type']="RIGHT";	
		$join[1]['and']=" AND av.attribute_id =7";	
			
		$this->data['color_dropdown_arr'] = $this->Common_model->find_data($table,'array','',array('p.id'=>$id),$select,$join);
		$this->data['id'] = $id;
		
		if($this->input->post('action') == 'add')
		{
			$get_error=array();	
			if ($this->validate_product_attr_image_form() == TRUE)
			{	
		
				if($_FILES['image1']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image1']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image1',$path); 	
						$img1 = $upload;						
						if($img1[0] !='error-upload')
						{
							$image1 = $img1[1][0];
						}
						else
						{
							$image1 = '';							
						}
					}
				}					
				if($_FILES['image2']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image2']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image2',$path); 	
						$img2 = $upload;						
						if($img2[0] !='error-upload')
						{
							$image2 = $img2[1][0];
						}
						else
						{
							$image2 = '';
						}
					}
				}		
				if($_FILES['image3']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image3']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image3',$path); 	
						$img3 = $upload;						
						if($img3[0] !='error-upload')
						{
							$image3 = $img3[1][0];
						}
						else
						{
							$image3 = '';
						}
					}
				}	
				if($_FILES['image4']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image4']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image4',$path); 	
						$img4 = $upload;						
						if($img4[0] !='error-upload')
						{
							$image4 = $img4[1][0];
						}
						else
						{
							$image4 = '';
						}
					}
				}
				if($_FILES['image5']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image5']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image5',$path); 	
						$img5 = $upload;						
						if($img5[0] !='error-upload')
						{
							$image5 = $img5[1][0];
						}
						else
						{
							$image5 = '';
						}
					}
				}	
				if($_FILES['image6']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image6']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image6',$path); 	
						$img6 = $upload;						
						if($img6[0] !='error-upload')
						{
							$image6 = $img6[1][0];
						}
						else
						{
							$image6 = '';
						}
					}
				}
				if($_FILES['image7']['tmp_name'][0] !='')				
				{
					if(array_filter($_FILES['image7']['name']))
					{
						$path='uploads/images/product/product_attribute_image/';					
						$upload=upload_image('image7',$path); 	
						$img7 = $upload;						
						if($img7[0] !='error-upload')
						{
							$image7 = $img7[1][0];
						}
						else
						{
							$image7 = '';
						}
					}
				}
				if($image1 !='' && $image2 !='' && $image3 !='' && $image4 !='' ){
				
					$postdata = array(
						'color_id'        => $this->input->post('color_id'),
						'product_id'	=>$id,
						'sku'			=>$this->input->post('sku'),
						'image1'        => $image1,
						'image2'        => $image2,
						'image3'        => $image3,
						'image4'        => $image4,					
						'image5'        => $image5,					
						'image6'        => $image6,					
						'image7'        => $image7,					
						'published'   => $this->input->post('published')
					);
					$result = $this->Common_model->save_data($this->table,$postdata);
					if($result){		
						$this->session->set_flashdata('success_message','Products attribute image has been added successfully.');
						redirect('admin/product_attribute_image/'.$id);
					}
					else
					{
						$this->session->set_flashdata('error_message','Some internal error.Please try again.');
					}		
				}
				else{
					$get_error = 'Image cannot be blank';	
					$this->data['error_message'] = $get_error;	
				}					
			}
			else
			{						
				$this->data['field']['color_id']=set_value('color_id');					
				$this->data['field']['sku']=set_value('sku');
				$this->data['field']['published']=set_value('published');				
			}
		}
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/product-attribute-image-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function edit($product_id='',$id='')
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit product attribute image";
		
		$this->data['product_id'] = $product_id;
		
		$table_cat['name']="folk_products";
		$table_cat['alias_name']='p';
		
		$select = 'p.id,p.published,p.name,av.value,av.attribute_id,aa.attribute_value_id';	
		
		$join[0]['table']="folk_assign_attributes";
		$join[0]['table_alias']="aa";		
		$join[0]['field']="product_id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";
		
		$join[1]['table']="folk_attribute_values";
		$join[1]['table_alias']="av";		
		$join[1]['field']="id";
		$join[1]['table_master']="aa";
		$join[1]['field_table_master']="attribute_value_id";			
		$join[1]['type']="RIGHT";	
		$join[1]['and']=" AND av.attribute_id =7";	
			
		$this->data['color_dropdown_arr'] = $this->Common_model->find_data($table_cat,'array','',array('p.id'=>$product_id),$select,$join);
		$this->data['id'] = $id;
		
		
		$table['name']="folk_products";
		$table['alias_name']='p';
		
		$select = 'p.id,p.published,p.name,pai.*,av.id as attibute_value_id';
		
		$join[0]['table']="folk_products_attribute_images";
		$join[0]['table_alias']="pai";		
		$join[0]['field']="product_id";
		$join[0]['table_master']="p";
		$join[0]['field_table_master']="id";			
		$join[0]['type']="LEFT";
		
		$join[1]['table']="folk_attribute_values";
		$join[1]['table_alias']="av";		
		$join[1]['field']="id";
		$join[1]['table_master']="pai";
		$join[1]['field_table_master']="color_id";			
		$join[1]['type']="LEFT";
			
		$row = $this->Common_model->find_data($table,'row','',array('pai.id'=>$id),$select,$join);
		
		if($row){
			$this->data['id']=$row->id;	
			$this->data['field']['name']=$row->name;
			$this->data['field']['product_id']=$row->product_id;			
			$this->data['field']['color_id'] = $row->color_id;			
			$this->data['field']['sku'] = $row->sku;
			$this->data['field']['image1'] = $row->image1;
			$this->data['field']['image2'] = $row->image2;
			$this->data['field']['image3'] = $row->image3;
			$this->data['field']['image4'] = $row->image4;
			$this->data['field']['image5'] = $row->image5;
			$this->data['field']['image6'] = $row->image6;
			$this->data['field']['image7'] = $row->image7;
			$this->data['field']['attibute_value_id'] = $row->attibute_value_id;			
			if($row->published ==1){
				$this->data['checked_y']=true;
				$this->data['checked_n']=false;
			}
			else
			{
				$this->data['checked_n']=true;
				$this->data['checked_y']=false;
			}
		}
		
        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_product_attr_image_form() == TRUE)
			{	
		
				$exist_image1=$this->input->post('old_image1');				
				if(array_filter($_FILES['image1']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image1',$path); 	
					$img = $upload;																		
					$upload1 =$img[1][0];;
					@unlink($path.$exist_image1[0]);
				}
				else
				{
					$upload1 = $exist_image1[0];
				}
				
				$exist_image2=$this->input->post('old_image2');				
				if(array_filter($_FILES['image2']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image2',$path); 	
					$img = $upload;																		
					$upload2 =$img[1][0];;
					@unlink($path.$exist_image2[0]);
				}
				else
				{
					$upload2 = $exist_image2[0];
				}
				
				$exist_image3=$this->input->post('old_image3');				
				if(array_filter($_FILES['image3']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image3',$path); 	
					$img = $upload;																		
					$upload3 =$img[1][0];;
					@unlink($path.$exist_image3[0]);
				}
				else
				{
					$upload3 = $exist_image3[0];
				}
				
				$exist_image4=$this->input->post('old_image4');
				
				if(array_filter($_FILES['image4']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image4',$path); 	
					$img = $upload;																		
					$upload4 =$img[1][0];;
					@unlink($path.$exist_image4[0]);
				}
				else
				{
					$upload4 = $exist_image4[0];
				}
				
				$exist_image5=$this->input->post('old_image5');
				
				if(array_filter($_FILES['image5']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image5',$path); 	
					$img = $upload;																		
					$upload5 =$img[1][0];;
					@unlink($path.$exist_image5[0]);
				}
				else
				{
					$upload5 = $exist_image5[0];
				}
				$exist_image6=$this->input->post('old_image6');
				
				if(array_filter($_FILES['image6']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image6',$path); 	
					$img = $upload;																		
					$upload6 =$img[1][0];;
					@unlink($path.$exist_image6[0]);
				}
				else
				{
					$upload6 = $exist_image6[0];
				}
				$exist_image7=$this->input->post('old_image7');
				
				if(array_filter($_FILES['image7']['name']))
				{
					$path='uploads/images/product/product_attribute_image/';					
					$upload=upload_image('image7',$path); 	
					$img = $upload;																		
					$upload7 =$img[1][0];;
					@unlink($path.$exist_image7[0]);
				}
				else
				{
					$upload7 = $exist_image7[0];
				}
				if($upload1 !='' && $upload2 !='' && $upload3 !='' && $upload4 !=''){
				
					$postdata = array(
						'color_id'        => $this->input->post('color_id'),
						'product_id'	=>$product_id,
						'sku'			=>$this->input->post('sku'),
						'image1'        => $upload1,
						'image2'        => $upload2,
						'image3'        => $upload3,
						'image4'        => $upload4,
						'image5'        => $upload5,
						'image6'        => $upload6,
						'image7'        => $upload7,
						'published'   => $this->input->post('published'),
						'modified'    => date('Y-m-d H:i:s')	
					);
					$result = $this->Common_model->save_data($this->table,$postdata,$id);
					if($result){		
						$this->session->set_flashdata('success_message','Products attribute image has been updated successfully.');
						redirect('admin/product_attribute_image/'.$product_id);
					}
					else
					{
						$this->session->set_flashdata('error_message','Some internal error.Please try again.');
					}		
				}
				else{
					$get_error = 'Image cannot be blank';	
					$this->data['error_message'] = $get_error;	
				}
			}
			else
			{
				$this->data['field']['color_id']=set_value('color_id');					
				$this->data['field']['sku']=set_value('sku');
				$this->data['field']['published']=set_value('published');				
			}
		}			
		$this->data['maincontent'] = $this->load->view('admin/maincontents/product-attribute-image-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
	function validate_product_attr_image_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');			
		if($this->uri->segment(3) != 'edit'){			
			$this->form_validation->set_rules('color_id', 'name', 'trim|required');
		}		
		else
		{					
			$this->form_validation->set_rules('color_id', 'color_id', 'trim|required');			
		}		
		$this->form_validation->set_rules('sku', 'sku', 'trim|required');	
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
	function delete()
	{			
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
	
	
	function change_status()
	{			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}
	function is_unique_name($str)
    {    
		$conditions = array('name'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('name'=>$str,'id !='=>$this->input->post('id'));
		}
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	

		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_unique_name', 'The name must contain a unique value.');
			return false;
		}       
    }
	
	
}
?>