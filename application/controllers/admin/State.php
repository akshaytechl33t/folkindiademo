<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class State extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();				
        $this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_states';
    }
##################################### LISTING ###################################################
	function index(){       
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}
		$table['name']='folk_states';
		$table['alias_name']='states';
		$select = 'states.*,country.name as  country_name';
		$join[0] = array('table'=>'folk_countries','table_alias'=>'country','field'=>'id','table_master'=>'states','field_table_master'=>'country_id','type'=>'inner');
		$join[0] = array('table'=>'folk_countries','table_alias'=>'country','field'=>'id','table_master'=>'states','field_table_master'=>'country_id','type'=>'inner');
		$this->data['rows']=$this->Common_model->find_data($table,'array','','',$select,$join);
			
		$this->data['page_title']  = "Manage State";
        $this->data['maincontent'] = $this->load->view('admin/maincontents/state_list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }
##################################### ADD ###################################################

	function add()
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "add";
		$this->data['id'] = "";
		$this->data['state_name'] = "";
		$table['name'] = "folk_countries";
		$order_by[0]['field'] = 'name';
		$order_by[0]['type'] = 'ASC';
		$this->data['country_list'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);
		$this->data['country_id']='';
		$this->data['checked_y']=true;
		$this->data['checked_n']=false;
        if($this->input->post('action') == 'add')
		{
			if ($this->validate_state_form() == TRUE)
			{
				$table['name']='folk_states';
				$postData = array(
							'state_name' => $this->input->post('state_name'),
							'country_id'=>$this->input->post('country_id'),
							'created' => date('Y-m-d H:i:s'),
							'published' => $this->input->post('published')
								 );
                $returnVal = $this->Common_model->save_data($table,$postData);
				if($returnVal)
				{
                	$this->session->set_flashdata('success_message','State has been added successfully.');
              	}
				else
				{
                   	$this->session->set_flashdata('error_message','Addition process was failed.');                                  
                }
				redirect('admin/state');
            }
			else
			{
					$this->data['country_id'] = set_value('country_id',$this->input->post('country_id'));
					$this->data['state_name']=set_value('state_name',$this->input->post('state_name'));				
					($this->input->post('published')==1)? $this->data['checked_y']=true: $this->data['checked_n']=true;
			}
        }
		
		//populate view, specific to this function
		$this->data['page_title']  = "Add State";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/state_add_edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
##################################### EDIT ###################################################
	
	function edit($id)
	{
        $this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "Edit";
		$table['name']='folk_states';
       if($this->input->post('action') == 'Edit')
		{
			//$id = $this->input->post('id');
            if ($this->validate_state_form() == TRUE)
			{		
				
				$postData = array(
									'state_name'	=> $this->input->post('state_name'),
									'country_id'	=> $this->input->post('country_id'),		
									'modified' 		=> date('Y-m-d H:i:s'),
									'published' 	=> $this->input->post('published')
								 );
                $returnVal = $this->Common_model->save_data($table,$postData,$id);
				if($returnVal)
				{
                	$this->session->set_flashdata('success_message','State has been updated successfully.');
              	}
				else
				{
                   	$this->session->set_flashdata('error_message','Update process was failed.');                                   
                }
				redirect('admin/state');
            }
        }
        
		$condition = array('id'=>$id);
		$select = '*';
		$row = $this->Common_model->find_data($table,'row','',$condition,$select);
		$table['name'] = 'folk_countries';
		$order_by[0]['field'] = 'name';
		$order_by[0]['type'] = 'ASC';
		$this->data['country_list'] = $this->Common_model->find_data($table,'list',array('empty_name'=>' Country','key'=>'id','value'=>'name'),array('published'=>1),'','','',$order_by);
		$this->data['id']=$row->id; 
		$this->data['state_name']=$row->state_name;	
		$this->data['country_id']=$row->country_id;	
		$this->data['published']=$row->published; 
	   	//populate view, specific to this function
		if($row->published ==1){
			$this->data['checked_y']=true;
			$this->data['checked_n']=false;
		}
		else
		{
			$this->data['checked_n']=true;
			$this->data['checked_y']=false;
		}
		 
		
		$this->data['page_title']  = "Edit State";
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/state_add_edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }
##################################### VALIDATION ###################################################
	function validate_state_form()
	{
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');
		$this->form_validation->set_rules('state_name', 'state name', 'strip_tags|trim|required|callback_is_chk_unique_state_name');	
		$this->form_validation->set_rules('country_id', 'Country Name', 'trim|required');	
		$this->form_validation->set_rules('published', 'state status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
##################################### DELETE ###################################################
	function delete()
	{
		$table['name']='folk_states';		
		$returnVal = $this->Common_model->delete_data($table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','State has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }
##################################### STATUS ###################################################

	function change_status(){
		
		// Change row status using ajax call 
		if($this->input->post('action')=='_ajax_change_status'){
			$id 	= 	$this->input->post('id');
			
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));
			
			$table['name']='folk_states';			
		
			$returnVal = $this->Common_model->save_data($table,$postData,$id);
			
			if($returnVal){
				echo 'success';
			}
		}
		
	}
##################################### Unique name ###################################################
	function is_chk_unique_state_name($str)
    {    
       $conditions = array('state_name'=>$str);
       if($this->input->post('id'))
	   {          
          $conditions = array('state_name'=>$str,'id !='=>$this->input->post('id'));
       }
	   
		$table['name']='folk_states';
		$select = '*';
		$query=$this->Common_model->find_data($table,'count','',$conditions,$select);	
      
       if($query == 0)
	   {
           return true;
       }
	   else
	   {
          $this->form_validation->set_message('is_chk_unique_state_name', 'The state field must contain a unique value.');
           return false;
       }
       
    }
##################################### Unique code ###################################################
	
}
?>