<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends CI_Controller
{   
    var $data;

    function  __construct(){
        parent::__construct();			
		$this->load->library('admin_init_elements');
		$this->admin_init_elements->init_elements();
		$this->table['name'] = 'folk_countries ';	
    }

	function index(){
      
	   	$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		if($this->session->flashdata('success_message')){
			$this->data['success_message'] = $this->session->flashdata('success_message');
		}
	   	if($this->session->flashdata('error_message')){
			$this->data['error_message'] = $this->session->flashdata('error_message');
		}	
		
		$this->data['rows']=$this->Common_model->find_data($this->table,'array');		
		$this->data['page_title']  = "Country list";
		
        $this->data['maincontent'] = $this->load->view('admin/maincontents/country-list',$this->data,TRUE);
		$this->load->view('admin/layout', $this->data);
    }

	function add()
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';	
		
		$this->data['action'] = 'add';
		$this->data['page_title']  = "Add Country";
		
		$this->data['field']['checked_y']=true;
		$this->data['field']['checked_n']=false;
		
		if($this->input->post('action') == 'add')
		{
			if ($this->validate_country_form() == TRUE)
			{						
				$postdata = array(
					'name'   => $this->input->post('name'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('name'), $this->table, 'seo_name', $key = NULL, $value = NULL)),
					'code' => $this->input->post('code'),
					'published'   => $this->input->post('published')
				);
				$result = $this->Common_model->save_data($this->table,$postdata);
				if($result){					
					$this->session->set_flashdata('success_message','Country has been added successfully.');
					redirect('admin/country');
				}
				else
				{
					$this->session->set_flashdata('error_message','Some internal error.Please try again.');
					$this->data['field']['name']=set_value('name');	
					$this->data['field']['code']=set_value('code');	
					$this->data['field']['short_description']=set_value('published');
				}								
			}
			else
			{
				$this->data['field']['name']=set_value('name');	
				$this->data['field']['code']=set_value('code');	
				$this->data['field']['short_description']=set_value('published');
			}
		}
		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/country-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function edit($id)
	{
		$this->data['success_message'] = '';
		$this->data['error_message'] = '';
		
		$this->data['action'] = "edit";				
		$this->data['page_title']  = "Edit Country";
		
		$condition = array('id'=>$id);		
		$row = $this->Common_model->find_data($this->table,'row','',$condition);		
		
		$this->data['id']=$row->id; 
		$this->data['field']['name']=$row->name;
		$this->data['field']['code']=$row->code;
		
		if($row->published ==1){
			$this->data['field']['checked_y']=true;
			$this->data['field']['checked_n']=false;
		}
		else
		{
			$this->data['field']['checked_n']=true;
			$this->data['field']['checked_y']=false;
		}

        if($this->input->post('action') == 'edit')
		{
            if ($this->validate_country_form() == TRUE)
			{						
				$postdata = array(				
					'name'   => $this->input->post('name'),
					'seo_name'    => strtolower(create_unique_slug($this->input->post('name'), $this->table, 'seo_name','id',$id)),
					'code' => $this->input->post('code'),
					'modified'    => date('Y-m-d H:i:s'),
					'published'   => $this->input->post('published')					
				);
				$returnVal = $this->Common_model->save_data($this->table,$postdata,$id);
				if($returnVal)
				{					
					$this->session->set_flashdata('success_message','Country has been updated successfully.');
					redirect('admin/country');
				}				
				else
				{
					$this->session->set_flashdata('error_message','Update process is failed.');  
					$this->data['field']['name']=set_value('name');	
					$this->data['field']['code']=set_value('code');	
					$this->data['field']['short_description']=set_value('published');
				}					             				
            }
        }		
       	$this->data['maincontent'] = $this->load->view('admin/maincontents/country-add-edit',$this->data,TRUE);
       	$this->load->view('admin/layout', $this->data);
    }

	function validate_country_form()
	{		
		$this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o">','</i></label></div>');	
		
		if($this->uri->segment(3) == 'edit')
		{
			$this->form_validation->set_rules('name', 'name', 'strip_tags|trim|required|callback_is_chk_unique_name');
			$this->form_validation->set_rules('code', 'code', 'strip_tags|trim|required|callback_is_chk_unique_code');
		}
		else
		{
			//$this->form_validation->set_rules('name', 'name', 'required|callback_is_chk_unique_name');
			//$this->form_validation->set_rules('code', 'code', 'required');
			/*added by koushik on 06/10/2015 */
			$this->form_validation->set_rules('name', 'name', 'strip_tags|trim|is_unique['.$this->table['name'].'.name]|required');
			$this->form_validation->set_rules('code', 'code', 'strip_tags|trim|required|is_unique['.$this->table['name'].'.code]');
		}	
		if ($this->form_validation->run() == TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }

	function delete()
	{		
		$returnVal = $this->Common_model->delete_data($this->table,$this->uri->segment(4));
		if($returnVal)
		{
			$this->session->set_flashdata('success_message','Data has been deleted successfully.');
		}
		else
		{
			$this->session->set_flashdata('error_message','Delete process was failed.');                                   
		}
		redirect($_SERVER['HTTP_REFERER']);
    }

	function change_status(){			
		if($this->input->post('action')=='_ajax_change_status'){
			$id = 	$this->input->post('id');				
			$postData = array('modified'=> date('Y-m-d H:i:s'),'published'=> $this->input->post('published'));					
			$returnVal = $this->Common_model->save_data($this->table,$postData,$id);			
			if($returnVal){
				echo 'success';
			}
		}		
	}

	function is_chk_unique_name($str)
	{    
		$conditions = array('name'=>$str);
		if($this->input->post('id'))
		{          
			$conditions = array('name'=>$str,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_name', 'The name field must contain a unique value..');
			return false;
		}
	}
	/*is unique code for editing country added by koushik sen 06/10/2015*/
	function is_chk_unique_code($code)
	{    
		$conditions = array('code'=>$code);
		if($this->input->post('id'))
		{          
			$conditions = array('code'=>$code,'id !='=>$this->input->post('id'));
		}	   
		$query=$this->Common_model->find_data($this->table,'count','',$conditions);	
		if($query == 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('is_chk_unique_code', 'The code field must contain a unique value..');
			return false;
		}
	}
	/*-----end ----*/
}
?>