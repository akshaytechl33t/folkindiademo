$(document).ready(function() { 
	var site_url = $(".logo").attr('href'); 
	
	$('.change_status').bind('click',function(e){
		e.preventDefault();
		var chk=confirm('Do you want to change the status?');
		if(chk!=true){return false;}else{
			var status_val 	= $(this).attr('data-rel');
			var row_id 		= $(this).attr('rel');
			var action		= $(this).attr('href');
			$.ajax({
				url: action,
				type: 'post',
				data: 'action=_ajax_change_status&published='+status_val+'&id='+row_id,
				success: function (data){
					if(data == "success"){ 
						if(status_val == 1){ 
							$("#change_status_"+row_id).html("<i class='fa fa-check-circle fa-lg' title='Click to unpublish'></i>");
							$("#change_status_"+row_id).attr("data-rel",0);
						}else{
							$("#change_status_"+row_id).html("<i class='fa fa-times-circle fa-lg' title='Click to publish'></i>");
							$("#change_status_"+row_id).attr("data-rel",1);
						}
						$('#notifyMessage').html('<section class="content gapp"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button><b>Record status has been changed successfully.</b></div></section>');
					}
				}
			});
		}			
	});
    
	$('.delete_cat_bnr_img').click(function(){
		var chk=confirm('Are you sure want to delete this image ?');
		if(chk==true)
		{
			var img_data = $(this).attr('data');
			$.ajax({
				url : site_url+'/Ajax/category_banner_delete',
				type : 'post',
				data: { image : img_data},
				dataType : 'json',
				success:function(data){
					if(data.flag == 'success'){
						location.reload();
					}
				}
			})
		}
		else
		{
			return false;
		}
	})

	$('.barnd_bnr').click(function(){
		var chk=confirm('Are you sure want to delete this image ?');
		if(chk==true)
		{
			var img_data = $(this).attr('data');		
			$.ajax({
				url : site_url+'/Ajax/brand_banner_delete',
				type : 'post',
				data: { image : img_data},
				dataType : 'json',
				success:function(data){
					if(data.flag == 'success'){
						location.reload();
					}
				}
			})
		}
		else
		{
			return false;
		}
		
	})
	
	$('.country_id').on('change',function(){
	 $.ajax({
			type: "POST",
			url: site_url+"/ajax/state_drop_down",
			data: { country_id: $(this).val()}
		})
		.done(function( msg ) { 
			   $('#state_id').html('').html(msg);            		
		}).fail(function(){        
			alert('Some problem occurred. Please try later.');
		});
	});
});

function confirmToDelete(confirm_parameter1,confirm_parameter2) {
    if(confirm_parameter1 == '' || confirm_parameter2 == ''){
        return confirm("Are you sure want to delete this record?");
    }else{       
        var warning_msg = "";
        var confirm_action = confirm("Are you sure want to delete this record?");       
        if(confirm_action){            
            if(confirm_parameter1!=='undefined' && confirm_parameter2!=='undefined'){
                warning_msg = "This will delete all "+confirm_parameter2+" under this "+confirm_parameter1+" ?";
            }
            return confirm(warning_msg);
        }else{
            return false;
        }
    }
}
function confirmDelete()
{
	var chk=confirm('Are you sure want to delete this record 11?');
	if(chk==true){
		var chk1=confirm('Are you sure want to delete this record 11?');
		if(chk1==true){		
			return true;
		}else{
			return false;
		}			
	}else{
		return false;
	}
}


 