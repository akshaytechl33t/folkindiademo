$(document).ready(function(){
	var baseUrl = $(".logo").find('a').attr('href');
	
	$('body').on('click','#SignupPopup',function(){		
		$('#Signuppopup').modal('show');
		$('#loginpopup').modal('hide');
		$('#forgotpop').modal('hide');
	})
	if ($.cookie('currencyselected')) {
		// alert('--'+$.cookie('currencyselected'))
		$('#currencydrop').val($.cookie('currencyselected'))
	} else {
		$.cookie('currencyselected','INR', { path: '/' })
	}
	$('#currencydrop').change(function(){
		$.cookie('currencyselected', $('#currencydrop').val(), { path: '/' });
        changePrice();
	})
	$('body').on('click','#loginPopUp',function(){
		$('#loginpopup').modal('show');
		$('#Signuppopup').modal('hide');
		$('#forgotpop').modal('hide');
	})
	$('body').on('click','#ForgotPopUp',function(){
		$('#forgotpop').modal('show');
		$('#loginpopup').modal('hide');
		$('#Signuppopup').modal('hide');
	})
	
	$("#registration").validationEngine(
		'attach', {promptPosition: "bottomLeft",
			ajaxFormValidation: true,
			scroll: true,
			showOneMessage: true,
			ajaxFormValidationMethod: 'post',
		   onAjaxFormComplete: ajaxValidationCallback,
		   onBeforeAjaxFormValidation: beforeCallLoader
		}
	 ); 	
	$("#login").validationEngine(
		'attach', {promptPosition: "bottomLeft",
			ajaxFormValidation: true,
			scroll: true,
			showOneMessage: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: ajaxLoginValidationCallback,
		    onBeforeAjaxFormValidation: beforeCallLoader
		}
	);
	$("#forgot").validationEngine(
		'attach', {promptPosition: "bottomLeft",
			ajaxFormValidation: true,
			scroll: true,
			showOneMessage: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: ajaxForgotValidationCallback,
		    onBeforeAjaxFormValidation: beforeCallLoader
		}
	);
	
	$("#rating_form").validationEngine(		
		'attach', {promptPosition: "bottomLeft",
			scroll: false,
			showOneMessage: true
		}
	);
	
	$("#personal_information").validationEngine(
		'attach', {promptPosition: "bottomLeft",
			scroll: false,
			showOneMessage: true
		}
	);
	$("#change_password").validationEngine	(
		'attach', {promptPosition: "bottomLeft",
			scroll: false,
			showOneMessage: true
		}
	);
	$("#shipping_address").validationEngine(
		'attach', {promptPosition: "bottomLeft",
			scroll: false,
			showOneMessage: true
		}
	);
	
	$('.starrr').click(function(){
		var checkStarRating = $(this).children('span');
		var count=0;
		$(checkStarRating).map(function(){
			if($(this).hasClass('fa-star') == true){
				count++;
			}				
		})
		$('#count_rating').val(count);
	})
		
	$('#country_id').on('change',function(){
	$('#state_id').val('');
	$('#location_loader').html('<i class="fa fa-clock-o fA-spin" aria-hidden="true"></i>');
	$('#location_loader').show();
	 $.ajax({
			type: "POST",
			url: baseUrl+"Ajax/state_drop_down",
			data: { country_id: $(this).val()},
			dataType : 'json',
			success:function(data){
				$('#location_loader').hide();
				if(data){
					var text = '<option value="">Select state</option>';
					$(data).map(function(i,j){
						text +='<option value="'+j.id+'">'+j.state_name+'</option>';						
					})
				}
				$('#state_id').html(text);	
			}
		})		
	});
	$('#state_id').on('change',function(){
	$('#city_id').val('');
	$('#location_loader').html('<i class="fa fa-clock-o fA-spin" aria-hidden="true"></i>');
	$('#location_loader').show();
	 $.ajax({
			type: "POST",
			url: baseUrl+"Ajax/city_drop_down",
			data: { state_id: $(this).val()},
			dataType : 'json',
			success:function(data){
			$('#location_loader').hide();
				if(data){
					var text = '<option value="">Select city</option>';
					$(data).map(function(i,j){
						text +='<option value="'+j.id+'">'+j.city_name+'</option>';						
					})
				}
				$('#city_id').html(text);	
			}
		})		
	});
	$('.delete_shipping_address').click(function(){
		var addressId = $(this).attr('id');
		var chk=confirm('Do you want to delete your address?');
			if(chk!=true){return false;}else{
			$.post(baseUrl+'Ajax/delete_shipping_address',{address_id:addressId},function(data){
				$('#'+addressId).fadeOut();
			},'json');
		}
	})
	
})
function ajaxValidationCallback(status, form, json,options){
	$('#reg_loader').css('display','none');	
	if (status === true)
	{
		var strngfy = JSON.stringify(json);
        var jsn_obj = JSON.parse(strngfy);		
		if($.trim(json) == '1') {
			$('#error_regmsg').hide();
			$('#succe_msg').html('<div class="alert alert-success">You have successfully registrated here. Please check your verification mail.</div>');
			$("input[type=text], password").val('');
		}	
		else{
			$('#error_regmsg').html(jsn_obj.err_msg);
		}		
	}
}
function ajaxForgotValidationCallback(status, form, json,options){
	$('#reg_loader').css('display','none');	
	if (status === true)
	{
		var strngfy = JSON.stringify(json);
        var jsn_obj = JSON.parse(strngfy);		
		if($.trim(json) == '1') {
			$('#forgot_check').css('display','none');	
			$('#forgot_s_msg').css('display','block');	
			
		}	
		else{
			$('#forgot_s_msg').css('display','none');	
			$('#forgot_check').css('display','block');	
				
		}		
	}
}
function ajaxLoginValidationCallback(status, form, errors, options,json){	
	$('#reg_loader').css('display','none');	
	
	var baseUrl = $(".logo").find('a').attr('href');
	if (errors.success_message == 'product_details')
	{
		location.reload();
		$('#RatingForm').trigger('click');
	}
	else if(errors.success_message == 'cart' ){		
		window.location.href=baseUrl+"viewcart";
	}
	else if(errors.error_message == 'error')
	{
		$('#login_check').css('display','block');	
		return false;		
	}
	else {
		window.location.href=baseUrl+"my-account";
	}
}
function changePrice() {
	$cookiecur = $.cookie('currencyselected');
	if ($cookiecur == 'INR') {
		$('.money').each(function(){
			$(this).text('Rs.'+$(this).attr('data-inr'));
		})
	} else {
		$currencyObj = $.parseJSON($('#currenciesdata').val());
		$exchangeRate = $currencyObj.rates[$cookiecur];
		$('.money').each(function(){
			$inrRate = parseFloat($.trim($(this).attr('data-inr').replace(',','')))
			$anotherRate = parseFloat($inrRate*parseFloat($exchangeRate)).toFixed(2);
            if ($cookiecur== 'EUR') {
                $(this).text('€ '+$anotherRate);
            } else if ($cookiecur== 'USD') {
                $(this).text('$ '+$anotherRate);
            } else if ($cookiecur== 'GBP') {
                $(this).text('£ '+$anotherRate);
            }
		})
	}
}

function beforeCallLoader(){
	$('#reg_loader').css('display','block');	
	var text = '<i class="fa fa-clock-o fA-spin" aria-hidden="true"></i>';
	$('#reg_loader').html(text);
}