(function($){

	$.fn.msdZoom = function(options){	
		var msd = $.extend({
			previewPane: '#zoom-preview-pane',
			mouseRectSize: [ 60, 80 ],
		},options);
		
		return this.each(function(n){
			var EL = $(this),
					jcrop_api,
					boundx,
					boundy,
	
					// Grab some information about the preview pane
					$preview = $(msd.previewPane),
					$pcnt = $(msd.previewPane + ' > div'),
					$pimg = $pcnt.children('img'),
	
					xsize = $pcnt.width(),
					ysize = $pcnt.height(),
					jcrop_api,
					mrs = msd.mouseRectSize;
			
			//console.log('init',[xsize,ysize]);
			function initJCrop()
			{
				if($(window).width() > 750)
				{
					$('#zoom-preview-pane .preview-container').width($('.medium-wrapper').width() - 13);
					$('#zoom-preview-pane .preview-container').height($('.medium-wrapper').height() - 13);
					$('#zoom-preview-pane').css({
						'top': (parseInt($('.big-preview-wrapper').css('margin-top')) * (-1))-1,
						'left': $('.medium-wrapper').width() - parseInt($('.big-preview-wrapper').css('margin-left'))
					});
					xsize = $pcnt.width();
					ysize = $pcnt.height();
					EL.Jcrop({
						bgOpacity: 1,
						allowResize: false,
						allowSelect: false,
						allowMove: false,
						minSize: [ mrs[0], mrs[1] ],
						maxSize: [ mrs[0], mrs[1] ],
						aspectRatio: mrs[0]/mrs[1],
						setSelect: [0, 0, mrs[0], mrs[1]],
						onChange: updatePreview,
						aspectRatio: xsize / ysize
					},function(){
						// Use the API to get the real image size
						var bounds = this.getBounds();
						boundx = bounds[0];
						boundy = bounds[1];
						// Store the API in the jcrop_api variable
						jcrop_api = this;
			
						// Move the preview into the jcrop container for css positioning
						$preview.clone().appendTo(jcrop_api.ui.holder).css('display', 'none');
					});
				}
			}
			initJCrop();
	
			function updatePreview(c)
			{
				if (parseInt(c.w) > 0)
				{
					var rx = xsize / c.w;
					var ry = ysize / c.h;
	
					$('.jcrop-holder').find(msd.previewPane).find('img').css({
						width: Math.round(rx * boundx) + 'px',
						height: Math.round(ry * boundy) + 'px',
						marginLeft: '-' + Math.round(rx * c.x) + 'px',
						marginTop: '-' + Math.round(ry * c.y) + 'px'
					});
				}
			};
			
			$(document).on('mouseenter', '.jcrop-holder', function(e){
				$(this).find(msd.previewPane).fadeIn(10);
			}).on('mouseleave', '.jcrop-holder', function(e){
				$(this).find(msd.previewPane).fadeOut(10);
			});
			
			$(document).on('mousemove', '.jcrop-holder '+msd.previewPane, function(e){
				$(this).fadeOut(10);
			});
			
			$('.big-preview-wrapper').on('mousemove', function(e){
				if(jcrop_api)
				{
					jcrop_api.setSelect([
						e.pageX - $(this).offset().left - ($('.jcrop-holder > div > div > .jcrop-tracker').width()/2),
						e.pageY - $(this).offset().top - ($('.jcrop-holder > div > div > .jcrop-tracker').height()/2),
						e.pageX - $(this).offset().left - ($('.jcrop-holder > div > div > .jcrop-tracker').width()/2) + mrs[0],
						e.pageY - $(this).offset().top - ($('.jcrop-holder > div > div > .jcrop-tracker').height()/2) + mrs[1]
					]);
				}
			});
			
			var resizeId;
			
			$(window).resize(function(e) {
				if(EL.data('Jcrop')) {
					jcrop_api.destroy();
					$('.big-preview-wrapper img').css({
						'width':'',
						'height':''
					});
				}
				$('#zoom-preview-pane .preview-container').width($('.medium-wrapper').width());
        $('#zoom-preview-pane .preview-container').height($('.medium-wrapper').height());
      });
			
			$('body, html').mousemove(function(e) {
       if($('.jcrop-holder').length == 0)
			 {
				 initJCrop();
			 }
      });
		});
	};
	
}(jQuery));