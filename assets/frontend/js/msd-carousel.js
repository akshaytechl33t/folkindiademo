(function($){

	$.fn.msdCarousel = function(options){	
		var msd = $.extend({
			perView: 3,
			navigationLeft: 'assets/frontend/images/nav-left.png',
			navigationRight: 'assets/frontend/images/nav-right.png',
			slideDelay: 8000,
			slideDuration: 1000,
		},options);
		
		return this.each(function(n){
			var EL = $(this),
					inrEl = EL.children('div').children('div'),
					sldrImgWrpr = inrEl.children('div'),
					sldrImg = inrEl.children('div').find('img'),
					intervalId,
					isSliding = 0,
					perView = ($(window).width() >  600) ? msd.perView : 1;
			//alert(inrEl.find(sldrImgWrpr).length)
			$('<img src="'+msd.navigationLeft+'" class="msd-left-nav" alt="Prev" title="Prev" />').appendTo(EL).css({
				'position':'absolute',
				'left': '0',
				'top': '40%',
				'width': '3%',
				'max-width': '50px',
				'min-width': '25px',
				'cursor':'pointer',
			}).on('click', function(e){
				if(perView < inrEl.find('img').parent(sldrImgWrpr).length)
					slideToLeft();
			});
			
			$('<img src="'+msd.navigationRight+'" class="msd-right-nav" alt="Next" title="Next" />').appendTo(EL).css({
				'position':'absolute',
				'right': '0',
				'top': '40%',
				'width': '3%',
				'max-width': '50px',
				'min-width': '25px',
				'cursor':'pointer',
			}).on('click', function(e){
				if(perView < inrEl.find('img').parent(sldrImgWrpr).length)
					slideToRight();
			});
			
			function setHeightWidth()
			{
				EL.css({
					'width':'100%',
					'overflow':'hidden',
					'position':'relative',
				});
				EL.children('div').css({
					'width':EL.parent().width() - (EL.find('.msd-left-nav').width() * 2),
					'overflow':'hidden',
					'position':'relative',
					'left': EL.find('.msd-left-nav').width(),
				});
				inrEl.find('img').parent(sldrImgWrpr).css({
					'width':($(window).width() >  600) ? EL.children('div').width()/perView : EL.children('div').width(),
					'float':'left',
					'padding':'0px 15px',
				});
				inrEl.find('img').css({
					'width':'100%',
					'float':'left',
				});
				inrEl.css({
					'width':(inrEl.find('img').parent(sldrImgWrpr).length + 2) * inrEl.find('img').parent(sldrImgWrpr).outerWidth(true),
				});
			}
				
				setHeightWidth();
				
			if(perView < inrEl.find('img').parent(sldrImgWrpr).length)
			{
				function slideToLeft()
				{
					if(isSliding == 0)
					{
						isSliding = 1;
						
						inrEl.find('img').parent(sldrImgWrpr).eq(inrEl.find('img').parent(sldrImgWrpr).length-1).clone().prependTo(inrEl);
						inrEl.css('margin-left',inrEl.find('img').parent(sldrImgWrpr).eq(0).outerWidth(true) * (-1));
						inrEl.animate({
							'margin-left': 0,
						}, msd.slideDuration,	function(){
							inrEl.find('img').parent(sldrImgWrpr).eq(inrEl.find('img').parent(sldrImgWrpr).length-1).remove();
							setHeightWidth();
							isSliding = 0;
						});
					}
				}
				
				function slideToRight()
				{
					if(isSliding == 0)
					{
						isSliding = 1;
						
						inrEl.find('img').parent(sldrImgWrpr).eq(0).clone().appendTo(inrEl);
						inrEl.animate({
							'margin-left':inrEl.find('img').parent(sldrImgWrpr).eq(0).outerWidth(true) * (-1),
						}, msd.slideDuration,	function(){
							inrEl.find('img').parent(sldrImgWrpr).eq(0).eq(0).remove();
							inrEl.css('margin-left', 0);
							setHeightWidth();
							isSliding = 0;
						});
					}
				}
				
				var startCarousel = setInterval(function(){
					slideToRight();
				},msd.slideDelay);
			}
			
			$(window).resize(function(e) {
        setHeightWidth();
				//perView = ($(window).width() >  600) ? msd.perView : 1;
      });
		});
	};
	
}(jQuery));