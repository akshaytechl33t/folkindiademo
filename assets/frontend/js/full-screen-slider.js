(function($){

	$.fn.msdFullScreenSlider = function(options){	
		var msd = $.extend({
			animationDuration: 1000,
			slideDelay: 8000,
			navigationLeft: '',
			navigationRight: '',
			keepAspectRatio: true,
		},options);
		
		return this.each(function(n){
			var EL = $(this),
					inrEl = EL.children('div'),
					sldrImgWrpr = inrEl.children('div'),
					sldrImg = inrEl.children('div').find('img'),
					intervalId,
					isSliding = 0;
			
			function fullHghtWdth()
			{
				EL.css({
					'overflow':'hidden',
					'width': '100%',
					'position':'relative'
				});
				if(msd.keepAspectRatio !== null)
				{
					EL.css({
						'height':$(window).height()
					});
				}
				inrEl.css({
					'width':EL.width() * (sldrImgWrpr.length + 1),
					'height':(msd.keepAspectRatio !== null) ? EL.height() : 'auto'
				});
				inrEl.children(sldrImgWrpr).css({
					'width':EL.width(),
					'height':(msd.keepAspectRatio !== null) ? EL.height() : 'auto',
					'float': 'left',
					'overflow':'hidden'
				});
				if(msd.keepAspectRatio === null)
				{
					EL.css({
						'height':'auto'
					});
				}
			}
			
			function imgFull()
			{
				inrEl.find('img').css({
					//'float':'left'
				});
				if(msd.keepAspectRatio === true)
				{
					var rW = 0,
							rH = 0;
					inrEl.find('img').each(function(i, k){
						rW = EL.width() / $(this).width();
						rH = EL.height() / $(this).height();
						
						if(rW > rH)
						{
							$(this).width($(this).width() * rW);
						}
						else
						{
							$(this).height($(this).height() * rH);
						}
						
						$(this).css({
							'margin-left': (EL.width() - $(this).width()) / 2,
							'margin-top': (EL.height() - $(this).height()) / 2,
						});
					});
				}
				else if(msd.keepAspectRatio === false)
				{
					inrEl.find('img').width(EL.width());
					inrEl.find('img').height(EL.height());
				}
				else if(msd.keepAspectRatio === null)
				{
					inrEl.find('img').css({
						'width':EL.width()
					});
					//inrEl.find('img').width(EL.width());
					//inrEl.find('img').height(EL.height());
				}
			}
			
			function leftNavClck()
			{
				if(isSliding == 0)
				{
					isSliding = 1;
					inrEl.children(sldrImgWrpr).eq(inrEl.children(sldrImgWrpr).length-1).clone().prependTo(inrEl);
					inrEl.children(sldrImgWrpr).eq(inrEl.children(sldrImgWrpr).length-1).remove();
					inrEl.css('margin-left', EL.width() * (-1));
					inrEl.animate({
						'margin-left': 0
					}, msd.animationDuration, function(){
						fullHghtWdth();
						imgFull();
						isSliding = 0;
					});
				}
			}
			
			function rightNavClck()
			{
				if(isSliding == 0)
				{
					isSliding = 1;
					inrEl.animate({
						'margin-left': EL.width() * (-1)
					}, msd.animationDuration, function(){
						inrEl.children(sldrImgWrpr).eq(0).clone().appendTo(inrEl);
						inrEl.children(sldrImgWrpr).eq(0).remove();
						inrEl.css('margin-left', 0);
						fullHghtWdth();
						imgFull();
						isSliding = 0;
					});
				}
			}
			
			fullHghtWdth();
			imgFull();
			//$(window).resize();
			
			if(msd.navigationLeft != '' && msd.navigationRight != '')
			{
				$('<img src="'+msd.navigationLeft+'" class="msd-left-nav" alt="Prev" title="Prev" />').appendTo(EL).css({
					'position':'absolute',
					'left': '20px',
					'top': '47%',
					'width': '3%',
					'max-width': '50px',
					'min-width': '25px',
					'cursor':'pointer',
				}).on('click', function(e){
					leftNavClck();
				});
				
				$('<img src="'+msd.navigationRight+'" class="msd-right-nav" alt="Next" title="Next" />').appendTo(EL).css({
					'position':'absolute',
					'right': '20px',
					'top': '47%',
					'width': '3%',
					'max-width': '50px',
					'min-width': '25px',
					'cursor':'pointer',
				}).on('click', function(e){
					rightNavClck();
				});
			}
			
			EL.find('img').on('dragstart', function(e){
				e.preventDefault();
			});
			
			intervalId = setInterval(function(){
				if(isSliding == 0)
				{
					isSliding = 1;
					inrEl.animate({
						'margin-left': EL.width() * (-1)
					}, msd.animationDuration, function(){
						inrEl.children(sldrImgWrpr).eq(0).clone().appendTo(inrEl);
						inrEl.children(sldrImgWrpr).eq(0).remove();
						inrEl.css('margin-left', 0);
						fullHghtWdth();
						imgFull();
						isSliding = 0;
					});
				}
			}, msd.slideDelay);
			
			$(window).resize(function(e) {
        fullHghtWdth();
				imgFull();
      });
		});
	};
	
}(jQuery));