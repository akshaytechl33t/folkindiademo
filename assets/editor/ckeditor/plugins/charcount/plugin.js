/**
* CKEditor Char Count Plugin
*
* Adds a char count to the bottom right hand corner of any CKEditor instance
*
* @package charcount
* @editor Santiago Chen
* @version 1.9
* ChangeLog: DivideMethod has been added here;
*/

(function(){
	
	CKEDITOR.plugins.charcount = {};
    
    var plugin = CKEDITOR.plugins.charcount,
		_pos, 
		_symbol,
	    charcount_maxChars_default = ''; //default count max; 
	
	/*divideMethod
	* 3 parameter: 
	* target:the number you want to deal with; I haven't lock the target or give any default value to it. you can set it if you wish; 
	* position:where to insert; Defaul Value: 3; You can set by adding <input type="hidden" name="{ckeditor's id}+CharCount" pos={_position}/>; 
	* symbol: insert with the symbol; Default Value: ','; You can set by adding <input type="hidden" name="{ckeditor's id}+CharCount" symbol={_symbol}/>
	*/
	function divideMethod(target,position,symbol){
		//remove negative symbol;
		var targetStr = target.toString().replace('-','');
		
		//if it is not a negative number;
		if(target.toString().indexOf('-')<0){
			if(targetStr.length>position){
			var headTarget = targetStr.substr(0,targetStr.length-position);
			var tailTarget = targetStr.slice(targetStr.length-position,targetStr.length);
			return headTarget + symbol.toString() + tailTarget;
			}
			else{
			return targetStr;
			}
			}
		//if it is absolutely a negative number;
		else if(target.toString().indexOf('-')==0){
			if(targetStr.length>position){
			var headTarget = targetStr.substr(0,targetStr.length-position);
			var tailTarget = targetStr.slice(targetStr.length-position,targetStr.length);

			//add the negative symbol back;
			return ('-' + headTarget + symbol.toString() + tailTarget);
			}
			else{
			//add the negative symbol back;
			return ('-' + targetStr);
			}
			}

		}
		
	/*key char event happens, trigger here;*/
	function ShowCharCount(evt) {
		
        var editor = evt.editor;
        if ($('div#cke_charcount_'+editor.name).length > 0) { 
            setTimeout(function() {
                
				var charCount = GetCharCount(editor.getData()); 
				//console.log(editor.getData());
				//console.log(charCount);
				var surplusCount = editor.config.charcount_maxChars - charCount;
				if (surplusCount > 0) {
                    // Display with limit
                    $('div#cke_charcount_'+editor.name).html('character count:'+ divideMethod(surplusCount,_pos,_symbol));
					$('div#cke_charcount_'+editor.name).css('color', 'gray');
					try{validCkPlugin(editor.name,false);}	//especially for cookbook.
					catch(e){}//console.log(e)
                }
                
                // Check we are within char limit
               	else if (surplusCount <= 0) {
					$('div#cke_charcount_'+editor.name).html('character count: '+ divideMethod(surplusCount,_pos,_symbol));
                    $('div#cke_charcount_'+editor.name).css('color', 'red');
					try{validCkPlugin(editor.name,false);}	//especially for cookbook.
					catch(e){}//console.log(e)
                } 
				
				 
            }, 200);
        }
    }
	
	/*
	* common String method is ready for using.
	*/
	function GetCharCount(htmlData) {
		var matchTag = /<(?:.|\s)*?>/g;
		return $.trim(htmlData.replace(matchTag,"").stripHTML().clearBr()).length;

    }
	
	String.prototype.clearBr = function () {
    var mes = this.replace(/<\/?.+?>/g, "");
    mes = mes.replace(/[\r\n]/g, "");
    return mes;
};
	String.prototype.stripHTML = function () {
    var matchTag = /<(?:.|\s)*?>|nbsp;/gm;
    // Replace the tag
    return this.replace(matchTag,"");
};
	
	/**
    * init charcount here;
    */
    CKEDITOR.plugins.add('charcount', {
		
        init: function(editor) {
            // Char Count Limit
			var existCharCount, currentCharCount;
			existCharCount = GetCharCount(editor.getData());
			
			//set maxChar;
			if($('input[name='+editor.name+'CharCount]').val()){				
				editor.config.charcount_maxChars = $('input[name='+editor.name+'CharCount]').val();
				currentCharCount = $('input[name='+editor.name+'CharCount]').val() - existCharCount;
				}
			else{
				editor.config.charcount_maxChars = charcount_maxChars_default ;
				currentCharCount = charcount_maxChars_default - existCharCount;
				}
            //set position for inserting symbol;
			if($('input[name='+editor.name+'CharCount]').attr('pos')){
				_pos = $('input[name='+editor.name+'CharCount]').attr('pos');
				}
			else{
				_pos = 3;
				}
			//set what kind of symbol should be inserted;
			if($('input[name='+editor.name+'CharCount]').attr('symbol')){
				_symbol = $('input[name='+editor.name+'CharCount]').attr('symbol');
				}
			else{
				_symbol =  ',';
				}
			
			
            // Char Count Label - setTimeout used as this element won't be available until after init
            setTimeout(function() {
                if (currentCharCount >= 0) { 

					//display not in td#cke_bottom
					$('td#cke_contents_'+editor.name).parent().after('<tr><td style="display:block; height:22px; width:100%; background-color:#fff; border:0px"><div id="cke_charcount_'+editor.name+'" style="display: inline-block; float: right; text-align: right; margin: 4px 3px 3px 0; cursor:auto; font:12px Arial,Helvetica,Tahoma,Verdana,Sans-Serif; height:auto; padding:0; text-align:left; text-decoration:none; vertical-align:baseline; white-space:nowrap; width:auto; color:gray">character count: ' + divideMethod(currentCharCount,_pos,_symbol) +'</div></td></tr>');
							
                } 
            }, 200);
			
			                                                                                                       
            editor.on('key', ShowCharCount);
			editor.on('paste', ShowCharCount);
			
			/*Especially for cookbook editRecipe switchSaveBtn;*/ 	
			editor.on('focus',function(){
				$('a').click(function(){
					try{switchSave()}	
					catch(e){}//console.log(e)
				})
			})
				      
        }
    });
	
	
	})()
	