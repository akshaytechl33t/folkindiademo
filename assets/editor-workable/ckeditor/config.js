﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.entities_latin = false;
	config.entities = false;
	config.basicEntities = false;
	CKEDITOR.config.autoParagraph = false;
 
	config.toolbar_mycustomtoolbar =
	[
		{ name: 'clipboard',	items : [ 'PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing',		items : [ 'SpellChecker', 'Scayt' ] },
		{ name: 'basicstyles',	items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph',	items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'links',		items : [ 'Link','Unlink' ] },
		{ name: 'colors',		items : [ 'TextColor','BGColor' ] },
		'/',
		{ name: 'styles',		items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'document',		items : [ 'Source' ] }
		
	];
	
	config.toolbar_bannertexttoolbar =
	[
		{ name: 'basicstyles',	items : [ 'Bold','-','Italic','-','Underline'] },
		{ name: 'colors',		items : [ 'TextColor'] },
		{ name: 'styles',		items : [ 'Font','FontSize'] }
		
	];
	config.toolbar_replymail =
	[
		{ name: 'basicstyles',	items : [ 'Bold','-','Italic','-','Underline'] },
		{ name: 'colors',		items : [ 'TextColor'] },
		{ name: 'styles',		items : [ 'Font','FontSize'] }
		
	];
	config.allowedContent = true;
	
	
};

/*CKEDITOR.editorConfig = function(config) {
config.extraPlugins = 'wordcount';
};

CKEDITOR.editorConfig = function(config) {
config.extraPlugins = 'charcount';
};*/